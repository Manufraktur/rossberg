import importlib
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatC
import version_3.creation.Schriftteile_GatC_mod
import version_3.creation.Halbboegen_GatC_mod

importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatC)
importlib.reload(version_3.creation.Schriftteile_GatC_mod)
importlib.reload(version_3.creation.Halbboegen_GatC_mod)

from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatC import *
from version_3.creation.Schriftteile_GatC_mod import *
from version_3.creation.Halbboegen_GatC_mod import *
#from version_3.creation.arc_path import ArcPath as BezierPath
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m




temp_x = 3        ## 3
temp_y = 9        ## 9


# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 20
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)





# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)







# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)


stroke(.1)
strokeWidth(.1)








def drawSchriftzug3(x,y, top="standard", bottom="standard", instrokeLen=0.5, outstrokeLen=0.5):
    
    Schriftzug3 = BezierPath()   
        
    if top == "ascender":
    
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1, y+1.5)    
        pos_stroke = drawGrundelementA(Raute_a[0]-1.5*modul_width, Raute_a[1]-1.75*modul_height)
        instroke = drawGrundelementA(*Raute_a)

        # 1st bend
        Schriftteil5 = drawSchriftteil5(*Raute_a)
        stroke_start = Schriftteil5.points[-1]     
        Schriftzug3_top = pos_stroke + instroke + Schriftteil5
        
    else:
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y) 
        Rundung_oben = drawSchriftteil3(*Raute_a)
        stroke_start = Rundung_oben.points[-1]
        instroke = drawInstroke(*Raute_a, instrokeLen)
        Schriftzug3_top = Rundung_oben + instroke


    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)    
    
    if bottom == "gerade":    # für g und j
        stroke_end = stroke_start[0],    Raute_a[1]+modul_height*0.25
        Schriftzug3_bottom = Schriftzug3_top
        
    elif bottom == "Kehlung":
        stroke_end = stroke_start[0],    Raute_a[1]-modul_height*0.75
        Schriftzug3_bottom = Schriftzug3_top
    
    else:
        Rundung_unten = drawSchriftteil4(*Raute_d)
        stroke_end = Rundung_unten.points[-1]
        outstroke = drawOutstroke(*Raute_d, outstrokeLen)
        Schriftzug3_bottom = Schriftzug3_top + Rundung_unten + outstroke

    
    Schriftzug3 = Schriftzug3_top + Schriftzug3_bottom
    Schriftzug3.line(stroke_start, stroke_end)
    drawPath(Schriftzug3)
    return Schriftzug3
    


#drawSchriftzug3(temp_x, temp_y, "standard", instrokeLen=1, outstrokeLen=1)
# drawSchriftzug3(temp_x, temp_y, "ascender", instrokeLen=1, outstrokeLen=1)
# drawSchriftzug3(temp_x, temp_y, bottom="gerade", instrokeLen=1)    
# drawSchriftzug3(temp_x, temp_y, bottom="Kehlung", instrokeLen=1.5)







def drawSchriftzug4(x, y, bottom="standard", instrokeLen=0.5, outstrokeLen=0.5):
    
    Schriftzug4 = BezierPath() 
    
    if bottom == "standard":
        main_stroke = drawSchriftzug3(x, y, "standard", instrokeLen=instrokeLen, outstrokeLen=outstrokeLen)
        
    if bottom == "Schwung":             # für g und j  
        main_stroke = drawSchriftzug3(x, y, instrokeLen=instrokeLen, bottom="gerade")  
        Transition_to_right = drawSchneckenzug(*main_stroke.points[-1], UPPER_B, 1, HSL_size=4, HSL_start=12, clockwise=False, inward=False)
        Bogen_unten = drawSchneckenzug(*Transition_to_right.points[-1], UPPER_A, 4, HSL_size=2, HSL_start=12.5, clockwise=True, inward=False)  
        Schriftzug4 += Transition_to_right + Bogen_unten #+ Endspitze
    
    if bottom == "Kehlung":
        main_stroke = drawSchriftzug3(x, y, instrokeLen=instrokeLen, bottom="Kehlung")
           
                
    Serife_nach_rechts = drawSchriftteil2((x-1.5)*modul_width, (y+0.5)*modul_height)
    Serife_nach_unten = drawSchriftteil5((x-0.5)*modul_width, y*modul_height)
    
    Schriftzug4 += main_stroke + Serife_nach_rechts + Serife_nach_unten
    drawPath(Schriftzug4)
    return Schriftzug4
    

#drawSchriftzug4(temp_x, temp_y, bottom = "standard", instrokeLen=.5, outstrokeLen=.5)        
# drawSchriftzug4(temp_x+3, temp_y, bottom = "Schwung", instrokeLen=2)       
# drawSchriftzug4(temp_x, temp_y, instrokeLen=0, bottom = "Kehlung")    

   
   
    
    
    
    
    
def drawSchriftzug5(x, y, top="standard", instrokeLen=0.5, outstrokeLen=0.5):
    
    Schriftzug5 = BezierPath()
    
    if top == "ascender":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+3)    
        pos_stroke = drawPosStroke(x, y)
        instroke = drawInstroke(*Raute_d, 1, "down")
        bend_top = drawSchriftteil5(*Raute_d)
        stroke_start = bend_top.points[-1]        ### keep as 1st reference
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)  
        bend_bottom = drawSchriftteil4(*Raute_d)
        stroke_end = bend_bottom.points[-1]        ### keep as 2nd reference

        Serife_nach_links = drawSchriftteil6(*Raute_a)
        Serife_nach_unten = drawSchriftteil1(*Raute_d, direction="CCW")
        
        Schriftzug5.line(stroke_start, stroke_end)        ### Hauptlinie nach unten
        outstroke = drawOutstroke(*Raute_d, outstrokeLen)
        Schriftzug5 += pos_stroke + instroke + bend_top + bend_bottom + Serife_nach_links + Serife_nach_unten + outstroke
        

    else:        # >>> if top == "standard":
        Schriftzug3 = drawSchriftzug3(x, y, instrokeLen=instrokeLen, outstrokeLen=outstrokeLen)

        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)
        
        Serife_nach_links = drawSchriftteil6(*Raute_a)
        Serife_nach_unten = drawSchriftteil1(*Raute_d, direction="CCW")

        Schriftzug5 = Schriftzug3 + Serife_nach_links + Serife_nach_unten

    
    drawPath(Schriftzug5)
    return Schriftzug5
    
    
# drawSchriftzug5(temp_x+3, temp_y, top="standard", outstrokeLen=0.5)      
# drawSchriftzug5(temp_x, temp_y, top="ascender", outstrokeLen=0.5)      








def drawAuge(x, y):
    
    Auge = BezierPath()

    Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height-1)

    start_oben = drawSchneckenzug(x, y, LOWER_A, 2, HSL_size=0.5, HSL_start=8, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*start_oben.points[-1], 0.4)
    bow = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 6, HSL_size=0.25, HSL_start=4, clockwise=True, inward=True)

    Auge += start_oben + Einsatz + bow
    drawPath(Auge)  
    return Auge
        
# drawAuge(temp_x, temp_y)




def drawSchriftzug3_Figures(x, y, instrokeLen=0.5, outstrokeLen=0.5):
    
    ### dieser Schriftzug ist wie 3, nur dass er 8 hoch ist, so hoch wie Zahlen/Ziffern
    
    Schriftzug3_fullHeight = BezierPath()   
        
        
    # TOP
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2) 

    Rundung_oben = drawSchriftteil3(*Raute_a)
    stroke_start = Rundung_oben.points[-1]

    instroke = drawInstroke(*Raute_a, instrokeLen)



    # BOTTOM 
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-x_height+1)    
        
    Rundung_unten = drawSchriftteil4(*Raute_d)
    stroke_end = Rundung_unten.points[-1]
    
    outstroke = drawOutstroke(*Raute_d, outstrokeLen)
    
    
    Schriftzug3_Figures = instroke + Rundung_oben + Rundung_unten + outstroke

    Schriftzug3_Figures.line(stroke_start, stroke_end)

    drawPath(Schriftzug3_Figures)
    return Schriftzug3_Figures
    
    
#drawSchriftzug3_Figures(temp_x, temp_y)
   
   
      


def drawSchriftzug_U_Hauptstrich_rechts(x, y):
        
    Schriftzug_U_Hauptstrich_rechts = BezierPath()
    
    
    ### Rechts, gerade
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.5, y+2)    
    stem = drawSchriftzug3_Figures(x+4.5, y, instrokeLen=0)
    serife_nach_rechts = drawSchriftteil2(*Raute_a)
    serife_nach_unten = drawSchriftteil5(*Raute_d)


    ### Liegender Schwung unten
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.5, y-5.15) 
    links = drawSchneckenzug(*Grund_a, UPPER_G, 2, HSL_size=2, HSL_start=10, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*Grund_a, 0.35)
    right = drawSchneckenzug(*Einsatz.points[-1], LOWER_G, 2, HSL_size=2, HSL_start=10, clockwise=False, inward=False)
    Einsatz2 = drawGrundelementA(*right.points[-1], 1)

    up = drawSchneckenzug(*Einsatz2.points[-1], LOWER_E, 3, HSL_size=8, HSL_start=43.2, clockwise=False, inward=True)

    
    
    Schriftzug_U_Hauptstrich_rechts += stem + serife_nach_rechts + serife_nach_unten + links + Einsatz + right + Einsatz2 + up

    drawPath(Schriftzug_U_Hauptstrich_rechts)

    return Schriftzug_U_Hauptstrich_rechts


#drawSchriftzug_U_Hauptstrich_rechts(temp_x, temp_y)







