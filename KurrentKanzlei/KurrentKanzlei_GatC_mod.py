import importlib
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatC
import version_3.creation.Halbboegen_GatC
import KurrentKanzlei_Schriftzuege_GatC

importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatC)
importlib.reload(version_3.creation.Halbboegen_GatC)
importlib.reload(KurrentKanzlei_Schriftzuege_GatC)

from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatC import *
from version_3.creation.Halbboegen_GatC import *
from KurrentKanzlei_Schriftzuege_GatC import *

# from version_3.creation.arc_path import ArcPath as BezierPath
from version_3.creation.special_drawbot import BezierPath, drawPath

from nibLib.pens.rectNibPen import RectNibPen
from version_3.creation.rect_nib_pen import RectNibPen as RectNibPen_Just
from math import radians
import math as m
import collections
import glyphContext


# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 20
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)





# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 5

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)

# temporary to check height of numbers
#line((1*modul_width, 11*modul_height), (8*modul_width, 11*modul_height))




# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

stroke(.1)
strokeWidth(.1)
    
    
temp_x = 2
temp_y = 8.5






# ________________________________________________________




def drawKurrentKanzleiGatC_a(x, y, version="isol"):     
    
    KurrentKanzleiGatC_a = BezierPath()
       
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
        
    if version == "isol":
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.2, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0, outstrokeLen=1)
  
    if version == "iniA":    # ini und med sind gleich
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.2, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0, outstrokeLen=0)
        ### deviation=1.4 > Kompromiss für Verbindung mit a und e, auch wenn d suboptimal
        constroke = drawConstroke(*Strich_rechts.points[-1], "A", 3.5, deviation=1.4)  
        KurrentKanzleiGatC_a += constroke

    if version == "iniH":    # ini und med sind gleich
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.2, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "H")
        KurrentKanzleiGatC_a += constroke
        
    if version == "fina":
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.2, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0, outstrokeLen=1)
        
        
    KurrentKanzleiGatC_a += Bogen_links + Connection_up + Strich_rechts
    drawPath(KurrentKanzleiGatC_a)
    trans_scale(KurrentKanzleiGatC_a, valueToMoveGlyph)
    return KurrentKanzleiGatC_a
    
    
        
def drawKurrentKanzleiGatC_adieresis(x, y, version="isol"):     
    
    KurrentKanzleiGatC_adieresis = BezierPath()
       
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)

    dieresis = drawDieresis(x+2, y)
    trans_scale(dieresis, valueToMoveGlyph)
        
    if version == "isol":
        glyph_a = drawKurrentKanzleiGatC_a(x, y, version="isol")
  
    if version == "iniA":    # ini und med sind gleich
        glyph_a = drawKurrentKanzleiGatC_a(x, y, version="iniA")

    if version == "iniH":    # ini und med sind gleich
        glyph_a = drawKurrentKanzleiGatC_a(x, y, version="iniH")
        
    if version == "fina":
        glyph_a = drawKurrentKanzleiGatC_a(x, y, version="fina")
        
    KurrentKanzleiGatC_adieresis += glyph_a + dieresis
    return KurrentKanzleiGatC_adieresis
    
    
    
    
              
    
    
def drawKurrentKanzleiGatC_b(x, y, version="isol"):        
    
    KurrentKanzleiGatC_b = BezierPath()
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)

        
    if version == "isol":
        Bogen_links = drawSchriftzug17(x, y)
        Strich_rechts = drawSchriftzug26(x+3.5, y, version="3.25", instrokeLen=0, outstrokeLen=2)
        hook = drawSchneckenzug(*Strich_rechts.points[1], UPPER_B, 5, HSL_size=1.5, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])
        #hook = drawSchriftzug6(x+4.5, y+0.5, "b")
  
    if version == "iniA":   
        Bogen_links = drawSchriftzug17(x, y)
        Strich_rechts = drawSchriftzug26(x+3.5, y, version="3.25", instrokeLen=0, outstrokeLen=2)
        hook = drawSchneckenzug(*Strich_rechts.points[1], UPPER_B, 5, HSL_size=1.5, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])

    if version == "iniH":  
        Bogen_links = drawSchriftzug17(x, y)
        Strich_rechts = drawSchriftzug26(x+3.5, y, version="3.25", instrokeLen=0, outstrokeLen=2)
        hook = drawSchneckenzug(*Strich_rechts.points[1], UPPER_B, 5, HSL_size=2.45, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])
        
    
    if version == "midA":   
        Bogen_links = drawSchriftzug17(x, y, version="con")
        Strich_rechts = drawSchriftzug26(x+3.5, y, version="3.25", instrokeLen=0, outstrokeLen=2)
        hook = drawSchneckenzug(*Strich_rechts.points[1], UPPER_B, 5, HSL_size=1.5, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])

    if version == "midH":  
        Bogen_links = drawSchriftzug17(x, y, version="con")
        Strich_rechts = drawSchriftzug26(x+3.5, y, version="3.25", instrokeLen=0, outstrokeLen=2)
        hook = drawSchneckenzug(*Strich_rechts.points[1], UPPER_B, 5, HSL_size=2.45, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])
        
    if version == "fina":
        Bogen_links = drawSchriftzug17(x, y, version="con")
        Strich_rechts = drawSchriftzug26(x+3.5, y, version="3.25", instrokeLen=0, outstrokeLen=2)
        hook = drawSchneckenzug(*Strich_rechts.points[1], UPPER_B, 5, HSL_size=1.5, HSL_start=2, clockwise=False, inward=False, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])
    
    KurrentKanzleiGatC_b += Bogen_links + Strich_rechts + hook
    drawPath(KurrentKanzleiGatC_b) 
    trans_scale(KurrentKanzleiGatC_b, valueToMoveGlyph)
    return KurrentKanzleiGatC_b
    
    
    
    


def drawKurrentKanzleiGatC_c(x,y, version="isol"):
           
    KurrentKanzleiGatC_c = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    
    Bogen_links = drawSchriftzug21(x, y)
    hook = drawGrundelementC(*Bogen_links.points[1])
  
    if version == "isol":  # wie fina
        outstroke = drawOutstroke(*Bogen_links.points[-1], 2) 
        KurrentKanzleiGatC_c += outstroke
        
    if version == "iniA":  # wie med
        constroke = drawConstroke(*Bogen_links.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_c += constroke

    if version == "iniH":  # wie med
        constroke = drawConstroke(*Bogen_links.points[-1], "H", deviation=1.2)
        KurrentKanzleiGatC_c += constroke


    KurrentKanzleiGatC_c += Bogen_links + hook
    trans_scale(KurrentKanzleiGatC_c, valueToMoveGlyph)    
    return KurrentKanzleiGatC_c
    
    
    


def drawKurrentKanzleiGatC_d(x, y, version="isol"):   #Endspitze=12      

    KurrentKanzleiGatC_d = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    full_stroke = drawSchriftzug30(x+3, y, Spitze="long") # Spitze="short"
  
    if version == "isol":  # wie fina
        pass
        
    if version == "iniA":  # wie med
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+5, baseline+1.5)
        # constroke = drawConstroke(*Raute_b, "A", 0)
        # KurrentKanzleiGatC_d += constroke

    if version == "iniH":  # wie med
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+7, y)
        constroke = drawConstroke(*Raute_a, "A", 1)
        KurrentKanzleiGatC_d += constroke

    KurrentKanzleiGatC_d += full_stroke
    trans_scale(KurrentKanzleiGatC_d, valueToMoveGlyph)    
    return KurrentKanzleiGatC_d
    




def drawKurrentKanzleiGatC_e(x, y, version="isol"):  
    
    KurrentKanzleiGatC_e = BezierPath()
 
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    
    Bogen_links = drawSchriftzug21(x, y)     # outstroke=True
    Bogen_rechts = drawSchriftzug4(x+2, y, "a")
    Mittelstrich = drawOutstroke(*Bogen_rechts.points[-1], 2, "down")
  
    if version == "isol":  # wie fina
        outstroke = drawOutstroke(*Bogen_links.points[-1], 2) 
        KurrentKanzleiGatC_e += outstroke
        
    if version == "iniA":  # wie med
        constroke = drawConstroke(*Bogen_links.points[-1], "A", 3, deviation=1.4)
        KurrentKanzleiGatC_e += constroke

    if version == "iniH":  # wie med
        #instroke = drawInstroke(*Grund_c, 2.5) 
        constroke = drawConstroke(*Bogen_links.points[-1], "H", deviation=1.2)
        KurrentKanzleiGatC_e += constroke
    
    KurrentKanzleiGatC_e += Bogen_links + Bogen_rechts + Mittelstrich
    trans_scale(KurrentKanzleiGatC_e, valueToMoveGlyph)
    return KurrentKanzleiGatC_e
    
#drawSchriftzug21(temp_x, temp_y)     # outstroke=True
    
    
    
    
       
def drawKurrentKanzleiGatC_f(x, y, version="isol"):
    
    KurrentKanzleiGatC_f = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y)
    
    Grundstrich = drawKurrentKanzleiGatC_longs(x, y, "pure")[0]
    values_to_return = drawKurrentKanzleiGatC_longs(x, y)[1]
    
    Querstrich = drawGrundelementB(*Grund_a, 2.5)
    trans_scale(Querstrich, valueToMoveGlyph)
    
    # Positionierung der Verbindungsstriche
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-0.5, y-4)
    
    if version == "isol":  # wie fina
        pass
        
    if version == "iniA":   # wie med
        constroke = drawConstroke(*Raute_d, "A", 3.5, deviation=1.4)
        trans_scale(constroke, valueToMoveGlyph)
        KurrentKanzleiGatC_f += constroke

    if version == "iniH":   # wie med
        constroke = drawConstroke(*Raute_d, "H")
        trans_scale(constroke, valueToMoveGlyph)
        KurrentKanzleiGatC_f += constroke

    drawPath(KurrentKanzleiGatC_f)
    KurrentKanzleiGatC_f += Grundstrich + Querstrich
    return KurrentKanzleiGatC_f, values_to_return
    
    
    
def drawKurrentKanzleiGatC_f_f(x, y, version="isol"):
    
    KurrentKanzleiGatC_f_f = BezierPath()

    Hauptstrich_links = drawSchriftzug13(x, y, instrokeLen=1, version="short")
    pkt_Ausstrich = Hauptstrich_links.points[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    Hauptstrich_rechts = drawSchriftzug_f_f_2nd(x, y)
    
    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x+6, y+3.5)
    con = drawGrundelementA(*Raute_a, 2.5, "down")
    Signatur = drawSchriftzug35(Raute_a[0]+modul_width, Raute_a[1])

    Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x-1, y)    
    Querstrich = drawGrundelementB(*Grund_a, 5.5)   

    if version == "isol":
        pass
        #instroke = drawPosStroke(x, y-0.25)
        #KurrentKanzleiGatC_f_f += instroke
      
    if version == "iniA":
        #instroke = drawPosStroke(x, y-0.25)
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_f_f += constroke
          
    if version == "iniH":       
        #instroke = drawPosStroke(x, y-0.25)
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "H")
        KurrentKanzleiGatC_f_f += constroke

    if version == "midA":
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_f_f += constroke

    if version == "midH":
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "H")
        KurrentKanzleiGatC_f_f += constroke
 
    KurrentKanzleiGatC_f_f += Hauptstrich_links + Hauptstrich_rechts + con + Signatur + Querstrich
    drawPath(KurrentKanzleiGatC_f_f)    
    trans_scale(KurrentKanzleiGatC_f_f, valueToMoveGlyph)
    return KurrentKanzleiGatC_f_f, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
def drawKurrentKanzleiGatC_f_f_t(x, y, version="isol"):
    
    KurrentKanzleiGatC_f_f_t = BezierPath()

    Hauptstrich_links = drawSchriftzug13(x, y, instrokeLen=1, version="short")
    pkt_Ausstrich = Hauptstrich_links.points[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    Hauptstrich_rechts = drawSchriftzug_longs_longs_2nd(x+0.5, y)
    
    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x+5.75, y+3)
    con = drawGrundelementA(*Raute_a, 1.75, "down")
    Signatur_left = drawSchneckenzug(*Raute_a, UPPER_E, 2, HSL_size=2, HSL_start=18, clockwise=True, inward=False)
    Signatur_right = drawSchneckenzug(*Signatur_left.points[-1], LOWER_G, 2, HSL_size=2, HSL_start=12, clockwise=False, inward=False)
    Signatur = Signatur_left + Signatur_right + con
    
    Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x-1, y)    
    Querstrich = drawGrundelementB(*Grund_a, 10)   


    if version == "isol":
        #instroke = drawPosStroke(x, y, 0.5)
        #KurrentKanzleiGatC_f_f_t += instroke
        glyph_t = drawSchriftzug19(x+7, y, version="b", outstrokeLen=0.5)
      
    if version == "iniA":
        #instroke = drawPosStroke(x, y, 0.5)
        constroke = drawConstroke((x+7)*modul_width, (y-4)*modul_height, "A", 3, deviation=1.4)
        KurrentKanzleiGatC_f_f_t +=  constroke
        glyph_t = drawSchriftzug19(x+7, y, version="b", outstrokeLen=0)
          
    if version == "iniH":       
        #instroke = drawPosStroke(x, y, 0.5)
        constroke = drawConstroke((x+7)*modul_width, (y-4)*modul_height, "H")
        KurrentKanzleiGatC_f_f_t += constroke
        glyph_t = drawSchriftzug19(x+7, y, version="b", outstrokeLen=0)

    if version == "midA":
        constroke = drawConstroke((x+7)*modul_width, (y-4)*modul_height, "A", 3, deviation=1.4)
        KurrentKanzleiGatC_f_f_t += constroke
        glyph_t = drawSchriftzug19(x+7, y, version="b", outstrokeLen=0)

    if version == "midH":
        constroke = drawConstroke((x+7)*modul_width, (y-4)*modul_height, "H")
        KurrentKanzleiGatC_f_f_t += constroke
        glyph_t = drawSchriftzug19(x+7, y, version="b", outstrokeLen=0)

    if version == "fina":
        glyph_t = drawSchriftzug19(x+7, y, version="b", outstrokeLen=0.5)

     
    KurrentKanzleiGatC_f_f_t.line(Hauptstrich_rechts.points[-1], glyph_t.points[11]) 
    
    KurrentKanzleiGatC_f_f_t += Hauptstrich_links + Hauptstrich_rechts + Signatur + glyph_t + Querstrich
    drawPath(KurrentKanzleiGatC_f_f_t)    
    trans_scale(KurrentKanzleiGatC_f_f_t, valueToMoveGlyph)
    return KurrentKanzleiGatC_f_f_t, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    

    
    

    
def drawKurrentKanzleiGatC_g(x, y, version="isol"):
    
    KurrentKanzleiGatC_g = BezierPath()
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)

    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
        
    if version == "isol":
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.175)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug_backstroke_g(x+3, y, version="fina")
        pkt_Auslauf = Strich_rechts.points[-11]
        #text("pkt_Auslauf", pkt_Auslauf)
  
    if version == "iniA":    # ini und med sind gleich
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.175)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug_backstroke_g(x+3, y, version="loop_A", deviation=1.4)

    if version == "iniH":    # ini und med sind gleich
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.175)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug_backstroke_g(x+3, y, version="loop_H")
        
    if version == "fina":
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.175)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug_backstroke_g(x+3, y, version="fina")
        pkt_Auslauf = Strich_rechts.points[-11]
        #text("pkt_Auslauf", pkt_Auslauf)       
        
    KurrentKanzleiGatC_g += Bogen_links + Connection_up + Strich_rechts
    trans_scale(KurrentKanzleiGatC_g, valueToMoveGlyph)
   
    if version == "isol" or version == "fina":
        return KurrentKanzleiGatC_g, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf) 

    return KurrentKanzleiGatC_g


    
def drawKurrentKanzleiGatC_g_thinStroke(x, y, *, pass_from_thick=None):
    
    KurrentKanzleiGatC_g_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.1)
              
    KurrentKanzleiGatC_g_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentKanzleiGatC_g_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatC_g_thinStroke) 
    trans_scale(KurrentKanzleiGatC_g_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatC_g_thinStroke
    
    
    
    

           


def drawKurrentKanzleiGatC_h(x, y, version="isol"):      
    
    KurrentKanzleiGatC_h = BezierPath()

    if version == "isol":
        pos_stroke = drawPosStroke(x, y)
        downstroke_left = drawSchriftzug13(x, y)
        connection = drawSchriftzug9(x, y, "b")
        bend_right = drawSchriftzug20_descender(x+3.5, y, "fina")    
        KurrentKanzleiGatC_h += pos_stroke

    if version == "iniA":
        pos_stroke = drawPosStroke(x, y)
        downstroke_left = drawSchriftzug13(x, y)
        connection = drawSchriftzug9(x, y, "b")
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_A", outstrokeLen=2.25)
        KurrentKanzleiGatC_h += pos_stroke

    if version == "iniH":
        pos_stroke = drawPosStroke(x, y)
        downstroke_left = drawSchriftzug13(x, y)
        connection = drawSchriftzug9(x, y, "b")
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_H")
        KurrentKanzleiGatC_h += pos_stroke

    if version == "midA":
        downstroke_left = drawSchriftzug11(x, y)
        connection = drawSchriftzug9(x, y, "b")
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_A", outstrokeLen=2.25)

    if version == "midH":
        downstroke_left = drawSchriftzug11(x, y)
        connection = drawSchriftzug9(x, y, "b")
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_H")

    if version == "fina":
        downstroke_left = drawSchriftzug11(x, y)
        connection = drawSchriftzug9(x, y, "b")
        bend_right = drawSchriftzug20_descender(x+3.5, y, "fina")    

    pkt_Auslauf = bend_right.points[-7]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    KurrentKanzleiGatC_h += downstroke_left + connection + bend_right
    trans_scale(KurrentKanzleiGatC_h, valueToMoveGlyph)

    if version == "isol" or version == "fina":
        return KurrentKanzleiGatC_h, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)

    return KurrentKanzleiGatC_h

    
    
    
    
    
        

def drawKurrentKanzleiGatC_i(x, y, version="isol"):      

    KurrentKanzleiGatC_i = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)

    ### Hilfslinie für i-Punkt
    #drawGrundelementG(*Grund_a, 3) 
    
    if version == "isol":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=1, outstrokeLen=1)

    if version == "iniA":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_i += constroke
        
    if version == "iniH":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=1, outstrokeLen=0)
        constroke = drawConstroke(*downstroke.points[-1], "H")
        KurrentKanzleiGatC_i += constroke
    
    if version == "midA":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_i += constroke
        
    if version == "midH":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke.points[-1], "H")
        KurrentKanzleiGatC_i += constroke
        
    if version == "fina":
        downstroke = drawSchriftzug7(x, y, version="b", instrokeLen=0,  outstrokeLen=1)

        
    iPunkt = drawSchriftzug3(x+1.5, y+3, "a", instroke=False)

    KurrentKanzleiGatC_i += downstroke + iPunkt
    trans_scale(KurrentKanzleiGatC_i, valueToMoveGlyph)
    return KurrentKanzleiGatC_i
        
    
        

        
    
def drawKurrentKanzleiGatC_j(x, y, version="isol"):      
    x = x+3
    KurrentKanzleiGatC_j = BezierPath()
                
    if version == "isol":
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="fina", instrokeLen=1)

    if version == "iniA":
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="loop_A", instrokeLen=1, deviation=1.4)

    if version == "iniH":
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="loop_H", instrokeLen=1)
        
    if version == "midA":
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="loop_A", deviation=1.4)

    if version == "midH":
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="loop_H")
        
    if version == "fina":
        main_stroke = drawSchriftzug_backstroke_g(x, y, version="fina")
    
    pkt_Auslauf = main_stroke.points[-11]
    #text("pkt_Auslauf", pkt_Auslauf) 
        
    Punkt = drawGrundelementC((x+0.5)*modul_width, (y+3.5)*modul_height)
        
    KurrentKanzleiGatC_j += main_stroke + Punkt
    trans_scale(KurrentKanzleiGatC_j, valueToMoveGlyph)
    
    if version == "isol" or version == "fina":
        return KurrentKanzleiGatC_j, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    return KurrentKanzleiGatC_j

    
    
    
def drawKurrentKanzleiGatC_j_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatC_j_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 4, HSL_size=2, HSL_start=22, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 5)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.035, Auslauf.points[-1][1]-modul_height*0.35)
         
    KurrentKanzleiGatC_j_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentKanzleiGatC_j_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatC_j_thinStroke) 
    trans_scale(KurrentKanzleiGatC_j_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatC_j_thinStroke
    

      



def drawKurrentKanzleiGatC_k(x, y, version="isol"):
    
    KurrentKanzleiGatC_k = BezierPath()
    
    if version == "isol":
        main_stroke = drawSchriftzug18(x, y)
        #hook = drawHalbbogen1((x+0.5)*modul_width, (y+2.75)*modul_width, 2)
        hook = drawSchriftzug4(x+1.25, y+2, "a", instroke=True, outstroke=True)
        outstroke = drawOutstroke(*main_stroke.points[14], 1)
        Querstrich = drawGrundelementB((x-2)*modul_width, (y+0.5)*modul_width, 3)
        KurrentKanzleiGatC_k += outstroke

    if version == "iniA":
        main_stroke = drawSchriftzug18(x, y)
        #hook = drawHalbbogen1((x+0.5)*modul_width, (y+2.75)*modul_width, 2)
        hook = drawSchriftzug4(x+1.25, y+2, "a", instroke=True, outstroke=True)
        constroke = drawConstroke(*main_stroke.points[14], "A", 3.5, deviation=1.4)
        Querstrich = drawGrundelementB((x-2)*modul_width, (y+0.5)*modul_width, 3)
        KurrentKanzleiGatC_k += constroke
        
    if version == "iniH":
        main_stroke = drawSchriftzug18(x, y)
        #hook = drawHalbbogen1((x+0.5)*modul_width, (y+2.75)*modul_width, 2)
        hook = drawSchriftzug4(x+1.25, y+2, "a", instroke=True, outstroke=True)
        constroke = drawConstroke(*main_stroke.points[14], "H")
        Querstrich = drawGrundelementB((x-2)*modul_width, (y+0.5)*modul_width, 3)
        KurrentKanzleiGatC_k += constroke
    
    if version == "midA":
        main_stroke = drawSchriftzug13(x, y)
        constroke = drawConstroke(*main_stroke.points[1], "A", 3, deviation=1.4)
        hook = drawSchriftzug4(x+1.75, y+2, "a", instroke=True, outstroke=True)
        Querstrich = drawGrundelementB((x-2)*modul_width, (y+0.5)*modul_width, 3)
        KurrentKanzleiGatC_k += constroke
        
    if version == "midH":
        main_stroke = drawSchriftzug13(x, y)
        constroke = drawConstroke(*main_stroke.points[1], "H")
        hook = drawSchriftzug4(x+1.75, y+2, "a", instroke=True, outstroke=True)
        Querstrich = drawGrundelementB((x-2)*modul_width, (y+0.5)*modul_width, 3)
        KurrentKanzleiGatC_k += constroke
        
    if version == "fina":
        main_stroke = drawSchriftzug13(x, y)
        hook = drawSchriftzug4(x+1.75, y+2, "a", instroke=True, outstroke=True)
        Querstrich = drawGrundelementB((x-2)*modul_width, (y+0.5)*modul_width, 3)
        outstroke = drawOutstroke(*main_stroke.points[1], 1.5)
        KurrentKanzleiGatC_k += outstroke

    KurrentKanzleiGatC_k += main_stroke + hook + Querstrich
    trans_scale(KurrentKanzleiGatC_k, valueToMoveGlyph)
    return KurrentKanzleiGatC_k
    
    
    
    
    
    
def drawKurrentKanzleiGatC_l(x, y, version="isol"):    
    
    KurrentKanzleiGatC_l = BezierPath()
    
    if version == "isol":
        pos_stroke = drawPosStroke(x+0.5, y, 0.5)
        downstroke_left = drawSchriftzug17(x, y)
        outstroke = drawOutstroke(*downstroke_left.points[-4], 1)
        KurrentKanzleiGatC_l += pos_stroke + outstroke
    
    if version == "iniA":
        pos_stroke = drawPosStroke(x+0.5, y, 0.5)
        downstroke_left = drawSchriftzug17(x, y)
        constroke = drawConstroke(*downstroke_left.points[-4], "A", 3, deviation=1.4)
        KurrentKanzleiGatC_l += pos_stroke + constroke
        
    if version == "iniH":
        pos_stroke = drawPosStroke(x+0.5, y, 0.5)
        downstroke_left = drawSchriftzug17(x, y)
        constroke = drawConstroke(*downstroke_left.points[-4], "H", 4.5)
        KurrentKanzleiGatC_l += pos_stroke + constroke
    
    if version == "midA":
        downstroke_left = drawSchriftzug17(x, y, version="con")
        constroke = drawConstroke(*downstroke_left.points[-4], "A", 3, deviation=1.4)
        KurrentKanzleiGatC_l += constroke
        
    if version == "midH":
        downstroke_left = drawSchriftzug17(x, y, version="con")
        constroke = drawConstroke(*downstroke_left.points[-4], "H", 4.5)
        KurrentKanzleiGatC_l += constroke
   
    if version == "fina":
        downstroke_left = drawSchriftzug17(x, y, version="con")
        outstroke = drawOutstroke(*downstroke_left.points[-4], 1)
        KurrentKanzleiGatC_l += outstroke  

    KurrentKanzleiGatC_l += downstroke_left
    trans_scale(KurrentKanzleiGatC_l, valueToMoveGlyph)
    return KurrentKanzleiGatC_l
    
    
    
    
    
    
    
    
        
        
def drawKurrentKanzleiGatC_m(x, y, version="isol"):
    
    KurrentKanzleiGatC_m = BezierPath()
    
    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1 , outstrokeLen=0)
        downstroke_right = drawSchriftzug7(x+7, y, "c", instrokeLen=1 , outstrokeLen=1)
               
    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1 , outstrokeLen=0)
        downstroke_right = drawSchriftzug7(x+7, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_m += constroke

    if version == "iniH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1 , outstrokeLen=0)        
        downstroke_right = drawSchriftzug7(x+7, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "H")
        KurrentKanzleiGatC_m += constroke

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1 , outstrokeLen=0)        
        downstroke_right = drawSchriftzug7(x+7, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_m += constroke

    if version == "midH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1 , outstrokeLen=0)        
        downstroke_right = drawSchriftzug7(x+7, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "H")
        KurrentKanzleiGatC_m += constroke
   
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b", instrokeLen=1 , outstrokeLen=0)
        downstroke_right = drawSchriftzug7(x+7, y, "c", instrokeLen=1 , outstrokeLen=1)
        #outstroke = drawOutstroke(*downstroke_right.points[-1], 1)


    KurrentKanzleiGatC_m += downstroke_left + downstroke_mid + downstroke_right
    trans_scale(KurrentKanzleiGatC_m, valueToMoveGlyph)
    return KurrentKanzleiGatC_m
    







def drawKurrentKanzleiGatC_n(x, y, version="isol"):
    
    KurrentKanzleiGatC_n = BezierPath()
    
    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1)
        downstroke_right = drawSchriftzug7(x+3.5, y, "c", instrokeLen=1 , outstrokeLen=1)
    
    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1)
        downstroke_right = drawSchriftzug7(x+3.5, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_n += constroke

    if version == "iniH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1)
        downstroke_right = drawSchriftzug7(x+3.5, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "H")
        KurrentKanzleiGatC_n += constroke

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0)
        downstroke_right = drawSchriftzug7(x+3.5, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "A", 3.5,  deviation=1.4)
        KurrentKanzleiGatC_n += constroke

    if version == "midH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0)
        downstroke_right = drawSchriftzug7(x+3.5, y, "a", instrokeLen=0, outstrokeLen=0)
        constroke = drawConstroke(*downstroke_right.points[-1], "H")
        KurrentKanzleiGatC_n += constroke
   
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0)
        downstroke_right = drawSchriftzug7(x+3.5, y, "c", instrokeLen=1 , outstrokeLen=1)

    KurrentKanzleiGatC_n += downstroke_left + downstroke_right
    trans_scale(KurrentKanzleiGatC_n, valueToMoveGlyph)
    return KurrentKanzleiGatC_n
    






def drawKurrentKanzleiGatC_o(x, y, version="isol"):
        
    KurrentKanzleiGatC_o = BezierPath()
    
    if version == "isol":
        Bogen_links = drawSchriftzug21(x, y)
        Bogen_rechts = drawSchriftzug26(x+2, y, "3.5", outstrokeLen=1.45)

    if version == "iniA":
        Bogen_links = drawSchriftzug21(x, y)
        Bogen_rechts = drawSchriftzug26(x+2, y, "3.5", outstrokeLen=1.45)
        hook = drawSchriftzug6(x+3, y+0.5, "b")
        KurrentKanzleiGatC_o += hook

    if version == "iniH":
        Bogen_links = drawSchriftzug21(x, y)
        Bogen_rechts = drawSchriftzug26(x+2, y, "3.5", outstrokeLen=1.45)
        hook = drawHook_o(*Bogen_links.points[1], outstrokeLen=0.85)
        KurrentKanzleiGatC_o += hook

    if version == "midA":
        Bogen_links = drawSchriftzug21(x, y)
        Bogen_rechts = drawSchriftzug26(x+2, y, "3.5", outstrokeLen=1.45)
        hook = drawSchriftzug6(x+3, y+0.5, "b")
        KurrentKanzleiGatC_o += hook

    if version == "midH":
        Bogen_links = drawSchriftzug21(x, y)
        Bogen_rechts = drawSchriftzug26(x+2, y, "3.5", outstrokeLen=1.45)
        hook = drawHook_o(*Bogen_links.points[1], outstrokeLen=0.85)
        KurrentKanzleiGatC_o += hook
   
    if version == "fina":
        Bogen_links = drawSchriftzug21(x, y)
        Bogen_rechts = drawSchriftzug26(x+2, y, "3.5", outstrokeLen=1.45)

    KurrentKanzleiGatC_o += Bogen_links + Bogen_rechts
    trans_scale(KurrentKanzleiGatC_o, valueToMoveGlyph)
    return KurrentKanzleiGatC_o
    
    
    
def drawKurrentKanzleiGatC_odieresis(x, y, version="isol"):     
    
    KurrentKanzleiGatC_odieresis = BezierPath()
       
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    dieresis = drawDieresis(x+2, y)
    trans_scale(dieresis, valueToMoveGlyph)
        
    if version == "isol":
        glyph_o = drawKurrentKanzleiGatC_o(x, y, version="isol")
  
    if version == "iniA":
        glyph_o = drawKurrentKanzleiGatC_o(x, y, version="iniA")

    if version == "iniH":
        glyph_o = drawKurrentKanzleiGatC_o(x, y, version="iniH")
        
    if version == "midA":
        glyph_o = drawKurrentKanzleiGatC_o(x, y, version="midA")

    if version == "midH":
        glyph_o = drawKurrentKanzleiGatC_o(x, y, version="midH")
        
    if version == "fina":
        glyph_o = drawKurrentKanzleiGatC_o(x, y, version="fina")
        
    KurrentKanzleiGatC_odieresis += glyph_o + dieresis
    return KurrentKanzleiGatC_odieresis
    
    
        
    

    
# def drawKurrentKanzleiGatC_p(x, y, version="isol"):
        
#     KurrentKanzleiGatC_p = BezierPath()
    
#     ### Für diesen Buchstaben habe ich einen extra Downbend entworfen: b-2 der enger ist und mit 2 Abstand anliegt.
    
#     if version == "isol":
#         downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=1 , outstrokeLen=0)
#         bend_right = drawSchriftzug26(x+3, y)
    
#     if version == "iniA":
#         downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=1 , outstrokeLen=0)
#         bend_right = drawSchriftzug26(x+3, y)
        
#         # Raute Positionierung Verbindungsstrich (weil weiter innen anfängt)
#         Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2.5, y-3.5)
#         constroke = drawConstroke(*Raute_a, "A", 4, deviation=0.9)
#         KurrentKanzleiGatC_p += constroke
        
        
#     if version == "iniH":
#         #pass     # I think this is not existent
#         downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=1 , outstrokeLen=0)
#         bend_right = drawSchriftzug26(x+3, y)
        
#         # Raute Positionierung Verbindungsstrich (weil manuell hingepflanzt)
#         Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.425, y-3)
#         Raute_a_btm, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+7.25, y)
#         KurrentKanzleiGatC_p.line(Raute_a_btm, Raute_a_top)
#         #constroke = drawConstroke(*Raute_a, "H", 3)
#         #KurrentKanzleiGatC_p += constroke
#         drawPath(KurrentKanzleiGatC_p)
    

#     if version == "midA":
#         downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=0 , outstrokeLen=0)
#         bend_right = drawSchriftzug26(x+3, y)
        
#         # Raute Positionierung Verbindungsstrich (weil weiter innen anfängt)
#         Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2.5, y-3.5)
#         constroke = drawConstroke(*Raute_a, "A", 4, deviation=0.9)
#         KurrentKanzleiGatC_p += constroke
        

#     if version == "midH":
#         #pass     # I think this is not existent
#         downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=0 , outstrokeLen=0)
#         bend_right = drawSchriftzug26(x+3, y)
        
#         # Raute Positionierung Verbindungsstrich (weil manuell hingepflanzt)
#         Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.425, y-3)
#         Raute_a_btm, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+7.25, y)
#         KurrentKanzleiGatC_p.line(Raute_a_btm, Raute_a_top)
#         #constroke = drawConstroke(*Raute_a, "H", 3)
#         #KurrentKanzleiGatC_p += constroke
#         drawPath(KurrentKanzleiGatC_p)
   
#     if version == "fina":
#         downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=0 , outstrokeLen=0)
#         bend_right = drawSchriftzug26(x+3, y)
    
    
#     pkt_Ausstrich = downstroke_left.points[3]
#     #text("pkt_Ausstrich", pkt_Ausstrich)

#     KurrentKanzleiGatC_p += downstroke_left + bend_right
#     trans_scale(KurrentKanzleiGatC_p, valueToMoveGlyph)
#     return KurrentKanzleiGatC_p, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
def drawKurrentKanzleiGatC_p(x, y, version="isol"):
        
    KurrentKanzleiGatC_p = BezierPath()
    
    if version == "isol":
        glyph_p = drawKurrentKanzleiGatC_v(x, y, "isol")
         
    if version == "iniA":
        glyph_p = drawKurrentKanzleiGatC_v(x, y, "iniA")
        
    if version == "iniH":
        glyph_p = drawKurrentKanzleiGatC_v(x, y, "iniH")

    if version == "midA":
        glyph_p = drawKurrentKanzleiGatC_v(x, y, "midA")
        
    if version == "midH":
        glyph_p = drawKurrentKanzleiGatC_v(x, y, "midH")
   
    if version == "fina":
        glyph_p = drawKurrentKanzleiGatC_v(x, y, "fina")

    
    ### Wo kommt die Zahl 0.672 zustande?
    pkt_Ausstrich = x+0.672*modul_width, baseline*modul_height
    #text("pkt_Ausstrich", pkt_Ausstrich)

    KurrentKanzleiGatC_p += glyph_p
    #trans_scale(KurrentKanzleiGatC_p, valueToMoveGlyph)
    return KurrentKanzleiGatC_p, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
    
    
    
def drawKurrentKanzleiGatC_q(x, y, version="isol"):      
    
    KurrentKanzleiGatC_q = BezierPath()
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
        
    if version == "isol":
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.2, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0 , outstrokeLen=0)
  
    if version == "iniA":    # ini und med sind gleich
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.2, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0 , outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_q += constroke

    if version == "iniH":    # ini und med sind gleich
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.2, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0 , outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "H")
        KurrentKanzleiGatC_q += constroke
        
    if version == "fina":
        Bogen_links = drawSchriftzug25(x, y, instrokeLen=2.2, outstrokeLen=1)
        Connection_up = drawConCurve_up(x, y)
        Strich_rechts = drawSchriftzug7(x+3, y, "a", instrokeLen=0 , outstrokeLen=0)
        
    pkt_Ausstrich = Strich_rechts.points[-1]
    #text("pkt_Ausstrich", pkt_Ausstrich)
      
    KurrentKanzleiGatC_q += Bogen_links + Connection_up + Strich_rechts
    trans_scale(KurrentKanzleiGatC_q, valueToMoveGlyph)
    return KurrentKanzleiGatC_q, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    






def drawKurrentKanzleiGatC_r(x, y, version="isol"):       

    KurrentKanzleiGatC_r = BezierPath()

    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=1 , outstrokeLen=0)
        #hook = drawSchriftzug6(x+3.5, y+0.5, "b")  ### meine vorherige Alternative
        hook = drawSchriftteil2(*downstroke_left.points[-1])

    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=1 , outstrokeLen=0)
        hook = drawSchriftteil2(*downstroke_left.points[-1])

    if version == "iniH":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=1 , outstrokeLen=0)
        hook = drawGrundelementC((x+1.5)*modul_width, (y+0.5)*modul_height, outstrokeLen=1)

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=0 , outstrokeLen=0)
        hook = drawSchriftteil2(*downstroke_left.points[-1])

    if version == "midH":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=0 , outstrokeLen=0)
        hook = drawGrundelementC((x+1.5)*modul_width, (y+0.5)*modul_height, outstrokeLen=1)
   
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b-3", instrokeLen=0 , outstrokeLen=0)
        hook = drawSchriftteil2(*downstroke_left.points[-1])


    KurrentKanzleiGatC_r += downstroke_left + hook
    trans_scale(KurrentKanzleiGatC_r, valueToMoveGlyph)
    return KurrentKanzleiGatC_r



def drawKurrentKanzleiGatC_rc(x, y):       
    
    r_links = drawSchriftzug8(x, y, instrokeLen=1, version="c")
    
    KurrentKanzleiGatC_rc = r_links
    trans_scale(KurrentKanzleiGatC_rc, valueToMoveGlyph) 
    
    letter_c = drawKurrentKanzleiGatC_c(x+3, y)
    KurrentKanzleiGatC_rc += letter_c
    return KurrentKanzleiGatC_rc
    
    
    
    
    
    
def drawKurrentKanzleiGatC_s(x, y, version="isol"):

    KurrentKanzleiGatC_s = BezierPath()
    #x +=2
    if version == "isol":
        final_s = drawSchriftzug29(x, y, version="isol")
        
    if version == "iniA":
        final_s = drawSchriftzug29(x, y, version="fina")

    if version == "iniH":
        final_s = drawSchriftzug29(x, y, version="iniH")

    if version == "midA":
        final_s = drawSchriftzug29(x, y, version="fina")

    if version == "midH":
        final_s = drawSchriftzug29(x, y, version="midH")
   
    if version == "fina":
        final_s = drawSchriftzug29(x, y, version="fina")
    
    KurrentKanzleiGatC_s += final_s
    trans_scale(KurrentKanzleiGatC_s, valueToMoveGlyph)
    return KurrentKanzleiGatC_s
    
      
  

    

def drawKurrentKanzleiGatC_longs(x, y, version="isol"):
    
    KurrentKanzleiGatC_longs = BezierPath()

    Hauptstrich = drawSchriftzug13(x, y)
    pkt_Ausstrich = Hauptstrich.points[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)

    Signatur = drawSchriftzug23(x+4.5, y+8.5)

    if version == "isol":
        instroke = drawPosStroke(x, y-0.25)
        KurrentKanzleiGatC_longs += instroke
      
    if version == "iniA":
        instroke = drawPosStroke(x, y-0.25)
        constroke = drawConstroke(*Hauptstrich.points[1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_longs += instroke + constroke
          
    if version == "iniH":       
        instroke = drawPosStroke(x, y-0.25)
        constroke = drawConstroke(*Hauptstrich.points[1], "H")
        KurrentKanzleiGatC_longs +=instroke + constroke

    if version == "midA":
        constroke = drawConstroke(*Hauptstrich.points[1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_longs += constroke

    if version == "midH":
        constroke = drawConstroke(*Hauptstrich.points[1], "H")
        KurrentKanzleiGatC_longs += constroke

    if version == "fina":
        pass

    KurrentKanzleiGatC_longs += Hauptstrich + Signatur
    drawPath(KurrentKanzleiGatC_longs)    
    trans_scale(KurrentKanzleiGatC_longs, valueToMoveGlyph)
    return KurrentKanzleiGatC_longs, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    


    
    
def drawKurrentKanzleiGatC_germandbls(x, y, version="isol"):

    KurrentKanzleiGatC_germandbls = BezierPath()

    Hauptstrich_links = drawSchriftzug13(x, y)
    pkt_Ausstrich = Hauptstrich_links.points[-7]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Bogen_rechts = drawSchriftzug_germandbls(x, y)
    pkt_Auslauf = Bogen_rechts.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)

    
    if version == "isol":
        instroke = drawPosStroke(x, y-0.25)
        KurrentKanzleiGatC_germandbls += instroke

    if version == "iniA":
        instroke = drawPosStroke(x, y-0.25)
        KurrentKanzleiGatC_germandbls += instroke
          
    if version == "iniH":       
        instroke = drawPosStroke(x, y-0.25)
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+8, y)
        constroke = drawConstroke(*Raute_a, "A", 1)
        KurrentKanzleiGatC_germandbls += instroke + constroke

    if version == "midA":
        pass
        
    if version == "midH":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+8, y)
        constroke = drawConstroke(*Raute_a, "A", 1)
        KurrentKanzleiGatC_germandbls += constroke         

    if version == "fina":
        pass        
    
    KurrentKanzleiGatC_germandbls += Hauptstrich_links + Bogen_rechts
    trans_scale(KurrentKanzleiGatC_germandbls, valueToMoveGlyph)
    return KurrentKanzleiGatC_germandbls, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
    
    
def drawKurrentKanzleiGatC_germandbls_thinStroke(x, y, *, pass_from_thick=None):
    
    KurrentKanzleiGatC_germandbls_Endspitze = BezierPath()   
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 5, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.3 * i      for  i in range(0, 6)])
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0], Auslauf.points[-1][1]-modul_height*0.55)
    
    KurrentKanzleiGatC_germandbls_Endspitze += Auslauf + Zierstrich + Endpunkt
    drawPath(KurrentKanzleiGatC_germandbls_Endspitze)
    trans_thinStroke_down_left(KurrentKanzleiGatC_germandbls_Endspitze) 
    trans_scale(KurrentKanzleiGatC_germandbls_Endspitze, valueToMoveGlyph)
    return KurrentKanzleiGatC_germandbls_Endspitze 
    
    
    


   
    
    
def drawKurrentKanzleiGatC_longs_longs(x, y, version="isol"):
    
    KurrentKanzleiGatC_longs_longs = BezierPath()

    Hauptstrich_links = drawSchriftzug13(x, y, instrokeLen=1, version="short")
    pkt_Ausstrich = Hauptstrich_links.points[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    Hauptstrich_rechts = drawSchriftzug_longs_longs_2nd(x, y)
    
    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x+6, y+3.5)
    con = drawGrundelementA(*Raute_a, 2.5, "down")
    Signatur = drawSchriftzug35(Raute_a[0]+modul_width, Raute_a[1])
         

    if version == "isol":
        instroke = drawPosStroke(x, y-0.25)
        KurrentKanzleiGatC_longs_longs += instroke
      
    if version == "iniA":
        instroke = drawPosStroke(x, y-0.25)
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_longs_longs += instroke + constroke
          
    if version == "iniH":       
        instroke = drawPosStroke(x, y-0.25)
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "H")
        KurrentKanzleiGatC_longs_longs +=instroke + constroke

    if version == "midA":
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_longs_longs += constroke

    if version == "midH":
        constroke = drawConstroke(*Hauptstrich_rechts.points[-1], "H")
        KurrentKanzleiGatC_longs_longs += constroke
 
    KurrentKanzleiGatC_longs_longs += Hauptstrich_links + Hauptstrich_rechts + con + Signatur
    drawPath(KurrentKanzleiGatC_longs_longs)    
    trans_scale(KurrentKanzleiGatC_longs_longs, valueToMoveGlyph)
    return KurrentKanzleiGatC_longs_longs, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
    
def drawKurrentKanzleiGatC_longs_t(x, y, version="isol"):

    KurrentKanzleiGatC_longs_t = BezierPath()

    Hauptstrich_links = drawSchriftzug13(x, y, instrokeLen=1)
    pkt_Ausstrich = Hauptstrich_links.points[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)

    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x+3.18, y+3.78)
    Signatur = drawSchriftteil12(Raute_a[0]+modul_width, Raute_a[1])
    
    if version == "isol":
        instroke = drawPosStroke(x, y-0.25)
        KurrentKanzleiGatC_longs_t += instroke
        glyph_t = drawSchriftzug19(x+3.75, y, version="b")

    if version == "iniA":
        instroke = drawPosStroke(x, y-0.25)
        glyph_t = drawSchriftzug19(x+3.75, y, version="b", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-1], "A", 3, deviation=1.6)
        KurrentKanzleiGatC_longs_t += instroke + constroke 
 
    if version == "iniH":       
        instroke = drawPosStroke(x, y-0.25)
        glyph_t = drawSchriftzug19(x+3.75, y, version="b", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-1], "H")
        KurrentKanzleiGatC_longs_t += instroke + constroke

    if version == "midA":
        glyph_t = drawSchriftzug19(x+3.75, y, version="b", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-1], "A", 3, deviation=1.6)
        KurrentKanzleiGatC_longs_t += constroke 
        
    if version == "midH":
        glyph_t = drawSchriftzug19(x+3.75, y, version="b", outstrokeLen=0)
        constroke = drawConstroke(*glyph_t.points[-1], "H")
        KurrentKanzleiGatC_longs_t += constroke
        
    if version == "fina":
        glyph_t = drawSchriftzug19(x+3.75, y, version="b")
        
    KurrentKanzleiGatC_longs_t.line(Hauptstrich_links.points[-7], glyph_t.points[0])
    Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x+3.25, y)    
    Querstrich = drawGrundelementB(*Grund_a, 2.5)
    
    KurrentKanzleiGatC_longs_t += Hauptstrich_links + Signatur + glyph_t + Querstrich
    drawPath(KurrentKanzleiGatC_longs_t)
    trans_scale(KurrentKanzleiGatC_longs_t, valueToMoveGlyph)
    return KurrentKanzleiGatC_longs_t, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
    
def drawKurrentKanzleiGatC_t(x, y, version="isol"):    
    
    KurrentKanzleiGatC_t = BezierPath()
    
    if version == "isol":
        main_stroke = drawSchriftzug12(x, y, outstrokeLen=1)

    if version == "iniA":   # auch midA
        main_stroke = drawSchriftzug12(x, y)
        constroke = drawConstroke((x-1)*modul_width, (y-4)*modul_height, "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_t += constroke
        
    if version == "iniH":    # auch midH
        main_stroke = drawSchriftzug12(x, y)
        constroke = drawConstroke((x-1)*modul_width, (y-4)*modul_height, "H")
        KurrentKanzleiGatC_t += constroke
        
    if version == "fina":
        main_stroke = drawSchriftzug12(x, y, outstrokeLen=1)

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y)    
    Querstrich = drawGrundelementB(*Grund_a, 2.5)

    KurrentKanzleiGatC_t += main_stroke + Querstrich
    trans_scale(KurrentKanzleiGatC_t, valueToMoveGlyph)
    return KurrentKanzleiGatC_t
    
    
    
    
    
    
    

def drawKurrentKanzleiGatC_u(x, y, version="isol"):
    
    KurrentKanzleiGatC_u = BezierPath()
                
    if version == "isol":
        Strich_links = drawSchriftzug10(x, y, instrokeLen=1, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+1, y)
        Strich_rechts = drawSchriftzug7(x+4, y, "a", instrokeLen=0 , outstrokeLen=1)
  
    if version == "iniA":  
        Strich_links = drawSchriftzug10(x, y, instrokeLen=1, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+1, y)
        Strich_rechts = drawSchriftzug7(x+4, y, "a", instrokeLen=0 , outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_u += constroke

    if version == "iniH": 
        Strich_links = drawSchriftzug10(x, y, instrokeLen=1, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+1, y)
        Strich_rechts = drawSchriftzug7(x+4, y, "a", instrokeLen=0 , outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "H")
        KurrentKanzleiGatC_u += constroke
        
    if version == "midA":  
        Strich_links = drawSchriftzug10(x, y, instrokeLen=0, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+1, y)
        Strich_rechts = drawSchriftzug7(x+4, y, "a", instrokeLen=0 , outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "A", 3.5, deviation=1.4)
        KurrentKanzleiGatC_u += constroke

    if version == "midH": 
        Strich_links = drawSchriftzug10(x, y, instrokeLen=0, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+1, y)
        Strich_rechts = drawSchriftzug7(x+4, y, "a", instrokeLen=0 , outstrokeLen=0)
        constroke = drawConstroke(*Strich_rechts.points[-1], "H")
        KurrentKanzleiGatC_u += constroke

    if version == "fina":
        Strich_links = drawSchriftzug10(x, y, instrokeLen=0, outstrokeLen=1)
        Connection_up = drawConCurve_up(x+1, y)
        Strich_rechts = drawSchriftzug7(x+4, y, "a", instrokeLen=0 , outstrokeLen=1)
        
        
    KurrentKanzleiGatC_u += Strich_links + Connection_up + Strich_rechts
    trans_scale(KurrentKanzleiGatC_u, valueToMoveGlyph)
    return KurrentKanzleiGatC_u
    
    
    
def drawKurrentKanzleiGatC_udieresis(x, y, version="isol"):     
    
    KurrentKanzleiGatC_udieresis = BezierPath()
       
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)

    dieresis = drawDieresis(x+2.5, y)
    trans_scale(dieresis, valueToMoveGlyph)
        
    if version == "isol":
        glyph_u = drawKurrentKanzleiGatC_u(x, y, version="isol")
  
    if version == "iniA":
        glyph_u = drawKurrentKanzleiGatC_u(x, y, version="iniA")

    if version == "iniH":
        glyph_u = drawKurrentKanzleiGatC_u(x, y, version="iniH")
        
    if version == "midA":
        glyph_u = drawKurrentKanzleiGatC_u(x, y, version="midA")

    if version == "midH":
        glyph_u = drawKurrentKanzleiGatC_u(x, y, version="midH")
        
    if version == "fina":
        glyph_u = drawKurrentKanzleiGatC_u(x, y, version="fina")
        
    KurrentKanzleiGatC_udieresis += glyph_u + dieresis
    return KurrentKanzleiGatC_udieresis
    
    
    
    
    
      
    
    
    
    
def drawKurrentKanzleiGatC_v(x, y, version="isol"):
    
    KurrentKanzleiGatC_v = BezierPath()
    
    ### Für diesen Buchstaben habe ich einen extra Downbend entworfen: b-2 der enger ist und mit 2 Abstand anliegt.
    
    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=1 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+3, y)
    
    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=1 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+3, y)
        
        # Raute Positionierung Verbindungsstrich (weil weiter innen anfängt)
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2.5, y-3.5)

        constroke = drawConstroke(*Raute_a, "A", 4, deviation=0.9)
        KurrentKanzleiGatC_v += constroke
        

    if version == "iniH":
        #pass     # I think this is not existent
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=1 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+3, y)
        
        # Raute Positionierung Verbindungsstrich (weil manuell hingepflanzt)
        Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.425, y-3)
        Raute_a_btm, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+7.25, y)
        KurrentKanzleiGatC_v.line(Raute_a_btm, Raute_a_top)
        #constroke = drawConstroke(*Raute_a, "H", 3)
        #KurrentKanzleiGatC_p += constroke
        drawPath(KurrentKanzleiGatC_v)
    

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=0 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+3, y)
        
        # Raute Positionierung Verbindungsstrich (weil weiter innen anfängt)
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2.5, y-3.5)

        constroke = drawConstroke(*Raute_a, "A", 4, deviation=0.9)
        KurrentKanzleiGatC_v += constroke
        

    if version == "midH":
        #pass     # I think this is not existent
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=0 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+3, y)
        
         # Raute Positionierung Verbindungsstrich (weil manuell hingepflanzt)
        Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.425, y-3)
        Raute_a_btm, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+7.25, y)
        KurrentKanzleiGatC_v.line(Raute_a_btm, Raute_a_top)
        #constroke = drawConstroke(*Raute_a, "H", 3)
        #KurrentKanzleiGatC_p += constroke
        drawPath(KurrentKanzleiGatC_v)
        
   
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b-2", instrokeLen=0 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+3, y)

    KurrentKanzleiGatC_v += downstroke_left + bend_right
    trans_scale(KurrentKanzleiGatC_v, valueToMoveGlyph)
    return KurrentKanzleiGatC_v

    
    
    

    
def drawKurrentKanzleiGatC_w(x, y, version="isol"):
    
    KurrentKanzleiGatC_w = BezierPath()
        
    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b-2", instrokeLen=1 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+6.5, y)
    
    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b-2", instrokeLen=1 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+6.5, y)
        
        # Raute Positionierung Verbindungsstrich (weil weiter innen anfängt)
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6, y-3.5)

        constroke = drawConstroke(*Raute_a, "A", 4, deviation=0.9)
        KurrentKanzleiGatC_w += constroke
        

    if version == "iniH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b-2", instrokeLen=1 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+6.5, y)
        
         # Raute Positionierung Verbindungsstrich (weil manuell hingepflanzt)
        Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.925, y-3)
        Raute_a_btm, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+10.75, y)
        KurrentKanzleiGatC_w.line(Raute_a_btm, Raute_a_top)
        #constroke = drawConstroke(*Raute_a, "H", 3)
        #KurrentKanzleiGatC_p += constroke
        drawPath(KurrentKanzleiGatC_w)
    

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b-2", instrokeLen=1 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+6.5, y)
        
        # Raute Positionierung Verbindungsstrich (weil weiter innen anfängt)
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6, y-3.5)

        constroke = drawConstroke(*Raute_a, "A", 4, deviation=0.9)
        KurrentKanzleiGatC_w += constroke
        

    if version == "midH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b-2", instrokeLen=1 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+6.5, y)
        
         # Raute Positionierung Verbindungsstrich (weil manuell hingepflanzt)
        Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.925, y-3)
        Raute_a_btm, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+10.75, y)
        KurrentKanzleiGatC_w.line(Raute_a_btm, Raute_a_top)
        #constroke = drawConstroke(*Raute_a, "H", 3)
        #KurrentKanzleiGatC_p += constroke
        drawPath(KurrentKanzleiGatC_w)
        
   
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0 , outstrokeLen=0)
        downstroke_mid = drawSchriftzug9(x+3.5, y, "b-2", instrokeLen=1 , outstrokeLen=0)
        bend_right = drawSchriftzug26(x+6.5, y)


    KurrentKanzleiGatC_w += downstroke_left + downstroke_mid + bend_right
    trans_scale(KurrentKanzleiGatC_w, valueToMoveGlyph)
    return KurrentKanzleiGatC_w
    
    
    
    
    
def drawKurrentKanzleiGatC_x(x, y, version="isol"):       

    KurrentKanzleiGatC_x = BezierPath()

    if version == "isol":
        glyph_r = drawKurrentKanzleiGatC_r(x, y, version="isol")

    if version == "iniA":
        glyph_r = drawKurrentKanzleiGatC_r(x, y, version="iniA")

    if version == "iniH":
        glyph_r = drawKurrentKanzleiGatC_r(x, y, version="iniH")

    if version == "midA":
        glyph_r = drawKurrentKanzleiGatC_r(x, y, version="midA")

    if version == "midH":
        glyph_r = drawKurrentKanzleiGatC_r(x, y, version="midH")

    if version == "fina":
        glyph_r = drawKurrentKanzleiGatC_r(x, y, version="fina")
        
    tail = drawTail_x(x, y)
    trans_scale(tail, valueToMoveGlyph)
    drawPath(KurrentKanzleiGatC_x)
    KurrentKanzleiGatC_x += glyph_r + tail
    return KurrentKanzleiGatC_x
    
    
    



def drawKurrentKanzleiGatC_y(x, y, version="isol"):
    
    KurrentKanzleiGatC_y = BezierPath()
    
    ### Für diesen Buchstaben habe ich einen extra Downbend entworfen: b-2 der enger ist und mit 2 Abstand anliegt.
    
    if version == "isol":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "fina")    #"fina"

    if version == "iniA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_A", outstrokeLen=4.25)

    if version == "iniH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=1)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_H")

    if version == "midA":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_A", outstrokeLen=2.25)

    if version == "midH":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "loop_H")
   
    if version == "fina":
        downstroke_left = drawSchriftzug9(x, y, "b", instrokeLen=0)
        bend_right = drawSchriftzug20_descender(x+3.5, y, "fina")  

    pkt_Auslauf = bend_right.points[-7]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    KurrentKanzleiGatC_y += downstroke_left + bend_right
    trans_scale(KurrentKanzleiGatC_y, valueToMoveGlyph)

    if version == "isol" or version == "fina":
        return KurrentKanzleiGatC_y, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    return KurrentKanzleiGatC_y
    
    



def drawKurrentKanzleiGatC_z(x,y, version="isol"):
    x += 3    
    KurrentKanzleiGatC_z = BezierPath()
        
    if version == "isol":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="begin")
        Bogen_unten = drawSchriftzug27(x-0.5, y-2.85)

    if version == "iniA":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="begin")
        Bogen_unten = drawSchriftzug_z_unten(x-0.35, y-2.27, version="loop_A")
        constroke = drawConstroke(*Bogen_unten.points[-10], "A", 3, deviation=1.4)
        KurrentKanzleiGatC_z += constroke

    if version == "iniH":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="begin")
        Bogen_unten = drawSchriftzug_z_unten(x-0.35, y-2.27, version="loop_H")
        constroke = drawConstroke(*Bogen_unten.points[-9], "H", 3.15)
        KurrentKanzleiGatC_z += constroke
    
    if version == "midA":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="mid")
        Bogen_unten = drawSchriftzug_z_unten(x-0.35, y-2.27, version="loop_A")
        constroke = drawConstroke(*Bogen_unten.points[-10], "A", 3, deviation=1.4)
        KurrentKanzleiGatC_z += constroke
        
    if version == "midH":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="mid")
        Bogen_unten = drawSchriftzug_z_unten(x-0.35, y-2.27, version="loop_H")
        constroke = drawConstroke(*Bogen_unten.points[-9], "H", 3.15)
        KurrentKanzleiGatC_z += constroke
   
    if version == "fina":
        Bogen_oben = drawSchriftzug_z_oben(x, y, version="mid")
        Bogen_unten = drawSchriftzug27(x-0.5, y-2.85)

    pkt_Auslauf = Bogen_unten.points[-5]
    #text("pkt_Auslauf", pkt_Auslauf)

    KurrentKanzleiGatC_z += Bogen_oben + Bogen_unten
    trans_scale(KurrentKanzleiGatC_z, valueToMoveGlyph)

    if version == "isol" or version == "fina":
        return KurrentKanzleiGatC_z, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    return KurrentKanzleiGatC_z
    
    
    
def drawKurrentKanzleiGatC_z_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatC_z_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 6, HSL_size=2, HSL_start=20, clockwise=False, inward=True)
    #drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 4, HSL_size=2, HSL_start=22, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 5)])
          
    KurrentKanzleiGatC_z_thinStroke += Auslauf
    drawPath(KurrentKanzleiGatC_z_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatC_z_thinStroke) 
    trans_scale(KurrentKanzleiGatC_z_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatC_z_thinStroke
    




    



#############################################################################
######     ab hier Zahlen
#############################################################################




def drawKurrentKanzleiGatC_zero(x, y):
    
    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y)

    KurrentKanzleiGatC_zero = Bogen_links + Bogen_rechts
    trans_scale(KurrentKanzleiGatC_zero, valueToMoveGlyph)
    return KurrentKanzleiGatC_zero
    
    
    
    
    
 
def drawKurrentKanzleiGatC_one(x, y):
    #x -= 5
    Raute_a, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, y+1.5)
    stem = drawGrundelementF(*Raute_a, 4.5)
    Signatur_oben = drawGrundelementC(*Raute_a, instrokeLen=0.5)
    pkt_Ausstrich = Raute_a[0]+modul_width*1.25, Raute_a[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x, baseline+1)
    transition = drawSchneckenzug(*Grund_a, LOWER_B, 3, HSL_size=1, HSL_start=12.8, clockwise=True, inward=True)
    pkt_Auslauf = transition.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    KurrentKanzleiGatC_one = stem + Signatur_oben + transition
    drawPath(KurrentKanzleiGatC_one)
    trans_scale(KurrentKanzleiGatC_one, valueToMoveGlyph)    
    return KurrentKanzleiGatC_one, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Ausstrich')(pkt_Auslauf, pkt_Ausstrich)
    

def drawKurrentKanzleiGatC_one_thinStroke(x, y, *, pass_from_thick=None): 
    
    KurrentKanzleiGatC_one_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 8.5)

    KurrentKanzleiGatC_one_thinStroke += Auslauf + Zierstrich + Endpunkt
    drawPath(KurrentKanzleiGatC_one_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatC_one_thinStroke) 
    trans_scale(KurrentKanzleiGatC_one_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatC_one_thinStroke
    
    
    
    
    
    

def drawKurrentKanzleiGatC_two(x, y):
     
    Bogen = drawSchriftzug_two_Bogen(x, y)
    pkt_Auslauf = Bogen.points[0]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Schwung_unten = drawSchriftzug_two_Schwung (x, y)
    
    KurrentKanzleiGatC_two = Bogen + Schwung_unten
    trans_scale(KurrentKanzleiGatC_two, valueToMoveGlyph)
    return KurrentKanzleiGatC_two, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
def drawKurrentKanzleiGatC_two_thinStroke(x, y, *, pass_from_thick=None): 
    
    KurrentKanzleiGatC_two_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 8, HSL_size=1, HSL_start=16, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.001 * i      for  i in range(0, 9)])

    KurrentKanzleiGatC_two_thinStroke += Auslauf 
    drawPath(KurrentKanzleiGatC_two_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatC_two_thinStroke) 
    trans_scale(KurrentKanzleiGatC_two_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatC_two_thinStroke 
    
    
    
    
    


def drawKurrentKanzleiGatC_three(x, y):

    KurrentKanzleiGatC_three = BezierPath()     
     
    Three_Top = drawSchriftzug_three_Top(x, y)
    Three_Schwung = drawSchriftzug_three_Bogen(x, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 
    
    con = drawGrundelementA(*Three_Schwung.points[0], 3)
    drawPath(KurrentKanzleiGatC_three)

    KurrentKanzleiGatC_three = Three_Top + con + Three_Schwung
    trans_scale(KurrentKanzleiGatC_three, valueToMoveGlyph)
    return KurrentKanzleiGatC_three, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
def drawKurrentKanzleiGatC_three_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatC_three_thinStroke = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, pass_from_thick.pkt_Auslauf[0]/modul_width, pass_from_thick.pkt_Auslauf[1]/modul_height)  
    Auslauf = drawSchneckenzug(*Grund_c, LOWER_E, 11, HSL_size=1, HSL_start=18, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 12)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.9, Auslauf.points[-1][1]-modul_height*1)
        
    KurrentKanzleiGatC_three_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentKanzleiGatC_three_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatC_three_thinStroke) 
    trans_scale(KurrentKanzleiGatC_three_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatC_three_thinStroke
    
    
    
    
    
    
    
def drawKurrentKanzleiGatC_four(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-2.75, y-0.25)

    stem = drawSchriftzug3_Figures(x, y, instrokeLen=0)
    stroke_down = drawGrundelementH(*stem.points[0], 2.75, "down")
    stroke_hor = drawGrundelementB(*Grund_d, 4)

    KurrentKanzleiGatC_four = stem + stroke_down + stroke_hor
    trans_scale(KurrentKanzleiGatC_four, valueToMoveGlyph)
    return KurrentKanzleiGatC_four   
    
    
    
    
def drawKurrentKanzleiGatC_five(x, y):
    #x -= 3
    KurrentKanzleiGatC_five = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.5, y+1.5)
    Five_Top = drawSchriftzug_five_Top(*Raute_a)
    Three_Schwung = drawSchriftzug_three_Bogen(x, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 
    con = drawGrundelementA(*Three_Schwung.points[0], 3)

    drawPath(KurrentKanzleiGatC_five)
    KurrentKanzleiGatC_five = Five_Top + con + Three_Schwung
    trans_scale(KurrentKanzleiGatC_five, valueToMoveGlyph)
    return KurrentKanzleiGatC_five, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    


def drawKurrentKanzleiGatC_six(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    pkt_Auslauf_top = Bogen_links.points[0]
    #text("pkt_Auslauf_top", pkt_Auslauf_top) 
    
    Bogen_rechts = drawSchriftzug_six_BogenRechts(x, y)
    pkt_Auslauf_inner = Bogen_rechts.points[0]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner)
    
    KurrentKanzleiGatC_six = Bogen_links + Bogen_rechts
    trans_scale(KurrentKanzleiGatC_six, valueToMoveGlyph)
    return KurrentKanzleiGatC_six, collections.namedtuple('dummy', 'pkt_Auslauf_top pkt_Auslauf_inner')(pkt_Auslauf_top, pkt_Auslauf_inner)
    

def drawKurrentKanzleiGatC_six_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatC_six_thinStroke = BezierPath() 
    
    Auslauf_top = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_top, UPPER_E, 4, HSL_size=0, HSL_start=13, clockwise=True, inward=False)
    
    ### wegen blödem Absatz
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1.6, y-0.25)
    Auslauf_inner = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=1, HSL_start=14, clockwise=False, inward=False)
        
    trans_thinStroke_down_left(Auslauf_inner) 
    #trans_thinStroke_down_left(Auslauf_inner) 
    trans_thinStroke_up_right(Auslauf_top)
    
    KurrentKanzleiGatC_six_thinStroke += Auslauf_top + Auslauf_inner 
    drawPath(KurrentKanzleiGatC_six_thinStroke)
    trans_scale(KurrentKanzleiGatC_six_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatC_six_thinStroke 
    
    
    
    
    
    
def drawKurrentKanzleiGatC_seven(x, y):

    KurrentKanzleiGatC_seven = BezierPath()     
     
    Seven_Top = drawSchriftzug_three_Top(x-1, y)
    Seven_Stem = drawSchriftzug_seven_Stem(x, y)
    
    drawPath(KurrentKanzleiGatC_seven)
    KurrentKanzleiGatC_seven = Seven_Top + Seven_Stem
    trans_scale(KurrentKanzleiGatC_seven, valueToMoveGlyph)
    return KurrentKanzleiGatC_seven
            


def drawKurrentKanzleiGatC_eight(x, y):

    KurrentKanzleiGatC_eight = drawSchriftzug_eight(x, y)
    trans_scale(KurrentKanzleiGatC_eight, valueToMoveGlyph)
    return KurrentKanzleiGatC_eight
    
    
    
    
    


def drawKurrentKanzleiGatC_nine(x, y):
    
    Bogen_links = drawSchriftzug_nine_BogenLinks(x, y)
    pkt_Auslauf_inner = Bogen_links.points[-1]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner) 
    
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y, version="nine")
    pkt_Auslauf_btm = Bogen_rechts.points[-1]
    #text("pkt_Auslauf_btm", pkt_Auslauf_btm) 
    
    KurrentKanzleiGatC_nine = Bogen_links + Bogen_rechts
    trans_scale(KurrentKanzleiGatC_nine, valueToMoveGlyph)
    return KurrentKanzleiGatC_nine, collections.namedtuple('dummy', 'pkt_Auslauf_btm pkt_Auslauf_inner')(pkt_Auslauf_btm, pkt_Auslauf_inner)
    


def drawKurrentKanzleiGatC_nine_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatC_nine_thinStroke = BezierPath() 
    
    Auslauf_btm = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_btm, LOWER_E, 4, HSL_size=0, HSL_start=14, clockwise=True, inward=False)

    Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, LOWER_E, 4, HSL_size=2, HSL_start=10, clockwise=False, inward=False)

    trans_thinStroke_down_left(Auslauf_btm) 
    #trans_thinStroke_up_right(Auslauf_inner)
    
    KurrentKanzleiGatC_nine_thinStroke += Auslauf_btm + Auslauf_inner 
    drawPath(KurrentKanzleiGatC_nine_thinStroke)
    trans_scale(KurrentKanzleiGatC_nine_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatC_nine_thinStroke










    
###################################
###### ab hier Interpunktion ######
###################################
    
    
    
    
def drawKurrentKanzleiGatC_period(x, y):
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, baseline)
    period = drawGrundelementC(*Raute_a)
        
    KurrentKanzleiGatC_period = period
    trans_scale(KurrentKanzleiGatC_period, valueToMoveGlyph)
    return KurrentKanzleiGatC_period
    
    
    
    
    
def drawKurrentKanzleiGatC_colon(x, y):
        
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)   
    colon_top = drawGrundelementC(*Raute_a)   
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, baseline) 
    colon_btm = drawGrundelementC(*Raute_a)  
    
    KurrentKanzleiGatC_colon = colon_top + colon_btm
    trans_scale(KurrentKanzleiGatC_colon, valueToMoveGlyph)
    return KurrentKanzleiGatC_colon
    
    
    
    
    
def drawKurrentKanzleiGatC_semicolon(x, y):
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    semicolon_top = drawGrundelementC(*Raute_a)
    semicolon_btm = drawSchriftzug_semicolon_btm(x-0.5, baseline+0.25)  

    KurrentKanzleiGatC_semicolon = semicolon_top + semicolon_btm
    trans_scale(KurrentKanzleiGatC_semicolon, valueToMoveGlyph)
    return KurrentKanzleiGatC_semicolon
    
    
    
    

def drawKurrentKanzleiGatC_quoteright(x, y):

    quoteright = drawSchriftzug_semicolon_btm(x, y+0.75) 
    
    KurrentKanzleiGatC_quoteright = quoteright
    trans_scale(KurrentKanzleiGatC_quoteright, valueToMoveGlyph)
    return KurrentKanzleiGatC_quoteright
    


    
def drawKurrentKanzleiGatC_quotesingle(x, y):

    KurrentKanzleiGatC_quotesingle = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    fake = drawGrundelementA(*Grund_c, 0)
    pkt_fake = fake.points[-1][0], fake.points[-1][1]+3*modul_height
    
    #### delete Grundelement A in RF once quotesingle is established
    #### just there because funktion doesn't work if there is no element in foreground layer
    
    KurrentKanzleiGatC_quotesingle = fake
    drawPath(KurrentKanzleiGatC_quotesingle)
    trans_scale(KurrentKanzleiGatC_quotesingle, valueToMoveGlyph)
    return KurrentKanzleiGatC_quotesingle, collections.namedtuple('dummy', 'pkt_fake')(pkt_fake)
    

def drawKurrentKanzleiGatC_quotesingle_thinstroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatC_quotesingle_thinstroke = BezierPath()

    quotesingle = drawSchneckenzug(*pass_from_thick.pkt_fake, LOWER_B, 3, HSL_size=2, HSL_start=6, clockwise=True, inward=False)
    Endpunkt = drawThinstroke_Endpunkt(pass_from_thick.pkt_fake[0]-modul_width*1.11, pass_from_thick.pkt_fake[1]-modul_height*0.55)
    
    KurrentKanzleiGatC_quotesingle_thinstroke += quotesingle + Endpunkt
    drawPath(KurrentKanzleiGatC_quotesingle_thinstroke)
    trans_scale(KurrentKanzleiGatC_quotesingle_thinstroke, valueToMoveGlyph)
    return KurrentKanzleiGatC_quotesingle_thinstroke
    
    
    
    
    
    
    
    
def drawKurrentKanzleiGatC_comma(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.5)
    comma = drawGrundelementG(*Grund_a, 2, "down") 

    KurrentKanzleiGatC_comma = comma
    trans_scale(KurrentKanzleiGatC_comma, valueToMoveGlyph)
    return KurrentKanzleiGatC_comma
    
    



    
def drawKurrentKanzleiGatC_endash(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, baseline-0.5)
    endash = drawGrundelementB(*Grund_a, 2) 

    KurrentKanzleiGatC_endash = endash
    trans_scale(KurrentKanzleiGatC_endash, valueToMoveGlyph)
    return KurrentKanzleiGatC_endash
    
    
    
    
    
def drawKurrentKanzleiGatC_hyphen(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1)
    hyphen_top = drawGrundelementH(*Grund_b, 2, "down") 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3)
    hyphen_btm = drawGrundelementH(*Grund_b, 2, "down") 
    
    KurrentKanzleiGatC_hyphen = hyphen_top + hyphen_btm
    trans_scale(KurrentKanzleiGatC_hyphen, valueToMoveGlyph)
    return KurrentKanzleiGatC_hyphen
     
    
    
    
def drawKurrentKanzleiGatC_exclam(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.25)
    exclam = drawGrundelementF(*Grund_a, 7) 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, baseline)
    period = drawGrundelementC(*Raute_a)

    KurrentKanzleiGatC_exclam = exclam + period
    trans_scale(KurrentKanzleiGatC_exclam, valueToMoveGlyph)
    return KurrentKanzleiGatC_exclam
    
    



def drawKurrentKanzleiGatC_question(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, baseline)
    
    period = drawGrundelementC(*Raute_a)

    question = drawSchriftzug_question(x, y) 

    KurrentKanzleiGatC_question = question + period
    trans_scale(KurrentKanzleiGatC_question, valueToMoveGlyph)
    return KurrentKanzleiGatC_question
    














##################################################################################
###            Ab hier Versalien
##################################################################################





def drawKurrentKanzleiGatC_helperUC(x, y):
    
    KurrentKanzleiGatC_helperUC = BezierPath()

    drawGrundelOrient(A1, A2, offset, x, baseline+0.5)

    helper = (0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5)
    for h in helper:
        drawGrundelOrient(A1, A2, offset, x+3.5, baseline+h)
        
    drawGrundelOrient(A1, A2, offset, x+7, baseline+0.5)
    
    helper = (0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        drawGrundelOrient(A1, A2, offset, x+10.5, baseline+h)
        
    drawGrundelOrient(A1, A2, offset, x+14, baseline+0.5)
    #KurrentKanzleiGatC_helperUC = Bogen_rechts
    #trans_scale(KurrentKanzleiGatC_helperUC)   
    return KurrentKanzleiGatC_helperUC






### drawKurrentKanzleiGatC_C
def drawKurrentKanzleiGatD_C(x, y):
  
    KurrentKanzleiGatD_C = BezierPath()
    x += 3
    ### stehender Schwung
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+3.35) 
    oben = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=1, HSL_start=21, clockwise=False, inward=True)
    instroke = drawInstroke(*oben.points[0], 1.75, "down")
    Einsatz = drawGrundelementE(*oben.points[-1], 0.4)
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=1, HSL_start=13, clockwise=True, inward=True)
    outstroke = drawOutstroke(*unten.points[-1], 1, "down")
    KurrentKanzleiGatD_C += oben + instroke + Einsatz + unten + outstroke

    ### liegender Schwung
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-4.1, y-3.5) 
    links = drawSchneckenzug(*Grund_a, UPPER_H, 3, HSL_size=2, HSL_start=8, clockwise=False, inward=False)
    Einsatz2 = drawGrundelementD(*Grund_a, 1)
    right = drawSchneckenzug(*Einsatz2.points[4], LOWER_H, 3, HSL_size=2, HSL_start=12, clockwise=False, inward=False)
    outstroke = drawOutstroke(*right.points[-1], 1)
    KurrentKanzleiGatD_C += links + Einsatz2 + right + outstroke
    
    ### Deckung
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3.75, y+6.3) 
    DeckungOben_left = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1.25, HSL_start=1, clockwise=False, inward=False)
    DeckungOben_right = drawSchneckenzug(*DeckungOben_left.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=10, clockwise=False, inward=False)
    KurrentKanzleiGatD_C += DeckungOben_left + DeckungOben_right

    drawPath(KurrentKanzleiGatD_C) 
    trans_scale(KurrentKanzleiGatD_C, valueToMoveGlyph)
    return KurrentKanzleiGatD_C
    




### drawKurrentKanzleiGatC_D
def drawKurrentKanzleiGatD_D(x, y):
  
    KurrentKanzleiGatD_D = BezierPath()
    x += 3
    ### stehender Schwung
    Grund_a, Grund_b, Grund_D, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+3.25) 
    oben = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=1, HSL_start=22, clockwise=False, inward=True)
    instroke = drawInstroke(*oben.points[0], 2, "down")
    Einsatz = drawGrundelementE(*oben.points[-1], 0.5)
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=1, HSL_start=13, clockwise=True, inward=True)
    outstroke = drawOutstroke(*unten.points[-1], 1.5, "down")
    KurrentKanzleiGatD_D += oben + instroke + Einsatz + unten + outstroke

    ### liegender Schwung
    Grund_a, Grund_b, Grund_D, Grund_d = drawGrundelOrient(A1, A2, offset, x-3.63, y-3.5) 
    links = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=4, HSL_start=6.25, clockwise=True, inward=False)
    outstroke = drawOutstroke(*links.points[-1], 1.7)
    up = drawSchneckenzug(*outstroke.points[0], LOWER_E, 8, HSL_size=2, HSL_start=42, clockwise=False, inward=True)
    Einsatz2 = drawOutstroke(*up.points[-1], 0.75, "down")
    optional = drawSchneckenzug(*Einsatz2.points[0], UPPER_E, 8, HSL_size=3.5, HSL_start=32, clockwise=False, inward=True)
    KurrentKanzleiGatD_D += links + outstroke + up + Einsatz2 + optional
    
    ## Deckung
    Grund_a, Grund_b, Grund_D, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3.75, y+5.75) 
    DeckungOben_left = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1, HSL_start=1, clockwise=False, inward=False)
    DeckungOben_right = drawSchneckenzug(*DeckungOben_left.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=6, clockwise=False, inward=False)
    KurrentKanzleiGatD_D += DeckungOben_left + DeckungOben_right

    drawPath(KurrentKanzleiGatD_D) 
    trans_scale(KurrentKanzleiGatD_D, valueToMoveGlyph)
    return KurrentKanzleiGatD_D
    
    
    
    
    
    

### drawKurrentKanzleiGatC_H
def drawKurrentKanzleiGatD_H(x, y):
  
    KurrentKanzleiGatD_H = BezierPath()
    x += 4
    ### stehender Schwung
    Grund_a, Grund_b, Grund_H, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+4.2) 
    oben = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=1, HSL_start=28, clockwise=False, inward=True)
    instroke = drawInstroke(*oben.points[0], 1.5, "down")
    Einsatz = drawGrundelementE(*oben.points[-1], 1.255)
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=1, HSL_start=14, clockwise=True, inward=True)
    outstroke = drawOutstroke(*unten.points[-1], 0.75, "down")
    KurrentKanzleiGatD_H += oben + instroke + Einsatz + unten + outstroke

    ### liegender Schwung
    Grund_a, Grund_b, Grund_H, Grund_d = drawGrundelOrient(A1, A2, offset, x-2.18, y-4.5) 
    links = drawSchneckenzug(*Grund_a, UPPER_A, 4, HSL_size=0.25, HSL_start=5, clockwise=False, inward=False)
    Einsatz2 = drawGrundelementD(*Grund_a, 0)
    right = drawSchneckenzug(*Einsatz2.points[4], LOWER_A, 4, HSL_size=0.25, HSL_start=4, clockwise=False, inward=False)
    outstroke = drawOutstroke(*right.points[-1], 0.5)
    KurrentKanzleiGatD_H += links + Einsatz2 + right + outstroke
    
    ### Bauch
    Grund_a, Grund_b, Grund_H, Grund_d = drawGrundelOrient(A1, A2, offset, x+1.25, y+2) 
    instroke_mitte = drawInstroke(*Grund_a, 1.75, "up")
    Bauch_oben = drawSchneckenzug(*Grund_a, LOWER_A, 1, HSL_size=1, HSL_start=24, clockwise=False, inward=True)
    Bauch_mitte = drawSchneckenzug(*Bauch_oben.points[-1], UPPER_H, 2, HSL_size=1, HSL_start=24, clockwise=True, inward=True)    
    Einsatz3 = drawGrundelementF(*Bauch_mitte.points[-1], 1.25)
    Bauch_unten = drawSchneckenzug(*Einsatz3.points[-1], LOWER_B, 3, HSL_size=1, HSL_start=44, clockwise=True, inward=True)    
    pkt_Auslauf = Bauch_unten.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    KurrentKanzleiGatD_H += instroke_mitte + Bauch_oben + Bauch_mitte + Einsatz3 + Bauch_unten

    ### Deckung
    Grund_a, Grund_b, Grund_H, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3.15, y+7) 
    DeckungOben_left = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1.25, HSL_start=1, clockwise=False, inward=False)
    DeckungOben_right = drawSchneckenzug(*DeckungOben_left.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=10, clockwise=False, inward=False)
    KurrentKanzleiGatD_H += DeckungOben_left + DeckungOben_right

    drawPath(KurrentKanzleiGatD_H) 
    trans_scale(KurrentKanzleiGatD_H, valueToMoveGlyph)
    return KurrentKanzleiGatD_H, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    


def drawKurrentKanzleiGatD_H_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatD_H_thinStroke = BezierPath()

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 3, HSL_size=2, HSL_start=38, clockwise=True, inward=True)
    
    KurrentKanzleiGatD_H_thinStroke += Auslauf 
    drawPath(KurrentKanzleiGatD_H_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatD_H_thinStroke)
    trans_scale(KurrentKanzleiGatD_H_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatD_H_thinStroke
    
    






    
### drawKurrentKanzleiGatC_G    
def drawKurrentKanzleiGatD_G(x, y):
    x += 2
    
    ### Hauptbogen
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+5.25) 
    oben = drawSchneckenzug(*Grund_a, UPPER_E, 6, HSL_size=1, HSL_start=5.5, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*oben.points[-1], 2.1)
    mitte = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 6, HSL_size=2, HSL_start=24, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.01 * i      for  i in range(0, 7)])
    outstroke = drawOutstroke(*mitte.points[-1], 1.75, "down")
    Deckung = drawSchneckenzug(*outstroke.points[0], UPPER_E, 8, HSL_size=0.5, HSL_start=6.25, clockwise=True, inward=True)
    Hauptbogen = oben + Einsatz + mitte + outstroke + Deckung
    
    ### Stehender Schwung  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1.25, y+3.75)    
    oben = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=4, HSL_start=32, clockwise=False, inward=True)
    instroke = drawInstroke(*oben.points[0], 1.4, "down")
    unten = drawSchneckenzug(*oben.points[-1], UPPER_H, 5, HSL_size=0.5, HSL_start=8, clockwise=True, inward=False)
    outstroke_con = drawOutstroke(*unten.points[-1], 1.9, "down")
    stehenderSchwung = instroke + oben + unten + Einsatz + outstroke_con
    
    ### Liegender Schwung 
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-2.75, y-3.25)     
    links = drawSchneckenzug(*Grund_a, UPPER_E, 3, HSL_size=1, HSL_start=9, clockwise=True, inward=False)
    Einsatz2 = drawGrundelementD(*links.points[-1], 1)
    rechts = drawSchneckenzug(*Einsatz2.points[4], LOWER_H, 3, HSL_size=1, HSL_start=24, clockwise=False, inward=True)
    liegenderSchwung = links + Einsatz2 + rechts

    ### Auge
    Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.5, y+4.5)
    start_oben = drawSchneckenzug(*Raute_a, LOWER_A, 2, HSL_size=4, HSL_start=8, clockwise=False, inward=False)
    bow = drawSchneckenzug(*start_oben.points[-1], UPPER_G, 6, HSL_size=0.75, HSL_start=3, clockwise=True, inward=False)
    Auge_out = drawOutstroke(*bow.points[-1], 1.25, "down")
    Auge = start_oben + bow + Auge_out
    pkt_instrokeAuge = Auge.points[0]
    #text("pkt_instrokeAuge", pkt_instrokeAuge)
    
    ### Bauch
    top = drawSchneckenzug(*Auge_out.points[0], UPPER_E, 2, HSL_size=4, HSL_start=9, clockwise=True, inward=False)
    oben_rechts = drawSchneckenzug(*top.points[-1], UPPER_G, 3, HSL_size=2, HSL_start=10.5, clockwise=True, inward=False)
    Einsatz = drawGrundelementF(*oben_rechts.points[-1], 0)
    unten_rechts = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=30.5, clockwise=True, inward=False)
    outstroke = drawOutstroke(*unten_rechts.points[-1], 1.5, "down")
    Bauch = top + oben_rechts + Einsatz + unten_rechts + outstroke    
    
    ### Deckung
    Grund_a, Grund_b, Grund_H, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+4, y+6) 
    Deckung_left = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1, HSL_start=1, clockwise=False, inward=False)
    Deckung_right = drawSchneckenzug(*Deckung_left.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=8, clockwise=False, inward=False)
    Deckung = Deckung_left + Deckung_right
    

    pkt_Wendung_rechts = liegenderSchwung.points[0]
    text("pkt_Wendung_rechts", pkt_Wendung_rechts)
    
    KurrentKanzleiGatD_G = Hauptbogen + Auge + Bauch + stehenderSchwung + liegenderSchwung + Deckung
    drawPath(KurrentKanzleiGatD_G)
    trans_scale(KurrentKanzleiGatD_G, valueToMoveGlyph)
    return KurrentKanzleiGatD_G, collections.namedtuple('dummy', 'pkt_instrokeAuge pkt_Wendung_rechts')(pkt_instrokeAuge, pkt_Wendung_rechts)
    
    
    
def drawKurrentKanzleiGatD_G_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatD_G_thinStroke = BezierPath()
    
    #### Auge
    instroke = drawInstroke(*pass_from_thick.pkt_instrokeAuge, 1)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 2, HSL_size=4, HSL_start=22, clockwise=False, inward=True)

    ### Wendung
    Wendung = drawSchneckenzug(*pass_from_thick.pkt_Wendung_rechts, UPPER_E, 8, HSL_size=0.198, HSL_start=0.5, clockwise=False, inward=False)

    KurrentKanzleiGatD_G_thinStroke += instroke + curve_toInstroke + Wendung
    trans_thinStroke_down_left(KurrentKanzleiGatD_G_thinStroke) 
    drawPath(KurrentKanzleiGatD_G_thinStroke)
    trans_scale(KurrentKanzleiGatD_G_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatD_G_thinStroke
    
    
    
    


    

### drawKurrentKanzleiGatC_W
def drawKurrentKanzleiGatD_W(x, y):

    KurrentKanzleiGatD_W = BezierPath()
    
    ### LINKS
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.5, y+5.5) 
    Spitze = drawSchneckenzug(*Grund_d, UPPER_E, 6, HSL_size=1, HSL_start=5, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*Spitze.points[-1], 2)
    Bogen = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 6, HSL_size=2, HSL_start=28, clockwise=True, inward=True)
    KurrentKanzleiGatD_W += Spitze + Einsatz + Bogen
    pkt_Auslauf = Bogen.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    ### MITTE
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6.25, y+3.5) 
    Bogen_oben = drawSchneckenzug(*Grund_a, LOWER_A, 1, HSL_size=1, HSL_start=32, clockwise=False, inward=False)
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 0)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[0], UPPER_H, 5, HSL_size=1, HSL_start=27, clockwise=True, inward=True)
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 1.5, "down")
    Fuss = drawSchneckenzug(*outstroke.points[0], UPPER_E, 4, HSL_size=0, HSL_start=12.6, clockwise=True, inward=False)
    KurrentKanzleiGatD_W += Bogen_oben + Bogen_unten + Einsatz + outstroke  + Fuss
    pkt_Einlauf_links = Bogen_oben.points[0]
    #text("pkt_Einlauf_links", pkt_Einlauf_links)

    ### RECHTS    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+10.6, y+3.75) 
    Bogen_oben_rechts = drawSchneckenzug(*Grund_a, UPPER_B, 2, HSL_size=2, HSL_start=14, clockwise=False, inward=False)
    Bogen_unten_rechts = drawSchneckenzug(*Bogen_oben_rechts.points[-1], UPPER_H, 5, HSL_size=4, HSL_start=18.5, clockwise=True, inward=False)
    outstroke_rechts = drawOutstroke(*Bogen_unten_rechts.points[-1], 2.25, "down")
    KurrentKanzleiGatD_W += Bogen_oben_rechts + Bogen_unten_rechts + outstroke_rechts
    pkt_Einlauf_rechts = Bogen_oben_rechts.points[0]
    #text("pkt_Einlauf_rechts", pkt_Einlauf_rechts)


    drawPath(KurrentKanzleiGatD_W)
    trans_scale(KurrentKanzleiGatD_W, valueToMoveGlyph)
    return KurrentKanzleiGatD_W, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Einlauf_links pkt_Einlauf_rechts')(pkt_Auslauf, pkt_Einlauf_links, pkt_Einlauf_rechts)



def drawKurrentKanzleiGatD_W_thinStroke(x, y, *, pass_from_thick=None):

    KurrentKanzleiGatD_W_thinStroke = BezierPath()
    
    instroke_rechts = drawInstroke(*pass_from_thick.pkt_Einlauf_rechts, 0.5)
    Einlauf_rechts = drawSchneckenzug(*instroke_rechts.points[-1], UPPER_E, 2, HSL_size=2, HSL_start=16, clockwise=False, inward=False)
    instroke_links = drawInstroke(*pass_from_thick.pkt_Einlauf_links, 0.5)
    Einlauf_links = drawSchneckenzug(*instroke_links.points[-1], UPPER_E, 2, HSL_size=2, HSL_start=16, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=18, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
        
    KurrentKanzleiGatD_W_thinStroke += Einlauf_rechts + Einlauf_links + Auslauf + Endpunkt + instroke_rechts + instroke_links 
    drawPath(KurrentKanzleiGatD_W_thinStroke)
    trans_thinStroke_down_left(KurrentKanzleiGatD_W_thinStroke)
    trans_scale(KurrentKanzleiGatD_W_thinStroke, valueToMoveGlyph)
    return KurrentKanzleiGatD_W_thinStroke
    
    
    
    
    
#################################
    
    
    


def drawKurrentKanzleiGatC_thinstroke_Straight(x, y, *, pass_from_thick=None):     
    
    KurrentKanzleiGatC_thinstroke_Straight = BezierPath()
    
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
          
    KurrentKanzleiGatC_thinstroke_Straight +=  Zierstrich
    trans_thinStroke_down_left(KurrentKanzleiGatC_thinstroke_Straight) 
    trans_scale(KurrentKanzleiGatC_thinstroke_Straight, valueToMoveGlyph)
    return KurrentKanzleiGatC_thinstroke_Straight
    
    
    
def drawKurrentKanzleiGatC_empty(x, y, *, pass_from_thick=None):     
    
    KurrentKanzleiGatC_empty = BezierPath()
    KurrentKanzleiGatC_empty.line((-100, -100), (-100, -110))
    return KurrentKanzleiGatC_empty


 
    
# ________________________________________________________
   
margin_Str = 60     
margin_Rnd = 60   
marginStr = 60  
marginRnd = -6
marginConA = 0
marginConA_dev = 0
marginConH = -31
marginFlush = 30
marginInt = 120

# ____________ ab hier in RF ____________________________
 
    
font = CurrentFont()

drawFunctions = {
    
    # 'a' : [ drawKurrentKanzleiGatC_a, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'a.isol' : [ drawKurrentKanzleiGatC_a, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'a.iniA' : [ drawKurrentKanzleiGatC_a, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA_dev ],
    # 'a.iniH' : [ drawKurrentKanzleiGatC_a, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'a.midA' : [ drawKurrentKanzleiGatC_a, [temp_x, temp_y, "iniA"], marginRnd, marginConA_dev ],
    # 'a.midH' : [ drawKurrentKanzleiGatC_a, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'a.fina' : [ drawKurrentKanzleiGatC_a, [temp_x, temp_y, "fina"], marginRnd, margin_Str ],
    
    # 'b' : [ drawKurrentKanzleiGatC_b, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'b.isol' : [ drawKurrentKanzleiGatC_b, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'b.iniA' : [ drawKurrentKanzleiGatC_b, [temp_x, temp_y, "iniA"], marginFlush, 56 ],
    # 'b.iniH' : [ drawKurrentKanzleiGatC_b, [temp_x, temp_y, "iniH"], marginFlush, 22 ],
    # 'b.midA' : [ drawKurrentKanzleiGatC_b, [temp_x, temp_y, "midA"], marginFlush, 56 ],
    # 'b.midH' : [ drawKurrentKanzleiGatC_b, [temp_x, temp_y, "midH"], marginFlush, 22 ],
    # 'b.fina' : [ drawKurrentKanzleiGatC_b, [temp_x, temp_y, "fina"], marginFlush, margin_Str ], 
    
    # 'c' : [ drawKurrentKanzleiGatC_c, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'c.isol' : [ drawKurrentKanzleiGatC_c, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'c.iniA' : [ drawKurrentKanzleiGatC_c, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA_dev ],
    # 'c.iniH' : [ drawKurrentKanzleiGatC_c, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'c.midA' : [ drawKurrentKanzleiGatC_c, [temp_x, temp_y, "iniA"], marginRnd, marginConA_dev ],
    # 'c.midH' : [ drawKurrentKanzleiGatC_c, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'c.fina' : [ drawKurrentKanzleiGatC_c, [temp_x, temp_y, "isol"], marginRnd, margin_Str ], 
    
    # 'd' : [ drawKurrentKanzleiGatC_d, [temp_x, temp_y], margin_Rnd, margin_Rnd ],
    # 'd.isol' : [ drawKurrentKanzleiGatC_d, [temp_x, temp_y, "isol"], margin_Rnd, margin_Rnd ],
    # 'd.iniA' : [ drawKurrentKanzleiGatC_d, [temp_x, temp_y, "iniA"], margin_Rnd, marginRnd+112 ],
    # 'd.iniH' : [ drawKurrentKanzleiGatC_d, [temp_x, temp_y, "iniH"], marginStr, marginConH ],
    # 'd.midA' : [ drawKurrentKanzleiGatC_d, [temp_x, temp_y, "iniA"], marginRnd, marginConA+112 ],
    # 'd.midH' : [ drawKurrentKanzleiGatC_d, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'd.fina' : [ drawKurrentKanzleiGatC_d, [temp_x, temp_y, "fina"], marginRnd, margin_Rnd],
    
    # 'e' : [ drawKurrentKanzleiGatC_e, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'e.isol' : [ drawKurrentKanzleiGatC_e, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'e.iniA' : [ drawKurrentKanzleiGatC_e, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA_dev ],
    # 'e.iniH' : [ drawKurrentKanzleiGatC_e, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'e.midA' : [ drawKurrentKanzleiGatC_e, [temp_x, temp_y, "iniA"], marginRnd, marginConA_dev ],
    # 'e.midH' : [ drawKurrentKanzleiGatC_e, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'e.fina' : [ drawKurrentKanzleiGatC_e, [temp_x, temp_y, "isol"], marginRnd, margin_Str ],
    
    # 'f' : [ drawKurrentKanzleiGatC_f, [temp_x, temp_y], marginStr, marginStr ],
    # 'f.isol' : [ drawKurrentKanzleiGatC_f, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'f.iniA' : [ drawKurrentKanzleiGatC_f, [temp_x, temp_y, "iniA"], margin_Str, marginConA-75 ],
    # 'f.iniH' : [ drawKurrentKanzleiGatC_f, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'f.midA' : [ drawKurrentKanzleiGatC_f, [temp_x, temp_y, "iniA"], -30, marginConA-75 ],
    # 'f.midH' : [ drawKurrentKanzleiGatC_f, [temp_x, temp_y, "iniH"], -30, marginConH ],
    # 'f.fina' : [ drawKurrentKanzleiGatC_f, [temp_x, temp_y, "isol"], -30, marginStr ],
    
    # 'g' : [ drawKurrentKanzleiGatC_g, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'g.isol' : [ drawKurrentKanzleiGatC_g, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'g.iniA' : [ drawKurrentKanzleiGatC_g, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA ],
    # 'g.iniH' : [ drawKurrentKanzleiGatC_g, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'g.midA' : [ drawKurrentKanzleiGatC_g, [temp_x, temp_y, "iniA"], marginRnd, marginConA ],
    # 'g.midH' : [ drawKurrentKanzleiGatC_g, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'g.fina' : [ drawKurrentKanzleiGatC_g, [temp_x, temp_y, "fina"], marginRnd, margin_Str ],
    
    # 'h' : [ drawKurrentKanzleiGatC_h, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'h.isol' : [ drawKurrentKanzleiGatC_h, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'h.iniA' : [ drawKurrentKanzleiGatC_h, [temp_x, temp_y, "iniA"], margin_Str, marginConA+25 ],
    # 'h.iniH' : [ drawKurrentKanzleiGatC_h, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'h.midA' : [ drawKurrentKanzleiGatC_h, [temp_x, temp_y, "midA"], -8, marginConA+25],
    # 'h.midH' : [ drawKurrentKanzleiGatC_h, [temp_x, temp_y, "midH"], -8, marginConH ],
    # 'h.fina' : [ drawKurrentKanzleiGatC_h, [temp_x, temp_y, "fina"], -8, margin_Str ],
    
    # 'i' : [ drawKurrentKanzleiGatC_i, [temp_x, temp_y], margin_Str, margin_Str-90],
    # 'i.isol' : [ drawKurrentKanzleiGatC_i, [temp_x, temp_y, "isol"], margin_Str, margin_Str-90],
    # 'i.iniA' : [ drawKurrentKanzleiGatC_i, [temp_x, temp_y, "iniA"], margin_Str, marginConA],
    # 'i.iniH' : [ drawKurrentKanzleiGatC_i, [temp_x, temp_y, "iniH"], marginStr, marginConH ],
    # 'i.midA' : [ drawKurrentKanzleiGatC_i, [temp_x, temp_y, "midA"], 30, marginConA ],
    # 'i.midH' : [ drawKurrentKanzleiGatC_i, [temp_x, temp_y, "midH"], 30, marginConH ],
    # 'i.fina' : [ drawKurrentKanzleiGatC_i, [temp_x, temp_y, "fina"], 30, margin_Str-90],
    
    # 'j' : [ drawKurrentKanzleiGatC_j, [temp_x, temp_y], margin_Str-60, margin_Str-90],
    # 'j.isol' : [ drawKurrentKanzleiGatC_j, [temp_x, temp_y, "isol"], margin_Str-60, margin_Str-90],
    # 'j.iniA' : [ drawKurrentKanzleiGatC_j, [temp_x, temp_y, "iniA"], margin_Str-80, marginConA ],
    # 'j.iniH' : [ drawKurrentKanzleiGatC_j, [temp_x, temp_y, "iniH"], margin_Str-80, marginConH ],
    # 'j.midA' : [ drawKurrentKanzleiGatC_j, [temp_x, temp_y, "midA"], margin_Str-167, marginConA ],
    # 'j.midH' : [ drawKurrentKanzleiGatC_j, [temp_x, temp_y, "midH"], margin_Str-162, marginConH ],
    #  'j.fina' : [ drawKurrentKanzleiGatC_j, [temp_x, temp_y, "fina"], margin_Str-118, margin_Str-90],
    
    # 'k' : [ drawKurrentKanzleiGatC_k, [temp_x, temp_y], margin_Str, margin_Str-120 ],
    # 'k.isol' : [ drawKurrentKanzleiGatC_k, [temp_x, temp_y, "isol"], margin_Str, margin_Str-120 ],
    # 'k.iniA' : [ drawKurrentKanzleiGatC_k, [temp_x, temp_y, "iniA"], margin_Str, marginConA-25 ],
    # 'k.iniH' : [ drawKurrentKanzleiGatC_k, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'k.midA' : [ drawKurrentKanzleiGatC_k, [temp_x, temp_y, "midA"], margin_Str-90, marginConA ],
    # 'k.midH' : [ drawKurrentKanzleiGatC_k, [temp_x, temp_y, "midH"], margin_Str-90, marginConH ],
    # 'k.fina' : [ drawKurrentKanzleiGatC_k, [temp_x, temp_y, "fina"], margin_Str-90, margin_Str-100 ],
    
    # 'l' : [ drawKurrentKanzleiGatC_l, [temp_x, temp_y], margin_Str, margin_Str-20 ],
    # 'l.isol' : [ drawKurrentKanzleiGatC_l, [temp_x, temp_y, "isol"], margin_Str, margin_Str-20 ],
    # 'l.iniA' : [ drawKurrentKanzleiGatC_l, [temp_x, temp_y, "iniA"], margin_Str-20, marginConA ],
    # 'l.iniH' : [ drawKurrentKanzleiGatC_l, [temp_x, temp_y, "iniH"], margin_Str-20, marginConH ],
    # 'l.midA' : [ drawKurrentKanzleiGatC_l, [temp_x, temp_y, "midA"], margin_Str-30, marginConA ],
    # 'l.midH' : [ drawKurrentKanzleiGatC_l, [temp_x, temp_y, "midH"], margin_Str-30, marginConH ],
    # 'l.fina' : [ drawKurrentKanzleiGatC_l, [temp_x, temp_y, "fina"], margin_Str-30, margin_Str-20  ],
    
    # 'm' : [ drawKurrentKanzleiGatC_m, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'm.isol' : [ drawKurrentKanzleiGatC_m, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'm.iniA' : [ drawKurrentKanzleiGatC_m, [temp_x, temp_y, "iniA"], marginStr, marginConA ],
    # 'm.iniH' : [ drawKurrentKanzleiGatC_m, [temp_x, temp_y, "iniH"], marginStr, marginConH ],
    # 'm.midA' : [ drawKurrentKanzleiGatC_m, [temp_x, temp_y, "midA"], -8, marginConA ],
    # 'm.midH' : [ drawKurrentKanzleiGatC_m, [temp_x, temp_y, "midH"], -8, marginConH ],
    # 'm.fina' : [ drawKurrentKanzleiGatC_m, [temp_x, temp_y, "fina"], -8, margin_Str ],
    
    # 'n' : [ drawKurrentKanzleiGatC_n, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'n.isol' : [ drawKurrentKanzleiGatC_n, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'n.iniA' : [ drawKurrentKanzleiGatC_n, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'n.iniH' : [ drawKurrentKanzleiGatC_n, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'n.midA' : [ drawKurrentKanzleiGatC_n, [temp_x, temp_y, "midA"], -7, marginConA ],
    # 'n.midH' : [ drawKurrentKanzleiGatC_n, [temp_x, temp_y, "midH"], -7, marginConH ],
    # 'n.fina' : [ drawKurrentKanzleiGatC_n, [temp_x, temp_y, "fina"], -7, margin_Str ],

    # 'o' : [ drawKurrentKanzleiGatC_o, [temp_x, temp_y], margin_Rnd, margin_Rnd ],
    # 'o.isol' : [ drawKurrentKanzleiGatC_o, [temp_x, temp_y, "isol"], margin_Rnd, margin_Rnd ],
    # 'o.iniA' : [ drawKurrentKanzleiGatC_o, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA+40 ],
    # 'o.iniH' : [ drawKurrentKanzleiGatC_o, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'o.midA' : [ drawKurrentKanzleiGatC_o, [temp_x, temp_y, "midA"], marginRnd, marginConA+40 ],
    # 'o.midH' : [ drawKurrentKanzleiGatC_o, [temp_x, temp_y, "midH"], marginRnd, marginConH ],
    # 'o.fina' : [ drawKurrentKanzleiGatC_o, [temp_x, temp_y, "fina"], marginRnd, margin_Rnd ],
        
    # 'p' : [ drawKurrentKanzleiGatC_p, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'p.isol' : [ drawKurrentKanzleiGatC_p, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'p.iniA' : [ drawKurrentKanzleiGatC_p, [temp_x, temp_y, "iniA"], margin_Str, marginConA-20 ],
    # 'p.iniH' : [ drawKurrentKanzleiGatC_p, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'p.midA' : [ drawKurrentKanzleiGatC_p, [temp_x, temp_y, "midA"], -9, marginConA ],
    # 'p.midH' : [ drawKurrentKanzleiGatC_p, [temp_x, temp_y, "midH"], -9, marginConH ],
    # 'p.fina' : [ drawKurrentKanzleiGatC_p, [temp_x, temp_y, "fina"], -9, margin_Rnd ],
    
    # 'q' : [ drawKurrentKanzleiGatC_q, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'q.isol' : [ drawKurrentKanzleiGatC_q, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'q.iniA' : [ drawKurrentKanzleiGatC_q, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA ],
    # 'q.iniH' : [ drawKurrentKanzleiGatC_q, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'q.midA' : [ drawKurrentKanzleiGatC_q, [temp_x, temp_y, "iniA"], marginRnd, marginConA ],
    # 'q.midH' : [ drawKurrentKanzleiGatC_q, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'q.fina' : [ drawKurrentKanzleiGatC_q, [temp_x, temp_y, "fina"], marginRnd, margin_Str ],
    
    # 'r' : [ drawKurrentKanzleiGatC_r, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'r.isol' : [ drawKurrentKanzleiGatC_r, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'r.iniA' : [ drawKurrentKanzleiGatC_r, [temp_x, temp_y, "iniA"], margin_Str, marginConA-10 ],
    # 'r.iniH' : [ drawKurrentKanzleiGatC_r, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'r.midA' : [ drawKurrentKanzleiGatC_r, [temp_x, temp_y, "midA"], -7, marginConA-10 ],
    # 'r.midH' : [ drawKurrentKanzleiGatC_r, [temp_x, temp_y, "midH"], -7, marginConH ],
    # 'r.fina' : [ drawKurrentKanzleiGatC_r, [temp_x, temp_y, "fina"], -7, margin_Str ],

    #  's' : [ drawKurrentKanzleiGatC_s, [temp_x, temp_y], margin_Str-3, margin_Str ],
    #  's.isol' : [ drawKurrentKanzleiGatC_s, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],    
    #  's.iniA' : [ drawKurrentKanzleiGatC_s, [temp_x, temp_y, "isol"], 30, marginConA+30],
    #  's.iniH' : [ drawKurrentKanzleiGatC_s, [temp_x, temp_y, "iniH"], 30, marginConH ],
    #  's.midA' : [ drawKurrentKanzleiGatC_s, [temp_x, temp_y, "fina"], 30, marginConA+30 ],
    #  's.midH' : [ drawKurrentKanzleiGatC_s, [temp_x, temp_y, "midH"], 30, marginConH ],    
    #  's.fina' : [ drawKurrentKanzleiGatC_s, [temp_x, temp_y, "fina"], 30, margin_Str-3 ],

    # 'longs' : [ drawKurrentKanzleiGatC_longs, [temp_x, temp_y], margin_Str, margin_Str-50 ],
    # 'longs.isol' : [ drawKurrentKanzleiGatC_longs, [temp_x, temp_y, "isol"], margin_Str, margin_Str-50 ],
    # 'longs.iniA' : [ drawKurrentKanzleiGatC_longs, [temp_x, temp_y, "iniA"], margin_Str, marginStr-124],
    # 'longs.iniH' : [ drawKurrentKanzleiGatC_longs, [temp_x, temp_y, "iniH"], margin_Str, marginStr-91 ],
    # 'longs.midA' : [ drawKurrentKanzleiGatC_longs, [temp_x, temp_y, "midA"], marginFlush, marginStr-124 ],
    # 'longs.midH' : [ drawKurrentKanzleiGatC_longs, [temp_x, temp_y, "midH"], marginFlush, marginStr-89 ],    
    # 'longs.fina' : [ drawKurrentKanzleiGatC_longs, [temp_x, temp_y, "fina"], marginFlush, marginStr-89 ],    

    # 'germandbls' : [ drawKurrentKanzleiGatC_germandbls, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'germandbls.isol' : [ drawKurrentKanzleiGatC_germandbls, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'germandbls.iniA' : [ drawKurrentKanzleiGatC_germandbls, [temp_x, temp_y, "iniA"], margin_Str, marginStr+30],
    # 'germandbls.iniH' : [ drawKurrentKanzleiGatC_germandbls, [temp_x, temp_y, "iniH"], margin_Str, marginStr-90 ],
    # 'germandbls.midA' : [ drawKurrentKanzleiGatC_germandbls, [temp_x, temp_y, "midA"], marginFlush, marginStr+30 ],
    # 'germandbls.midH' : [ drawKurrentKanzleiGatC_germandbls, [temp_x, temp_y, "midH"], marginFlush, marginStr-90 ],    
    # 'germandbls.fina' : [ drawKurrentKanzleiGatC_germandbls, [temp_x, temp_y, "fina"], marginFlush, marginStr ],    

    # 't' : [ drawKurrentKanzleiGatC_t, [temp_x, temp_y], margin_Str, margin_Str ],
    # 't.isol' : [ drawKurrentKanzleiGatC_t, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 't.iniA' : [ drawKurrentKanzleiGatC_t, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 't.iniH' : [ drawKurrentKanzleiGatC_t, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 't.midA' : [ drawKurrentKanzleiGatC_t, [temp_x, temp_y, "iniA"], marginStr-89, marginConA ],
    # 't.midH' : [ drawKurrentKanzleiGatC_t, [temp_x, temp_y, "iniH"], marginStr-89, marginConH ],
    # 't.fina' : [ drawKurrentKanzleiGatC_t, [temp_x, temp_y, "fina"], marginStr-89, margin_Str ],
    
    # 'u' : [ drawKurrentKanzleiGatC_u, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'u.isol' : [ drawKurrentKanzleiGatC_u, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'u.iniA' : [ drawKurrentKanzleiGatC_u, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'u.iniH' : [ drawKurrentKanzleiGatC_u, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'u.midA' : [ drawKurrentKanzleiGatC_u, [temp_x, temp_y, "midA"], marginFlush, marginConA ],
    # 'u.midH' : [ drawKurrentKanzleiGatC_u, [temp_x, temp_y, "midH"], marginFlush, marginConH ],
    # 'u.fina' : [ drawKurrentKanzleiGatC_u, [temp_x, temp_y, "fina"], marginFlush, margin_Str ],
    
    # 'v' : [ drawKurrentKanzleiGatC_v, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'v.isol' : [ drawKurrentKanzleiGatC_v, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'v.iniA' : [ drawKurrentKanzleiGatC_v, [temp_x, temp_y, "iniA"], margin_Str, marginConA-20 ],
    # 'v.iniH' : [ drawKurrentKanzleiGatC_v, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'v.midA' : [ drawKurrentKanzleiGatC_v, [temp_x, temp_y, "midA"], -10, marginConA ],
    # 'v.midH' : [ drawKurrentKanzleiGatC_v, [temp_x, temp_y, "midH"], -10, marginConH ],
    # 'v.fina' : [ drawKurrentKanzleiGatC_v, [temp_x, temp_y, "fina"], -10, margin_Rnd ],
    
    # 'w' : [ drawKurrentKanzleiGatC_w, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'w.isol' : [ drawKurrentKanzleiGatC_w, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'w.iniA' : [ drawKurrentKanzleiGatC_w, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'w.iniH' : [ drawKurrentKanzleiGatC_w, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'w.midA' : [ drawKurrentKanzleiGatC_w, [temp_x, temp_y, "midA"], -7, marginConA ],
    # 'w.midH' : [ drawKurrentKanzleiGatC_w, [temp_x, temp_y, "midH"], -7, marginConH ],  ### new and ok
    # 'w.fina' : [ drawKurrentKanzleiGatC_w, [temp_x, temp_y, "fina"], -7, margin_Rnd ],
    
    # 'x' : [ drawKurrentKanzleiGatC_x, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'x.isol' : [ drawKurrentKanzleiGatC_x, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'x.iniA' : [ drawKurrentKanzleiGatC_x, [temp_x, temp_y, "iniA"], margin_Str-139, marginConA-10 ],
    # 'x.iniH' : [ drawKurrentKanzleiGatC_x, [temp_x, temp_y, "iniH"], margin_Str-139, marginConH ],
    # 'x.midA' : [ drawKurrentKanzleiGatC_x, [temp_x, temp_y, "midA"], marginStr-198, marginConA-10 ],
    # 'x.midH' : [ drawKurrentKanzleiGatC_x, [temp_x, temp_y, "midH"], marginStr-198, marginConH ],
    # 'x.fina' : [ drawKurrentKanzleiGatC_x, [temp_x, temp_y, "fina"], marginStr-198, margin_Str ],
    
    # 'y' : [ drawKurrentKanzleiGatC_y, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'y.isol' : [ drawKurrentKanzleiGatC_y, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'y.iniA' : [ drawKurrentKanzleiGatC_y, [temp_x, temp_y, "iniA"], margin_Str, marginConA+30 ],
    # 'y.iniH' : [ drawKurrentKanzleiGatC_y, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'y.midA' : [ drawKurrentKanzleiGatC_y, [temp_x, temp_y, "midA"], -8, marginConA+30],
    # 'y.midH' : [ drawKurrentKanzleiGatC_y, [temp_x, temp_y, "midH"], -8, marginConH ],
    # 'y.fina' : [ drawKurrentKanzleiGatC_y, [temp_x, temp_y, "fina"], -8, margin_Str ],
    
    # 'z' : [ drawKurrentKanzleiGatC_z, [temp_x, temp_y], margin_Str, margin_Rnd ],
    # 'z.isol' : [ drawKurrentKanzleiGatC_z, [temp_x, temp_y, "isol"], margin_Str, margin_Rnd ],
    # 'z.iniA' : [ drawKurrentKanzleiGatC_z, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'z.iniH' : [ drawKurrentKanzleiGatC_z, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'z.midA' : [ drawKurrentKanzleiGatC_z, [temp_x, temp_y, "midA"], marginStr-213, marginConA ],
    # 'z.midH' : [ drawKurrentKanzleiGatC_z, [temp_x, temp_y, "midH"], marginStr-143, marginConH ],
    #  'z.fina' : [ drawKurrentKanzleiGatC_z, [temp_x, temp_y, "fina"], marginStr-118, margin_Str ],





    # # Dieresis

    # 'adieresis' : [ drawKurrentKanzleiGatC_adieresis, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'adieresis.isol' : [ drawKurrentKanzleiGatC_adieresis, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'adieresis.iniA' : [ drawKurrentKanzleiGatC_adieresis, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA ],
    # 'adieresis.iniH' : [ drawKurrentKanzleiGatC_adieresis, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'adieresis.midA' : [ drawKurrentKanzleiGatC_adieresis, [temp_x, temp_y, "iniA"], marginRnd, marginConA ],
    # 'adieresis.midH' : [ drawKurrentKanzleiGatC_adieresis, [temp_x, temp_y, "iniH"], marginRnd, marginConH ],
    # 'adieresis.fina' : [ drawKurrentKanzleiGatC_adieresis, [temp_x, temp_y, "fina"], marginRnd, margin_Str ],
    
    # 'odieresis' : [ drawKurrentKanzleiGatC_odieresis, [temp_x, temp_y], margin_Rnd, margin_Str ],
    # 'odieresis.isol' : [ drawKurrentKanzleiGatC_odieresis, [temp_x, temp_y, "isol"], margin_Rnd, margin_Str ],
    # 'odieresis.iniA' : [ drawKurrentKanzleiGatC_odieresis, [temp_x, temp_y, "iniA"], margin_Rnd, marginConA ],
    # 'odieresis.iniH' : [ drawKurrentKanzleiGatC_odieresis, [temp_x, temp_y, "iniH"], margin_Rnd, marginConH ],
    # 'odieresis.midA' : [ drawKurrentKanzleiGatC_odieresis, [temp_x, temp_y, "midA"], marginRnd, marginConA ],
    # 'odieresis.midH' : [ drawKurrentKanzleiGatC_odieresis, [temp_x, temp_y, "midH"], marginRnd, marginConH ],
    # 'odieresis.fina' : [ drawKurrentKanzleiGatC_odieresis, [temp_x, temp_y, "fina"], marginRnd, margin_Str ],
    
    # 'udieresis' : [ drawKurrentKanzleiGatC_udieresis, [temp_x, temp_y], margin_Str, margin_Str ],
    # 'udieresis.isol' : [ drawKurrentKanzleiGatC_udieresis, [temp_x, temp_y, "isol"], margin_Str, margin_Str ],
    # 'udieresis.iniA' : [ drawKurrentKanzleiGatC_udieresis, [temp_x, temp_y, "iniA"], margin_Str, marginConA ],
    # 'udieresis.iniH' : [ drawKurrentKanzleiGatC_udieresis, [temp_x, temp_y, "iniH"], margin_Str, marginConH ],
    # 'udieresis.midA' : [ drawKurrentKanzleiGatC_udieresis, [temp_x, temp_y, "midA"], marginFlush, marginConA ],
    # 'udieresis.midH' : [ drawKurrentKanzleiGatC_udieresis, [temp_x, temp_y, "midH"], marginFlush, marginConH ],
    # 'udieresis.fina' : [ drawKurrentKanzleiGatC_udieresis, [temp_x, temp_y, "fina"], marginFlush, margin_Str ],
    
    
    
    
    
    # ### Ligatures
    
    # 'r_c' : [ drawKurrentKanzleiGatC_rc, [temp_x, temp_y], marginStr, marginStr-60 ],

    #  'longs_longs' : [ drawKurrentKanzleiGatC_longs_longs, [temp_x, temp_y], marginStr-30, marginStr-250],
    #  'longs_longs.isol' : [ drawKurrentKanzleiGatC_longs_longs, [temp_x, temp_y, "isol"], marginStr-30, marginStr-250],
    #  'longs_longs.iniA' : [ drawKurrentKanzleiGatC_longs_longs, [temp_x, temp_y, "iniA"], marginStr-30, marginStr-160],
    #  'longs_longs.iniH' : [ drawKurrentKanzleiGatC_longs_longs, [temp_x, temp_y, "iniH"], marginStr-30, marginStr-120],
    #  'longs_longs.midA' : [ drawKurrentKanzleiGatC_longs_longs, [temp_x, temp_y, "midA"], -60, marginStr-160],
    #  'longs_longs.midH' : [ drawKurrentKanzleiGatC_longs_longs, [temp_x, temp_y, "midH"], -60, marginStr-120],

    #  'f_f' : [ drawKurrentKanzleiGatC_f_f, [temp_x, temp_y], marginStr, marginStr ],
    #  'f_f.isol' : [ drawKurrentKanzleiGatC_f_f, [temp_x, temp_y, "isol"], marginStr-30, marginStr-250],
    #  'f_f.iniA' : [ drawKurrentKanzleiGatC_f_f, [temp_x, temp_y, "iniA"], marginStr-30, marginStr-160],
    #  'f_f.iniH' : [ drawKurrentKanzleiGatC_f_f, [temp_x, temp_y, "iniH"], marginStr-30, marginStr-120],
    #  'f_f.midA' : [ drawKurrentKanzleiGatC_f_f, [temp_x, temp_y, "midA"], marginStr-90, marginStr-160],
    #  'f_f.midH' : [ drawKurrentKanzleiGatC_f_f, [temp_x, temp_y, "midH"], marginStr-90, marginStr-120],
    #  'f_f.fina' : [ drawKurrentKanzleiGatC_f_f, [temp_x, temp_y, "fina"], marginStr-90, marginStr-250],
    
    #  'longs_t' : [ drawKurrentKanzleiGatC_longs_t, [temp_x, temp_y], marginStr, marginStr-50 ],
    #  'longs_t.isol' : [ drawKurrentKanzleiGatC_longs_t, [temp_x, temp_y, "isol"], marginStr-30, marginStr-50],
    #   'longs_t.iniA' : [ drawKurrentKanzleiGatC_longs_t, [temp_x, temp_y, "iniA"], marginStr-30, marginConA-8],
    #  'longs_t.iniH' : [ drawKurrentKanzleiGatC_longs_t, [temp_x, temp_y, "iniH"], marginStr-130, marginConH],
    #   'longs_t.midA' : [ drawKurrentKanzleiGatC_longs_t, [temp_x, temp_y, "midA"], marginStr-30, marginConA-8],
    #  'longs_t.midH' : [ drawKurrentKanzleiGatC_longs_t, [temp_x, temp_y, "midH"], marginStr-30, marginConH],
    #  'longs_t.fina' : [ drawKurrentKanzleiGatC_longs_t, [temp_x, temp_y, "fina"], marginStr-30, marginStr-50],

    #  'f_f_t' : [ drawKurrentKanzleiGatC_f_f_t, [temp_x, temp_y], marginStr, marginStr ],
    #  'f_f_t.isol' : [ drawKurrentKanzleiGatC_f_f_t, [temp_x, temp_y, "isol"], marginStr-30, marginStr ],
    #  'f_f_t.iniA' : [ drawKurrentKanzleiGatC_f_f_t, [temp_x, temp_y, "iniA"], marginStr-30, marginStr-60],
    #  'f_f_t.iniH' : [ drawKurrentKanzleiGatC_f_f_t, [temp_x, temp_y, "iniH"], marginStr-30, marginStr-90],
    #  'f_f_t.midA' : [ drawKurrentKanzleiGatC_f_f_t, [temp_x, temp_y, "midA"], marginStr-90, marginStr-60],
    #  'f_f_t.midH' : [ drawKurrentKanzleiGatC_f_f_t, [temp_x, temp_y, "midH"], marginStr-90, marginStr-90],
    #  'f_f_t.fina' : [ drawKurrentKanzleiGatC_f_f_t, [temp_x, temp_y, "fina"], marginStr-90, marginStr-100],


    
    # #### Interpunction
    
    # 'period' : [ drawKurrentKanzleiGatC_period, [temp_x, temp_y], marginInt, marginInt ],
    # 'colon' : [ drawKurrentKanzleiGatC_colon, [temp_x, temp_y], marginInt, marginInt ],
    # 'semicolon' : [ drawKurrentKanzleiGatC_semicolon, [temp_x, temp_y], marginInt, marginInt ],
    # 'quoteright' : [ drawKurrentKanzleiGatC_quoteright, [temp_x, temp_y], marginInt, marginInt ],
    # 'quotesingle' : [ drawKurrentKanzleiGatC_quotesingle, [temp_x, temp_y], marginInt, marginInt ],
    # 'comma' : [ drawKurrentKanzleiGatC_comma, [temp_x, temp_y], marginInt, marginInt ],
    # 'endash' : [ drawKurrentKanzleiGatC_endash, [temp_x, temp_y], marginInt, marginInt ],
    # 'hyphen' : [ drawKurrentKanzleiGatC_hyphen, [temp_x, temp_y], marginInt, marginInt ],
    # 'exclam' : [ drawKurrentKanzleiGatC_exclam, [temp_x, temp_y], marginInt, marginInt ],
    # 'question' : [ drawKurrentKanzleiGatC_question, [temp_x, temp_y], marginInt, marginInt ],



    # #### Numbers
    
    # 'zero' : [ drawKurrentKanzleiGatC_zero, [temp_x, temp_y], marginStr, marginStr ],
    # 'one' : [ drawKurrentKanzleiGatC_one, [temp_x, temp_y], marginStr+140, marginStr ],
    # 'two' : [ drawKurrentKanzleiGatC_two, [temp_x, temp_y], marginStr, marginStr ],
    # 'three' : [ drawKurrentKanzleiGatC_three, [temp_x, temp_y], marginStr+60, marginStr ],
    # 'four' : [ drawKurrentKanzleiGatC_four, [temp_x, temp_y], marginStr, marginStr ],
    # 'five' : [ drawKurrentKanzleiGatC_five, [temp_x, temp_y], marginStr+122, marginStr-120 ],
    # 'six' : [ drawKurrentKanzleiGatC_six, [temp_x, temp_y], marginStr, marginStr ],
    # 'seven' : [ drawKurrentKanzleiGatC_seven, [temp_x, temp_y], marginStr, marginStr-90 ],
    # 'eight' : [ drawKurrentKanzleiGatC_eight, [temp_x, temp_y], marginStr, marginStr ],    
    # 'nine' : [ drawKurrentKanzleiGatC_nine, [temp_x, temp_y], marginStr, marginStr ],    
    
    }
    
    
    

drawFunctions_thinStroke = {

     'f' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f.isol' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f.iniA' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f.iniH' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f.midA' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f.midH' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f.fina' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],

     'f_f' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.isol' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.iniA' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.iniH' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.midA' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.midH' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
     'f_f.fina' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],

      'g' : [ drawKurrentKanzleiGatC_g_thinStroke, [temp_x, temp_y] ],     
      'g.isol' : [ drawKurrentKanzleiGatC_g_thinStroke, [temp_x, temp_y] ],
      'g.fina' : [ drawKurrentKanzleiGatC_g_thinStroke, [temp_x, temp_y] ],
      
      'h' : [ drawKurrentKanzleiGatC_j_thinStroke, [temp_x, temp_y] ],
      'h.isol' : [ drawKurrentKanzleiGatC_j_thinStroke, [temp_x, temp_y] ],
      'h.fina' : [ drawKurrentKanzleiGatC_j_thinStroke, [temp_x, temp_y] ],

      'j' : [ drawKurrentKanzleiGatC_j_thinStroke, [temp_x, temp_y] ],
      'j.isol' : [ drawKurrentKanzleiGatC_j_thinStroke, [temp_x, temp_y] ],
      'j.fina' : [ drawKurrentKanzleiGatC_j_thinStroke, [temp_x, temp_y] ],

      'p' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
      'q' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],

      'y' : [ drawKurrentKanzleiGatC_j_thinStroke, [temp_x, temp_y] ],
      'y.isol' : [ drawKurrentKanzleiGatC_j_thinStroke, [temp_x, temp_y] ],
      'y.fina' : [ drawKurrentKanzleiGatC_j_thinStroke, [temp_x, temp_y] ],

      'z' : [ drawKurrentKanzleiGatC_z_thinStroke, [temp_x, temp_y] ],
      'z.isol' : [ drawKurrentKanzleiGatC_z_thinStroke, [temp_x, temp_y] ],
      'z.fina' : [ drawKurrentKanzleiGatC_z_thinStroke, [temp_x, temp_y] ],

     'longs' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],

     'germandbls' : [ drawKurrentKanzleiGatC_germandbls_thinStroke, [temp_x, temp_y] ],

     'longs_longs' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],   
    
     'longs_t' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
         
     'f_f_t' : [ drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
    
     'quotesingle' : [ drawKurrentKanzleiGatC_quotesingle_thinstroke, [temp_x, temp_y] ],  



     'one' : [ drawKurrentKanzleiGatC_one_thinStroke, [temp_x, temp_y] ],
     'two' : [ drawKurrentKanzleiGatC_two_thinStroke, [temp_x, temp_y] ],
     'three' : [ drawKurrentKanzleiGatC_three_thinStroke, [temp_x, temp_y] ],
     'five' : [ drawKurrentKanzleiGatC_three_thinStroke, [temp_x, temp_y] ],
     'six' : [ drawKurrentKanzleiGatC_six_thinStroke, [temp_x, temp_y] ],
     'nine' : [ drawKurrentKanzleiGatC_nine_thinStroke, [temp_x, temp_y] ],

     'W' : [ drawKurrentKanzleiGatD_W_thinStroke, [temp_x, temp_y] ],
     'G' : [ drawKurrentKanzleiGatD_G_thinStroke, [temp_x, temp_y] ],
     'H' : [ drawKurrentKanzleiGatD_H_thinStroke, [temp_x, temp_y] ],

    }
    
    

    
for variant in ['isol', 'fina','iniA', 'iniH', 'midA', 'midH']: 
	drawFunctions_thinStroke['p.' + variant] = [drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y]]

for variant in ['isol', 'fina','iniA', 'iniH', 'midA', 'midH']: 
	drawFunctions_thinStroke['q.' + variant] = [drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y]]
     
for variant in ['isol', 'iniA', 'iniH', 'midA', 'midH', 'fina']: 
	drawFunctions_thinStroke['germandbls.' + variant] = [drawKurrentKanzleiGatC_germandbls_thinStroke, [temp_x, temp_y]]

for variant in ['isol', 'iniA', 'iniH', 'midA', 'midH', 'fina']: 
	drawFunctions_thinStroke['longs.' + variant] = [drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y]]

for variant in ['isol', 'iniA', 'iniH', 'midA', 'midH']: 
	drawFunctions_thinStroke['longs_longs.' + variant] = [drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y]]
     
for variant in ['isol','iniA', 'iniH', 'midA', 'midH' , 'fina']: 
	drawFunctions_thinStroke['longs_t.' + variant] = [drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y]]   
     
for variant in ['isol','iniA', 'iniH', 'midA', 'midH' , 'fina']: 
	drawFunctions_thinStroke['f_f_t.' + variant] = [drawKurrentKanzleiGatC_thinstroke_Straight, [temp_x, temp_y]]   
  
     
     
     
  
  
     
for key in drawFunctions:
    
    glyph = font[[key][0]]
    glyph.clear()
    
    foreground = glyph.getLayer("foreground")
    foreground.clear()
    background = glyph.getLayer("background")
    background.clear()
     
    function = drawFunctions[key][0]
    arguments = drawFunctions[key][1]

    output = function(*arguments)
	
    if key in drawFunctions_thinStroke:
        
        assert isinstance(output, tuple) and len(output)==2
        pass_from_thick_to_thin = output[1]
        output = output[0]
    
    # assert isinstance(output, glyphContext.GlyphBezierPath)
		
    writePathInGlyph(output, foreground)

    print("___",font[[key][0]])
    
    # ask for current status
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin   
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_before = margin_ts - margin_fg
    
    ### change value of margin
    glyph.leftMargin = drawFunctions[key][2]
    glyph.rightMargin = drawFunctions[key][3]
    
    ### ask inbetween 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_mid = margin_ts - margin_fg

    ### calculate value for movement of bg
    move_thinStroke = difference_before - difference_mid

    glyph.copyLayerToLayer('foreground', 'background')
 
   
    if key in drawFunctions_thinStroke:		
        thinstroke = glyph.getLayer("thinstroke")
        thinstroke.clear()

        thinfunction = drawFunctions_thinStroke[key][0]
        thinarguments = drawFunctions_thinStroke[key][1]
        thinoutput = thinfunction(*thinarguments, pass_from_thick=pass_from_thick_to_thin)
        
        writePathInGlyph(thinoutput, thinstroke)
        
    thinstroke = glyph.getLayer("thinstroke")
    thinstroke.translate((move_thinStroke, 0))
    
    ### ask final 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0 
        #print("(no thin line)")   
    difference_final = margin_ts - margin_fg
    
    foreground.clear()

    
    
    
    ### now draw the whole glphy with the magic tool
    ### first thick main stroke 
    guide_glyph = glyph.getLayer("background")

    w = A_A * penWidth 
    a = radians(90-alpha)
    h = penThickness

    if True:
        RectNibPen = RectNibPen_Just

    p = RectNibPen(
        glyphSet=CurrentFont(),
        angle=a,
        width=w,
        height=penThickness,
        trace=True
    )

    guide_glyph.draw(p)
    p.trace_path(glyph)


    ### now thin Stroke
    if key in drawFunctions_thinStroke:		

        guide_glyph = glyph.getLayer("thinstroke")
        w = penThickness 
        p = RectNibPen(
            glyphSet=CurrentFont(),
            angle=a,
            width=w,
            height=penThickness,
            trace=True
        )
        guide_glyph.draw(p)
        p.trace_path(glyph)

    ### keep a copy in background layer
    newLayerName = "original_math"
    glyph.layers[0].copyToLayer(newLayerName, clear=True)
    glyph.getLayer(newLayerName).leftMargin = glyph.layers[0].leftMargin
    glyph.getLayer(newLayerName).rightMargin = glyph.layers[0].rightMargin

    ### this will make it all one shape
    #glyph.removeOverlap(round=0)
    
    ### from robstevenson to remove extra points on curves
    ### function itself is in create_stuff.py
    #select_extra_points(glyph, remove=True)




    

    
    

    