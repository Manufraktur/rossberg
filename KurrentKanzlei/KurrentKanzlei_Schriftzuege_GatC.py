import importlib
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatC
import version_3.creation.Halbboegen_GatC

importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatC)
importlib.reload(version_3.creation.Halbboegen_GatC)

from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatC import *
from version_3.creation.Halbboegen_GatC import *
#from version_3.creation.arc_path import ArcPath as BezierPath
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m




# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 22
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(1)




# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 5

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)

# temporary to check height of numbers
#line((1*modul_width, 11*modul_height), (9*modul_width, 11*modul_height))



# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 8.5


#Helper
#Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, temp_x, temp_y)
#Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, temp_x, temp_y-0.25)





def drawGrundSchriftzug(x, y, instroke=False, outstroke=False):
    
    GrundSchriftzug = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y) 
    downstroke = drawGrundelementF(*Raute_a, 4.5)
            
    if instroke == True:
        instroke = drawInstroke(*Raute_a, 1)
        GrundSchriftzug += instroke
        
    if outstroke == True:
        outstroke = drawOutstroke(*downstroke.points[1], 1)
        GrundSchriftzug += outstroke
        
    GrundSchriftzug += downstroke    
    drawPath(GrundSchriftzug) 
    return GrundSchriftzug
       
# drawGrundSchriftzug(temp_x, temp_y, instroke=True, outstroke=True)





def drawConnection(x, y, position="btm"):
    
    Connection = BezierPath()
    
    if position == "btm":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-4)
        upstroke = drawGrundelementH(*Raute_a, 2)     
                
    if position == "top":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        upstroke = drawGrundelementH(*Raute_a, 2, "down")
    
    Connection += upstroke  
    drawPath(Connection)   
    return Connection

# drawConnection(temp_x, temp_y, "btm")






def drawConCurve_up(x, y):    # definiert für a und g etc.
    
    ConCurve_up = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
    drawRauteOrientKurTop(offset, offsetDir, x+3, y)
    
    instroke = drawInstroke(*Raute_c, 0.5, "down")
    curve_up = drawSchneckenzug(*instroke.points[-1], LOWER_E, 3, HSL_size=1, HSL_start=18.725, clockwise=False, inward=False)

    ConCurve_up += instroke + curve_up
    drawPath(ConCurve_up)
    return ConCurve_up

# drawConCurve_up(temp_x, temp_y)





    


def drawSchriftzug1(x,y, version="a"):
    
    Schriftzug1 = BezierPath() 
     
    if version == "a":       
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        stroke = drawInstroke(*Raute_a, 1)
        downstroke = drawGrundelementF(*Raute_a)
        
    if version == "b":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)
        downstroke = drawGrundelementF(*Raute_b)
        stroke = drawOutstroke(*Raute_d, 1)  
    
    Schriftzug1 += stroke + downstroke
    drawPath(Schriftzug1)  
    return Schriftzug1
   
#drawSchriftzug1 (temp_x, temp_y, "a")

    
    




def drawSchriftzug2(x,y):
    
    Schriftzug2 = BezierPath() 
     
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    Querstrich = drawGrundelementB(*Raute_a)    # Positionierung noch nicht ganz klar
                
    drawPath(Schriftzug2)
    return Schriftzug2

#drawSchriftzug2(temp_x, temp_y)








def drawSchriftzug3(x, y, version="a", instroke=True, outstroke=True):
    
    Schriftzug3 = BezierPath() 
            
    if version == "a":
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        downstroke = drawGrundelementC(*Raute_a)

        if instroke == True:
            instroke = drawInstroke(*Raute_a, 1)
            Schriftzug3 = instroke + downstroke 

        if instroke == False:
            Schriftzug3 = downstroke
            
            
    if version == "b":
       
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)
        downstroke = drawGrundelementC(*Raute_a)
        
        if outstroke == True:
            outstroke = drawOutstroke(*Raute_d, 1)
            Schriftzug3 = outstroke + downstroke   
        
        if outstroke == False:
            Schriftzug3 = downstroke
    
    drawPath(Schriftzug3)
    return Schriftzug3
    
#drawSchriftzug3(temp_x, temp_y, "a")       







def drawSchriftzug4(x, y, version="a", instroke=False, outstroke=False):
    
    Schriftzug4 = BezierPath() 
    
            
    if version == "a":
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        downstroke = drawSchriftteil8(*Raute_a)
        
        if instroke == True:
            instroke = drawInstroke(*Raute_a, 1)
            Schriftzug4 += instroke
               
        if outstroke == True:
            outstroke = drawOutstroke(*downstroke.points[-1], 1, "down")
            Schriftzug4 += outstroke
        
        
    if version == "b":
       
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)
        downstroke = drawSchriftteil7(*Raute_b)   
        
        if instroke == True:
            instroke = drawInstroke(*Raute_b, 1, "down")
            Schriftzug4 += instroke
        
        if outstroke == True:
            outstroke = drawOutstroke(*Raute_d, 1)
            Schriftzug4 += outstroke
            
             
    Schriftzug4 += downstroke
    
    drawPath(Schriftzug4)
    
    return Schriftzug4
    

# drawSchriftzug4(temp_x, temp_y, "a", instroke=True) 
# drawSchriftzug4(temp_x, temp_y, "b") 
    
    
    
    
    

def drawSchriftzug5(x, y, version="a", instroke=True, outstroke=True):
    
    Schriftzug5 = BezierPath() 
    
            
    if version == "a":
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        downstroke = drawSchriftteil2(*Raute_a)
        
        if instroke == True:
            instroke = drawInstroke(*Raute_a, 1)
            Schriftzug5 = instroke + downstroke
            
        if instroke == False:
            Schriftzug5 = downstroke
            
        
    if version == "b":
       
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)
        downstroke = drawSchriftteil1(*Raute_d)
            
        if outstroke == True:
            outstroke = drawOutstroke(*Raute_d, 1)   
            Schriftzug5 = downstroke + outstroke
            
        if outstroke == False:
            Schriftzug5 = downstroke 
    
        
    drawPath(Schriftzug5)
    
    return Schriftzug5
    

# drawSchriftzug5(temp_x, temp_y, "a", instroke=True) 
# drawSchriftzug5(temp_x, temp_y, "b", outstroke=False) 

     





def drawSchriftzug6(x, y, version="a"):
    
    Schriftzug6 = BezierPath() 
       
    if version == "a":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        Schwung = drawSchriftteil9(*Raute_d, direction="CW")
        
    if version == "b":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)
        Schwung = drawSchriftteil10(*Raute_a)
    
    
    Schriftzug6 = Schwung 
    drawPath(Schriftzug6)
    return Schriftzug6
    
# drawSchriftzug6(temp_x, temp_y, "b") 








def drawSchriftzug7(x, y, version="a", instrokeLen=0, outstrokeLen=1):
    
    Schriftzug7 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
 
    instroke = drawInstroke(*Raute_a, instrokeLen)



    if version == "a":
        
        Signatur = drawSchriftzug3(x, y, "a", instroke=False)
        downstroke = drawGrundelementF(*Raute_a, 4.5)
        outstroke = drawOutstroke(*downstroke.points[1], outstrokeLen)

        Schriftzug7 += Signatur + downstroke
        
        
    if version == "b":

        Signatur = drawSchriftzug5(x, y, "a", instroke=False)
        bend_top = drawSchriftteil5(*Raute_d, "1")
        downstroke = drawGrundelementF(*bend_top.points[-1], 2.73)
        outstroke = drawOutstroke(*downstroke.points[1], outstrokeLen)

        Schriftzug7 += Signatur + bend_top + downstroke


    if version == "c":

        Signatur = drawSchriftzug5(x, y, "a", instroke=False)
        bend_top = drawSchriftteil5(*Raute_d, "1")
        downstroke = drawGrundelementF(*bend_top.points[-1], 1.4)
        bend_btm = drawSchriftteil4(Raute_d[0], Raute_d[1]-(x_height-1)*modul_height, "1")
        outstroke = drawOutstroke(*bend_btm.points[0], outstrokeLen)

        Schriftzug7 += Signatur + bend_top + downstroke + bend_btm
    

    Schriftzug7 += instroke + outstroke 
    
    drawPath(Schriftzug7)
    
    return Schriftzug7


#drawSchriftzug7(temp_x, temp_y, "a", instrokeLen=0, outstrokeLen=0)
#drawSchriftzug7(temp_x, temp_y, "b", instrokeLen=1, outstrokeLen=1)
#drawSchriftzug7(temp_x, temp_y, "c", instrokeLen=1, outstrokeLen=1)






def drawSchriftzug8(x, y, version="a", instrokeLen=0, outstrokeLen=1):
    
    Schriftzug8 = BezierPath()
    
    Raute_a_top, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x, y)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)

    instroke = drawInstroke(*Raute_a_top, instrokeLen)    

    if version == "a":

        downstroke = drawGrundelementF(Raute_d[0], Raute_d[1] + 4.5*modul_height, 4.5)
        Signatur = drawGrundelementC(*Raute_a)
        
        Schriftzug8 += Signatur + downstroke
   
        
    if version == "b":

        downstroke = drawGrundelementF(Raute_d[0], Raute_d[1]+4.5*modul_height, 2.73)
        bend_btm = drawSchriftteil6(*Raute_a, "1")
        Signatur = drawSchriftzug5(x, y-3.5, "b", outstroke=False)

        Schriftzug8 += downstroke + bend_btm + Signatur


    if version == "c":

        bend_top = drawSchriftteil3(*Raute_a_top, "0.75")
        downstroke = drawGrundelementF(*bend_top.points[-1], 1.25)
        bend_btm = drawSchneckenzug(*downstroke.points[1], LOWER_B, 3, HSL_size=0.5, HSL_start=12, clockwise=True, inward=True)
        stroke_extension = drawOutstroke(*bend_btm.points[-1], 0.78, "down")

        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+1, y-3.5)
        Fuss = drawSchneckenzug(*Raute_d, UPPER_H, 3, HSL_size=4, HSL_start=21, clockwise=False, inward=True)

        Schriftzug8 += bend_top + downstroke + stroke_extension + bend_btm + Fuss
    

    outstroke = drawOutstroke(*Raute_d, outstrokeLen)    
    
    Schriftzug8 += instroke + outstroke
    drawPath(Schriftzug8)    
    return Schriftzug8


#drawSchriftzug8(temp_x, temp_y, "a", instrokeLen=0, outstrokeLen=1)
#drawSchriftzug8(temp_x, temp_y, "c", instrokeLen=0, outstrokeLen=1)








# # # # def drawSchriftzug9(x, y, version="a", instrokeLen=0, outstrokeLen=1):
    
# # # #     Schriftzug9 = BezierPath()
    
# # # #     Raute oben
# # # #     Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)

# # # #     instroke = drawInstroke(*Raute_a_top, instrokeLen)

# # # #     Raute unten
# # # #     Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
   
        
# # # #     if version == "a":

# # # #         downstroke = drawGrundelementF(Raute_d[0], Raute_d[1]+4.5*modul_height, 2.73)
# # # #         bend_btm = drawSchriftteil6(*Raute_a, "1")
# # # #         Signatur = drawSchriftzug5(x, y-3.5, "b", outstroke=False)

        
# # # #         Kurve von unten beginnend nach oben        
# # # #         HSL_size = 6               # Wert von Lisa: 11.127          # Wert von Petra mit connection: 4  oder 6
# # # #         HSL_start = 12.8         # Wert von Lisa: 7.2555/part     # Wert von Petra mit connection: 14.4    oder 12.8

# # # #         A1, A2 = line_A_vonH_u_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)
# # # #         B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
# # # #         C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
# # # #         D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
# # # #         E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)

# # # #         Schriftzug9.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_8, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_8, angle_7, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_6, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_6, angle_5, True))

# # # #         strokeContinuation = drawGrundelementA(*E2, 1)


# # # #         Raute oben Abstand definieren
# # # #         Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+3.5, y)
# # # #         Raute_a_top, Raute_b_top, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+2.5, y-0.5)

        
# # # #         #oberer Teil der Kurve
# # # #         HSL_size = 22.8
# # # #         HSL_start = 76

# # # #         E2 = Raute_a_top
# # # #         E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),  E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
# # # #         line(E1, E2)

# # # #         D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
# # # #         C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
# # # #         B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        

# # # #         #unterer Teil der Kurve
# # # #         HSL_size = 8
# # # #         HSL_start = 21.6
        
# # # #         B4 = B2
# # # #         B3 = B4[0] + HSL_start*part,     B4[1]
# # # #         line(B1, B2)
        
# # # #         A3, A4 = line_A_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size)
# # # #         H3, H4 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*2)
        
# # # #         Schriftzug9.arc(*drawKreisSeg(H3, HSL_start-HSL_size*2, angle_10, angle_9, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(A3, HSL_start-HSL_size, angle_9, angle_8, True))

# # # #         Schriftzug9.arc(*drawKreisSeg(B1, 7.6, angle_8, angle_7, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(C1, 30.4, angle_7, angle_6, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(D1, 53.2, angle_6, angle_5, True))
        
                
# # # #         Schriftzug9 += bend_btm + Signatur



# # # #     if version == "b":

# # # #         downstroke = drawGrundelementF(Raute_d[0], Raute_d[1] + 4.5*modul_height, 4.5)
        
        
# # # #         Kurve von unten beginnend nach oben        
# # # #         HSL_size = 6               # Wert von Lisa: 11.127          # Wert von Petra mit connection: 4  oder 6
# # # #         HSL_start = 12.8         # Wert von Lisa: 7.2555/part     # Wert von Petra mit connection: 14.4    oder 12.8

# # # #         A1, A2 = line_A_vonH_u_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)
# # # #         B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
# # # #         C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
# # # #         D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
# # # #         E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)

# # # #         Schriftzug9.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_8, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_8, angle_7, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_6, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_6, angle_5, True))
        
# # # #         strokeContinuation = drawGrundelementA(*E2, 1)
        
        
# # # #         Raute oben Abstand definieren
# # # #         drawRauteOrientKurTop(offset, offsetDir, x+3.5, y)
# # # #         Raute_a_top, Raute_b_top, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+2.5, y-0.5)
        
# # # #         #oberer Teil der Kurve
# # # #         HSL_size = 24
# # # #         HSL_start = 80      

# # # #         E2 = instroke_conElement.points[0]
# # # #         E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),  E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
# # # #         line(E1, E2)

# # # #         D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
# # # #         C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
# # # #         B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        
        
# # # #         #unterer Teil der Kurve
# # # #         HSL_size = 1.7
# # # #         HSL_start = 10
        
# # # #         B4 = B2
# # # #         B3 = B4[0] + HSL_start*part,     B4[1]
# # # #         line(B1, B2)
        
# # # #         A3, A4 = line_A_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size)
# # # #         H3, H4 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*2)
# # # #         G3, G4 = line_G_vonH_u_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size*3)

# # # #         Schriftzug9.arc(*drawKreisSeg(G3, HSL_start-HSL_size*3, angle_11, angle_10, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(H3, HSL_start-HSL_size*2, angle_10, angle_9, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(A3, HSL_start-HSL_size, angle_9, angle_8, True))

# # # #         Schriftzug9.arc(*drawKreisSeg(B1, 8, angle_8, angle_7, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(C1, 8+24, angle_7, angle_6, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(D1, 8+24+24, angle_6, angle_5, True))
    
    
# # # #     if version == "b-2":    # für v, w, p dazu gefügt zum probieren erstmal

# # # #         downstroke = drawGrundelementF(Raute_d[0], Raute_d[1] + 4.5*modul_height, 4.5)

        
# # # #         Kurve von unten beginnend nach oben        
# # # #         HSL_size = 4         # Wert von Lisa: 11.127          # Wert von Petra mit connection: 4  oder 6
# # # #         HSL_start = 9.1     # Wert von Lisa: 7.2555/part     # Wert von Petra mit connection: 14.4    oder 12.8

# # # #         H1, H2 = line_H_vonG_u_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)
# # # #         A1, A2 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
# # # #         B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*2)
# # # #         C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*3)
# # # #         D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*4)
# # # #         E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*5)

# # # #         Schriftzug9.arc(*drawKreisSeg(H1, HSL_start, angle_10, angle_9, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(A1, HSL_start+HSL_size, angle_9, angle_8, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(B1, HSL_start+HSL_size*2, angle_8, angle_7, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(C1, HSL_start+HSL_size*3, angle_7, angle_6, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(D1, HSL_start+HSL_size*4, angle_6, angle_5, True))
        
# # # #         strokeContinuation = drawGrundelementA(*E2, 0.75)
        
        
# # # #         Raute oben Abstand definieren
# # # #         drawRauteOrientKurTop(offset, offsetDir, x+3, y)
# # # #         Raute_a_top, Raute_b_top, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+2, y-0.5)
        

# # # #     if version == "b-3":    # für r und x dazu gefügt 

# # # #         downstroke = drawGrundelementF(Raute_d[0], Raute_d[1] + 4.5*modul_height, 4.5)

        
# # # #         Kurve von unten beginnend nach oben        
# # # #         HSL_size = 6     
# # # #         HSL_start = 7.58   

# # # #         H1, H2 = line_H_vonG_u_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)
# # # #         A1, A2 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
# # # #         B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*2)
# # # #         C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*3)
# # # #         D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*4)
# # # #         E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*5)

# # # #         Schriftzug9.arc(*drawKreisSeg(H1, HSL_start, angle_10, angle_9, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(A1, HSL_start+HSL_size, angle_9, angle_8, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(B1, HSL_start+HSL_size*2, angle_8, angle_7, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(C1, HSL_start+HSL_size*3, angle_7, angle_6, True))
# # # #         Schriftzug9.arc(*drawKreisSeg(D1, HSL_start+HSL_size*4, angle_6, angle_5, True))
        
# # # #         strokeContinuation = drawGrundelementA(*E2, 0.2)
        
        
# # # #         Raute oben Abstand definieren
# # # #         drawRauteOrientKurTop(offset, offsetDir, x+2.5, y)
# # # #         Raute_a_top, Raute_b_top, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+1.5, y-0.5)
             
        
        
    
# # # #     outstroke = drawInstroke(*Raute_b_top, outstrokeLen)

# # # #     Schriftzug9 += instroke + downstroke + strokeContinuation + outstroke

# # # #     drawPath(Schriftzug9)
    
# # # #     return Schriftzug9


# # # # drawSchriftzug9(temp_x, temp_y, "a", 1, 1) 
# # # # drawSchriftzug9(temp_x, temp_y, "b", 0, 1) 
# # # # drawSchriftzug9(temp_x, temp_y, "b-2", 0, 1) 
# # # # drawSchriftzug9(temp_x, temp_y, "b-3", 0, 0.75) 










def drawSchriftzug9(x, y, version="a", instrokeLen=0, outstrokeLen=0):
    
    Schriftzug9 = BezierPath()
    
    Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)

    instroke = drawInstroke(*Raute_a_top, instrokeLen)


    if version == "a":
        Abstand = 2.5
        downstroke = drawGrundelementF(*Raute_a_top, 2.73)
        bend_btm = drawSchriftteil6(*Raute_a, "1")
        Signatur = drawSchriftzug5(x, y-3.5, "b", outstroke=False)
        con = drawSchneckenzug(*Raute_d, LOWER_H, 5, HSL_size=2.765, HSL_start=5, clockwise=True, inward=False, HSL_size_multipliers=[ 1+.75 * i      for  i in range(0, 6)]) 
        strokeContinuation = drawGrundelementA(*con.points[-1], 1)

        Schriftzug9 += bend_btm + Signatur


    if version == "b":
        Abstand = 2.5
        downstroke = drawGrundelementF(*Raute_a_top, 4.5)
        con_btm = drawSchneckenzug(*Raute_d, LOWER_G, 4, HSL_size=0.75, HSL_start=9.5, clockwise=True, inward=True)
        con_top = drawSchneckenzug(*con_btm.points[-1], UPPER_C, 2, HSL_size=24, HSL_start=29, clockwise=True, inward=False)
        strokeContinuation = drawGrundelementA(*con_top.points[-1], 1.35)
        con = con_btm + con_top

    
    if version == "b-2":    # für v, w, p dazu gefügt zum probieren erstmal
        Abstand = 2
        downstroke = drawGrundelementF(*Raute_a_top, 4.5)
        con_btm = drawSchneckenzug(*Raute_d, LOWER_G, 4, HSL_size=0.325, HSL_start=9.55, clockwise=True, inward=True)
        con_top = drawSchneckenzug(*con_btm.points[-1], UPPER_C, 2, HSL_size=24, HSL_start=29, clockwise=True, inward=False)
        strokeContinuation = drawGrundelementA(*con_top.points[-1], 0.89)
        con = con_btm + con_top


    if version == "b-3":    # für r und x dazu gefügt 
        Abstand = 1.5
        downstroke = drawGrundelementF(*Raute_a_top, 4.5)
        con_btm = drawSchneckenzug(*Raute_d, LOWER_G, 3, HSL_size=0.5, HSL_start=10, clockwise=True, inward=True)
        con_top = drawSchneckenzug(*con_btm.points[-1], UPPER_B, 3, HSL_size=16, HSL_start=13.5, clockwise=True, inward=False)
        strokeContinuation = drawGrundelementA(*con_top.points[-1], 0.5825)
        con = con_btm + con_top    
        
        
    #Raute oben Abstand definieren
    drawRauteOrientKurTop(offset, offsetDir, x+Abstand+1, y)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+Abstand, y-0.5)
    
    outstroke = drawOutstroke(*Raute_b, outstrokeLen)

    Schriftzug9 += instroke + downstroke + con + strokeContinuation + outstroke

    drawPath(Schriftzug9)
    return Schriftzug9


#drawSchriftzug9(temp_x, temp_y, "a") 
#drawSchriftzug9(temp_x, temp_y, "b") 
#drawSchriftzug9(temp_x, temp_y, "b-2", 1, 0) 
#drawSchriftzug9(temp_x, temp_y, "b-3", 1, 0) 







    
def drawSchriftzug10(x, y, version="a", instrokeLen=0, outstrokeLen=0):
    
    Schriftzug10 = BezierPath()
    
    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    top = drawSchneckenzug(*Raute_a, UPPER_G, 4, HSL_size=.4, HSL_start=10, clockwise=True, inward=False)
    end = top.points[-1]
    
    instroke = drawInstroke(*top.points[0], instrokeLen)
 
    _, _, _, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+1, y-3.5)
    btm = drawSchneckenzug(*Raute_d, LOWER_G, 4, HSL_size=.4, HSL_start=10, clockwise=True, inward=False)
    btm.reverse()
    Schriftzug10.line(end, btm.points[0])
        
    outstroke = drawOutstroke(*btm.points[-1], outstrokeLen)
    
    Schriftzug10 += instroke + top + btm + outstroke

    drawPath(Schriftzug10)
    return Schriftzug10

#drawSchriftzug10(temp_x, temp_y)






def drawSchriftzug11(x, y, instroke=False):
    
    Schriftzug11 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    downstroke = drawGrundSchriftzug(x, y)
    

    
    ### left side up
    
    HSL_size = 12
    HSL_start = 52

    B2 = Raute_a
    B1 = B2[0] + (part*HSL_start),     B2[1]
    #line(B1, B2)

    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug11.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_8, angle_7, True))

    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug11.arc(*drawKreisSeg(D1, HSL_start-HSL_size*2, angle_7, angle_6, True))

    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug11.arc(*drawKreisSeg(E1, HSL_start-HSL_size*3, angle_6, angle_5, True))

    # Diesen Wert festhalten für Schnittpunkt später
    E4 = E2

    
    ### right side up
    
    HSL_size = 21.2      # 2 1/5 BM
    HSL_start = 38.5

    E2 = Raute_a
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start), E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    line(E1, E2)

    C1 = E1
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    line(C1, C2)

    B1 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size) 
    B2 = B1[0] + (part*(HSL_start-HSL_size)),  B1[1]
    line(B1, B2)
    
    
    ### Wendung oben
    
    CP = E4[0]+4.1, E4[1]+2
    #text("CP", CP) 
    Schriftzug11.moveTo(E4)
    Schriftzug11.curveTo(CP, B2)
    
    Schriftzug11.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_16, angle_15, True))
    Schriftzug11.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_13, True))


    ### wollte ich eigentlich schöner machen aber dann hat es nicht funktioniert

    '''
    HSL_size =  0      #21.2    # 2 1/5 BM
    HSL_start = 50
    
    E2 = Raute_a
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start), E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    #line(E1, E2)

    D1, D2 = line_D_vonE_u_kl(E1, angle_seg_1, epsilon, part, HSL_size, HSL_start-HSL_size)
    Schriftzug11.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_13, angle_14))

    C1, C2 = line_C_vonD_u_kl(D1, epsilon, angle_seg_4, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug11.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_14, angle_15))

    #B1, B2 = line_B_vonC_u_kl(C1, angle_seg_4, part, HSL_size, HSL_start-HSL_size*3)
    #Schriftzug11.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_15, angle_16))
  

    #Schriftzug11 += con_stroke_in + con_stroke_out
    '''

    Schriftzug11 += downstroke
    
    drawPath(Schriftzug11)
    
    return Schriftzug11 
 
   
#drawSchriftzug11(temp_x, temp_y)






def drawSchriftzug12(x, y, instrokeLen=0.5, outstrokeLen=0):
    
    Schriftzug12 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    stroke_xheight = drawGrundSchriftzug(x, y)
    
    HSL_size = 2  
    HSL_start = 17.5

    B2 = Raute_a
    B1 = B2[0] + (part*HSL_start),     B2[1]
    #line(B1, B2)
    
    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug12.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_8, angle_7, True))

    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug12.arc(*drawKreisSeg(D1, HSL_start-HSL_size*2, angle_7, angle_6, True))

    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug12.arc(*drawKreisSeg(E1, HSL_start-HSL_size*3, angle_6, angle_5, True))
    
    
    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(*stroke_xheight.points[1], outstrokeLen)

    Schriftzug12 += instroke + stroke_xheight + outstroke
    drawPath(Schriftzug12)
    return Schriftzug12 
 
   
#drawSchriftzug12(temp_x, temp_y)





def drawSchriftzug13(x, y, instrokeLen=1.5, version="standard"):
    
    Schriftzug13 = BezierPath()

    # Raute auf x-Höhe
    drawRauteOrientKurTop(offset, offsetDir, x, y)
    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x, y+0.25)

    downstroke = drawGrundelementF(*Raute_a, 4.75)
    
    if version == "standard":
        bend = drawSchneckenzug(*Raute_a, UPPER_B, 3, HSL_size=4, HSL_start=31, clockwise=True, inward=True)
    
    if version == "short":
        bend = drawSchneckenzug(*Raute_a, UPPER_B, 3, HSL_size=1, HSL_start=22, clockwise=True, inward=True)
    
    
    instroke = drawOutstroke(*bend.points[-1], instrokeLen)

    Schriftzug13 += downstroke + instroke + bend
    drawPath(Schriftzug13)
    return Schriftzug13 
   
#drawSchriftzug13(temp_x, temp_y, instrokeLen=1.5, version="standard")
# drawSchriftzug13(temp_x, temp_y, instrokeLen=1, version="short")




def drawSchriftzug_germandbls(x, y):
    
    Schriftzug_germandbls = BezierPath()

    #drawRauteOrientKurTop(offset, offsetDir, x, y)
    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x+3.15, y+4)
    bend_top_left = drawSchneckenzug(*Raute_a, UPPER_E, 3, HSL_size=1, HSL_start=6, clockwise=True, inward=False)
    bend_top_right = drawSchneckenzug(*bend_top_left.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=7.75, clockwise=False, inward=False)

    straight = drawGrundelementA(*bend_top_right.points[-1], 1.25, "down")
    
    s_top = drawSchneckenzug(*straight.points[-1], UPPER_E, 5, HSL_size=1, HSL_start=10, clockwise=False, inward=False)
    s_btm = drawSchneckenzug(*s_top.points[-1], UPPER_H, 5, HSL_size=1, HSL_start=23, clockwise=True, inward=True)
    
    Schriftzug_germandbls += bend_top_left + bend_top_right + straight + s_top + s_btm 
    drawPath(Schriftzug_germandbls)
    return Schriftzug_germandbls 
   
#drawSchriftzug_germandbls(temp_x, temp_y)





def drawSchriftzug14(x, y, instroke=False):
    
    Schriftzug14 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)

    Signatur = drawGrundelementC(*Raute_a)


    # Raute unten 
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
    
    downstroke = drawGrundSchriftzug(x, y)
    

    
    ### right side down
    
    HSL_size = 12
    HSL_start = 52

    B2 = Raute_d
    B1 = B2[0] - (part*HSL_start),     B2[1]
    line(B1, B2)

    C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug14.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_16, angle_15, True))

    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug14.arc(*drawKreisSeg(D1, HSL_start-HSL_size*2, angle_15, angle_14, True))

    E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug14.arc(*drawKreisSeg(E1, HSL_start-HSL_size*3, angle_14, angle_13, True))

    # Diesen Wert festhalten für Schnittpunkt später
    E4 = E2

    
    ### left side up/down
    
    HSL_size = 21.2      # 2 1/5 BM
    HSL_start = 38.5

    E2 = Raute_d
    E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start), E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    line(E1, E2)

    C1 = E1
    C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    line(C1, C2)

    B1 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_size) 
    B2 = B1[0] - (part*(HSL_start-HSL_size)),  B1[1]
    line(B1, B2)
    
    
    ### Wendung unten
    
    CP = E4[0]-4.1,  E4[1]-1.5
    #text("CP", CP) 
    Schriftzug14.moveTo(E4)
    Schriftzug14.curveTo(CP, B2)
    
    Schriftzug14.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_8, angle_7, True))
    Schriftzug14.arc(*drawKreisSeg(C1, HSL_start, angle_7, angle_5, True))

    Schriftzug14 += Signatur + downstroke

    drawPath(Schriftzug14)
    
    return Schriftzug14 
 
   
#drawSchriftzug14(temp_x, temp_y)





def drawSchriftzug15(x, y, instroke=False):
    
    Schriftzug15 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    downstroke = drawSchriftzug8(x, y, "b")
    

    
    ### left side up
    
    HSL_size = 12
    HSL_start = 52

    B2 = Raute_a
    B1 = B2[0] + (part*HSL_start),     B2[1]
    #line(B1, B2)

    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug15.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_8, angle_7, True))

    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug15.arc(*drawKreisSeg(D1, HSL_start-HSL_size*2, angle_7, angle_6, True))

    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug15.arc(*drawKreisSeg(E1, HSL_start-HSL_size*3, angle_6, angle_5, True))

    # Diesen Wert festhalten für Schnittpunkt später
    E4 = E2

    
    ### right side up
    
    HSL_size = 21.2      # 2 1/5 BM
    HSL_start = 38.5

    E2 = Raute_a
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start), E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    line(E1, E2)

    C1 = E1
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    line(C1, C2)

    B1 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size) 
    B2 = B1[0] + (part*(HSL_start-HSL_size)),  B1[1]
    line(B1, B2)
    
    
    ### Wendung oben
    
    CP = E4[0]+4.1, E4[1]+2
    #text("CP", CP) 
    Schriftzug15.moveTo(E4)
    Schriftzug15.curveTo(CP, B2)
    
    Schriftzug15.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_16, angle_15, True))
    Schriftzug15.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_13, True))


    
    Schriftzug15 += downstroke
    
    drawPath(Schriftzug15)
    
    return Schriftzug15 
 
   
#drawSchriftzug15(temp_x, temp_y)






def drawSchriftzug16(x, y, instroke=False):
    
    Schriftzug16 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    StartSchleife = Raute_a        # Wert für später aufheben
    
    drawGrundelOrient(A1, A2, offset, x, y+1)
    drawGrundelOrient(A1, A2, offset, x, y+2)
    drawGrundelOrient(A1, A2, offset, x, y+3)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+3.5)
    
    text("Grund_b", Grund_b)
    
    
    ### left side down
    
    HSL_size = 14
    HSL_start = 28

    E2 = Grund_b
    E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start) ,   E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    line(E1, E2)

    C1 = E1
    C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    line(C1, C2)

    
    StartPunkt = Raute_a[0]+offset[0]/2,  Raute_a[1]-offset[1]/2
    text("StartPunkt", StartPunkt)
    
    
    Schnittpunkt = StartPunkt[0]+offset[0]*3.5,  StartPunkt[1]+offset[1]*3.5
    text("Schnittpunkt", Schnittpunkt)
    line(StartPunkt, Schnittpunkt)
    
    Endpunkt = Schnittpunkt[0]-23.47, Schnittpunkt[1]
    
    text("Endpunkt", Endpunkt) 

    
    Schriftzug16.arc(*drawKreisSeg(C1, HSL_start, angle_5, angle_7))

    # Verbindungsstück
    CP = C2[0]-1.1,  C2[1]-2.2
    text("CP", CP) 
    Schriftzug16.moveTo(C2)
    Schriftzug16.curveTo(CP, Endpunkt)
    

    Schriftzug16.arc(*drawKreisSeg(Schnittpunkt, HSL_start, angle_8, angle_9))


    # Bogen zwischen x-Höhe und Baseline
    HSL_size = 2
    HSL_start = 22.75
    
    A4 = StartPunkt
    A3 = A4[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      A4[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    line(A3, A4)
    
    B1, B2 = line_B_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start- HSL_size) 
    C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start- HSL_size*2) 
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start- HSL_size*3)
    E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start- HSL_size*4)
    
    
    # Rauten unten rechts
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y-4)


    Schriftzug16.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_1, angle_0, True))
    Schriftzug16.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_16, angle_15, True))
    Schriftzug16.arc(*drawKreisSeg(D1, HSL_start-HSL_size*3, angle_15, angle_14, True))
    Schriftzug16.arc(*drawKreisSeg(E1, HSL_start-HSL_size*4, angle_14, angle_13, True))

    
    
    # Signatur unten (Fuß)
    HSL_size = 4
    HSL_start = 21
    
    H2 = Raute_d
    H1 = H2[0] - m.sin(m.radians(90-epsilon)) * (part*HSL_start),    H2[1] - m.cos(m.radians(90-epsilon)) * (part*HSL_start)
    line(H1, H2)
    
    G1, G2 = line_G_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size)
    F1, F2 = line_F_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    E1, E2 = line_E_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    
    # Raute unten links
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x-1, y-4)

    Schriftzug16.arc(*drawKreisSeg(E1, HSL_start-HSL_size*3, angle_5, angle_4, True)) 
    Schriftzug16.arc(*drawKreisSeg(F1, HSL_start-HSL_size*2, angle_4, angle_3, True))
    Schriftzug16.arc(*drawKreisSeg(G1, HSL_start-HSL_size, angle_3, angle_2, True))

    

    ### right side 
    
    HSL_size = 21.2      # 2 1/5 BM
    HSL_start = 38.5

    E2 = StartSchleife
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start), E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    line(E1, E2)

    C1 = E1
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    line(C1, C2)

    B1 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size) 
    B2 = B1[0] + (part*(HSL_start-HSL_size)),  B1[1]
    line(B1, B2)
    
    
    ### Wendung oben
    
    CP = Grund_b[0]+3.9, Grund_b[1]+1.8
    #text("CP", CP) 
    Schriftzug16.moveTo(Grund_b)
    Schriftzug16.curveTo(CP, B2)
    
    Schriftzug16.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_16, angle_15, True))
    Schriftzug16.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_13, True))


    
    #Schriftzug16 += downstroke
    
    drawPath(Schriftzug16)
    
    return Schriftzug16 
 
   
#drawSchriftzug16(temp_x, temp_y)










def drawSchriftzug17(x, y, version="standard", instroke=False):
    
    Schriftzug17 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    StartSchleife = Raute_a
    
    drawGrundelOrient(A1, A2, offset, x, y+1)
    drawGrundelOrient(A1, A2, offset, x, y+2)
    drawGrundelOrient(A1, A2, offset, x, y+3)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+3.5)
    #text("Grund_b", Grund_b)
    
    ### left side down
    HSL_size = 14
    HSL_start = 28
    
    E1, E2 = line_E_vonF_o_gr_s(Grund_b, *angles, part, HSL_size, HSL_start)

    C1 = E1
    C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    line(C1, C2)

    StartPunkt = Raute_a[0]+offset[0]/2,  Raute_a[1]-offset[1]/2
    #text("StartPunkt", StartPunkt)
    Schnittpunkt = StartPunkt[0]+offset[0]*3.5,  StartPunkt[1]+offset[1]*3.5
    #text("Schnittpunkt", Schnittpunkt)    
    Endpunkt = Schnittpunkt[0]-23.47, Schnittpunkt[1]
    #text("Endpunkt", Endpunkt) 

    Schriftzug17.arc(*drawKreisSeg(C1, HSL_start, angle_5, angle_7))

    # Verbindungsstück
    CP = C2[0]-1.1,  C2[1]-2.2
    #text("CP", CP) 
    Schriftzug17.moveTo(C2)
    Schriftzug17.curveTo(CP, Endpunkt)
    
    Schriftzug17.arc(*drawKreisSeg(Schnittpunkt, HSL_start, angle_8, angle_9))


    ### right side 
    if version == "standard":

        HSL_size = 21.2      # 2 1/5 BM
        HSL_start = 38.5 

        E1, E2 = line_E_vonF_u_gr_s(StartSchleife, *angles, part, HSL_size, HSL_start)

        C1 = E1
        C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
        line(C1, C2)

        B1 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size) 
        B2 = B1[0] + (part*(HSL_start-HSL_size)),  B1[1]
        line(B1, B2)    
        
        ### Wendung oben
        CP = Grund_b[0]+3.9, Grund_b[1]+1.8
        #text("CP", CP) 
        Schriftzug17.moveTo(Grund_b)
        Schriftzug17.curveTo(CP, B2)
    
        Schriftzug17.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_16, angle_15, True))
        Schriftzug17.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_13, True))

    
    
    if version == "con":  # für connected

        HSL_size = 20      # 2 1/5 BM
        HSL_start = 55 

        instroke = drawConstroke(*StartSchleife, "H", 1.5)
        
        D1, D2 = line_D_vonE_u_kl_s(instroke.points[1], *angles, part, HSL_size, HSL_start)
        C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size)
        B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    
        ### Wendung oben
        CP = Grund_b[0]+3.9, Grund_b[1]+1.8
        #text("CP", CP) 
        Schriftzug17.moveTo(Grund_b)
        Schriftzug17.curveTo(CP, B2)
    
        Schriftzug17.arc(*drawKreisSeg(B1, HSL_start-HSL_size*2, angle_16, angle_15, True))
        Schriftzug17.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_15, angle_14, True))
        
        Schriftzug17 += instroke


    ### x-height curve
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+1, y-3.5)
    xheight_top = drawSchneckenzug(*StartPunkt, UPPER_A, 2, HSL_size=2, HSL_start=13.5, clockwise=True, inward=False)
    xheight_btm = drawSchneckenzug(*Raute_d, LOWER_G, 4, HSL_size=1, HSL_start=10, clockwise=True, inward=False)
    Schriftzug17.line(xheight_top.points[-1], xheight_btm.points[-1])
    Schriftzug17 += xheight_top + xheight_btm
    drawPath(Schriftzug17)
    return Schriftzug17 

   
#drawSchriftzug17(temp_x, temp_y, version="con")
#drawSchriftzug17(temp_x, temp_y, version="standard")







def drawSchriftzug18(x, y, instroke=False):
    
    Schriftzug18 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        
    drawGrundelOrient(A1, A2, offset, x, y+1)
    drawGrundelOrient(A1, A2, offset, x, y+2)
    drawGrundelOrient(A1, A2, offset, x, y+3)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+3.5)
    
    StartWendung = Grund_b
    text("Grund_b", Grund_b)
    
    
    ### left side down
    
    HSL_size = 6.5
    HSL_start = 40
    
    E1, E2 = line_E_vonF_o_kl_s(Grund_b, *angles, part, HSL_size, HSL_start)  
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    
    
    StartPunkt = Raute_a
    text("StartPunkt", StartPunkt)
    
    
    # Schnittpunkt = StartPunkt[0]+offset[0]*3.5,  StartPunkt[1]+offset[1]*3.5
    # text("Schnittpunkt", Schnittpunkt)
    # line(StartPunkt, Schnittpunkt)
    
    
    Schriftzug18.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    Schriftzug18.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    Schriftzug18.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    Schriftzug18.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_8, angle_9))




    # Bogen zwischen x-Höhe und Baseline
    HSL_size = 2
    HSL_start = 17.6
    
    A3, A4 = line_A_vonH_o_gr_s(StartPunkt, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+ HSL_size)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+ HSL_size*2)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+ HSL_size*3)
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+ HSL_size*4)
    
    
    # Rauten unten rechts
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)


    Schriftzug18.arc(*drawKreisSeg(A3, HSL_start, angle_1, angle_0, True))
    Schriftzug18.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    Schriftzug18.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
    Schriftzug18.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))

    
    
    # Signatur unten (Fuß)
    HSL_size = 4
    HSL_start = 6.8
        
    A5, A6 = line_A_vonB_o_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)

    
    # Raute unten links
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x-0.5, y-3.25)

    Schriftzug18.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))  # hier mit dem Winkel geschummelt
    Schriftzug18.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftzug18.arc(*drawKreisSeg(A5, HSL_start, angle_2, angle_1, True))

    
    
    ### right side 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1.5, y+1.75)

    StartSchleife = Grund_c        # Wert für später aufheben

    text("StartSchleife", StartSchleife)
    
    
    HSL_size = 2
    HSL_start = 19.6

    E1, E2 = line_E_vonF_u_kl_s(StartSchleife, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    
    ### neuer Pfad anfangen
    Schriftzug18_SchleifeOffen = BezierPath()
    
    Schriftzug18_SchleifeOffen.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_13, angle_14))
    Schriftzug18_SchleifeOffen.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_14, angle_15))
    Schriftzug18_SchleifeOffen.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_15, angle_16))

    
    ### Wendung oben
    HSL_size = 1
    HSL_start = 11
    
    E1, E2 = line_E_vonD_o_kl_s(StartWendung, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    G1, G2 = line_G_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A7, A8 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    B1, B2 = line_B_vonA_u_kl(A7, *angles, part, HSL_size, HSL_start-HSL_size*5)
    
    Schriftzug18_SchleifeOffen.arc(*drawKreisSeg(B1, HSL_start-HSL_size*5, angle_0, angle_1))
    Schriftzug18_SchleifeOffen.arc(*drawKreisSeg(A7, HSL_start-HSL_size*4, angle_1, angle_2))
    Schriftzug18_SchleifeOffen.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_2, angle_3))
    Schriftzug18_SchleifeOffen.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_3, angle_4))
    Schriftzug18_SchleifeOffen.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_4, angle_5))

    
    
    Schriftzug18 += Schriftzug18_SchleifeOffen
    
    drawPath(Schriftzug18)
    
    return Schriftzug18 
 
   
#drawSchriftzug18(temp_x, temp_y)








def drawSchriftzug19(x, y, version="a", outstrokeLen=0.5):
    
    Schriftzug19 = BezierPath()

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x+1, y+2)


    if version == "a":
        Bogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=3, HSL_start=19.3, clockwise=False, inward=True)
        Bogen_unten = drawSchneckenzug(*Bogen_oben.points[-1], UPPER_A, 4, HSL_size=2, HSL_start=17.6, clockwise=True, inward=False)

        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x-0.5, y-3.25)
        _, _, _, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
        Fuss = drawSchneckenzug(*Raute_a, UPPER_F, 3, HSL_size=4, HSL_start=19, clockwise=True, inward=True)
        
        Schriftzug19 += Fuss


    if version == "b":
        Bogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1, HSL_start=13.1, clockwise=False, inward=True)
        Bogen_mitte = drawSchneckenzug(*Bogen_oben.points[-1], UPPER_H, 3, HSL_size=2, HSL_start=7.2, clockwise=True, inward=False)

        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+1, y-3.5)
        Bogen_unten = drawSchneckenzug(*Raute_d, LOWER_G, 4, HSL_size=1, HSL_start=10, clockwise=True, inward=False)
        
        Einsatz = drawGrundelementG(*Bogen_mitte.points[-1], 0.55, "down")
        
        Schriftzug19 += Bogen_mitte + Einsatz

  
    outstroke = drawOutstroke(*Raute_d, outstrokeLen)

    Schriftzug19 += Bogen_oben + Bogen_unten + outstroke
    drawPath(Schriftzug19)
    return Schriftzug19 
 
   
#drawSchriftzug19(temp_x, temp_y, "a", outstrokeLen=1)
#drawSchriftzug19(temp_x, temp_y, "b", outstrokeLen=0.5)





temp_x= 5


def drawSchriftzug20_descender(x, y, version="fina", outstrokeLen=0):
    
    Schriftzug20_descender = BezierPath()


    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    # Raute zur Orientierung
    drawRauteOrientKurTop(offset, offsetDir, x, y-7.75)
    # Hilfe für Abstand zum Hauptstrich und Pos Ausstrich
    drawRauteOrientKurTop(offset, offsetDir, x-3.5, y-4.5)
    drawRauteOrientKurTop(offset, offsetDir, x, y-4)

    
    Bogen_rechts_oben = drawSchneckenzug(*Raute_a, UPPER_G, 3, HSL_size=6, HSL_start=8.5, clockwise=True, inward=False)
    Einsatz = drawGrundelementF(*Bogen_rechts_oben.points[-1], 0.975)


    if version == "fina":
        Bogen_rechts_unten = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=10, HSL_start=37.5, clockwise=True, inward=False)
        
        Schriftzug20_descender += Bogen_rechts_unten
        

    

    if version == "loop_A": 
        
        right_down = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=10, HSL_start=37.5, clockwise=True, inward=False)
        Wendung_unten = drawSchneckenzug(*right_down.points[-1], LOWER_E, 5, HSL_size=0.5, HSL_start=4, clockwise=True, inward=False)

        left_up = drawSchneckenzug(*Wendung_unten.points[-1], UPPER_B, 3, HSL_size=8.75, HSL_start=18.5, clockwise=True, inward=False)

        outstroke = drawConstroke(*left_up.points[-1],"A", 4.55, deviation=1)
    
        Schriftzug20_descender += right_down + Wendung_unten + left_up + outstroke
        
        
        
        
    if version == "loop_H": 
        
        
        right_down = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=28, clockwise=True, inward=False)
        Wendung_unten = drawSchneckenzug(*right_down.points[-1], LOWER_E, 5, HSL_size=0.5, HSL_start=4, clockwise=True, inward=False)
        # Hilfe Pos Ausstrich
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.75, y)
        #drawRauteOrientKurTop(offset, offsetDir, x-3.5, y-4.5)
        #drawRauteOrientKurTop(offset, offsetDir, x, y-4)
        
        left_up = drawSchneckenzug(*Wendung_unten.points[-1], UPPER_B, 2, HSL_size=4, HSL_start=10.25, clockwise=True, inward=False)
        
        ### Hier ist die Verbindung zu n nicht gemöäß einer der vorgegebenen Richtungen
        
        #outstroke =  drawGrundelementA(*left_up.points[-1], 4.81)
        #outstroke =  drawGrundelementH(*left_up.points[-1], 4.81)
        Schriftzug20_descender.line(left_up.points[-1], Raute_a)
        Schriftzug20_descender += right_down + Wendung_unten + left_up #+ outstroke
        
    Schriftzug20_descender += Bogen_rechts_oben + Einsatz 
    drawPath(Schriftzug20_descender)  
    return Schriftzug20_descender 
 
   
#drawSchriftzug20_descender(temp_x, temp_y, "fina")
#drawSchriftzug20_descender(temp_x, temp_y, "loop_A")
#drawSchriftzug20_descender(temp_x, temp_y, "loop_H")







def drawSchriftzug20(x, y, instroke=False):
    
    Schriftzug20 = BezierPath()
                
    backstroke = drawSchriftzug11(x, y)
    connection = drawSchriftzug9(x, y, "b")
    downstroke_right = drawSchriftzug20_descender(x+3.5, y)    #"fina"
    
    drawPath(Schriftzug20)
    return Schriftzug20 
 
#drawSchriftzug20(temp_x, temp_y)







def drawSchriftzug21(x, y, instrokeLen=1.175, outstrokeLen=0):
    
    Schriftzug21 = BezierPath()

    drawRauteOrientKurTop(offset, offsetDir, x+2, y)        
    drawRauteOrientKurBtm(offset, offsetDir, x, y-1.5)
    drawRauteOrientKurBtm(offset, offsetDir, x, y-2.5)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
 
    Bogen = drawSchneckenzug(*Raute_d, LOWER_G, 6, HSL_size=3, HSL_start=8.15, clockwise=True, inward=False)
    instroke = drawInstroke(*Bogen.points[-1], instrokeLen, "down")        
    outstroke = drawOutstroke(*Bogen.points[0], outstrokeLen)
    
    Schriftzug21 += instroke + Bogen + outstroke
    drawPath(Schriftzug21)
    return Schriftzug21 
 
#drawSchriftzug21(temp_x, temp_y) 







def drawSchriftzug22(x, y, instroke=True):
    
    Schriftzug22 = BezierPath()
    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)

    
    if instroke == True:
        instroke = drawInstroke(*Raute_a, 1)
            

    HSL_size = 1
    HSL_start = 8
        
    G2 = Raute_a     # x, y
    G1 = G2[0] - m.sin(m.radians(angle_seg_1))*(part*HSL_start),    G2[1] - m.cos(m.radians(angle_seg_1))*(part*HSL_start)
    line(G1, G2)
    
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*1)
    Schriftzug22.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_2, True))
    
    A1, A2 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug22.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_2, angle_1, True))
    
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug22.arc(*drawKreisSeg(A1, HSL_start+HSL_size*2, angle_1, angle_0, True))
    
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug22.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Schriftzug22.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Schriftzug22.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_14, angle_13, True))


    Schriftzug22 += instroke
    
    drawPath(Schriftzug22)
        
    return Schriftzug22
    
    
#drawSchriftzug22(temp_x, temp_y) 
    
    
    
    
    
    
    
    
def drawSchriftzug23(x, y):
    
    Schriftzug23 = BezierPath()

    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
 

    HSL_size = 0.58
    HSL_start = 0.8
        
    E1, E2 = line_E_vonF_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
        
    D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug23.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

    C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug23.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

    B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug23.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

    A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug23.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))

    H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Schriftzug23.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_9, angle_10))

    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Schriftzug23.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*7)
    Schriftzug23.arc(*drawKreisSeg(G1, HSL_start+HSL_size*6, angle_11, angle_12))

    x = F1[1]-F2[1]
    y = 3.6             # Höhenunterschied / Versatz nach oben. 
                        # Diesen Wert verstellen um Kurve zu beeinflussen.
   
    HSL_start = x+y

    
    E1 = F1[0],            F1[1] +(y)
    E2 = E1[0] + m.sin(m.radians(angle_seg_1)) * (HSL_start) , E1[1] - m.cos(m.radians(angle_seg_1)) * (HSL_start)
    #line(E1, E2)    # mit Absicht ausgeblendet weil irritierend
    
    
    Schriftzug23.arc(*drawKreisSeg(E1, HSL_start/part, angle_12, angle_13))
    
    
    drawPath(Schriftzug23)
    
    return Schriftzug23 
 
   
#drawSchriftzug23(temp_x, temp_y)







def drawSchriftzug24(x, y, version="a"):
    
    Schriftzug24 = BezierPath()


    if version == "a":
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x-0.5, y-3.25)
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)

        HSL_size = 0.66
        HSL_start = 1.9
                
        E1, E2 = line_E_vonF_u_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)
        
        D1, D2 = line_D_vonE_u_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug24.arc(*drawKreisSeg(E1, HSL_start, angle_13, angle_14))

        C1, C2 = line_C_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug24.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_14, angle_15))

        B1, B2 = line_B_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        Schriftzug24.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_16))

        A3, A4 = line_A_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        Schriftzug24.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_0, angle_1))

        H1, H2 = line_H_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
        Schriftzug24.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_1, angle_2))

        G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
        Schriftzug24.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_2, angle_3))

        F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*7)
        Schriftzug24.arc(*drawKreisSeg(G1, HSL_start+HSL_size*6, angle_3, angle_4))

        x = F2[1]-F1[1]
        y = 7.4              # Höhenunterschied / Versatz nach oben. 
                             # Diesen Wert verstellen um Kurve zu beeinflussen.
   
        HSL_start = x+y
    
        E1 = F1[0],            F1[1] -(y)
        E2 = E1[0] - m.sin(m.radians(angle_seg_1)) * (HSL_start) , E1[1] + m.cos(m.radians(angle_seg_1)) * (HSL_start)
        #line(E1, E2)    # ausgeblendet
        Schriftzug24.arc(*drawKreisSeg(E1, HSL_start/part, angle_4, angle_5))
        
        
        
        
    if version == "b":
                
        drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x-0.5, y-3.25)

        HSL_size = 0.66
        HSL_start = 1.9
        
        E1, E2 = line_E_vonF_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
        
        D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug24.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

        C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug24.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

        B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        Schriftzug24.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

        A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        Schriftzug24.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))

        H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
        Schriftzug24.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_9, angle_10))

        G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
        Schriftzug24.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))

        F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*7)
        Schriftzug24.arc(*drawKreisSeg(G1, HSL_start+HSL_size*6, angle_11, angle_12))

        x = F1[1]-F2[1]
        y = 7.4             # Höhenunterschied / Versatz nach oben. 
                            # Diesen Wert verstellen um Kurve zu beeinflussen.
   
        HSL_start = x+y

    
        E1 = F1[0],            F1[1] +(y)
        E2 = E1[0] + m.sin(m.radians(angle_seg_1)) * (HSL_start) , E1[1] - m.cos(m.radians(angle_seg_1)) * (HSL_start)
        #line(E1, E2)    # ausgeblendet
        Schriftzug24.arc(*drawKreisSeg(E1, HSL_start/part, angle_12, angle_13))
    
    
    drawPath(Schriftzug24)
    
    return Schriftzug24 
 
   
#drawSchriftzug24(temp_x, temp_y, "a")
# drawSchriftzug24(temp_x, temp_y, "b")













def drawSchriftzug25(x, y, instrokeLen=1, outstrokeLen=1):
    
    Schriftzug25 = BezierPath()

    drawRauteOrientKurBtm(offset, offsetDir, x, y-1.5)
    drawRauteOrientKurBtm(offset, offsetDir, x, y-2.5)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
 
    Bogen = drawSchneckenzug(*Raute_d, LOWER_G, 6, HSL_size=3, HSL_start=6.125, clockwise=True, inward=False) 
    Bogen.reverse()
    instroke = drawInstroke(*Bogen.points[0], instrokeLen, "down")
    outstroke = drawOutstroke(*Bogen.points[-1], outstrokeLen)

    Schriftzug25 += Bogen + instroke + outstroke
    drawPath(Schriftzug25)
    return Schriftzug25 
 
   
#drawSchriftzug25(temp_x, temp_y, instrokeLen=1, outstrokeLen=3)










def drawSchriftzug26(x, y, version="standard", instrokeLen=1, outstrokeLen=1):
    
    Schriftzug26 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    drawRauteOrientKurTop(offset, offsetDir, x, y-1)
    drawRauteOrientKurTop(offset, offsetDir, x, y-2)
    
    instroke = drawInstroke(*Raute_a, instrokeLen)
    
    if version == "standard":
        HSL_size = 1.54
        HSL_start = 9
        
    if version == "3.25":
        drawRauteOrientKurTop(offset, offsetDir, x, y-3)
        drawRauteOrientKurTop(offset, offsetDir, x-0.5, y-2.5)
        HSL_size = 1.8
        HSL_start = 9.5
        
    if version == "3.5":
        drawRauteOrientKurTop(offset, offsetDir, x-1, y-3)
        HSL_size = 2
        HSL_start = 10.1
        
    curve = drawSchneckenzug(*Raute_a, UPPER_G, 6, HSL_size=HSL_size, HSL_start=HSL_start, clockwise=True, inward=False)
    outstroke = drawOutstroke(*curve.points[-1], outstrokeLen, "down")
    
    Schriftzug26 += instroke + outstroke + curve
    drawPath(Schriftzug26)     
    return Schriftzug26
    
    
#drawSchriftzug26(temp_x, temp_y)    







def drawSchriftzug27(x, y):
    
    Schriftzug27 = BezierPath()
    
    y += 0.5
    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    Spitze = drawSchriftzug6(x, y, "a")

    HSL_size = 1.5
    HSL_start = 9
                
    G1, G2 = line_G_vonF_o_gr_s(Spitze.points[0], *angles, part, HSL_size, HSL_start)

    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug27.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_2, True))
    
    A1, A2 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug27.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_2, angle_1, True))
    
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug27.arc(*drawKreisSeg(A1, HSL_start+HSL_size*2, angle_1, angle_0, True))
    
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug27.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Schriftzug27.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Schriftzug27.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_14, angle_13, True))

    Schriftzug27 += Spitze        
    drawPath(Schriftzug27)        
    return Schriftzug27
      
#drawSchriftzug27(temp_x, temp_y)   












def drawSchriftzug28(x, y, instroke=False):
    
    Schriftzug28 = BezierPath()

    y += 0.5
    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    Spitze = drawSchriftzug6(x, y, "a")
    
    
    
        
    ### Kurve rechts oben
    HSL_size = 1
    HSL_start = 10
    
    G2 = Spitze.points[0]
    G1 = G2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),    G2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    line(G1, G2)
    
    H1, H2 = line_H_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size)
    A3, A4 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    
    Schriftzug28.arc(*drawKreisSeg(H1, HSL_start-HSL_size, angle_3, angle_2, True))
    Schriftzug28.arc(*drawKreisSeg(A3, HSL_start-HSL_size*2, angle_2, angle_1, True))
    
    
    
    ### Kurve rechts unten
    HSL_size = 2.4
    HSL_start = 23
    
    A6 = A4
    A5 = A6[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      A6[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    line(A5, A6)

    B1, B2 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    F1, F2 = line_F_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size*5)
    
    Schriftzug28.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_1, angle_0, True))
    Schriftzug28.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_16, angle_15, True))
    Schriftzug28.arc(*drawKreisSeg(D1, HSL_start-HSL_size*3, angle_15, angle_14, True))
    Schriftzug28.arc(*drawKreisSeg(E1, HSL_start-HSL_size*4, angle_14, angle_13, True))
    Schriftzug28.arc(*drawKreisSeg(F1, HSL_start-HSL_size*5, angle_13, angle_12, True))



    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+1, y-1.5)

 
    # Strich nach unten links
    HSL_size = 8
    HSL_start = 35.9
    
    E2 = Raute_a
    E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),  E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    line(E1, E2)

    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    B1_festgehalten = B1



    # Wendung
    HSL_size = 1
    HSL_start = 8
    
    B1 = B2[0] + (part*HSL_start),     B2[1]
    line(B1, B2)
    
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    G1, G2 = line_G_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*4)

    Schriftzug28.arc(*drawKreisSeg(F1, HSL_start-HSL_size*4, angle_12, angle_11, True))
    Schriftzug28.arc(*drawKreisSeg(G1, HSL_start-HSL_size*3, angle_11, angle_10, True))
    Schriftzug28.arc(*drawKreisSeg(H1, HSL_start-HSL_size*2, angle_10, angle_9, True))
    Schriftzug28.arc(*drawKreisSeg(A3, HSL_start-HSL_size, angle_9, angle_8, True))

    # von oben noch fertig ziehen
    HSL_size = 8
    HSL_start = 35.9
    Schriftzug28.arc(*drawKreisSeg(B1_festgehalten, HSL_start-HSL_size*3, angle_8, angle_7, True))
    Schriftzug28.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_6, True))
    Schriftzug28.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_6, angle_5, True))

    
    drawPath(Schriftzug28)
    
    return Schriftzug28 
 
   
#drawSchriftzug28(temp_x, temp_y)





temp_x=5






def drawSchriftzug29(x, y, instroke=False, version="isol"):
    
    Schriftzug29 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x-2, y+0.5)
    Grund_a, Grund_b, Grund_c, _ = drawGrundelOrient(A1, A2, offset, x-0.5, y-0.25)
        
    links_2 = drawSchneckenzug(*Grund_a, UPPER_F, 3, HSL_size=1, HSL_start=10, clockwise=True, inward=True)
    links_3 = drawSchneckenzug(*links_2.points[-1], UPPER_A, 5, HSL_size=2.4, HSL_start=23.7, clockwise=True, inward=True)

    rechts_unten = drawSchneckenzug(*links_3.points[-1], LOWER_F, 5, HSL_size=1, HSL_start=5, clockwise=True, inward=False)
    rechts_2 = drawSchneckenzug(*rechts_unten.points[-1], UPPER_C, 2, HSL_size=2, HSL_start=33, clockwise=True, inward=False)
    Einsatz = drawOutstroke(*rechts_2.points[-1], 1.65)
        
    Abschluss = drawGrundelementC(*Einsatz.points[0])

    if version=="isol":
        links_Spitze = drawSchneckenzug(*Grund_a, LOWER_F, 7, HSL_size=0.5, HSL_start=6.75, clockwise=True, inward=True)

    if version=="iniH":
        links_Spitze = drawSchneckenzug(*Grund_a, LOWER_F, 7, HSL_size=0.5, HSL_start=6.75, clockwise=True, inward=True)
        outstroke_top = drawOutstroke(*Abschluss.points[2], 1)
        Schriftzug29 += outstroke_top

    if version=="midH":
        links_Spitze = drawSchneckenzug(*Grund_a, LOWER_F, 6, HSL_size=0.5, HSL_start=6.675, clockwise=True, inward=True)
        _, _, _, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-2, y+0.75)
        instroke = drawGrundelementH(*Grund_d, 1)
        outstroke_top = drawOutstroke(*Abschluss.points[2], 1)
        Schriftzug29 += instroke + outstroke_top     
 
    if version=="fina":
        links_Spitze = drawSchneckenzug(*Grund_a, LOWER_F, 6, HSL_size=0.5, HSL_start=6.675, clockwise=True, inward=True)
        _, _, _, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-2, y+0.75)
        instroke = drawGrundelementH(*Grund_d, 1)
        Schriftzug29 += instroke
        
    Schriftzug29 += links_Spitze + links_2 + links_3 + rechts_unten + rechts_2 + Einsatz + Abschluss
    drawPath(Schriftzug29)   
    return Schriftzug29 
 
   
#drawSchriftzug29(temp_x, temp_y, version="iniH")






def drawSchriftzug30(x, y, Spitze="long"):
    
    Schriftzug30 = BezierPath()

    drawRauteOrientKurTop(offset, offsetDir, x, y+1)
    
    if Spitze == "long":
        _, Raute_b, _, _ = drawRauteOrientKurBtm(offset, offsetDir, x, y+4)
        instroke = drawInstroke(*Raute_b, 0.35)
        Spitze = drawSchneckenzug(*instroke.points[-1], UPPER_E, 3, HSL_size=1, HSL_start=16, clockwise=False, inward=True)
        Trans = drawSchneckenzug(*Spitze.points[-1], UPPER_B, 4, HSL_size=1, HSL_start=7, clockwise=False, inward=False)
    
    if Spitze == "short":
        Raute_a, Raute_b, _, _ = drawRauteOrientKurBtm(offset, offsetDir, x+0.5, y+3.25)
        instroke = drawInstroke(*Raute_a, 0.2)
        Spitze = drawSchneckenzug(*instroke.points[-1], UPPER_E, 4, HSL_size=1, HSL_start=8.75, clockwise=False, inward=True)
        Trans = drawSchneckenzug(*Spitze.points[-1], LOWER_A, 3, HSL_size=1.5, HSL_start=6, clockwise=False, inward=False)
    
    
    Down = drawSchneckenzug(*Trans.points[-1], UPPER_F, 7, HSL_size=2, HSL_start=10.5, clockwise=True, inward=False)

    outstroke = drawOutstroke(*Down.points[-1], 1.9, "down")
    
    linker_Bogen = drawSchriftzug21(x-2, y)
  

    Schriftzug30 += instroke + Spitze + Trans  + Down + outstroke + linker_Bogen
    drawPath(Schriftzug30)
    return Schriftzug30 
 
   
# drawSchriftzug30(temp_x, temp_y, "long")
# drawSchriftzug30(temp_x, temp_y, "short")







def drawSchriftzug31(x, y):
    
    Schriftzug31 = BezierPath()
    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    drawRauteOrientKurTop(offset, offsetDir, x+1, y-0.5)
    drawRauteOrientKurTop(offset, offsetDir, x+1, y-1.5)
    
    
    Schwung_oben = drawSchriftteil7(*Raute_b)
    Schwung_unten = drawHalbbogen3(*Raute_d)
    
    Schriftzug31 = Schwung_oben + Schwung_unten
    
    drawPath(Schriftzug31)
    
    return Schriftzug31
    
#drawSchriftzug31(temp_x, temp_y)





def drawSchriftzug32(x, y):
    
    Schriftzug32 = BezierPath()
    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    drawRauteOrientKurTop(offset, offsetDir, x+1, y-0.5)
    drawRauteOrientKurTop(offset, offsetDir, x+1, y-1.5)
    
    
    Schwung_oben = drawSchriftteil7(*Raute_b)
    Schwung_unten = drawHalbbogen2(*Raute_d)
    
    Schriftzug32 = Schwung_oben + Schwung_unten
    
    drawPath(Schriftzug32)
    
    return Schriftzug32
    
#drawSchriftzug32(temp_x, temp_y)









def drawSchriftzug33(x, y):
    
    Schriftzug33 = BezierPath()
    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    
    HSL_start = 4

    ### oberer Teil
    
    # 1. Element oben
    G2 = Raute_b
    G1 = G2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),    G2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    line(G1, G2)
    
    H1 = G1
    H2 = H1[0] - m.cos(m.radians(90-epsilon)) * (part*HSL_start),    H1[1] + m.sin(m.radians(90-epsilon)) * (part*HSL_start)
    line(H1, H2)
    

    # 2. Element unten
    E2 = G2[0], G2[1]-modul_height/2    #x, y - modul_height      
    E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),    E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    line(E1, E2)

    D1 = E1
    D2 = D1[0] - m.cos(m.radians(90-epsilon)) * (part*HSL_start),    D1[1] - m.sin(m.radians(90-epsilon)) * (part*HSL_start)
    line(D1, D2)

    # Radius über a2 + b2 = c2 ausrechnen, dann noch (wegen der Formel) durch part teilen
    center = G2[0],     G2[1] - (G2[1]-E2[1])/2
    radius = m.sqrt(pow(H2[0]-G2[0], 2) + pow(H2[1]-center[1], 2))
    
    Schriftzug33.arc(*drawKreisSeg(H1, HSL_start, angle_5, angle_6))
    Schriftzug33.arc(*drawKreisSeg(center, radius/part, angle_6, angle_10))
    Schriftzug33.arc(*drawKreisSeg(D1, HSL_start, angle_10, angle_11))

    
    ### unterer Teil
    
    # 1. Element oben
    G2 = G2[0], G2[1]-modul_height/2   # Raute_b
    G1 = G2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),    G2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    line(G1, G2)
    
    H1 = G1
    H2 = H1[0] + m.cos(m.radians(90-epsilon)) * (part*HSL_start),    H1[1] + m.sin(m.radians(90-epsilon)) * (part*HSL_start)
    line(H1, H2)
    
    # 2. Element unten
    E2 = Raute_d        # Raute_d
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),    E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    line(E1, E2)
    
    D1 = E1
    D2 = D1[0] + m.cos(m.radians(90-epsilon)) * (part*HSL_start),    D1[1] - m.sin(m.radians(90-epsilon)) * (part*HSL_start)
    line(D1, D2)

    # Radius über a2 + b2 = c2 ausrechnen, dann noch (wegen der Formel) durch part teilen
    center = G2[0], G2[1]-(G2[1]-E2[1])/2
    radius = m.sqrt(pow(H2[0]-G2[0], 2) + pow(H2[1]-center[1], 2))
    
    Schriftzug33.arc(*drawKreisSeg(H1, HSL_start, angle_3, angle_2, True))
    Schriftzug33.arc(*drawKreisSeg(center, radius/part, angle_2, angle_14, True))
    Schriftzug33.arc(*drawKreisSeg(D1, HSL_start, angle_14, angle_13, True))
    
    
    drawPath(Schriftzug33)
    
    return Schriftzug33
    
#drawSchriftzug33(temp_x, temp_y)






def drawSchriftzug34(x, y):
    
    Schriftzug34 = BezierPath()
    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    drawRauteOrientKurTop(offset, offsetDir, x+1, y-0.5)

    
    HSL_size = 2
    HSL_start = 13   
      
    
    #eigentlich:  Schwung_links = drawSchriftzug6(x, y, "a")
    
    G2 = Raute_d 
    G1 = G2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),     G2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    line(G1, G2) 

    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    
    Schriftzug34.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug34.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))

    
    #eigentlich:   Schwung_rechts = drawSchriftzug6(x+2, y, "b")
    
    G2 = Raute_d 
    G1 = G2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),     G2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    line(G1, G2) 

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug34.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    Schriftzug34.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))
    
    
    drawPath(Schriftzug34)
    
    return Schriftzug34
    
#drawSchriftzug34(temp_x, temp_y)





def drawSchriftzug35(x, y):
    
    Schriftzug35 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    drawRauteOrientKurTop(offset, offsetDir, x+0.5, y-0.25)
    Schwung_links = drawSchneckenzug(x, y, UPPER_G, 2, HSL_size=1, HSL_start=7.8, clockwise=False, inward=False)
    Schwung_rechts = drawSchneckenzug(x, y, LOWER_G, 2, HSL_size=1, HSL_start=7.8, clockwise=False, inward=False)

    #Schwung_links = drawSchneckenzug(*Raute_d, UPPER_G, 2, HSL_size=1, HSL_start=7.8, clockwise=False, inward=False)
    #eigentlich:  Schwung_links = drawSchriftzug6(x, y, "a")
    #eigentlich:   Schwung_rechts = drawSchriftzug6(x+2, y, "b")
    #Schwung_rechts = drawSchneckenzug(*Raute_d, LOWER_G, 2, HSL_size=1, HSL_start=7.8, clockwise=False, inward=False)
    
    Schriftzug35 = Schwung_links + Schwung_rechts
    drawPath(Schriftzug35)
    return Schriftzug35
    
#drawSchriftzug35(temp_x, temp_y)






def drawSchriftzug36(x, y):
    
    Schriftzug36 = BezierPath()
    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    drawRauteOrientKurTop(offset, offsetDir, x+0.5, y-0.25)

    
    HSL_size = 1
    HSL_start = 7.8
      
    
    #eigentlich:  Schwung_links = drawSchriftzug6(x, y, "a")
    
    G2 = Raute_d 
    G1 = G2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),     G2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    line(G1, G2) 

    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    
    Schriftzug36.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug36.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))

    
    #eigentlich:   Schwung_rechts = drawSchriftzug6(x+2, y, "b")
    
    G2 = Raute_d 
    G1 = G2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),     G2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    line(G1, G2) 

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug36.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    Schriftzug36.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))
    
    
    drawPath(Schriftzug36)
    
    return Schriftzug36
    
#drawSchriftzug36(temp_x, temp_y)







def drawSchriftzug_backstroke_g(x, y, version="fina", instrokeLen=0, deviation=1):
    
    #x += 2
    Schriftzug_backstroke_g = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    instroke = drawInstroke(*Raute_a, instrokeLen)
    Signatur = drawSchriftzug3(x, y, "a", instroke=False)
    downstroke = drawGrundelementF(*Raute_a, 3)


    if version == "fina":

        bend_to_right = drawSchneckenzug(*downstroke.points[-1], UPPER_B, 1, HSL_size=6, HSL_start=24, clockwise=False, inward=False)
        Bogen_unten = drawSchneckenzug(*bend_to_right.points[-1], UPPER_A, 4, HSL_size=6, HSL_start=12.5, clockwise=True, inward=False)

        Schriftzug_backstroke_g += bend_to_right + Bogen_unten



    if version == "loop_A": 
        bendToRight = drawSchneckenzug(*downstroke.points[-1], UPPER_B, 1, HSL_size=6, HSL_start=20, clockwise=False, inward=False)
        bendRightDown = drawSchneckenzug(*bendToRight.points[-1], UPPER_A, 4, HSL_size=6, HSL_start=12.5, clockwise=True, inward=False)
        turnDown = drawSchneckenzug(*bendRightDown.points[-1], LOWER_E, 5, HSL_size=0.5, HSL_start=3, clockwise=True, inward=False)
        
        # Hilfslinie und Hilfsraute zur Positionierung
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-4)
        bendLeftUp = drawSchneckenzug(*turnDown.points[-1], UPPER_B, 3, HSL_size=18, HSL_start=11, clockwise=True, inward=False)
        constroke = drawConstroke(*bendLeftUp.points[-1], "A", 3, deviation)
        Schriftzug_backstroke_g += bendToRight + bendRightDown + turnDown + bendLeftUp + constroke
        
    
    
    
    
    if version == "loop_H": 

        # Übergang bend to right
        HSL_size = 6
        HSL_start = 20

        B1, B2 = line_B_vonC_o_gr_s(downstroke.points[-1], *angles, part, HSL_size, HSL_start)
        A1, A2 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)

        Schriftzug_backstroke_g.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_9))



        # Bogen rechts runter 
        HSL_size = 6
        HSL_start = 13

        A3, A4 = line_A_vonH_o_gr_s(A2, *angles, part, HSL_size, HSL_start)
        B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)    
        C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)    
        D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)    
        E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)

        Schriftzug_backstroke_g.arc(*drawKreisSeg(A3, HSL_start, angle_1, angle_0, True))
        Schriftzug_backstroke_g.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
        Schriftzug_backstroke_g.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
        Schriftzug_backstroke_g.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))



    
        ### Wendung unten
        HSL_size = 0.5
        HSL_start = 2.5
    
        E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),   E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
        line(E1, E2)
   
 
        F1, F2 = line_F_vonE_u_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        G1, G2 = line_G_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        A5, A6 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        B1, B2 = line_B_vonA_o_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*5)
    
        Schriftzug_backstroke_g.arc(*drawKreisSeg(E1, HSL_start, angle_13, angle_12, True))
        Schriftzug_backstroke_g.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_11, True))
        Schriftzug_backstroke_g.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_10, True))
        Schriftzug_backstroke_g.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_10, angle_9, True))
        Schriftzug_backstroke_g.arc(*drawKreisSeg(A5, HSL_start+HSL_size*4, angle_9, angle_8, True))



        # Hilfslinie und Hilfsraute zur Positionierung
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-4)



        ### left side up
        HSL_size = 4
        HSL_start = 12

        B1 = B2[0] + (part*HSL_start),    B2[1] 
        line(B1, B2)
    
        C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)
        D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    
        Schriftzug_backstroke_g.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_7, True))
        Schriftzug_backstroke_g.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_6, True))    
    
    
        constroke = drawConstroke(*D2, "H", 6.12)

        Schriftzug_backstroke_g += constroke
              
        
        
 
    

    Schriftzug_backstroke_g += instroke + Signatur + downstroke 
    
    drawPath(Schriftzug_backstroke_g)
    
    return Schriftzug_backstroke_g



#drawSchriftzug_backstroke_g(temp_x, temp_y, version="fina")
#drawSchriftzug_backstroke_g(temp_x, temp_y, version="loop_A")
#drawSchriftzug_backstroke_g(temp_x, temp_y, version="loop_A", deviation=1.4)
#drawSchriftzug_backstroke_g(temp_x, temp_y, version="loop_H")

        




    
    
    
def drawHook_o(x, y, outstrokeLen=0):
    
    Hook_o = BezierPath() 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
            
    HSL_size = 2
    HSL_start = 17.25              
    
    H1, H2 = line_H_vonA_u_gr_s((x,y), *angles, part, HSL_size, HSL_start)
    
    G1, G2 = line_G_vonF_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    Hook_o.arc(*drawKreisSeg(H1, HSL_start, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Hook_o.arc(*drawKreisSeg(G1, HSL_start+HSL_size, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*3)     
    Hook_o.arc(*drawKreisSeg(F1, HSL_start+HSL_size*2, angle_12, angle_13))
    
    outstroke = drawOutstroke(*E2, outstrokeLen)
    
    Hook_o += outstroke 
    drawPath(Hook_o)
    
    return Hook_o
    
#drawHook_o(temp_x, temp_y) 






def drawTail_x(x, y):
    
    Tail_x = BezierPath() 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-4)
     
    instroke = drawInstroke(*Raute_a, 1)
           
    HSL_size = 1
    HSL_start = 18.8  
        
    E1, E2 = line_E_vonF_o_kl_s(instroke.points[1], *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    G1, G2 = line_G_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*8)


    Tail_x.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    Tail_x.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    Tail_x.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    Tail_x.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_8, angle_9))
    Tail_x.arc(*drawKreisSeg(H1, HSL_start-HSL_size*5, angle_9, angle_10))
    Tail_x.arc(*drawKreisSeg(G1, HSL_start-HSL_size*6, angle_10, angle_11))
    Tail_x.arc(*drawKreisSeg(F1, HSL_start-HSL_size*7, angle_11, angle_12))
    Tail_x.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_12, angle_13))
    
    Tail_x += instroke    
    drawPath(Tail_x)
    
    return Tail_x
    
#drawTail_x(temp_x, temp_y) 






def drawSchriftzug_z_oben(x, y, version="mid"):
    
    Schriftzug_z_oben = BezierPath() 
    
    # Beginn oben
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)

    if version == "begin":
        
        HSL_size = 1.5
        HSL_start = 12
       
        G1, G2 = line_G_vonF_u_kl_s(Grund_a, *angles, part, HSL_size, HSL_start-HSL_size)
        H1, H2 = line_H_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        A7, A8 = line_A_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        B1, B2 = line_B_vonA_o_kl(A7, *angles, part, HSL_size, HSL_start-HSL_size*4)
        C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*5)
        D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*6)
        E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    
        Schriftzug_z_oben.arc(*drawKreisSeg(E1, HSL_start-HSL_size*7, angle_5, angle_6))
        Schriftzug_z_oben.arc(*drawKreisSeg(D1, HSL_start-HSL_size*6, angle_6, angle_7))
        Schriftzug_z_oben.arc(*drawKreisSeg(C1, HSL_start-HSL_size*5, angle_7, angle_8))
        Schriftzug_z_oben.arc(*drawKreisSeg(B1, HSL_start-HSL_size*4, angle_8, angle_9))
        Schriftzug_z_oben.arc(*drawKreisSeg(A7, HSL_start-HSL_size*3, angle_9, angle_10))
        Schriftzug_z_oben.arc(*drawKreisSeg(H1, HSL_start-HSL_size*2, angle_10, angle_11))


    
    ### Kurve rechts oben
    HSL_size = 0.5
    HSL_start = 7.5   # war erst 8, im Feb 21 umgestellt
    
    #F1, F2 = line_F_vonE_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    G1, G2 = line_G_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)

    Schriftzug_z_oben.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_2, True))
    Schriftzug_z_oben.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_2, angle_1, True))
    Schriftzug_z_oben.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_1, angle_0, True))
    Schriftzug_z_oben.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_16, angle_15, True))
    Schriftzug_z_oben.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_15, angle_14, True))
    Schriftzug_z_oben.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E2, 1.25, "down")
    
    Schriftzug_z_oben += outstroke    
    drawPath(Schriftzug_z_oben) 
    return Schriftzug_z_oben
    
# drawSchriftzug_z_oben(temp_x, temp_y) 








def drawSchriftzug_z_unten(x, y, version="loop_A"):
    
    Schriftzug_z_unten = BezierPath() 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    Spitze = drawSchriftzug6(x, y, "a")
    bogen = drawSchneckenzug(*Spitze.points[0], UPPER_G, 3, HSL_size=1.5, HSL_start=9, clockwise=True, inward=False)
        
    B2 = bogen.points[-1]
    
    
    if version == "loop_A": 

        # right_down = drawSchneckenzug(*bogen.points[-1], LOWER_B, 3, HSL_size=10, HSL_start=37.5, clockwise=True, inward=True)
        # turn_btm = drawSchneckenzug(*right_down.points[-1], LOWER_E, 5, HSL_size=0.5, HSL_start=4, clockwise=True, inward=True)  
        # left_up = drawSchneckenzug(*turn_btm.points[-1], UPPER_B, 3, HSL_size=6.6, HSL_start=18, clockwise=True, inward=True)  
        ### right side down
        HSL_size = 10
        HSL_start = 37.5


        B1, B2 = line_B_vonA_u_gr_s(B2, *angles, part, HSL_size, HSL_start)
        
        C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug_z_unten.arc(*drawKreisSeg(B1, HSL_start, angle_16, angle_15, True))
    
        HSL_start += HSL_size 
        HSL_size = 0
    
        D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start)
        E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start)

        Schriftzug_z_unten.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_14, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(D1, HSL_start, angle_14, angle_13, True))
   
    
        ### Wendung unten
        HSL_size = 0.5
        HSL_start = 4
       
        E1, E2 = line_E_vonD_u_gr_s(E2, *angles, part, HSL_size, HSL_start)
        F1, F2 = line_F_vonE_u_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        G1, G2 = line_G_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        A5, A6 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        B1, B2 = line_B_vonA_o_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*5)
    
        Schriftzug_z_unten.arc(*drawKreisSeg(E1, HSL_start, angle_13, angle_12, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_11, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_10, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_10, angle_9, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(A5, HSL_start+HSL_size*4, angle_9, angle_8, True))

        ### left side up/down
        HSL_size = 6.6
        HSL_start = 18

        B1, B2 = line_B_vonA_o_gr_s(B2, *angles, part, HSL_size, HSL_start)
        C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)
        D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    
        Schriftzug_z_unten.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_7, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_6, True))    
        Schriftzug_z_unten.arc(*drawKreisSeg(D1, HSL_start+HSL_size*2, angle_6, angle_5, True))
  
        outstroke = drawOutstroke(*E2, 2.46)
        
        # outstroke = drawOutstroke(*left_up.points[-1], 2.46)
        Schriftzug_z_unten += outstroke #+ right_down + turn_btm + left_up
            
        
    if version == "loop_H":       
        ### right side down
        HSL_size = 2
        HSL_start = 28


        B2 = B2[0], B2[1] #- modul_height*0.5        ### Hier evtl. anpassen
        B1 = B2[0] - (part*HSL_start),     B2[1]
        line(B1, B2)
        #B1, B2 = line_B_vonA_o_gr_s(B2, *angles, part, HSL_size, HSL_start)

        C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug_z_unten.arc(*drawKreisSeg(B1, HSL_start, angle_16, angle_15, True))
    
        # HSL_start += HSL_size 
        # HSL_size = 0
    
        D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*3)

        Schriftzug_z_unten.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_15, angle_14, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(D1, HSL_start+HSL_size*2, angle_14, angle_13, True))
        
           
        
        ### Wendung unten
        HSL_size = 0.5
        HSL_start = 4
    
        E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),   E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
        line(E1, E2) 
 
        F1, F2 = line_F_vonE_u_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        G1, G2 = line_G_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        A5, A6 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        B1, B2 = line_B_vonA_o_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*5)
    
        Schriftzug_z_unten.arc(*drawKreisSeg(E1, HSL_start, angle_13, angle_12, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_11, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_10, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_10, angle_9, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(A5, HSL_start+HSL_size*4, angle_9, angle_8, True))

        # Hilfe für Abstand zum Hauptstrich und Pos Ausstrich
        drawRauteOrientKurTop(offset, offsetDir, x-3.5, y-4.5)
        drawRauteOrientKurTop(offset, offsetDir, x, y-4)

        ### left side up/down
        HSL_size = 4
        HSL_start = 10

        B1 = B2[0] + (part*HSL_start),    B2[1] 
        line(B1, B2)
    
        C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)
        D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    
        Schriftzug_z_unten.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_7, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_6, True))    
    
        outstroke = drawConstroke(*D2, "H", 2.8)
        Schriftzug_z_unten += outstroke


    Schriftzug_z_unten += Spitze + bogen
    drawPath(Schriftzug_z_unten)
    return Schriftzug_z_unten
    
# drawSchriftzug_z_unten(temp_x, temp_y, version="loop_A") 
# drawSchriftzug_z_unten(temp_x, temp_y, version="loop_H") 







    
#############################################################################################

########   ab hier Zahlen         

#############################################################################################











def drawSchriftzug3_Figures(x, y, instrokeLen=0, outstrokeLen=0.5):
        
    Schriftzug3_Figures = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.5)
    instroke = drawInstroke(*Grund_a, instrokeLen)
    stem = drawGrundelementF(*Grund_a, 5)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.5)
    bend = drawSchneckenzug(*Grund_a, UPPER_B, 5, HSL_size=0.5, HSL_start=8, clockwise=False, inward=True)


    Schriftzug3_Figures += instroke + stem + bend
    drawPath(Schriftzug3_Figures)
    return Schriftzug3_Figures
    

#drawSchriftzug3_Figures(temp_x, temp_y)
    
    
       
    
    
    


def drawSchriftzug_two_Bogen(x, y, instrokeLen=0, outstrokeLen=1.7):
        
    Schriftzug_two_Bogen = BezierPath()   
           
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.4)
    drawGrundelOrient(A1, A2, offset, x+1, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+1, y-3.5)
    drawGrundelOrient(A1, A2, offset, x-1, y-4.5)
    drawGrundelOrient(A1, A2, offset, x+3, y-4.5)

    instroke = drawInstroke(*Grund_a, instrokeLen)
    Bogen = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=2, HSL_start=9.05, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Bogen.points[-1], outstrokeLen, "down")
        
    Schriftzug_two_Bogen += instroke + Bogen + outstroke
    drawPath(Schriftzug_two_Bogen)
    return Schriftzug_two_Bogen
    
    
#drawSchriftzug_two_Bogen(temp_x, temp_y)



def drawSchriftzug_two_Schwung(x, y, instrokeLen=0):
    
    Schriftzug_two_Schwung = BezierPath()  
    
    drawGrundelOrient(A1, A2, offset, x, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
     
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y-4)

    instroke = drawInstroke(*Grund_a, instrokeLen, "down")
    left = drawSchneckenzug(*Grund_a, UPPER_E, 3, HSL_size=1, HSL_start=8, clockwise=True, inward=False)
    right = drawSchneckenzug(*left.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=5, clockwise=False, inward=False)
    outstroke = drawOutstroke(*right.points[-1], 1)
        
    Schriftzug_two_Schwung += instroke + left + right + outstroke

    drawPath(Schriftzug_two_Schwung)
    return Schriftzug_two_Schwung
    
    
#drawSchriftzug_two_Schwung(temp_x, temp_y)






def drawSchriftzug_three_Bogen(x, y):
        
    Schriftzug_three_Bogen = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y)
    

    HSL_size = 2
    HSL_start = 8
        
    E1, E2 = line_E_vonD_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*7)
    E3, E4 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*8)

    Schriftzug_three_Bogen.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_2, angle_1, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_1, angle_0, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(B1, HSL_start+HSL_size*5, angle_16, angle_15, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(C1, HSL_start+HSL_size*6, angle_15, angle_14, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(D1, HSL_start+HSL_size*7, angle_14, angle_13, True))


    drawPath(Schriftzug_three_Bogen)
    return Schriftzug_three_Bogen

#drawSchriftzug_three_Bogen(temp_x, temp_y)






def drawSchriftzug_three_Top(x, y, instrokeLen=1):
        
    Schriftzug_three_Top = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+1.5)
    
    instroke = drawInstroke(*Grund_a, instrokeLen)


    HSL_size = 2
    HSL_start = 26.5
        
    G1, G2 = line_G_vonH_u_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size)
    E1, E2 = line_E_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    
    Schriftzug_three_Top.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_11, angle_12))
    Schriftzug_three_Top.arc(*drawKreisSeg(E1, HSL_start-HSL_size*2, angle_12, angle_13))

    Schriftzug_three_Top += instroke
   
    drawPath(Schriftzug_three_Top)
    return Schriftzug_three_Top

#drawSchriftzug_three_Top(temp_x, temp_y)







def drawSchriftzug_five_Top(x, y):
        
    Schriftzug_five_Top = BezierPath()   
        
    # drawGrundelOrient(A1, A2, offset, x, y-4.5)
    # drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    # drawGrundelOrient(A1, A2, offset, x+4, y+2)  
    # Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+2)
    
    HSL_size = 2
    HSL_start = 20

    ### Positionierung hier relativ (also x, y), damit ich das Teil direkt ansetzen kann   
    G1, G2 = line_G_vonF_u_gr_s((x,y), *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_five_Top.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    Schriftzug_five_Top.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))



    drawPath(Schriftzug_five_Top)
    return Schriftzug_five_Top

#drawSchriftzug_five_Top(temp_x, temp_y)






def drawSchriftzug_seven_Stem(x, y):
        
    Schriftzug_seven_Stem = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3, y+1.5)
    

    HSL_size = 8.2
    HSL_start = 52.75
        
    E1, E2 = line_E_vonF_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    G1, G2 = line_G_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*6)

    Schriftzug_seven_Stem.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    Schriftzug_seven_Stem.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    Schriftzug_seven_Stem.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    Schriftzug_seven_Stem.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_8, angle_9))
    Schriftzug_seven_Stem.arc(*drawKreisSeg(H1, HSL_start-HSL_size*5, angle_9, angle_10))
    Schriftzug_seven_Stem.arc(*drawKreisSeg(G1, HSL_start-HSL_size*6, angle_10, angle_11))


    drawPath(Schriftzug_seven_Stem)
    return Schriftzug_seven_Stem

#drawSchriftzug_seven_Stem(temp_x, temp_y)





def drawSchriftzug_zero_BogenLinks(x, y):
        
    Schriftzug_zero_BogenLinks = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.6, y+1.35)
    

    HSL_size = 18
    HSL_start = 13.6

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonD_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_kl(C1, *angles, part, 0, HSL_start+HSL_size)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonF_u_kl(H1, *angles, part, 0, HSL_start)

    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_7))
    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_9))
    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(H1, HSL_start, angle_9, angle_13))



    drawPath(Schriftzug_zero_BogenLinks)
    return Schriftzug_zero_BogenLinks

#drawSchriftzug_zero_BogenLinks(temp_x, temp_y)





def drawSchriftzug_zero_BogenRechts(x, y, version="zero"):
        
    Schriftzug_zero_BogenRechts = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.6, y+1.35)
    

    HSL_size = 18
    HSL_start = 13.6

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(E1, *angles, part, 0, HSL_start)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_u_gr(A3, *angles, part, 0, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_u_kl(A3, *angles, part, HSL_size, HSL_start)
    G1, G2 = line_G_vonF_u_gr(D1, *angles, part, 0, HSL_start)

    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_1, True))
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(A3, HSL_start+HSL_size, angle_1, angle_15, True))
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_15, angle_13, True))

    if version == "zero":
    
        Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_13, angle_11, True))
        
    drawPath(Schriftzug_zero_BogenRechts)
    return Schriftzug_zero_BogenRechts

#drawSchriftzug_zero_BogenRechts(temp_x, temp_y)
#drawSchriftzug_zero_BogenRechts(temp_x, temp_y, version="nine")




def drawSchriftzug_six_BogenRechts(x, y):
        
    Schriftzug_six_BogenRechts = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5) 
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.6, y-0.75)
    

    HSL_size = 2
    HSL_start = 13.3

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(E1, *angles, part, 0, HSL_start)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_u_gr(A3, *angles, part, 0, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_u_kl(A3, *angles, part, HSL_size, HSL_start)
    #G1, G2 = line_G_vonF_u_gr(D1, *angles, part, 0, HSL_start)

    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_1, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(A3, HSL_start+HSL_size, angle_1, angle_15, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_15, angle_13, True))


    drawPath(Schriftzug_six_BogenRechts)
    return Schriftzug_six_BogenRechts

#drawSchriftzug_six_BogenRechts(temp_x, temp_y)






def drawSchriftzug_nine_BogenLinks(x, y):
        
    Schriftzug_nine_BogenLinks = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.6, y+1.35)
    

    HSL_size = 2
    HSL_start = 13

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonD_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_kl(C1, *angles, part, 0, HSL_start+HSL_size)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonF_u_kl(H1, *angles, part, 0, HSL_start)

    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_7))
    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_9))
    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(H1, HSL_start, angle_9, angle_13))


    drawPath(Schriftzug_nine_BogenLinks)
    return Schriftzug_nine_BogenLinks

#drawSchriftzug_nine_BogenLinks(temp_x, temp_y)





def drawSchriftzug_eight(x, y):
        
    Schriftzug_eight_BogenMiddle = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+1.35)
    oben_rechts = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=0, HSL_start=10.1, clockwise=True, inward=False)
    oben_links = drawSchneckenzug(*Grund_a, UPPER_E, 6, HSL_size=0, HSL_start=10.1, clockwise=False, inward=False)
    straight_middle = drawGrundelementC(*oben_links.points[-1], length=1.25)
    unten_rechts = drawSchneckenzug(*straight_middle.points[-1], UPPER_G, 6, HSL_size=0, HSL_start=10.1, clockwise=True, inward=False)
    straight_middle2 = drawGrundelementA(*oben_rechts.points[-1], 1.25, "down")
    unten_links = drawSchneckenzug(*straight_middle2.points[-1], UPPER_E, 8, HSL_size=0, HSL_start=10.1, clockwise=False, inward=False)

    Schriftzug_eight = oben_rechts + oben_links + unten_rechts + unten_links + straight_middle + straight_middle2
    drawPath(Schriftzug_eight)
    return Schriftzug_eight

# drawSchriftzug_eight(temp_x, temp_y)










#############################################################################
######     ab hier UC
#############################################################################




def drawKurrentKanzleiGatC_helperUC(x, y):
    
    KurrentKanzleiGatC_helperUC = BezierPath()

    drawGrundelOrient(A1, A2, offset, x, baseline+0.5)

    helper = (0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5)
    for h in helper:
        drawGrundelOrient(A1, A2, offset, x+3.5, baseline+h)
        
    drawGrundelOrient(A1, A2, offset, x+7, baseline+0.5)
    
    helper = (0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        drawGrundelOrient(A1, A2, offset, x+10.5, baseline+h)
        
    drawGrundelOrient(A1, A2, offset, x+14, baseline+0.5)

        
        
    #KurrentKanzleiGatC_helperUC = Bogen_rechts
    #trans_scale(KurrentKanzleiGatC_helperUC)
    
    return KurrentKanzleiGatC_helperUC


drawKurrentKanzleiGatC_helperUC(temp_x, temp_y)







def drawSchriftzug_longs_longs_2nd(x, y):
    
    Schriftzug_longs_longs_2nd = BezierPath() 
  
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1, y+4) 
    connection_left = drawSchneckenzug(*Raute_a, UPPER_E, 2, HSL_size=1, HSL_start=28, clockwise=True, inward=False)
    connection_transition = drawSchneckenzug(*connection_left.points[-1], UPPER_G, 3, HSL_size=2, HSL_start=15.5, clockwise=True, inward=True)
    downstroke = drawGrundelementF(*connection_transition.points[-1], 7.125)

    Schriftzug_longs_longs_2nd += connection_left + connection_transition + downstroke
    drawPath(Schriftzug_longs_longs_2nd)
    return Schriftzug_longs_longs_2nd
    
#drawSchriftzug_longs_longs_2nd(temp_x, temp_y)




def drawSchriftzug_f_f_2nd(x, y):
    
    Schriftzug_f_f_2nd = BezierPath() 
  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+5)
    connection_left = drawSchneckenzug(*Grund_a, UPPER_E, 7, HSL_size=2, HSL_start=1, clockwise=False, inward=False)
    Einsatz = drawGrundelementB(*connection_left.points[-1], 0.5)
    connection_transition = drawSchneckenzug(*Einsatz.points[-1], UPPER_F, 4, HSL_size=1, HSL_start=10, clockwise=True, inward=True)
    downstroke = drawGrundelementF(*connection_transition.points[-1], 7.1)

    Schriftzug_f_f_2nd += connection_left + Einsatz + connection_transition + downstroke  
    drawPath(Schriftzug_f_f_2nd)
    return Schriftzug_f_f_2nd
    
#drawSchriftzug_f_f_2nd(temp_x, temp_y)


    
    


def drawDieresis(x, y):
    
    Dieresis = BezierPath() 
    
    drawGrundelOrientMittig(A1, A2, offset, x-2, y+1)
    drawGrundelOrientMittig(A1, A2, offset, x-2, y+2)
    drawGrundelOrientMittig(A1, A2, offset, x-1, y+1.5)
    drawGrundelOrientMittig(A1, A2, offset, x-1, y+2.5)
    drawGrundelOrientMittig(A1, A2, offset, x, y+1)
    drawGrundelOrientMittig(A1, A2, offset, x, y+2)
    drawGrundelOrientMittig(A1, A2, offset, x+1, y+2.5)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y+3)
    links = drawGrundelementC(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+2, y+3)
    rechts = drawGrundelementC(*Raute_a)

    Dieresis = links + rechts
    drawPath(Dieresis) 
    return Dieresis
    
#drawDieresis(temp_x, temp_y)







def drawSchriftzug_semicolon_btm(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y) 
    drawGrundelOrientMittig(A1, A2, offset, x, y-0.5) 

    Schriftzug_semicolon_btm = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=0.5, HSL_start=3.15, clockwise=True, inward=False)

    drawPath(Schriftzug_semicolon_btm)
    return Schriftzug_semicolon_btm
    
#drawSchriftzug_semicolon_btm(temp_x, temp_y) 
    
    
    
    
def drawSchriftzug_question(x, y):
    
    Schriftzug_question = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.25)
    drawGrundelOrient(A1, A2, offset, x-0.5, y+4)
    drawGrundelOrient(A1, A2, offset, x, y-2.75)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, baseline)   
    oben = drawSchneckenzug(*Grund_a, LOWER_E, 8, HSL_size=1.125, HSL_start=14.85, clockwise=False, inward=True)
    unten = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=1.125, HSL_start=18, clockwise=False, inward=True)

    Schriftzug_question = oben + unten
    drawPath(Schriftzug_question)
    return Schriftzug_question
    
#drawSchriftzug_question(temp_x, temp_y)
    
    
    
    