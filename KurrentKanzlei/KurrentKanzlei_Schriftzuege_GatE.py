import importlib
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatE
import version_3.creation.Halbboegen_GatE

importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatE)
importlib.reload(version_3.creation.Halbboegen_GatE)

from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatE import *
from version_3.creation.Halbboegen_GatE import *
#from version_3.creation.arc_path import ArcPath as BezierPath
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m




# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 10
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(1)




# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 5

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)

# temporary to check height of numbers
#line((1*modul_width, 11*modul_height), (9*modul_width, 11*modul_height))



# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 8.5


#Helper
#Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, temp_x, temp_y)
#Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, temp_x, temp_y-0.25)






def drawGrundSchriftzug(x, y, instroke=False, outstroke=False):
    
    GrundSchriftzug = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    downstroke = drawGrundelementF(*Raute_a, 4.5)               

    if instroke == True:
        instroke = drawInstroke(*Raute_a, 1)
        GrundSchriftzug += instroke
        
    if outstroke == True:
        outstroke = drawOutstroke(*downstroke.points[1], 1)
        GrundSchriftzug += outstroke        
 
    GrundSchriftzug += downstroke     
    drawPath(GrundSchriftzug) 
    return GrundSchriftzug
    
    
# drawGrundSchriftzug(temp_x, temp_y, instroke=True, outstroke=True)




def drawConCurve_up(x, y):    # definiert für a
    
    ConCurve_up = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
    drawRauteOrientKurTop(offset, offsetDir, x+3, y)
    instroke = drawInstroke(*Raute_c, 0.5, "down")
    curve = drawSchneckenzug(*instroke.points[-1], LOWER_E, 3, HSL_size=1, HSL_start=18.7, clockwise=False, inward=False)  

    ConCurve_up += curve + instroke
    drawPath(ConCurve_up)
    return ConCurve_up

# drawConCurve_up(temp_x, temp_y)






def drawSchriftzug1(x,y, version="a"):
    
    Schriftzug1 = BezierPath() 
     
    if version == "a":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        stroke = drawInstroke(*Raute_a, 1)
        downstroke = drawGrundelementF(*Raute_a)
        
    if version == "b":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)
        downstroke = drawGrundelementF(*Raute_b)
        stroke = drawOutstroke(*Raute_d, 1)
    
    Schriftzug1 += stroke + downstroke
    drawPath(Schriftzug1)
    return Schriftzug1
   
# drawSchriftzug1 (temp_x, temp_y, "a")

    
    




def drawSchriftzug2(x,y):
    
    Schriftzug2 = BezierPath() 
     
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    Querstrich = drawGrundelementB(*Raute_a)    # Positionierung noch nicht ganz klar
                
    drawPath(Schriftzug2)
    return Schriftzug2

# drawSchriftzug2(temp_x, temp_y)








def drawSchriftzug3(x, y, version="a", instroke=True, outstroke=True):
    
    Schriftzug3 = BezierPath() 
            
    if version == "a":
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        downstroke = drawGrundelementE(*Raute_a)

        if instroke == True:
            instroke = drawInstroke(*Raute_a, 1)
            Schriftzug3 = instroke + downstroke 

        if instroke == False:
            Schriftzug3 = downstroke
            
            
    if version == "b":
       
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)
        downstroke = drawGrundelementE(*Raute_a)
        
        if outstroke == True:
            outstroke = drawOutstroke(*Raute_d, 1)
            Schriftzug3 = outstroke + downstroke   
        
        if outstroke == False:
            Schriftzug3 = downstroke
    
    
    drawPath(Schriftzug3)
    return Schriftzug3
    
# drawSchriftzug3(temp_x, temp_y, "a")       







def drawSchriftzug4(x, y, version="a", instroke=False, outstroke=False):
    
    Schriftzug4 = BezierPath()
            
    if version == "a":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        downstroke = drawSchriftteil8(*Raute_a)
        
        if instroke == True:
            instroke = drawInstroke(*Raute_a, 1, "up")     ### falls hier Richtung anders
            Schriftzug4 += instroke                        ### dann mit k checken!
            
        if outstroke == True:
            outstroke = drawOutstroke(*downstroke.points[-1], 1, "down")
            Schriftzug4 += outstroke   
        
        
    if version == "b":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)
        downstroke = drawSchriftteil7(*Raute_d)   
        
        if instroke == True:
            instroke = drawInstroke(*downstroke.points[-1], 1, "up")   ### falls hier Richtung anders
            Schriftzug4 += instroke                                    ### dann mit k checken!
            
        if outstroke == True:
            outstroke = drawOutstroke(*downstroke.points[0], 1, "down")
            Schriftzug4 += outstroke
            
             
    Schriftzug4 += downstroke
    drawPath(Schriftzug4)
    return Schriftzug4
    

# drawSchriftzug4(temp_x, temp_y, "a", outstroke=False, instroke=False) 
# drawSchriftzug4(temp_x, temp_y, "b", instroke= True, outstroke=True) 
    
    
    
    
    

def drawSchriftzug5(x, y, version="a", instroke=False, outstroke=False):
    
    Schriftzug5 = BezierPath() 
    
            
    if version == "a":
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        downstroke = drawSchriftteil2(*Raute_d)
        
        if instroke == True:
            instroke = drawInstroke(*Raute_b, 1)
            Schriftzug5 += instroke 

        if outstroke == True:
            outstroke = drawInstroke(*Raute_c, 1)
            Schriftzug5 += outstroke
        
    if version == "b":
       
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)
        downstroke = drawSchriftteil1(*Raute_a)
        
        if instroke == True:
            instroke = drawInstroke(*Raute_a, 1)
            Schriftzug5 += instroke 
            
        if outstroke == True:
            outstroke = drawOutstroke(*Raute_d, 1, "down")   
            Schriftzug5 += outstroke


    Schriftzug5 = downstroke 
        
    drawPath(Schriftzug5)
    
    return Schriftzug5
    

#drawSchriftzug5(temp_x, temp_y, "a", outstroke=True) 
#drawSchriftzug5(temp_x, temp_y, "b", instroke=True) 

     




def drawSchriftzug6(x, y, version="a"):
    
    Schriftzug6 = BezierPath() 
    
            
    if version == "a":
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
        HSL_size = 2
        HSL_start = 4
        
        # A2 = Raute_d
        # A1 = A2[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      A2[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
        # polygon(A1, A2)
        
        A1, A2 = line_A_vonB_o_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)

        H1, H2 = line_H_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug6.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_2))
        
        G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug6.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_2, angle_3))

        F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        Schriftzug6.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_4))
 
        E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        Schriftzug6.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_4, angle_5))


    if version == "b":
       
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y)
        
        HSL_size = 2
        HSL_start = 4
        
        A1, A2 = line_A_vonB_u_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)

        H1, H2 = line_H_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug6.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_10))
        
        G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug6.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))

        F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        Schriftzug6.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
 
        E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        Schriftzug6.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))
    
    
    
    drawPath(Schriftzug6)
    
    return Schriftzug6
    
    
#drawSchriftzug6(temp_x, temp_y, "a") 
#drawSchriftzug6(temp_x, temp_y, "b") 








def drawSchriftzug7(x, y, version="a", instrokeLen=0, outstrokeLen=0):
    
    Schriftzug7 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
 
      
    instroke = drawInstroke(*Raute_a, instrokeLen)
    
    if version == "a":
        
        # Signatur = drawSchriftzug3(x, y, "a", instroke=False)
        # downstroke = drawGrundelementF(*Raute_a, 4.5)
        # con_stroke = drawGrundelementH(*downstroke.points[1], 3)
        

        Signatur = drawSchriftzug3(x, y, "a", instroke=False)
        downstroke = drawGrundelementF(*Raute_a, 4.5)
        outstroke = drawOutstroke(*downstroke.points[1], outstrokeLen)

        Schriftzug7 += Signatur + downstroke
        
                
        
        
    if version == "b":

        # Signatur = drawSchriftzug5(x, y, "a", instroke=False)
        # bend_top = drawSchriftteil5(*Raute_d, "1")
        # downstroke = drawGrundelementF(*bend_top.points[-1], 2.08)
        # con_stroke = drawGrundelementH(*downstroke.points[1], 3)
        
        Signatur = drawSchriftzug5(x-0.4, y+0.8, "a", instroke=False)   #### Positionierung fragwürdig
        bend_top = drawSchriftteil5(*Raute_d, "1")
        downstroke = drawGrundelementF(*bend_top.points[-1], 2.08)
        outstroke = drawOutstroke(*downstroke.points[1], outstrokeLen)

        Schriftzug7 += Signatur + bend_top + downstroke





    if version == "c":

        # Signatur = drawSchriftzug5(x, y, "a", instroke=False)
        # bend_top = drawSchriftteil5(*Raute_d, "1")
        # downstroke = drawGrundelementF(*bend_top.points[-1], 0.5)
        # bend_btm = drawSchriftteil4(Raute_d[0], Raute_d[1]-(x_height-1)*modul_height+1.8, "1")
        # con_stroke = drawGrundelementH(*bend_btm.points[0], 3)
        
        Signatur = drawSchriftzug5(x-0.4, y+0.8, "a", instroke=False)    #### Positionierung fragwürdig
        bend_top = drawSchriftteil5(*Raute_d, "1")
        downstroke = drawGrundelementF(*bend_top.points[-1], 0.5)
        bend_btm = drawSchriftteil4(Raute_d[0], Raute_d[1]-(x_height-1.3)*modul_height, "1")
        outstroke = drawOutstroke(*bend_btm.points[0], outstrokeLen)

        Schriftzug7 += Signatur + bend_top + downstroke + bend_btm
    
    
    Schriftzug7 += instroke + outstroke 
        
    
    drawPath(Schriftzug7)
    
    return Schriftzug7




#drawSchriftzug7(temp_x, temp_y, "a", instrokeLen=1, outstrokeLen=1)
#drawSchriftzug7(temp_x, temp_y, "b", instrokeLen=0, outstrokeLen=1)
#drawSchriftzug7(temp_x, temp_y, "c", instrokeLen=1, outstrokeLen=1)












def drawSchriftzug8(x, y, version="a", instrokeLen=0, outstrokeLen=3):
    
    Schriftzug8 = BezierPath()
    
    Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)

    instroke = drawInstroke(*Raute_a_top, instrokeLen)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)

    if version == "a":
        downstroke = drawGrundelementF(Raute_d[0], Raute_d[1] + 4.5*modul_height, 4.5)
        Signatur = drawGrundelementE(*Raute_a)
        Schriftzug8 += Signatur + downstroke
           
    if version == "b":
        downstroke = drawGrundelementF(Raute_d[0], Raute_d[1]+4.5*modul_height, 2.1)
        bend_btm = drawSchriftteil6(*Raute_a, "1")
        Signatur = drawSchriftzug5(x, y-3.5, "b", outstroke=False)
        Schriftzug8 += downstroke + bend_btm + Signatur

    if version == "c":
        bend_top = drawSchriftteil3(*Raute_a_top, "0.75")
        downstroke = drawGrundelementF(*bend_top.points[-1], 1.09)

        
        bend_btm = drawSchneckenzug(*downstroke.points[1], LOWER_B, 3, HSL_size=1, HSL_start=8.2, clockwise=True, inward=False)
        
        Signatur = drawSchneckenzug(*bend_btm.points[-1], UPPER_G, 2, HSL_size=1, HSL_start=12.1, clockwise=True, inward=False)       

        Schriftzug8 += bend_top + bend_btm + downstroke + Signatur 
        
        #Raute unten
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+0.4, y-3.5)


    outstroke = drawOutstroke(*Raute_d, outstrokeLen)
    
    Schriftzug8 += instroke + outstroke
    drawPath(Schriftzug8) 
    return Schriftzug8


# drawSchriftzug8(temp_x, temp_y, "a") 
# drawSchriftzug8(temp_x, temp_y, "b") 
# drawSchriftzug8(temp_x, temp_y, "c") 







def drawSchriftzug9(x, y, version="a",  instrokeLen=0, outstrokeLen=0):
    
    Schriftzug9 = BezierPath()
    
    #Raute oben
    Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    instroke = drawInstroke(*Raute_a_top, instrokeLen)


    #Raute unten
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
    
   
        
    if version == "a":

        downstroke = drawGrundelementF(Raute_d[0], Raute_d[1]+4.5*modul_height, 2.1)
        bend_btm = drawSchriftteil6(*Raute_a, "1")
        Signatur = drawSchriftzug5(x, y-3.5, "b", outstroke=False)

        #Raute oben Abstand definieren
        Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+3.5, y)
        Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+2.5, y-0.5)
        
        
        #oberer Teil der Kurve
        HSL_size = 5
        HSL_start = 50

        E1, E2 = line_E_vonF_o_kl_s(Raute_a_top, *angles, part, HSL_size, HSL_start)
        D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
        C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        

        #unterer Teil der Kurve
        HSL_size_2 = 1
        HSL_start_2 = 10.3
        
        C4 = C2
        C3 = C4[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start_2),      C4[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start_2)
        #polygon(C3, C4)
        

        B1, B2 = line_B_vonC_o_kl(C3, *angles, part, HSL_size_2, HSL_start_2-HSL_size_2)
        A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size_2, HSL_start_2-HSL_size_2*2)
        H3, H4 = line_H_vonA_u_kl(A3, *angles, part, HSL_size_2, HSL_start_2-HSL_size_2*+3)
        
        Schriftzug9.arc(*drawKreisSeg(H3, HSL_start_2-HSL_size_2*3, angle_10, angle_9, True))
        Schriftzug9.arc(*drawKreisSeg(A3, HSL_start_2-HSL_size_2*2, angle_9, angle_8, True))
        Schriftzug9.arc(*drawKreisSeg(B1, HSL_start_2-HSL_size_2, angle_8, angle_7, True))


        Schriftzug9.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_6, True))
        Schriftzug9.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_6, angle_5, True))
        

        Schriftzug9 += downstroke + bend_btm + Signatur



    if version == "b":            ### 2.5 Abstand ist standard.

        downstroke = drawGrundelementF(Raute_d[0], Raute_d[1] + 4.5*modul_height, 4.5)
               
        #Raute oben Abstand definieren
        Raute_a_top, Raute_b_top, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+3.5, y)
        Raute_a_top, Raute_b_top, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+2, y-0.75)

        
        HSL_size = 10
        HSL_start = 47.1

        E1, E2 = line_E_vonF_o_kl_s(Raute_a_top, *angles, part, HSL_size, HSL_start)
        D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)      
        C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
         
        Schriftzug9.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_10, angle_8, True))  ### hier geschummelt
        Schriftzug9.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_8, angle_7, True))
        Schriftzug9.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_6, True))
        Schriftzug9.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_6, angle_5, True))
        
          
    
    
    if version == "b-2":        ### für p mit nur zwei Modul Abstand.

        downstroke = drawGrundelementF(Raute_d[0], Raute_d[1] + 4.5*modul_height, 4.5)
               
        #Raute oben Abstand definieren
        Raute_a_top, Raute_b_top, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+3, y)
        Raute_a_top, Raute_b_top, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+1.5, y-0.75)

        
        HSL_size = 6.25
        HSL_start = 35.8

        E1, E2 = line_E_vonF_o_kl_s(Raute_a_top, *angles, part, HSL_size, HSL_start)
        D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)      
        C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
         
        Schriftzug9.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_10, angle_8, True))  ### hier geschummelt
        Schriftzug9.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_8, angle_7, True))
        Schriftzug9.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_6, True))
        Schriftzug9.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_6, angle_5, True))
 
 
 
 
        
    if version == "b-3":        ### für r 

        downstroke = drawGrundelementF(Raute_d[0], Raute_d[1] + 4.5*modul_height, 4.5)
               
        #Raute oben Abstand definieren
        Raute_a_top, Raute_b_top, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+3, y)
        Raute_a_top, Raute_b_top, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+2.5, y)
        Raute_a_top, Raute_b_top, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+2, y-0.25)

        
        HSL_size = 5.35
        HSL_start = 38

        E1, E2 = line_E_vonF_o_kl_s(Raute_a_top, *angles, part, HSL_size, HSL_start)
        D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)      
        C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
         
        Schriftzug9.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_9, angle_8, True))
        Schriftzug9.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_8, angle_7, True))
        Schriftzug9.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_6, True))
        Schriftzug9.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_6, angle_5, True))
        
        
        
           
            
    outstroke = drawInstroke(*Raute_a_top, outstrokeLen, "down")  ### Verlängert die Kurve oben
    
    Schriftzug9 += instroke + downstroke + outstroke
    
    
    drawPath(Schriftzug9)
    
    return Schriftzug9


# drawSchriftzug9(temp_x, temp_y, "a", 0, 0) 
# drawSchriftzug9(temp_x, temp_y, "b", 1, 1.5)
# drawSchriftzug9(temp_x, temp_y, "b-2", 1, 1.5)        
# #drawSchriftzug9(temp_x, temp_y, "b-3", 1, 0.5)        









def drawSchriftzug10(x, y, version="a", instrokeLen=1, outstrokeLen=0):
    
    Schriftzug10 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    instroke = drawInstroke(*Raute_a, instrokeLen)
    oben = drawSchneckenzug(*Raute_a, UPPER_H, 3, HSL_size=0, HSL_start=12.4, clockwise=True, inward=False)
 
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+0.4, y-3.5)
    unten = drawSchneckenzug(*Raute_d, LOWER_H, 3, HSL_size=0, HSL_start=12.4, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Raute_d, outstrokeLen)
    
    Schriftzug10.line(oben.points[-1], unten.points[-1])
    Schriftzug10 += instroke + oben + unten + outstroke
    drawPath(Schriftzug10)
    return Schriftzug10

#drawSchriftzug10(temp_x, temp_y)





def drawSchriftzug11(x, y, instroke=False):
    
    Schriftzug11 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    downstroke = drawGrundSchriftzug(x, y)
    

    
    ### left side up
    
    HSL_size = 12
    HSL_start = 52

    B2 = Raute_a
    B1 = B2[0] + (part*HSL_start),     B2[1]
    #polygon(B1, B2)

    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug11.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_8, angle_7, True))

    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug11.arc(*drawKreisSeg(D1, HSL_start-HSL_size*2, angle_7, angle_6, True))

    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug11.arc(*drawKreisSeg(E1, HSL_start-HSL_size*3, angle_6, angle_5, True))

    # Diesen Wert festhalten für Schnittpunkt später
    E4 = E2

    
    ### right side up
    
    HSL_size = 21.2      # 2 1/5 BM
    HSL_start = 38.5

    E2 = Raute_a
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start), E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    polygon(E1, E2)

    C1 = E1
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(C1, C2)

    B1 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size) 
    B2 = B1[0] + (part*(HSL_start-HSL_size)),  B1[1]
    polygon(B1, B2)
    
    
    ### Wendung oben
    
    CP = E4[0]+4.1, E4[1]+2
    #text("CP", CP) 
    Schriftzug11.moveTo(E4)
    Schriftzug11.curveTo(CP, B2)
    
    Schriftzug11.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_16, angle_15, True))
    Schriftzug11.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_13, True))



    Schriftzug11 += downstroke
    
    drawPath(Schriftzug11)
    
    return Schriftzug11 
 
   
#drawSchriftzug11(temp_x, temp_y)






def drawSchriftzug12(x, y, instroke=False, outstroke=False):
    
    Schriftzug12 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    stroke_xheight = drawGrundSchriftzug(x, y)
    
    HSL_size = 2  
    HSL_start = 17.5

    B2 = Raute_a
    B1 = B2[0] + (part*HSL_start),     B2[1]
    #polygon(B1, B2)
    
    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug12.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_8, angle_7, True))

    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug12.arc(*drawKreisSeg(D1, HSL_start-HSL_size*2, angle_7, angle_6, True))

    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug12.arc(*drawKreisSeg(E1, HSL_start-HSL_size*3, angle_6, angle_5, True))
    
    
    if instroke == True:
        instroke = drawInstroke(*E2, 0.5, "down")
        Schriftzug12 += instroke

    if outstroke == True:
        outstroke = drawOutstroke(*stroke_xheight.points[1], 1)
        Schriftzug12 += outstroke

    Schriftzug12 += stroke_xheight

    drawPath(Schriftzug12)
    
    return Schriftzug12 
 
   
#drawSchriftzug12(temp_x, temp_y)








def drawSchriftzug13(x, y, instrokeLen=1, version="standard"):
    
    #x -= 1.25
    Schriftzug13 = BezierPath()

    # untere Raute auf x-Höhe
    Raute_a_unten, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x, y)
    downstroke = drawGrundSchriftzug(x, y)        
    
    Raute_a_oben, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x, y+0.25)
    
    if version == "standard":
        bend = drawSchneckenzug(*Raute_a_oben, UPPER_B, 3, HSL_size=4, HSL_start=32, clockwise=True, inward=True)  
    if version == "germandbls":
        bend = drawSchneckenzug(*Raute_a_oben, UPPER_B, 3, HSL_size=4, HSL_start=30.5, clockwise=True, inward=True) 
    if version == "short":
        Raute_a_oben, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x, y)
        bend = drawSchneckenzug(*Raute_a_oben, UPPER_B, 3, HSL_size=4, HSL_start=29, clockwise=True, inward=True)
            
    Schriftzug13.line(Raute_a_unten, Raute_a_oben)
    instroke = drawOutstroke(*bend.points[-1], instrokeLen)
    
    Schriftzug13 += downstroke + bend + instroke
    drawPath(Schriftzug13)
    return Schriftzug13 
 
#drawSchriftzug13(temp_x, temp_y, version="short")








def drawSchriftzug14(x, y, instroke=False):
    
    Schriftzug14 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)

    Signatur = drawGrundelementE(*Raute_a)


    # Raute unten 
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
    
    downstroke = drawGrundSchriftzug(x, y)
    

    
    ### right side down
    
    HSL_size = 12
    HSL_start = 52

    B2 = Raute_d
    B1 = B2[0] - (part*HSL_start),     B2[1]
    polygon(B1, B2)

    C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug14.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_16, angle_15, True))

    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug14.arc(*drawKreisSeg(D1, HSL_start-HSL_size*2, angle_15, angle_14, True))

    E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug14.arc(*drawKreisSeg(E1, HSL_start-HSL_size*3, angle_14, angle_13, True))

    # Diesen Wert festhalten für Schnittpunkt später
    E4 = E2

    
    ### left side up/down
    
    HSL_size = 21.2      # 2 1/5 BM
    HSL_start = 38.5

    E2 = Raute_d
    E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start), E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    polygon(E1, E2)

    C1 = E1
    C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(C1, C2)

    B1 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_size) 
    B2 = B1[0] - (part*(HSL_start-HSL_size)),  B1[1]
    polygon(B1, B2)
    
    
    ### Wendung unten
    
    CP = E4[0]-4.1,  E4[1]-1.5
    #text("CP", CP) 
    Schriftzug14.moveTo(E4)
    Schriftzug14.curveTo(CP, B2)
    
    Schriftzug14.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_8, angle_7, True))
    Schriftzug14.arc(*drawKreisSeg(C1, HSL_start, angle_7, angle_5, True))

    Schriftzug14 += Signatur + downstroke

    drawPath(Schriftzug14)
    
    return Schriftzug14 
 
   
#drawSchriftzug14(temp_x, temp_y)






def drawSchriftzug15(x, y, instroke=False):
    
    Schriftzug15 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    downstroke = drawSchriftzug8(x, y, "b")
    

    
    ### left side up
    
    HSL_size = 12
    HSL_start = 52

    B2 = Raute_a
    B1 = B2[0] + (part*HSL_start),     B2[1]
    #polygon(B1, B2)

    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schriftzug15.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_8, angle_7, True))

    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schriftzug15.arc(*drawKreisSeg(D1, HSL_start-HSL_size*2, angle_7, angle_6, True))

    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schriftzug15.arc(*drawKreisSeg(E1, HSL_start-HSL_size*3, angle_6, angle_5, True))

    # Diesen Wert festhalten für Schnittpunkt später
    E4 = E2

    
    ### right side up
    
    HSL_size = 21.2      # 2 1/5 BM
    HSL_start = 38.5

    E2 = Raute_a
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start), E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    polygon(E1, E2)

    C1 = E1
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(C1, C2)

    B1 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size) 
    B2 = B1[0] + (part*(HSL_start-HSL_size)),  B1[1]
    polygon(B1, B2)
    
    
    ### Wendung oben
    
    CP = E4[0]+4.1, E4[1]+2
    #text("CP", CP) 
    Schriftzug15.moveTo(E4)
    Schriftzug15.curveTo(CP, B2)
    
    Schriftzug15.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_16, angle_15, True))
    Schriftzug15.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_13, True))


    
    Schriftzug15 += downstroke
    
    drawPath(Schriftzug15)
    
    return Schriftzug15 
 
   
#drawSchriftzug15(temp_x, temp_y)

#temp_x=5






def drawSchriftzug16(x, y, instroke=False):
    
    Schriftzug16 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    untererTeil = drawSchriftzug8(x, y, "c")


    
    drawGrundelOrient(A1, A2, offset, x, y+1)
    drawGrundelOrient(A1, A2, offset, x, y+2)
    drawGrundelOrient(A1, A2, offset, x, y+3)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+3.5)
    
    StartPunkt = Raute_a        # Wert für später aufheben
    
    

    ### left side down curve down
    
    HSL_size = 3
    HSL_start = 20

    A4 = StartPunkt[0]+offsetDir[0]/2, StartPunkt[1]-offsetDir[1]/2
    A3 = A4[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      A4[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(A3, A4)
    
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug16.arc(*drawKreisSeg(A3, HSL_start, angle_9, angle_8, True))
    Schriftzug16.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_8, angle_7, True))
    Schriftzug16.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_6, True))
    Schriftzug16.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_6, angle_5, True))

    

    ### right side curve up
    
    HSL_size = 21.2   
    HSL_start = 38.5

    E2 = StartPunkt
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start), E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start) 
    polygon(E1, E2)

    C1 = E1
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(C1, C2)

    B1 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size) 
    B2 = B1[0] + (part*(HSL_start-HSL_size)),  B1[1]
    polygon(B1, B2)
    
    
    
    ### Wendung oben
    
    CP = Grund_b[0]+3.9, Grund_b[1]+1.8
    #text("CP", CP) 
    Schriftzug16.moveTo(Grund_b)
    Schriftzug16.curveTo(CP, B2)
    
    
    
    ### right side curve up –– BezierPath zeichnen (damit Reihenfolge passt)
    
    Schriftzug16.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_16, angle_15, True))
    Schriftzug16.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_13, True))


    
    Schriftzug16 += untererTeil
    
    drawPath(Schriftzug16)
    
    return Schriftzug16 
 
   
#drawSchriftzug16(temp_x, temp_y)






def drawSchriftzug17(x, y, version="standard", instroke=False, outstrokeLen=0):
    
    Schriftzug17 = BezierPath()

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)

    StartSchleife = Raute_a
    
    drawGrundelOrient(A1, A2, offset, x, y+1)
    drawGrundelOrient(A1, A2, offset, x, y+2)
    drawGrundelOrient(A1, A2, offset, x, y+3)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+3.5)
    
    StartPunkt = Raute_a        # Wert für später aufheben


    ### Schleife oben –– left side down curve down
    
    HSL_size = 3
    HSL_start = 20

    StartPunkt_2 = StartPunkt[0]+offsetDir[0]/2, StartPunkt[1]-offsetDir[1]/2
    
    A3, A4 = line_A_vonH_u_kl_s(StartPunkt_2, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug17.arc(*drawKreisSeg(A3, HSL_start, angle_9, angle_8, True))
    Schriftzug17.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_8, angle_7, True))
    Schriftzug17.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_6, True))
    Schriftzug17.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_6, angle_5, True))

    

    ### right side 
    if version == "standard":
    
        HSL_size = 21.2   
        HSL_start = 38.5


        E1, E2 = line_E_vonF_u_kl_s(StartPunkt, *angles, part, HSL_size, HSL_start)

        C1 = E1
        C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
        polygon(C1, C2)

        B1 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size) 
        B2 = B1[0] + (part*(HSL_start-HSL_size)),  B1[1]
        polygon(B1, B2)
    
    
    
        ### Schleife –– Wendung oben
    
        CP = Grund_b[0]+3.6, Grund_b[1]+1.2
        #text("CP", CP) 
        Schriftzug17.moveTo(Grund_b)
        Schriftzug17.curveTo(CP, B2)
    
    
        ### Schleife –– right side curve up >>> BezierPath zeichnen (damit Reihenfolge passt)
    
        Schriftzug17.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_16, angle_15, True))
        Schriftzug17.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_13, True))


    ### right side 
    if version == "con":
    
        HSL_size = 20      # 2 1/5 BM
        HSL_start = 55 

        instroke = drawConstroke(*StartSchleife, "H", 1.5)

        D1, D2 = line_D_vonE_u_kl_s(instroke.points[1], *angles, part, HSL_size, HSL_start)
        C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size)
        B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    
    
        ### Wendung oben
        CP = Grund_b[0]+3.6, Grund_b[1]+1.2
        #text("CP", CP) 
        Schriftzug17.moveTo(Grund_b)
        Schriftzug17.curveTo(CP, B2)
    
        Schriftzug17.arc(*drawKreisSeg(B1, HSL_start-HSL_size*2, angle_16, angle_15, True))
        Schriftzug17.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_15, angle_14, True))
        Schriftzug17 += instroke


    ### unterer Teil
    btm_curveTop = drawSchneckenzug(*A4, UPPER_A, 2, HSL_size=4, HSL_start=23.5, clockwise=True, inward=True)
    # Raute unten
    _, _, _, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+0.5, y-3.5)
    btm_curveBtm = drawSchneckenzug(*Raute_d, LOWER_H, 3, HSL_size=1, HSL_start=13, clockwise=True, inward=True)
    Schriftzug17.line(btm_curveTop.points[-1],btm_curveBtm.points[-1])
    outstroke = drawOutstroke(*Raute_d, outstrokeLen)
    
    Schriftzug17 += btm_curveTop + btm_curveBtm + outstroke
    drawPath(Schriftzug17) 
    return Schriftzug17 
 
   
# drawSchriftzug17(temp_x, temp_y, version="standard")
# drawSchriftzug17(temp_x, temp_y, version="con")









def drawSchriftzug18(x, y, outstroke=False):
    
    Schriftzug18 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        
    drawGrundelOrient(A1, A2, offset, x, y+1)
    drawGrundelOrient(A1, A2, offset, x, y+2)
    drawGrundelOrient(A1, A2, offset, x, y+3)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+3.5)
    
    StartWendung = Grund_b
    #text("Grund_b", Grund_b)
    
    
    
    ### Schleife –– left side down
    
    HSL_size = 6.5
    HSL_start = 40

    E1, E2 = line_E_vonF_o_kl_s(Grund_b, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    
    
    StartPunkt = Raute_a
    text("StartPunkt", StartPunkt)
    

    Schriftzug18.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    Schriftzug18.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    Schriftzug18.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    Schriftzug18.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_8, angle_9))




    # Bogen zwischen x-Höhe und Baseline
    HSL_size = 6
    HSL_start = 31
    
    A3, A4 = line_A_vonH_o_kl_s(StartPunkt, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    
    
    # Rauten unten rechts
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)

    Schriftzug18.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_1, angle_0, True))
    Schriftzug18.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_16, angle_15, True))
    Schriftzug18.arc(*drawKreisSeg(D1, HSL_start-HSL_size*3, angle_15, angle_14, True))
    Schriftzug18.arc(*drawKreisSeg(E1, HSL_start-HSL_size*4, angle_14, angle_13, True))

 
        
    
    # Signatur unten (Fuß)
    HSL_size = 2
    HSL_start = 10
    
    B1, B2 = line_B_vonC_u_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)
    A5, A6 = line_A_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)
    H1, H2 = line_H_vonA_o_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*2)

    
    # Raute unten links
    drawRauteOrientKurBtm(offset, offsetDir, x-0.125, y-3.25)

    Schriftzug18.arc(*drawKreisSeg(A5, HSL_start+HSL_size, angle_2, angle_1, True))
    Schriftzug18.arc(*drawKreisSeg(B1, HSL_start, angle_1, angle_0, True))



    if outstroke == True:
        outstroke = drawOutstroke(*Raute_d, 1)
        Schriftzug18 += outstroke
        
    
    ### Schleife –– right side 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1.5, y+1.75)

    StartSchleife = Grund_c        # Wert für später aufheben
    #text("StartSchleife", StartSchleife)
    
    
    HSL_size = 2
    HSL_start = 19.6

    E1, E2 = line_E_vonF_u_kl_s(StartSchleife, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)

    
    
    ### Schleife –– Wendung oben
    
    CP = StartWendung[0]+9.8, StartWendung[1]+3
    #text("CP", CP) 
    Schriftzug18.moveTo(StartWendung)
    Schriftzug18.curveTo(CP, B2)
    
    
    
    
    ### neuer Pfad anfangen
    Schriftzug18_SchleifeOffen = BezierPath()
    
    Schriftzug18_SchleifeOffen.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_13, angle_14))
    Schriftzug18_SchleifeOffen.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_14, angle_15))
    Schriftzug18_SchleifeOffen.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_15, angle_16))


        
    
    Schriftzug18 += Schriftzug18_SchleifeOffen
    
    drawPath(Schriftzug18)
    
    return Schriftzug18 
 
   
#drawSchriftzug18(temp_x, temp_y)








def drawSchriftzug19(x, y, version="a", outstrokeLen=0):
    
    Schriftzug19 = BezierPath()

    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x, y) ### x-Höhe
    drawGrundelOrient(A1, A2, offset, x, y+1)
    drawGrundelOrient(A1, A2, offset, x+1, y+2)

    if version == "a":
        #text("StartPunkt", StartPunkt)        
        Bogen_oben = drawSchneckenzug(*Raute_a, LOWER_A, 4, HSL_size=3, HSL_start=7.2, clockwise=True, inward=False)
        Bogen_mitte = drawSchneckenzug(*Raute_a, UPPER_A, 4, HSL_size=6, HSL_start=31.25, clockwise=True, inward=True)
        _, _, _, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)  ### Rauten unten rechts
        drawRauteOrientKurBtm(offset, offsetDir, x-0.125, y-3.25)   ### Raute unten links
        Signatur = drawSchneckenzug(*Raute_d, LOWER_B, 2, HSL_size=8, HSL_start=7.7, clockwise=False, inward=False)

        Schriftzug19 += Bogen_oben + Bogen_mitte + Signatur

    if version == "b":
    
        StartPunkt = Raute_a[0] + offsetDir[0]*0.75, Raute_a[1]-offsetDir[1]*0.75
        #text("StartPunkt", StartPunkt)
    
        Bogen_oben = drawSchneckenzug(*StartPunkt, LOWER_A, 4, HSL_size=1, HSL_start=13.5, clockwise=True, inward=False)
        Bogen_mitte = drawSchneckenzug(*StartPunkt, UPPER_A, 2, HSL_size=2, HSL_start=15, clockwise=True, inward=True)
        _, _, _, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+0.3, y-3.5)  ### Raute unten
        Bogen_unten = drawSchneckenzug(*Raute_d, LOWER_A, 2, HSL_size=2, HSL_start=18.2, clockwise=True, inward=False)
        
        Schriftzug19 += Bogen_oben + Bogen_mitte + Bogen_unten

    if version == "short": ### for f_f_t Ligature
        
        StartPunkt = Raute_a[0] + offsetDir[0]*0.75, Raute_a[1]-offsetDir[1]*0.75
        #text("StartPunkt", StartPunkt)
    
        Bogen_oben = drawSchneckenzug(*StartPunkt, LOWER_A, 4, HSL_size=1, HSL_start=10.25, clockwise=True, inward=False)
        Bogen_mitte = drawSchneckenzug(*StartPunkt, UPPER_A, 2, HSL_size=2, HSL_start=15, clockwise=True, inward=True)
        _, _, _, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+0.3, y-3.5)  ### Raute unten
        Bogen_unten = drawSchneckenzug(*Raute_d, LOWER_A, 2, HSL_size=2, HSL_start=18.2, clockwise=True, inward=False)
        
        Schriftzug19 += Bogen_oben + Bogen_mitte + Bogen_unten
        
            
    outstroke = drawOutstroke(*Raute_d, outstrokeLen)
    Schriftzug19 += outstroke
    drawPath(Schriftzug19)
    return Schriftzug19 
 
   
#drawSchriftzug19(temp_x, temp_y, "a")
#drawSchriftzug19(temp_x, temp_y, "b")
#drawSchriftzug19(temp_x, temp_y, "short")





#temp_x= 5

def drawSchriftzug20(x, y, instroke=False):
    
    Schriftzug20 = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        
    drawGrundelOrient(A1, A2, offset, x, y+1)
    drawGrundelOrient(A1, A2, offset, x, y+2)
    drawGrundelOrient(A1, A2, offset, x, y+3)
        
    backstroke = drawSchriftzug11(x, y)

    connection = drawSchriftzug9(x, y, "b")

    
    RefPoint = Raute_a[0]+3.5*modul_width, Raute_a[1]

    bend_top = drawSchriftteil3(*RefPoint, "0.75")

    Zwischenstueck = drawGrundelementF(*bend_top.points[-1], 1.75)



    ### Schleife unten
    
    # Raute zur Orientierung
    Grund_a, Grund_b, Grund_c, Grund_d = drawRauteOrientKurBtm(offset, offsetDir, x+0.4, y-7.5)
    
    
    ### right side down
    HSL_size = 0
    HSL_start = 40.8

    B1, B2 = line_B_vonA_u_gr_s(Zwischenstueck.points[1], *angles, part, HSL_size, HSL_start)

    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug20.arc(*drawKreisSeg(B1, HSL_start, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start)
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start)

    Schriftzug20.arc(*drawKreisSeg(C1, HSL_start, angle_15, angle_14, True))
    Schriftzug20.arc(*drawKreisSeg(D1, HSL_start, angle_14, angle_13, True))

   
    ### Wendung unten
    CP = Grund_a[0], Grund_a[1]-5.6
    #text("CP", CP) 
    Schriftzug20.moveTo(E2)
    Schriftzug20.curveTo(CP, Grund_a)


    ### left side up 
    HSL_size = 16
    HSL_start = 16
        
    B1, B2 = line_B_vonA_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    
    Schriftzug20.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_7, True))
    Schriftzug20.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_6, True))    
    Schriftzug20.arc(*drawKreisSeg(D1, HSL_start+HSL_size*2, angle_6, angle_5, True))
    
    
    outstroke = drawOutstroke(*E2, 2)
    
    
    Schriftzug20 += backstroke + connection + bend_top + Zwischenstueck + outstroke
    
    drawPath(Schriftzug20)
    
    return Schriftzug20 
 
   
#drawSchriftzug20(temp_x, temp_y)




temp_x= 5


def drawSchriftzug20_descender(x, y, version="fina", length=5, deviation=1):
    
    Schriftzug20_descender = BezierPath()

    # Raute auf x-Höhe
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    # Raute zur Orientierung
    drawRauteOrientKurTop(offset, offsetDir, x, y-5)
    drawRauteOrientKurTop(offset, offsetDir, x, y-6)
 

    bogen_rechts = drawSchneckenzug(*Raute_a, UPPER_A, 1, HSL_size=0, HSL_start=24, clockwise=True, inward=False)
    straight_down = drawGrundelementF(*bogen_rechts.points[-1], 1.8)    

    if version == "fina":
        right_side_down = drawSchneckenzug(*straight_down.points[-1], LOWER_B, 3, HSL_size=4, HSL_start=28, clockwise=True, inward=False)
            
    if version == "loop_A": 
        right_side_down = drawSchneckenzug(*straight_down.points[-1], LOWER_B, 3, HSL_size=4, HSL_start=28, clockwise=True, inward=False)
        wendung_unten = drawSchneckenzug(*right_side_down.points[-1], LOWER_E, 5, HSL_size=0.5, HSL_start=4, clockwise=True, inward=False)        
        left_up = drawSchneckenzug(*wendung_unten.points[-1], UPPER_B, 3, HSL_size=6.6, HSL_start=18, clockwise=True, inward=False)
        outstroke = drawOutstroke(*left_up.points[-1], length)
        Schriftzug20_descender += wendung_unten + left_up + outstroke
         
    if version == "loop_H": 
        right_side_down = drawSchneckenzug(*straight_down.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=32, clockwise=True, inward=False)
        wendung_unten = drawSchneckenzug(*right_side_down.points[-1], LOWER_E, 5, HSL_size=0.5, HSL_start=4, clockwise=True, inward=False)        
        left_up = drawSchneckenzug(*wendung_unten.points[-1], UPPER_B, 2, HSL_size=4, HSL_start=9, clockwise=True, inward=False)
        outstroke = drawConstroke(*left_up.points[-1], "H", length, deviation)
        Schriftzug20_descender += wendung_unten + left_up + outstroke
  
   
    Schriftzug20_descender += right_side_down + bogen_rechts + straight_down            
    drawPath(Schriftzug20_descender)
    return Schriftzug20_descender 
 
   
# drawSchriftzug20_descender(temp_x, temp_y, "fina")
# drawSchriftzug20_descender(temp_x, temp_y, "loop_A")
# drawSchriftzug20_descender(temp_x, temp_y, "loop_H", 5.5)






def drawSchriftzug21(x, y, version="standard", instrokeLen=1.06, outstrokeLen=0):
    
    Schriftzug21 = BezierPath()

    drawRauteOrientKurTop(offset, offsetDir, x+2, y)    
    drawRauteOrientKurBtm(offset, offsetDir, x, y-1.5)
    drawRauteOrientKurBtm(offset, offsetDir, x, y-2.5)
    Raute_a, _, _, _ = drawRauteOrientKurBtm(offset, offsetDir, x+1.4, y-0.3)    
    Raute_a, _, _, _ = drawRauteOrientKurBtm(offset, offsetDir, x+1.4, y-0.8)

    _, _, _, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)

    if version == "standard":
        Bogen = drawSchneckenzug(*Raute_d, LOWER_H, 5, HSL_size=7, HSL_start=7.42, clockwise=True, inward=False)
    if version == "for e":
        Bogen = drawSchneckenzug(*Raute_d, LOWER_A, 4, HSL_size=4, HSL_start=29.6, clockwise=True, inward=True)
    if version == "for o":
        Bogen = drawSchneckenzug(*Raute_d, LOWER_A, 4, HSL_size=4, HSL_start=16, clockwise=True, inward=False)

    outstroke = drawOutstroke(*Bogen.points[0], outstrokeLen)
    instroke = drawInstroke(*Bogen.points[-1], instrokeLen, "down")

    Schriftzug21 += Bogen + instroke + outstroke    
    drawPath(Schriftzug21)    
    return Schriftzug21 
 
#drawSchriftzug21(temp_x, temp_y, version="standard")
#drawSchriftzug21(temp_x, temp_y, version="for e")
#drawSchriftzug21(temp_x, temp_y, version="for o")







def drawSchriftzug22(x, y, instroke=True):
    
    Schriftzug22 = BezierPath()
    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)

    
    if instroke == True:
        Bogen = drawHalbbogen4(*Raute_a, instroke=True)
            
    else: 
        Bogen = drawHalbbogen4(*Raute_a, instroke=False)


    Schriftzug22 += Bogen
    
    drawPath(Schriftzug22)
        
    return Schriftzug22
    
    
#drawSchriftzug22(temp_x, temp_y, instroke=True) 
    
    
    
    
    
    
    
    
def drawSchriftzug23(x, y):
    
    Schriftzug23 = BezierPath()

    # Rauten
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)

    HSL_size = 0.1
    HSL_start = 2.83
    
    E1, E2 = line_E_vonF_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug23.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

    C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug23.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

    B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug23.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

    A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug23.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))

    H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Schriftzug23.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_9, angle_10))

    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Schriftzug23.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*7)
    Schriftzug23.arc(*drawKreisSeg(G1, HSL_start+HSL_size*6, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*8)
    Schriftzug23.arc(*drawKreisSeg(F1, HSL_start+HSL_size*7, angle_12, angle_13))
    
    
    drawPath(Schriftzug23)
    
    return Schriftzug23 
 
   
#drawSchriftzug23(temp_x, temp_y)







def drawSchriftzug24(x, y, version="a"):
    
    Schriftzug24 = BezierPath()


    if version == "a":
        
        # Rauten
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+0.2, y-0.4)
        drawGrundelOrient(A1, A2, offset, x, y-0.5)


        HSL_size = 0.1
        HSL_start = 4.45
        
        E1, E2 = line_E_vonF_u_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)
        
        D1, D2 = line_D_vonE_u_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug24.arc(*drawKreisSeg(E1, HSL_start, angle_13, angle_14))

        C1, C2 = line_C_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug24.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_14, angle_15))

        B1, B2 = line_B_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        Schriftzug24.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_16))

        A3, A4 = line_A_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        Schriftzug24.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_0, angle_1))

        H1, H2 = line_H_vonA_o_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
        Schriftzug24.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_1, angle_2))

        G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
        Schriftzug24.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_2, angle_3))

        F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*7)
        Schriftzug24.arc(*drawKreisSeg(G1, HSL_start+HSL_size*6, angle_3, angle_4))
        
        E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*8)
        Schriftzug24.arc(*drawKreisSeg(F1, HSL_start+HSL_size*7, angle_4, angle_5))

        
        
        
        # x = F2[1]-F1[1]
        # y = 7.4              # Höhenunterschied / Versatz nach oben. 
        #                      # Diesen Wert verstellen um Kurve zu beeinflussen.
   
        # HSL_start = x+y

        # E1 = F1[0],            F1[1] -(y)
        # E2 = E1[0] - m.sin(m.radians(angle_seg_1)) * (HSL_start) , E1[1] + m.cos(m.radians(angle_seg_1)) * (HSL_start)
        # #polygon(E1, E2)    # ausgeblendet
        # #Schriftzug24.arc(*drawKreisSeg(E1, HSL_start/part, angle_4, angle_5))
        
        
        
        
    if version == "b":
                
        # Rauten
        drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x-0.2, y-3.1)
        drawGrundelOrient(A1, A2, offset, x, y-3)

        HSL_size = 0.1
        HSL_start = 4.45
        
        E1, E2 = line_E_vonF_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
        
        D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug24.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

        C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug24.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

        B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        Schriftzug24.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

        A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        Schriftzug24.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))

        H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
        Schriftzug24.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_9, angle_10))

        G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
        Schriftzug24.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))

        F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*7)
        Schriftzug24.arc(*drawKreisSeg(G1, HSL_start+HSL_size*6, angle_11, angle_12))
        
        E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*8)
        Schriftzug24.arc(*drawKreisSeg(F1, HSL_start+HSL_size*7, angle_12, angle_13))

        # x = F1[1]-F2[1]
        # y = 7.4             # Höhenunterschied / Versatz nach oben. 
        #                     # Diesen Wert verstellen um Kurve zu beeinflussen.
   
        # HSL_start = x+y

        # E1 = F1[0],            F1[1] +(y)
        # E2 = E1[0] + m.sin(m.radians(angle_seg_1)) * (HSL_start) , E1[1] - m.cos(m.radians(angle_seg_1)) * (HSL_start)
        # polygon(E1, E2)    # ausgeblendet
        # Schriftzug24.arc(*drawKreisSeg(E1, HSL_start/part, angle_12, angle_13))
    
    
    drawPath(Schriftzug24)
    
    return Schriftzug24 
 
   
#drawSchriftzug24(temp_x, temp_y, "a")
#drawSchriftzug24(temp_x, temp_y, "b")













def drawSchriftzug25(x, y, instrokeLen=1.3, outstrokeLen=0):
    
    Schriftzug25 = BezierPath()

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-1.5)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-2.5)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)
            
    HSL_size = 4
    HSL_start = 16
    
    A1, A2 = line_A_vonH_u_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        
    instroke = drawInstroke(*E2, instrokeLen, "down")

    Schriftzug25.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_5, angle_6))
    Schriftzug25.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_6, angle_7))
    Schriftzug25.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_7, angle_8))
    Schriftzug25.arc(*drawKreisSeg(A1, HSL_start, angle_8, angle_9))

    outstroke = drawOutstroke(*Raute_d, outstrokeLen)

    Schriftzug25 += instroke + outstroke 
    drawPath(Schriftzug25)
    return Schriftzug25 
 
#drawSchriftzug25(temp_x, temp_y)






def drawSchriftzug26(x, y, version="standard", instrokeLen=1, outstrokeLen=0):
    
    Schriftzug26 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    drawRauteOrientKurTop(offset, offsetDir, x, y-1)
    drawRauteOrientKurTop(offset, offsetDir, x, y-2)
    drawRauteOrientKurTop(offset, offsetDir, x, y-2.5)
    drawRauteOrientKurTop(offset, offsetDir, x-1.5, y-3.25)
    drawRauteOrientKurTop(offset, offsetDir, x-2, y-3)

    instroke = drawInstroke(*Raute_a, instrokeLen)   


    if version == "standard":
        HSL_size = 4
        HSL_start = 16
        
    if version == "3.5":
        HSL_size = 3
        HSL_start = 20
        

    A1, A2 = line_A_vonH_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug26.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_0,True))
    Schriftzug26.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    Schriftzug26.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
    Schriftzug26.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E2, outstrokeLen, "down")

    Schriftzug26 += instroke + outstroke
    drawPath(Schriftzug26)
    return Schriftzug26
    
#drawSchriftzug26(temp_x, temp_y)    







def drawSchriftzug27(x, y):
    
    Schriftzug27 = BezierPath()
    
    # Raute
    drawRauteOrientKurTop(offset, offsetDir, x, y+1)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    drawRauteOrientKurTop(offset, offsetDir, x, y-1)
    drawRauteOrientKurTop(offset, offsetDir, x, y-2)

    
    ### Spitze oben
    HSL_size = 0.25
    HSL_start = 6
    
    A1, A2 = line_A_vonH_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug27.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))
    Schriftzug27.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))
    Schriftzug27.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftzug27.arc(*drawKreisSeg(A1, HSL_start, angle_2, angle_1, True))

    
    ### Mittel bzw. Hauptteil
    HSL_size = 4
    HSL_start = 16
    
    A1, A2 = line_A_vonH_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        
    Schriftzug27.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_0,True))
    Schriftzug27.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    Schriftzug27.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
    Schriftzug27.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))



    ### dünnes Verzierungselement    # AB HIER DÜNN!!!
    Schriftzug27_thin = BezierPath()

    HSL_size = 2
    HSL_start = 14

    E3, E4 = line_E_vonF_o_kl_s(E2, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E3, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    G1, G2 = line_G_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*7)

    Schriftzug27_thin.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    Schriftzug27_thin.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    Schriftzug27_thin.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    Schriftzug27_thin.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_8, angle_9))
    Schriftzug27_thin.arc(*drawKreisSeg(H1, HSL_start-HSL_size*5, angle_9, angle_10))
    Schriftzug27_thin.arc(*drawKreisSeg(G1, HSL_start-HSL_size*6, angle_10, angle_11))
    Schriftzug27_thin.arc(*drawKreisSeg(F1, HSL_start-HSL_size*7, angle_11, angle_12))

    drawPath(Schriftzug27_thin)
        
    drawPath(Schriftzug27)
        
    return Schriftzug27
    
    
#drawSchriftzug27(temp_x, temp_y)   












def drawSchriftzug28(x, y, instroke=False):
    
    Schriftzug28 = BezierPath()

    # Raute
    drawRauteOrientKurTop(offset, offsetDir, x, y+1)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    drawRauteOrientKurTop(offset, offsetDir, x, y-1)
    drawRauteOrientKurTop(offset, offsetDir, x, y-2)

    
    ### Spitze oben
    HSL_size = 0.25
    HSL_start = 6
    
    A1, A2 = line_A_vonH_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)    
    H1, H2 = line_H_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug28.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))
    Schriftzug28.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))
    Schriftzug28.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftzug28.arc(*drawKreisSeg(A1, HSL_start, angle_2, angle_1, True))

    
    ### Mittel bzw. Hauptteil
    HSL_size = 1
    HSL_start = 22
    
    A1, A2 = line_A_vonH_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        
    Schriftzug28.arc(*drawKreisSeg(A1, HSL_start, angle_1, angle_0,True))
    Schriftzug28.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))
    Schriftzug28.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))
    Schriftzug28.arc(*drawKreisSeg(D1, HSL_start+HSL_size*3, angle_14, angle_13, True))
    E2_festgehalten = E2

    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x-0.8, y-1.4)
 
    # Strich nach unten auf linker Seite
    HSL_size = 8
    HSL_start = 36
    
    E1, E2 = line_E_vonF_o_kl_s(Raute_a, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    B1_festgehalten = B1


    # Wendung
    Schriftzug28.moveTo(E2_festgehalten)
    CP = B2[0]+0.5, B2[1]-6
    #text("CP", CP) 
    Schriftzug28.curveTo(CP, B2)
    
    
    # von oben noch fertig ziehen
    Schriftzug28.arc(*drawKreisSeg(B1_festgehalten, HSL_start-HSL_size*3, angle_8, angle_7, True))
    Schriftzug28.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_7, angle_6, True))
    Schriftzug28.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_6, angle_5, True))

    outstroke = drawOutstroke(*E2, 1.5)
    
    Schriftzug28 += outstroke
    
    drawPath(Schriftzug28)
    
    return Schriftzug28 
 
   
#drawSchriftzug28(temp_x, temp_y)











def drawSchriftzug29(x, y, instroke=False, version="isol"):  #isol als Option
    
    Schriftzug29 = BezierPath()
    
    drawRauteOrientKurTop(offset, offsetDir, x, y)   #zur Orientierung für Strich nach oben
    _, Grund_b, _, _ = drawGrundelOrient(A1, A2, offset, x-1.5, y+0.75)
    drawGrundelOrient(A1, A2, offset, x-1, y+1)  

        
        
    Spitze = drawSchneckenzug(*Grund_b, UPPER_C, 2, HSL_size=0, HSL_start=8, clockwise=False, inward=True)
    Einsatz = drawGrundelementE(*Spitze.points[-1])    
    rechts_runter = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=4, HSL_start=29.25, clockwise=True, inward=True)
    Wendung = drawSchneckenzug(*rechts_runter.points[-1], LOWER_E, 4, HSL_size=0.5, HSL_start=6.5, clockwise=True, inward=False)
    links_hoch = drawSchneckenzug(*Wendung.points[-1], LOWER_A, 4, HSL_size=7.9, HSL_start=5, clockwise=True, inward=False)
    outstroke = drawOutstroke(*links_hoch.points[-1], 2.6)
    Abschluss = drawGrundelementE(*outstroke.points[0])
    
    if version=="fina":
        instroke = drawGrundelementH(*Grund_b, 1.25, "down")
        Schriftzug29 += instroke
        
    if version=="iniH":
        outstroke_top = drawOutstroke(*Abschluss.points[2], 1.25)
        Schriftzug29 += outstroke_top
        
    if version=="midH":
        instroke = drawGrundelementH(*Grund_b, 1.25, "down")
        outstroke_top = drawOutstroke(*Abschluss.points[2], 1.25)
        Schriftzug29 += instroke + outstroke_top

    Schriftzug29 += Spitze + Einsatz + rechts_runter + Wendung + links_hoch + outstroke + Abschluss 
    drawPath(Schriftzug29)
    return Schriftzug29 
 
#drawSchriftzug29(temp_x, temp_y, version="mid")







def drawSchriftzug30(x, y, Spitze="long"):
    
    Schriftzug30 = BezierPath()

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y+1.5)

    
    ### Kurve rechts mittig-oben
    HSL_size = 2
    HSL_start = 24
    
    H1, H2 = line_H_vonG_o_kl_s(Raute_a, *angles, part, HSL_size, HSL_start)
    A7, A8 = line_A_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size)
    B1, B2 = line_B_vonA_u_kl(A7, *angles, part, HSL_size, HSL_start-HSL_size*2)
    
    Schriftzug30.arc(*drawKreisSeg(A7, HSL_start-HSL_size, angle_2, angle_1, True))
    Schriftzug30.arc(*drawKreisSeg(B1, HSL_start-HSL_size*2, angle_1, angle_0, True))

    ### Kurve rechts unten
    HSL_size = 2
    HSL_start = 22.4

    B3, B4 = line_B_vonA_u_gr_s(B2, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonB_u_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    
    Schriftzug30.arc(*drawKreisSeg(B3, HSL_start, angle_16, angle_15, True))
    Schriftzug30.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_15, angle_14, True))
    Schriftzug30.arc(*drawKreisSeg(D1, HSL_start+HSL_size*2, angle_14, angle_13, True))


    linker_Bogen = drawSchriftzug21(x-3, y, instrokeLen=0.25, outstrokeLen=2)
        
    ### Spitze oben
    Schriftzug30_Spitze = BezierPath()

    
    if Spitze == "short":
        HSL_size = 0.5
        HSL_start = 9       
        
    if Spitze == "long":
        HSL_size = 0.5
        HSL_start = 13.8
    
        
    H1, H2 = line_H_vonA_u_kl_s(Raute_a, *angles, part, HSL_size, HSL_start)
    A7, A8 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    B1, B2 = line_B_vonA_o_gr(A7, *angles, part, HSL_size, HSL_start+HSL_size*2)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*5)
   
    Schriftzug30_Spitze.arc(*drawKreisSeg(D1, HSL_start+HSL_size*4, angle_5, angle_6))
    Schriftzug30_Spitze.arc(*drawKreisSeg(C1, HSL_start+HSL_size*3, angle_6, angle_7))
    Schriftzug30_Spitze.arc(*drawKreisSeg(B1, HSL_start+HSL_size*2, angle_7, angle_8))
    Schriftzug30_Spitze.arc(*drawKreisSeg(A7, HSL_start+HSL_size, angle_8, angle_9))
    Schriftzug30_Spitze.arc(*drawKreisSeg(H1, HSL_start, angle_9, angle_10))
    
    Schriftzug30 += linker_Bogen + Schriftzug30_Spitze
    drawPath(Schriftzug30)
    return Schriftzug30 
 
   
#drawSchriftzug30(temp_x, temp_y, "long")






def drawSchriftzug31(x, y):
    
    Schriftzug31 = BezierPath()

    # Rauten
    drawRauteOrientKurTop(offset, offsetDir, x, y-1)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    drawRauteOrientKurBtm(offset, offsetDir, x, y+1)
    
    
    Schwung_oben = drawSchriftteil7(*Raute_a)
    Schwung_unten = drawHalbbogen3(*Raute_a)
    
    Schriftzug31 = Schwung_oben + Schwung_unten
    
    drawPath(Schriftzug31)
    
    return Schriftzug31
    
#drawSchriftzug31(temp_x, temp_y)





def drawSchriftzug32(x, y):
    
    Schriftzug32 = BezierPath()
    
    # Rauten
    drawRauteOrientKurTop(offset, offsetDir, x, y-1)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    drawRauteOrientKurBtm(offset, offsetDir, x, y+1)
    
    
    Schwung_oben = drawSchriftteil7(*Raute_a)
    Schwung_unten = drawHalbbogen2(*Raute_a)
    
    Schriftzug32 = Schwung_oben + Schwung_unten
    
    drawPath(Schriftzug32)
    
    return Schriftzug32
    
#drawSchriftzug32(temp_x, temp_y)









def drawSchriftzug33(x, y):
    
    Schriftzug33 = BezierPath()
    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    

    ### oberer Teil
    
    # 1. Element oben (Schriftteil 7, nur kleiner)
    HSL_size = 0
    HSL_start = 4

    A2 = Raute_a
    A1 = A2[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),     A2[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)    
    polygon(A1, A2)

    C1 = A1
    C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),     C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(C1, C2)
    Schriftzug33.arc(*drawKreisSeg(A1, HSL_start, angle_7, angle_9))
    
       
    ### unterer Teil (Schriftteil 8, nur kleiner)
    HSL_size = 1
    HSL_start = 4
    
    A2 = Raute_a
    A1 = A2[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),     A2[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(A1, A2)

    C1 = A1
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(C1, C2)
    Schriftzug33.arc(*drawKreisSeg(C1, HSL_start, angle_1, angle_15, True))

    
    
    drawPath(Schriftzug33)
    
    return Schriftzug33
    
    
#drawSchriftzug33(temp_x, temp_y)






def drawSchriftzug34(x, y):
    
    Schriftzug34 = BezierPath()
    
    Schwung_links = drawSchriftzug6(x, y, "a")
    
    Schwung_rechts = drawSchriftzug6(x+0.8, y-0.6, "b")

    Schriftzug34 = Schwung_links + Schwung_rechts
    
    drawPath(Schriftzug34)
    
    return Schriftzug34
    
#drawSchriftzug34(temp_x, temp_y)





def drawSchriftzug35(x, y):
    
    Schriftzug35 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)



    # Schwung_links (siehe Schriftzug 6, nur kleiner)
    
    HSL_size = 0.3
    HSL_start = 4
    
    A1, A2 = line_A_vonB_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug35.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))
    Schriftzug35.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))
    Schriftzug35.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftzug35.arc(*drawKreisSeg(A1, HSL_start, angle_2, angle_1, True))


    
    # Schwung_rechts
    
    A1, A2 = line_A_vonB_u_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)

    H1, H2 = line_H_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug35.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_10))
    
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug35.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug35.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
 
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug35.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))


    
    drawPath(Schriftzug35)
    
    return Schriftzug35
    
#drawSchriftzug35(temp_x, temp_y)






def drawSchriftzug36(x, y):
    
    Schriftzug36 = BezierPath()
    
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)

        
    # Schwung_links (siehe Schriftzug 6, nur kleiner)
    
    HSL_size = 0.125
    HSL_start = 3
    
    A1, A2 = line_A_vonB_o_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    Schriftzug36.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_5, angle_4, True))
    Schriftzug36.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_4, angle_3, True))
    Schriftzug36.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_3, angle_2, True))
    Schriftzug36.arc(*drawKreisSeg(A1, HSL_start, angle_2, angle_1, True))

    
    # Schwung_rechts
    
    A1, A2 = line_A_vonB_u_gr_s(Raute_a, *angles, part, HSL_size, HSL_start)

    H1, H2 = line_H_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug36.arc(*drawKreisSeg(A1, HSL_start, angle_9, angle_10))
    
    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug36.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug36.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_12))
 
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug36.arc(*drawKreisSeg(F1, HSL_start+HSL_size*3, angle_12, angle_13))
    
    
    drawPath(Schriftzug36)
    
    return Schriftzug36

#drawSchriftzug36(temp_x, temp_y)






def drawSchriftzug_backstroke_g(x, y, version="fina", deviation=1):
    
    drawSchriftzug_backstroke_g = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
 
        
    Signatur = drawSchriftzug3(x, y, "a", instroke=False)
    downstroke = drawGrundelementF(*Raute_a, 3.35)


    if version == "fina":

        # Übergang bend to right
        HSL_size = 1    #6
        HSL_start =  20   #24

        B1, B2 = line_B_vonC_o_gr_s(downstroke.points[-1], *angles, part, HSL_size, HSL_start)
        A1, A2 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)

        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_9))



        # Bogen unten 
        HSL_size = 2
        HSL_start = 24

        A3, A4 = line_A_vonH_o_kl_s(A2, *angles, part, HSL_size, HSL_start)
        B1, B2 = line_B_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size)    
        C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*2)    
        D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)    
        E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*4)

        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_1, angle_0, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_16, angle_15, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(D1, HSL_start-HSL_size*3, angle_15, angle_14, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(E1, HSL_start-HSL_size*4, angle_14, angle_13, True))




    if version == "loop_A": 
        
        # Übergang bend to right
        HSL_size = 6
        HSL_start = 20

        B1, B2 = line_B_vonC_o_gr_s(downstroke.points[-1], *angles, part, HSL_size, HSL_start)
        A1, A2 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)

        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_9))


        # Bogen rechts runter 
        HSL_size = 1
        HSL_start = 22 

        A3, A4 = line_A_vonH_o_kl_s(A2, *angles, part, HSL_size, HSL_start)
        B1, B2 = line_B_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size)    
        C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*2)    
        D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)    
        E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*4)

        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_1, angle_0, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_16, angle_15, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(D1, HSL_start-HSL_size*3, angle_15, angle_14, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(E1, HSL_start-HSL_size*4, angle_14, angle_13, True))
        

        ### Wendung unten
        HSL_size = 0.5
        HSL_start = 3
   
        E3, E4 = line_E_vonD_u_gr_s(E2, *angles, part, HSL_size, HSL_start)
        F1, F2 = line_F_vonE_u_gr(E3, *angles, part, HSL_size, HSL_start+HSL_size)
        G1, G2 = line_G_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        A5, A6 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        B1, B2 = line_B_vonA_o_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*5)
    
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(E3, HSL_start, angle_13, angle_12, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_11, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_10, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_10, angle_9, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(A5, HSL_start+HSL_size*4, angle_9, angle_8, True))



        # Hilfslinie und Hilfsraute zur Positionierung
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-4)


        ### left side up
        HSL_size = 2
        HSL_start = 24

        B3, B4 = line_B_vonA_o_gr_s(B2, *angles, part, HSL_size, HSL_start)    
        C1, C2 = line_C_vonB_o_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size)
        D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(B3, HSL_start, angle_8, angle_7, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_6, True))    
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(D1, HSL_start+HSL_size*2, angle_6, angle_5, True))
    
    
        constroke = drawConstroke(*E2, "A", 3, deviation)

        drawSchriftzug_backstroke_g += constroke
        
    
    
    
    if version == "loop_H": 

        # Übergang bend to right
        HSL_size = 1 ##6
        HSL_start = 20

        B1, B2 = line_B_vonC_o_gr_s(downstroke.points[-1], *angles, part, HSL_size, HSL_start)
        A1, A2 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)

        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_9))



        # Bogen rechts runter 
        HSL_size = 1
        HSL_start = 22 

        A3, A4 = line_A_vonH_o_kl_s(A2, *angles, part, HSL_size, HSL_start)
        B1, B2 = line_B_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size)    
        C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*2)    
        D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)    
        E1, E2 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*4)

        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(B1, HSL_start-HSL_size, angle_1, angle_0, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_16, angle_15, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(D1, HSL_start-HSL_size*3, angle_15, angle_14, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(E1, HSL_start-HSL_size*4, angle_14, angle_13, True))
        
        

    
        ### Wendung unten
        HSL_size = 0.75
        HSL_start = 4
       
        E3, E4 = line_E_vonD_u_gr_s(E2, *angles, part, HSL_size, HSL_start)
        F1, F2 = line_F_vonE_u_gr(E3, *angles, part, HSL_size, HSL_start+HSL_size)
        G1, G2 = line_G_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        A5, A6 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        B1, B2 = line_B_vonA_o_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*5)
    
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(E3, HSL_start, angle_13, angle_12, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_11, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_10, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_10, angle_9, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(A5, HSL_start+HSL_size*4, angle_9, angle_8, True))


        # Hilfslinie und Hilfsraute zur Positionierung
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-4)


        ### left side up
        HSL_size = 4
        HSL_start = 11

        B3, B4 = line_B_vonA_o_gr_s(B2, *angles, part, HSL_size, HSL_start)    
        C1, C2 = line_C_vonB_o_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size)
        D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(B3, HSL_start, angle_8, angle_7, True))
        drawSchriftzug_backstroke_g.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_6, True))    
    
        constroke = drawConstroke(*D2, "H", 6.3, deviation)

        drawSchriftzug_backstroke_g += constroke
              
    drawSchriftzug_backstroke_g += Signatur + downstroke 
    drawPath(drawSchriftzug_backstroke_g)
    return drawSchriftzug_backstroke_g


#drawSchriftzug_backstroke_g(temp_x, temp_y, version="fina")
#drawSchriftzug_backstroke_g(temp_x, temp_y, version="loop_A")
#drawSchriftzug_backstroke_g(temp_x, temp_y, version="loop_H")

        




    
    
    
def drawHook_o(x, y, outstrokeLen=0):
    
    Hook_o = BezierPath() 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
            
    HSL_size = 2
    HSL_start = 13              
    
    H1, H2 = line_H_vonA_u_gr_s((x,y), *angles, part, HSL_size, HSL_start)
    
    G1, G2 = line_G_vonF_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    Hook_o.arc(*drawKreisSeg(H1, HSL_start, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Hook_o.arc(*drawKreisSeg(G1, HSL_start+HSL_size, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*3)     
    Hook_o.arc(*drawKreisSeg(F1, HSL_start+HSL_size*2, angle_12, angle_13))
    
    outstroke = drawOutstroke(*E2, outstrokeLen)
    
    Hook_o +=  outstroke 
    drawPath(Hook_o)
    return Hook_o
    
#drawHook_o(temp_x, temp_y) 






def drawTail_x(x, y):
    
    Tail_x = BezierPath() 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-4)
     
    instroke = drawInstroke(*Raute_a, 1)
           
    HSL_size = 1
    HSL_start = 18.8  
        
    E1, E2 = line_E_vonF_o_kl_s(instroke.points[1], *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    G1, G2 = line_G_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*8)


    Tail_x.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_5, angle_6))
    Tail_x.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_6, angle_7))
    Tail_x.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    Tail_x.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_8, angle_9))
    Tail_x.arc(*drawKreisSeg(H1, HSL_start-HSL_size*5, angle_9, angle_10))
    Tail_x.arc(*drawKreisSeg(G1, HSL_start-HSL_size*6, angle_10, angle_11))
    Tail_x.arc(*drawKreisSeg(F1, HSL_start-HSL_size*7, angle_11, angle_12))
    Tail_x.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_12, angle_13))
    
    Tail_x += instroke    
    drawPath(Tail_x)
    
    return Tail_x
    
#drawTail_x(temp_x, temp_y) 






def drawSchriftzug_z_oben(x, y, version="mid"):
    
    Schriftzug_z_oben = BezierPath() 
    
    # Beginn oben
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)

    if version == "begin":
        
        HSL_size = 1.5
        HSL_start = 12
       
        G1, G2 = line_G_vonF_u_kl_s(Grund_a, *angles, part, HSL_size, HSL_start-HSL_size)
        H1, H2 = line_H_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        A7, A8 = line_A_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        B1, B2 = line_B_vonA_o_kl(A7, *angles, part, HSL_size, HSL_start-HSL_size*4)
        C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*5)
        D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*6)
        E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    
        Schriftzug_z_oben.arc(*drawKreisSeg(E1, HSL_start-HSL_size*7, angle_5, angle_6))
        Schriftzug_z_oben.arc(*drawKreisSeg(D1, HSL_start-HSL_size*6, angle_6, angle_7))
        Schriftzug_z_oben.arc(*drawKreisSeg(C1, HSL_start-HSL_size*5, angle_7, angle_8))
        Schriftzug_z_oben.arc(*drawKreisSeg(B1, HSL_start-HSL_size*4, angle_8, angle_9))
        Schriftzug_z_oben.arc(*drawKreisSeg(A7, HSL_start-HSL_size*3, angle_9, angle_10))
        Schriftzug_z_oben.arc(*drawKreisSeg(H1, HSL_start-HSL_size*2, angle_10, angle_11))


    
    ### Kurve rechts oben
    HSL_size = 0.5
    HSL_start = 8
    
    #F1, F2 = line_F_vonE_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    G1, G2 = line_G_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*3)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)

    Schriftzug_z_oben.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_2, True))
    Schriftzug_z_oben.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_2, angle_1, True))
    Schriftzug_z_oben.arc(*drawKreisSeg(A3, HSL_start+HSL_size*2, angle_1, angle_0, True))
    Schriftzug_z_oben.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_16, angle_15, True))
    Schriftzug_z_oben.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_15, angle_14, True))
    Schriftzug_z_oben.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_14, angle_13, True))


    outstroke = drawOutstroke(*E2, 1, "down")
    
      
    drawPath(Schriftzug_z_oben)
    
    return Schriftzug_z_oben
    
#drawSchriftzug_z_oben(temp_x, temp_y) 





#temp_x=7


def drawSchriftzug_z_unten(x, y, version="loop_H", length=2.46, deviation=1):  
    
    Schriftzug_z_unten = BezierPath() 
    
        
    # Raute
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    
    #Spitze = drawSchriftzug6(x, y, "a")


    # HSL_size = 0
    # HSL_start = 22
                
    # # G1, G2 = line_G_vonF_o_gr_s(Spitze.points[0], *angles, part, HSL_size, HSL_start)

    # # H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    # # Schriftzug_z_unten.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_2, True))
    
    # A1, A2 = line_A_vonH_o_gr_s(Raute_d, *angles, part, HSL_size, HSL_start+HSL_size*2)
    # #Schriftzug_z_unten.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_2, angle_1, True))
    
    # B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    # Schriftzug_z_unten.arc(*drawKreisSeg(A1, HSL_start+HSL_size*2, angle_1, angle_0, True))
        
    
    

    if version == "loop_A": 
        
        ### right side down
        HSL_size = 4
        HSL_start = 3

        E1, E2 = line_E_vonD_o_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)

        F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug_z_unten.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))

        G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug_z_unten.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))

        H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        Schriftzug_z_unten.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))

        A1, A2 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        Schriftzug_z_unten.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_2, angle_1, True))

        B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*5)
        Schriftzug_z_unten.arc(*drawKreisSeg(A1, HSL_start+HSL_size*4, angle_1, angle_0, True))

        C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*6)
        Schriftzug_z_unten.arc(*drawKreisSeg(B1, HSL_start+HSL_size*5, angle_16, angle_15, True))
    
        D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*7)
        Schriftzug_z_unten.arc(*drawKreisSeg(C1, HSL_start+HSL_size*6, angle_15, angle_14, True))

        E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*8)
        Schriftzug_z_unten.arc(*drawKreisSeg(D1, HSL_start+HSL_size*7, angle_14, angle_13, True))
    
    
    
        ### Wendung unten
        HSL_size = 0.5
        HSL_start = 4
       
        E1, E2 = line_E_vonD_u_gr_s(E2, *angles, part, HSL_size, HSL_start)
        F1, F2 = line_F_vonE_u_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        G1, G2 = line_G_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        A5, A6 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        B1, B2 = line_B_vonA_o_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*5)
    
        Schriftzug_z_unten.arc(*drawKreisSeg(E1, HSL_start, angle_13, angle_12, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_11, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_10, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_10, angle_9, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(A5, HSL_start+HSL_size*4, angle_9, angle_8, True))


        ### left side up/down
        HSL_size = 4
        HSL_start = 12

        B1, B2 = line_B_vonA_o_gr_s(B2, *angles, part, HSL_size, HSL_start)
        C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size)
        D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    
        Schriftzug_z_unten.arc(*drawKreisSeg(B1, HSL_start, angle_8, angle_7, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_6, True))    
        Schriftzug_z_unten.arc(*drawKreisSeg(D1, HSL_start+HSL_size*2, angle_6, angle_5, True))
    
    
        outstroke = drawConstroke(*E2, "A", length, deviation)
    
        Schriftzug_z_unten += outstroke
        
        
        
        
    if version == "loop_H": 
        
        
        ### right side down
        HSL_size = 4
        HSL_start = 3

        E1, E2 = line_E_vonD_o_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)

        F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
        Schriftzug_z_unten.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))

        G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        Schriftzug_z_unten.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))

        H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        Schriftzug_z_unten.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))

        A1, A2 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        Schriftzug_z_unten.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_2, angle_1, True))

        B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*5)
        Schriftzug_z_unten.arc(*drawKreisSeg(A1, HSL_start+HSL_size*4, angle_1, angle_0, True))

        C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*6)
        Schriftzug_z_unten.arc(*drawKreisSeg(B1, HSL_start+HSL_size*5, angle_16, angle_15, True))
    
        D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*7)
        Schriftzug_z_unten.arc(*drawKreisSeg(C1, HSL_start+HSL_size*6, angle_15, angle_14, True))

        E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*8)
        Schriftzug_z_unten.arc(*drawKreisSeg(D1, HSL_start+HSL_size*7, angle_14, angle_13, True))
        
        
        
        
        ### Wendung unten
        HSL_size = 0.5
        HSL_start = 4
   
        E3, E4 = line_E_vonD_u_gr_s(E2, *angles, part, HSL_size, HSL_start)
        F1, F2 = line_F_vonE_u_gr(E3, *angles, part, HSL_size, HSL_start+HSL_size)
        G1, G2 = line_G_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
        H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
        A5, A6 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
        B1, B2 = line_B_vonA_o_gr(A5, *angles, part, HSL_size, HSL_start+HSL_size*5)
    
        Schriftzug_z_unten.arc(*drawKreisSeg(E3, HSL_start, angle_13, angle_12, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_11, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_11, angle_10, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_10, angle_9, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(A5, HSL_start+HSL_size*4, angle_9, angle_8, True))


        # Hilfe für Abstand zum Hauptstrich und Pos Ausstrich
        drawRauteOrientKurTop(offset, offsetDir, x-3.5, y-4.5)
        drawRauteOrientKurTop(offset, offsetDir, x, y-4)


        ### left side up/down
        HSL_size = 4
        HSL_start = 8

        B3, B4 = line_B_vonA_o_gr_s(B2, *angles, part, HSL_size, HSL_start)
        C1, C2 = line_C_vonB_o_gr(B3, *angles, part, HSL_size, HSL_start+HSL_size)
        D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    
        Schriftzug_z_unten.arc(*drawKreisSeg(B3, HSL_start, angle_8, angle_7, True))
        Schriftzug_z_unten.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_6, True))    
       
        outstroke = drawConstroke(*D2, "H", length, deviation)
        Schriftzug_z_unten += outstroke


    #Schriftzug_z_unten += Spitze      
    drawPath(Schriftzug_z_unten)    
    return Schriftzug_z_unten
    
#drawSchriftzug_z_unten(temp_x, temp_y, "loop_A", 7, deviation=1)   # , "loop_A"




    




    
########################################################################################################################

#######         Ab hier Zahlen 

########################################################################################################################








def drawSchriftzug3_Figures(x, y, instrokeLen=0, outstrokeLen=0.5):
        
    Schriftzug3_Figures = BezierPath()   
        
        
    # TOP
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.5)
    instroke = drawInstroke(*Grund_a, instrokeLen)
    stem = drawGrundelementF(*Grund_a, 5)
    
    # BOTTOM
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.5)

    HSL_size = 0.5
    HSL_start = 8

    B1, B2 = line_B_vonC_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    G1, G2 = line_G_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    E3, E4 = line_E_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*5)

    Schriftzug3_Figures.arc(*drawKreisSeg(A3, HSL_start-HSL_size, angle_8, angle_9))
    Schriftzug3_Figures.arc(*drawKreisSeg(H1, HSL_start-HSL_size*2, angle_9, angle_10))
    Schriftzug3_Figures.arc(*drawKreisSeg(G1, HSL_start-HSL_size*3, angle_10, angle_11))
    Schriftzug3_Figures.arc(*drawKreisSeg(F1, HSL_start-HSL_size*4, angle_11, angle_12))
    Schriftzug3_Figures.arc(*drawKreisSeg(E3, HSL_start-HSL_size*5, angle_12, angle_13))
    
    
    Schriftzug3_Figures += instroke + stem
    drawPath(Schriftzug3_Figures)
    return Schriftzug3_Figures
    

#drawSchriftzug3_Figures(temp_x, temp_y)
    
    
    
    
    
def drawSchriftzug_one_Auslauf(x, y):
        
    Schriftzug_one_Auslauf = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3)


    HSL_size = 1
    HSL_start = 12.8
        
    B1, B2 = line_B_vonA_u_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size)
    D1, D2 = line_D_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    E3, E4 = line_E_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    F3, F4 = line_F_vonE_u_kl(E3, *angles, part, HSL_size, HSL_start-HSL_size*4)
    G3, G4 = line_G_vonF_u_kl(F3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    H3, H4 = line_H_vonG_u_kl(G3, *angles, part, HSL_size, HSL_start-HSL_size*6)
    A5, A6 = line_A_vonH_u_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size*7)
    B3, B4 = line_B_vonA_o_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*8)
    C3, C4 = line_C_vonB_o_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*9)
    D3, D4 = line_D_vonC_o_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*10)

    Schriftzug_one_Auslauf.arc(*drawKreisSeg(C1, HSL_start-HSL_size, angle_16, angle_15, True))
    Schriftzug_one_Auslauf.arc(*drawKreisSeg(D1, HSL_start-HSL_size*2, angle_15, angle_14, True))
    Schriftzug_one_Auslauf.arc(*drawKreisSeg(E3, HSL_start-HSL_size*3, angle_14, angle_13, True))
    # Schriftzug_one_Auslauf.arc(*drawKreisSeg(F3, HSL_start-HSL_size*9, angle_13, angle_12, True))
    # Schriftzug_one_Auslauf.arc(*drawKreisSeg(G3, HSL_start-HSL_size*10, angle_12, angle_11, True))
    # Schriftzug_one_Auslauf.arc(*drawKreisSeg(H3, HSL_start-HSL_size*11, angle_11, angle_10, True))
    # Schriftzug_one_Auslauf.arc(*drawKreisSeg(A5, HSL_start-HSL_size*12, angle_10, angle_9, True))
    # Schriftzug_one_Auslauf.arc(*drawKreisSeg(B3, HSL_start-HSL_size*13, angle_9, angle_8, True))
    # Schriftzug_one_Auslauf.arc(*drawKreisSeg(C3, HSL_start-HSL_size*14, angle_8, angle_7, True))
    # Schriftzug_one_Auslauf.arc(*drawKreisSeg(D3, HSL_start-HSL_size*15, angle_7, angle_6, True))
    # Schriftzug_one_Auslauf.arc(*drawKreisSeg(E1, HSL_start-HSL_size*16, angle_6, angle_5, True))
    
        

    drawPath(Schriftzug_one_Auslauf)
    return Schriftzug_one_Auslauf
    
    
    
    
#drawSchriftzug_one_Auslauf(temp_x, temp_y)

    

    
    
    
    
    
    
    
    
    
# def drawSchriftzug_two_Bogen(x, y, instrokeLen=0, outstrokeLen=1.125):
        
#     Schriftzug_two_Bogen = BezierPath()   
          
#     drawGrundelOrient(A1, A2, offset, x, y-3.5)

#     drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
#     drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
#     drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
#     drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
#     drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
#     drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
#     drawGrundelOrient(A1, A2, offset, x+3, y-3.5)

#     Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+1.5)


#     instroke = drawInstroke(*Grund_a, instrokeLen)


#     HSL_size = 3
#     HSL_start = 6.1
        
#     E1, E2 = line_E_vonD_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
#     F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
#     G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
#     H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
#     A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
#     B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
#     C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*6)
#     D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*7)
#     E3, E4 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*8)

#     Schriftzug_two_Bogen.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))
#     Schriftzug_two_Bogen.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))
#     Schriftzug_two_Bogen.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))
#     Schriftzug_two_Bogen.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_2, angle_1, True))
#     Schriftzug_two_Bogen.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_1, angle_0, True))
#     Schriftzug_two_Bogen.arc(*drawKreisSeg(B1, HSL_start+HSL_size*5, angle_16, angle_15, True))
#     Schriftzug_two_Bogen.arc(*drawKreisSeg(C1, HSL_start+HSL_size*6, angle_15, angle_14, True))
#     Schriftzug_two_Bogen.arc(*drawKreisSeg(D1, HSL_start+HSL_size*7, angle_14, angle_13, True))


#     outstroke = drawOutstroke(*E4, outstrokeLen, "down")
        
#     Schriftzug_two_Bogen += instroke + outstroke

#     drawPath(Schriftzug_two_Bogen)
#     return Schriftzug_two_Bogen
    
    
#drawSchriftzug_two_Bogen(temp_x, temp_y)




def drawSchriftzug_two_Top(x, y, instrokeLen=1, outstrokeLen=2.15):
        
    Schriftzug_two_Top = BezierPath()  
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5) 
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.5)


    instroke = drawInstroke(*Grund_a, instrokeLen)


    HSL_size = 0
    HSL_start = 24
        
    G1, G2 = line_G_vonF_u_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonF_u_gr(G1, *angles, part, HSL_size, HSL_start)

    Schriftzug_two_Top.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_13))

        
    Schriftzug_two_Top += instroke

    drawPath(Schriftzug_two_Top)
    return Schriftzug_two_Top
    
    
#drawSchriftzug_two_Top(temp_x, temp_y)

    
    


def drawSchriftzug_two_Schwung(x, y, instrokeLen=0):
    
    Schriftzug_two_Schwung = BezierPath()  
    
    drawGrundelOrient(A1, A2, offset, x, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
     
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-4)

    instroke = drawInstroke(*Grund_a, instrokeLen, "down")


    ### Teil 1 – links
    HSL_size = 2
    HSL_start = 5
    
    E1, E2 = line_E_vonD_o_gr_s(instroke.points[-1], *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)

    Schriftzug_two_Schwung.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))
    Schriftzug_two_Schwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))
    Schriftzug_two_Schwung.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))



    ### Teil 2 – rechts
    HSL_size = 2
    HSL_start = 5
        
    H3, H4 = line_H_vonA_u_gr_s(H2, *angles, part, HSL_size, HSL_start)
   
    G3, G4 = line_G_vonH_u_gr(H3, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_two_Schwung.arc(*drawKreisSeg(H3, HSL_start, angle_10, angle_11))

    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_two_Schwung.arc(*drawKreisSeg(G3, HSL_start+HSL_size, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug_two_Schwung.arc(*drawKreisSeg(F1, HSL_start+HSL_size*2, angle_12, angle_13))

    outstroke = drawOutstroke(*E2, 1)
        
    Schriftzug_two_Schwung += instroke + outstroke

    drawPath(Schriftzug_two_Schwung)
    return Schriftzug_two_Schwung
    
    
#drawSchriftzug_two_Schwung(temp_x, temp_y)






def drawSchriftzug_three_Bogen(x, y):
        
    Schriftzug_three_Bogen = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y-0.75)
    

    HSL_size = 1.5
    HSL_start = 7.8
        
    E1, E2 = line_E_vonD_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    G1, G2 = line_G_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    B1, B2 = line_B_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*7)
    E3, E4 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*8)

    Schriftzug_three_Bogen.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_4, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_4, angle_3, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(G1, HSL_start+HSL_size*2, angle_3, angle_2, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(H1, HSL_start+HSL_size*3, angle_2, angle_1, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_1, angle_0, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(B1, HSL_start+HSL_size*5, angle_16, angle_15, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(C1, HSL_start+HSL_size*6, angle_15, angle_14, True))
    Schriftzug_three_Bogen.arc(*drawKreisSeg(D1, HSL_start+HSL_size*7, angle_14, angle_13, True))


    drawPath(Schriftzug_three_Bogen)
    return Schriftzug_three_Bogen

#drawSchriftzug_three_Bogen(temp_x, temp_y)






def drawSchriftzug_three_Top(x, y, instrokeLen=1):
        
    Schriftzug_three_Top = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+1.5)
    
    instroke = drawInstroke(*Grund_a, instrokeLen)


    HSL_size = 2
    HSL_start = 27
        
    G1, G2 = line_G_vonH_u_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size)
    E1, E2 = line_E_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    
    Schriftzug_three_Top.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_11, angle_12))
    Schriftzug_three_Top.arc(*drawKreisSeg(E1, HSL_start-HSL_size*2, angle_12, angle_13))

    Schriftzug_three_Top += instroke
   
    drawPath(Schriftzug_three_Top)
    return Schriftzug_three_Top

#drawSchriftzug_three_Top(temp_x, temp_y)







def drawSchriftzug_five_Top(x, y):
        
    Schriftzug_five_Top = BezierPath()   
        
    # drawGrundelOrient(A1, A2, offset, x, y-4.5)
    # drawGrundelOrient(A1, A2, offset, x+2, y-4.5)
    # drawGrundelOrient(A1, A2, offset, x+4, y+2)  
    # Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+2)
    
    HSL_size = 2
    HSL_start = 20

    ### Positionierung hier relativ (also x, y), damit ich das Teil direkt ansetzen kann   
    G1, G2 = line_G_vonF_u_gr_s((x,y), *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_five_Top.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_12))
    
    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    Schriftzug_five_Top.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))



    drawPath(Schriftzug_five_Top)
    return Schriftzug_five_Top

#drawSchriftzug_five_Top(temp_x, temp_y)






def drawSchriftzug_seven_Stem(x, y):
        
    Schriftzug_seven_Stem = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3, y+1.5)
    
    stem = drawGrundelementG(*Grund_a, length=4.2, direction="down")

    HSL_size = 2
    HSL_start = 21.5
        
    C1, C2 = line_C_vonD_o_kl_s(stem.points[-1], *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_u_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)

    Schriftzug_seven_Stem.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_7, angle_8))
    Schriftzug_seven_Stem.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_8, angle_9))

    Schriftzug_seven_Stem += stem
    drawPath(Schriftzug_seven_Stem)
    return Schriftzug_seven_Stem

#drawSchriftzug_seven_Stem(temp_x, temp_y)





def drawSchriftzug_zero_BogenLinks(x, y):
        
    Schriftzug_zero_BogenLinks = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.6, y+1.35)
    

    HSL_size = 18
    HSL_start = 13.6

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonD_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_kl(C1, *angles, part, 0, HSL_start+HSL_size)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonF_u_kl(H1, *angles, part, 0, HSL_start)

    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_7))
    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_9))
    Schriftzug_zero_BogenLinks.arc(*drawKreisSeg(H1, HSL_start, angle_9, angle_13))



    drawPath(Schriftzug_zero_BogenLinks)
    return Schriftzug_zero_BogenLinks

#drawSchriftzug_zero_BogenLinks(temp_x, temp_y)




def drawSchriftzug_zero_BogenRechts(x, y, version="zero"):
        
    Schriftzug_zero_BogenRechts = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
      
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.6, y+1.35)
    

    HSL_size = 18
    HSL_start = 13.6

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(E1, *angles, part, 0, HSL_start)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_u_gr(A3, *angles, part, 0, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_u_kl(A3, *angles, part, HSL_size, HSL_start)
    G1, G2 = line_G_vonF_u_gr(D1, *angles, part, 0, HSL_start)

    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_1, True))
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(A3, HSL_start+HSL_size, angle_1, angle_15, True))
    Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_15, angle_13, True))

    if version == "zero":
    
        Schriftzug_zero_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_13, angle_11, True))
        
    drawPath(Schriftzug_zero_BogenRechts)
    return Schriftzug_zero_BogenRechts

#drawSchriftzug_zero_BogenRechts(temp_x, temp_y)
#drawSchriftzug_zero_BogenRechts(temp_x, temp_y, version="nine")




def drawSchriftzug_six_BogenRechts(x, y):
        
    Schriftzug_six_BogenRechts = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5) 
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.6, y-0.75)
    

    HSL_size = 2
    HSL_start = 13.3

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H1, H2 = line_H_vonG_o_gr(E1, *angles, part, 0, HSL_start)
    A3, A4 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_u_gr(A3, *angles, part, 0, HSL_start+HSL_size)
    D1, D2 = line_D_vonC_u_kl(A3, *angles, part, HSL_size, HSL_start)
    #G1, G2 = line_G_vonF_u_gr(D1, *angles, part, 0, HSL_start)

    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_1, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(A3, HSL_start+HSL_size, angle_1, angle_15, True))
    Schriftzug_six_BogenRechts.arc(*drawKreisSeg(D1, HSL_start, angle_15, angle_13, True))


    drawPath(Schriftzug_six_BogenRechts)
    return Schriftzug_six_BogenRechts

#drawSchriftzug_six_BogenRechts(temp_x, temp_y)






def drawSchriftzug_nine_BogenLinks(x, y):
        
    Schriftzug_nine_BogenLinks = BezierPath()   
        
    drawGrundelOrient(A1, A2, offset, x, y-3.5)

    drawGrundelOrient(A1, A2, offset, x+1.5, y-3.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-2.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-1.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+0.5)
    drawGrundelOrient(A1, A2, offset, x+1.5, y+1.5)
    
    drawGrundelOrient(A1, A2, offset, x+3, y-3.5)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+0.6, y+1.35)
    

    HSL_size = 2
    HSL_start = 13

    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    C1, C2 = line_C_vonD_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    A3, A4 = line_A_vonB_u_kl(C1, *angles, part, 0, HSL_start+HSL_size)
    H1, H2 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start)
    E3, E4 = line_E_vonF_u_kl(H1, *angles, part, 0, HSL_start)

    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_7))
    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(C1, HSL_start+HSL_size, angle_7, angle_9))
    Schriftzug_nine_BogenLinks.arc(*drawKreisSeg(H1, HSL_start, angle_9, angle_13))


    drawPath(Schriftzug_nine_BogenLinks)
    return Schriftzug_nine_BogenLinks

#drawSchriftzug_nine_BogenLinks(temp_x, temp_y)






def drawSchriftzug_eight(x, y):
        
    Schriftzug_eight_BogenMiddle = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y+1.35)
    oben_rechts = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=0, HSL_start=10.1, clockwise=True, inward=False)
    oben_links = drawSchneckenzug(*Grund_a, UPPER_E, 6, HSL_size=0, HSL_start=10.1, clockwise=False, inward=False)
    straight_middle = drawGrundelementC(*oben_links.points[-1], length=1.25)
    unten_rechts = drawSchneckenzug(*straight_middle.points[-1], UPPER_G, 6, HSL_size=0, HSL_start=10.1, clockwise=True, inward=False)
    straight_middle2 = drawGrundelementA(*oben_rechts.points[-1], 1.25, "down")
    unten_links = drawSchneckenzug(*straight_middle2.points[-1], UPPER_E, 8, HSL_size=0, HSL_start=10.1, clockwise=False, inward=False)

    Schriftzug_eight = oben_rechts + oben_links + unten_rechts + unten_links + straight_middle + straight_middle2
    drawPath(Schriftzug_eight)
    return Schriftzug_eight

# drawSchriftzug_eight(temp_x, temp_y)









def drawDieresis(x, y):
    
    Dieresis = BezierPath() 
    
    # drawGrundelOrientMittig(A1, A2, offset, x-2, y+1)
    # drawGrundelOrientMittig(A1, A2, offset, x-2, y+2)
    # drawGrundelOrientMittig(A1, A2, offset, x-1, y+1.5)
    # drawGrundelOrientMittig(A1, A2, offset, x-1, y+2.5)
    # drawGrundelOrientMittig(A1, A2, offset, x, y+1)
    # drawGrundelOrientMittig(A1, A2, offset, x, y+2)
    # drawGrundelOrientMittig(A1, A2, offset, x+1, y+2.5)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y+2.5)
    links = drawGrundelementE(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+1.5, y+2.5)
    rechts = drawGrundelementE(*Raute_a)

    Dieresis = links + rechts
    drawPath(Dieresis) 
    return Dieresis
    
#drawDieresis(temp_x, temp_y)






def drawSchriftzug_germandbls(x, y):
    
    Schriftzug_germandbls = BezierPath()

    #drawRauteOrientKurTop(offset, offsetDir, x, y)
    Raute_a, _, _, _ = drawRauteOrientKurTop(offset, offsetDir, x, y+4)
    bend_top_left = drawSchneckenzug(*Raute_a, UPPER_E, 4, HSL_size=1, HSL_start=4, clockwise=True, inward=False)
    bend_top_right = drawSchneckenzug(*bend_top_left.points[-1], LOWER_A, 4, HSL_size=1, HSL_start=5, clockwise=False, inward=False)

    straight = drawGrundelementA(*bend_top_right.points[-1], 1, "down")
    
    s_top = drawSchneckenzug(*straight.points[-1], UPPER_E, 4, HSL_size=2, HSL_start=12, clockwise=False, inward=False)
    s_btm = drawSchneckenzug(*s_top.points[-1], UPPER_A, 4, HSL_size=8, HSL_start=39, clockwise=True, inward=True)
    
    Schriftzug_germandbls += bend_top_left + bend_top_right + straight + s_top + s_btm 
    drawPath(Schriftzug_germandbls)
    return Schriftzug_germandbls 
   
#drawSchriftzug_germandbls(temp_x, temp_y)




def drawSchriftzug_longs_longs_2nd(x, y):
    
    Schriftzug_longs_longs_2nd = BezierPath() 
  
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+5) 
    connection_left = drawSchneckenzug(*Raute_a, UPPER_E, 5, HSL_size=2, HSL_start=14, clockwise=True, inward=False)
    #connection_transition = drawSchneckenzug(*connection_left.points[-1], UPPER_G, 3, HSL_size=2, HSL_start=15.5, clockwise=True, inward=True)
    downstroke = drawGrundelementF(*connection_left.points[-1], 7)

    Schriftzug_longs_longs_2nd += connection_left + downstroke
    drawPath(Schriftzug_longs_longs_2nd)
    return Schriftzug_longs_longs_2nd
    
#drawSchriftzug_longs_longs_2nd(temp_x, temp_y)



 
 
def drawSchriftzug_f_f_2nd(x, y):
    
    Schriftzug_f_f_2nd = BezierPath() 
  
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+5)
    connection_left = drawSchneckenzug(*Grund_a, UPPER_E, 6, HSL_size=2, HSL_start=1, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*connection_left.points[-1], 0.5)
    connection_transition = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 3, HSL_size=1, HSL_start=14, clockwise=True, inward=True)
    downstroke = drawGrundelementF(*connection_transition.points[-1], 6.53)

    Schriftzug_f_f_2nd += connection_left + Einsatz + connection_transition + downstroke  
    drawPath(Schriftzug_f_f_2nd)
    return Schriftzug_f_f_2nd
    
#drawSchriftzug_f_f_2nd(temp_x, temp_y)






def drawSchriftzug_semicolon_btm(x, y):
    
    Schriftzug_semicolon_btm = drawSchneckenzug(x, y, UPPER_E, 8, HSL_size=0.5, HSL_start=3.15, clockwise=True, inward=False)

    drawPath(Schriftzug_semicolon_btm)
    return Schriftzug_semicolon_btm
    
#drawSchriftzug_semicolon_btm(temp_x, temp_y) 






def drawSchriftzug_question(x, y):
    
    Schriftzug_question = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.25)
    drawGrundelOrient(A1, A2, offset, x-0.5, y+3)
    drawGrundelOrient(A1, A2, offset, x, y-3.75)
    
    top = drawSchneckenzug(*Grund_a, LOWER_E, 8, HSL_size=1.125, HSL_start=14.85, clockwise=False, inward=True)
    btm = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=1.125, HSL_start=18, clockwise=False, inward=True)

    Schriftzug_question += top + btm
    drawPath(Schriftzug_question)    
    return Schriftzug_question
    
#drawSchriftzug_question(temp_x, temp_y)






