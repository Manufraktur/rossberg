import importlib
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatC

importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatC)

from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatC import *
from version_3.creation.special_drawbot import BezierPath, drawPath

from nibLib.pens.rectNibPen import RectNibPen
from version_3.creation.rect_nib_pen import RectNibPen as RectNibPen_Just
from math import radians
import math as m
import collections
import glyphContext



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 20
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)







# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Kanzlei, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)



# ___________ Sidebearings, Margin _______________

# # font = CurrentFont()

# # for key in sidebearingFunctions:
# #     glyph = font[[key][0]]  
    




# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)



stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 9



# ________________________________________________________





##################################################################################
######           Schriftzüge
##################################################################################






      
          
def drawSchriftzug_A_DeckungOben(x, y):
        
    Schriftzug_A_DeckungOben = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+7.25, y+4.25) 


    DeckungOben_left = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1.25, HSL_start=1, clockwise=False, inward=False)

    DeckungOben_right = drawSchneckenzug(*DeckungOben_left.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=10, clockwise=False, inward=False)

    Schriftzug_A_DeckungOben  = DeckungOben_left + DeckungOben_right
    drawPath(Schriftzug_A_DeckungOben)

    return Schriftzug_A_DeckungOben

#drawSchriftzug_A_DeckungOben(temp_x, temp_y)





def drawSchriftzug_C_stehenderSchwung(x, y):
        
    Schriftzug_C_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y-1) 

    oben = drawSchneckenzug(*Grund_a, LOWER_H, 5, HSL_size=1, HSL_start=17, clockwise=True, inward=True)
    instroke = drawInstroke(*oben.points[-1], 1.25, "down")
    Einsatz = drawGrundelementC(*Grund_a, 0, "unten")
    unten = drawSchneckenzug(*Einsatz.points[-1], UPPER_H, 5, HSL_size=1, HSL_start=12, clockwise=True, inward=True)
    outstroke = drawOutstroke(*unten.points[-1], 0.75, "down")
    
    Schriftzug_C_stehenderSchwung += oben + instroke + Einsatz + unten + outstroke
    drawPath(Schriftzug_C_stehenderSchwung)

    return Schriftzug_C_stehenderSchwung

#drawSchriftzug_C_stehenderSchwung(temp_x, temp_y)






def drawSchriftzug_C_liegenderSchwung(x, y):
        
    Schriftzug_C_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+0.25) 
    links = drawSchneckenzug(*Grund_a, UPPER_H, 3, HSL_size=2, HSL_start=6, clockwise=False, inward=False)
    Einsatz = drawGrundelementD(*Grund_a, 1)
    right = drawSchneckenzug(*Einsatz.points[4], LOWER_H, 3, HSL_size=2, HSL_start=10, clockwise=False, inward=False)
    outstroke = drawOutstroke(*right.points[-1], 1)

    Schriftzug_C_liegenderSchwung +=  links + Einsatz + right + outstroke
    drawPath(Schriftzug_C_liegenderSchwung)
    return Schriftzug_C_liegenderSchwung

#drawSchriftzug_C_liegenderSchwung(temp_x, temp_y)








def drawSchriftzug_D_liegenderSchwung(x, y):
        
    Schriftzug_D_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1.5, y-4.8) 
    links = drawSchneckenzug(*Grund_d, UPPER_A, 4, HSL_size=1, HSL_start=8, clockwise=False, inward=False)
    Einsatz = drawGrundelementD(*Grund_d, 0)
    outstroke = drawOutstroke(*Einsatz.points[4], 2.05)
    
    Schriftzug_D_liegenderSchwung += links + Einsatz + outstroke
    drawPath(Schriftzug_D_liegenderSchwung)
    return Schriftzug_D_liegenderSchwung

#drawSchriftzug_D_liegenderSchwung(temp_x, temp_y)

 
def drawSchriftzug_D_Hauptbogen(x, y):
        
    Schriftzug_D_Hauptbogen = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1, y+3.75) 
    
    bogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1, HSL_start=31.675, clockwise=True, inward=True)
    Einsatz = drawGrundelementF(*bogen_oben.points[-1], 2)
    bogen_unten = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=1, HSL_start=26.5, clockwise=True, inward=True)

    #outstroke = drawOutstroke(*E4, 0.25, "down")
    Schriftzug_D_Hauptbogen = bogen_oben + Einsatz + bogen_unten
    drawPath(Schriftzug_D_Hauptbogen)
    return Schriftzug_D_Hauptbogen

# drawSchriftzug_D_Hauptbogen(temp_x, temp_y)




    
def drawSchriftzug_D_Deckung(x, y):
        
    Schriftzug_D_Deckung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3.75, y+2.15)    
    

    # Wendepunkt ab Raute_b nach links
    HSL_size = 0.5
    HSL_start = 1
        
    E1, E2 = line_E_vonF_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_o_gr(E1, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(E1, HSL_start, angle_5, angle_6))

    C1, C2 = line_C_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(D1, HSL_start+HSL_size, angle_6, angle_7))

    B1, B2 = line_B_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_7, angle_8))

    A3, A4 = line_A_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_9))

    H1, H2 = line_H_vonA_u_gr(A3, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(A3, HSL_start+HSL_size*4, angle_9, angle_10))

    G1, G2 = line_G_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(H1, HSL_start+HSL_size*5, angle_10, angle_11))



    HSL_size = 1
    HSL_start = 8
    
    G3, G4 = line_G_vonH_u_gr_s(G2, *angles, part, HSL_size, HSL_start)

    F1, F2 = line_F_vonG_u_gr(G3, *angles, part, HSL_size, HSL_start+HSL_size)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(G3, HSL_start, angle_11, angle_12))

    E1, E2 = line_E_vonF_u_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Schriftzug_D_Deckung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_12, angle_13))

    outstroke = drawOutstroke(*E2, 0.25)

    Schriftzug_D_Deckung+= outstroke
    drawPath(Schriftzug_D_Deckung)
    return Schriftzug_D_Deckung

#drawSchriftzug_D_Deckung(temp_x, temp_y)




    

def drawSchriftzug_D_stehenderSchwung(x, y, instrokeLen=0.5):
        
    Schriftzug_D_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y) 
    Einsatz = drawGrundelementD(*Grund_a, length=0.5)
    
    oben = drawSchneckenzug(*Grund_a, LOWER_H, 5, HSL_size=1, HSL_start=6, clockwise=True, inward=False)
    instroke = drawInstroke(*oben.points[-1], instrokeLen, "down")
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=0.5, HSL_start=13, clockwise=True, inward=False)

    Schriftzug_D_stehenderSchwung += instroke + oben + unten + Einsatz
    drawPath(Schriftzug_D_stehenderSchwung)
    return Schriftzug_D_stehenderSchwung

#drawSchriftzug_D_stehenderSchwung(temp_x, temp_y)










def drawSchriftzug_G_Hauptbogen(x, y):
        
    Schriftzug_G_Hauptbogen = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.5, y+4.5) 

    oben = drawSchneckenzug(*Grund_a, UPPER_E, 6, HSL_size=1, HSL_start=6.5, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*oben.points[-1], 1.75)
    mitte = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 6, HSL_size=2, HSL_start=26, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.01 * i      for  i in range(0, 7)])

    Schriftzug_G_Hauptbogen = oben + Einsatz + mitte
    drawPath(Schriftzug_G_Hauptbogen)
    return Schriftzug_G_Hauptbogen

# drawSchriftzug_G_Hauptbogen(temp_x, temp_y)







def drawSchriftzug_G_Deckung(x, y):
        
    Schriftzug_G_Deckung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1.8, y-3.5) 

    
    # fast wie Schriftteil 9
    HSL_size = 2
    HSL_start = 5
          
         
    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    
    Schriftzug_G_Deckung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))

    
    ### zweiter Teil
    HSL_size = 0.5
    HSL_start = 5
    
    G3, G4 = line_G_vonF_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_kl(G3, *angles, part, HSL_size, HSL_start-HSL_size)
    A5, A6 = line_A_vonH_o_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*3)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*4)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*6)
    
    Schriftzug_G_Deckung.arc(*drawKreisSeg(H3, HSL_start-HSL_size, angle_3, angle_2, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(A5, HSL_start-HSL_size*2, angle_2, angle_1, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(B3, HSL_start-HSL_size*3, angle_1, angle_0, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*4, angle_16, angle_15, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*5, angle_15, angle_14, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*6, angle_14, angle_13, True))
    


    drawPath(Schriftzug_G_Deckung)

    return Schriftzug_G_Deckung

#drawSchriftzug_G_Deckung(temp_x, temp_y)








def drawSchriftzug_G_Fuss(x, y):
        
    Schriftzug_G_Fuss = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.25, y-4.25) 
 
    links = drawSchneckenzug(*Grund_a, UPPER_H, 3, HSL_size=4, HSL_start=6, clockwise=False, inward=False)
    
    Einsatz = drawGrundelementD(*Grund_a, 1.25)
    rechts = drawSchneckenzug(*Einsatz.points[4], LOWER_H, 3, HSL_size=4, HSL_start=16, clockwise=False, inward=False)

    outstroke = drawOutstroke(*rechts.points[-1], 1)

    Schriftzug_G_Fuss = links + Einsatz + rechts + outstroke
    drawPath(Schriftzug_G_Fuss)

    return Schriftzug_G_Fuss
    
    
#drawSchriftzug_G_Fuss(temp_x, temp_y)





    
    
    
def drawSchriftzug_G_Bauch(x, y):
        
    Schriftzug_G_Bauch = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6.5, y+0.3) 
    top = drawSchneckenzug(*Grund_a, UPPER_G, 2, HSL_size=4, HSL_start=9, clockwise=False, inward=False)
    oben_rechts = drawSchneckenzug(*Grund_a, UPPER_G, 3, HSL_size=2, HSL_start=10, clockwise=True, inward=False)
    Einsatz = drawGrundelementF(*oben_rechts.points[-1], 0.75)
    unten_rechts = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=21.5, clockwise=True, inward=False)
    outstroke = drawOutstroke(*unten_rechts.points[-1], 1, "down")

    Schriftzug_G_Bauch = top + oben_rechts + Einsatz + unten_rechts + outstroke
    drawPath(Schriftzug_G_Bauch)
    return Schriftzug_G_Bauch 
    
#drawSchriftzug_G_Bauch(temp_x, temp_y)

    
    
    

def drawSchriftzug_G_stehenderSchwung(x, y, instrokeLen=0.5):
        
    Schriftzug_G_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-0.5) 
    Einsatz = drawGrundelementD(*Grund_a, length=0)
    
    oben = drawSchneckenzug(*Grund_a, LOWER_H, 5, HSL_size=1, HSL_start=11, clockwise=True, inward=False)
    instroke = drawInstroke(*oben.points[-1], instrokeLen, "down")
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=0.5, HSL_start=12, clockwise=True, inward=False)

    Schriftzug_G_stehenderSchwung += instroke + oben + unten + Einsatz
    drawPath(Schriftzug_G_stehenderSchwung)
    return Schriftzug_G_stehenderSchwung

#drawSchriftzug_G_stehenderSchwung(temp_x, temp_y)

   



def drawAuge(x, y):
    
    Auge = BezierPath()
    
    Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.25, y+3.5)

    start_oben = drawSchneckenzug(*Raute_a, LOWER_A, 2, HSL_size=4, HSL_start=8, clockwise=False, inward=False)
    bow = drawSchneckenzug(*start_oben.points[-1], UPPER_G, 6, HSL_size=0.75, HSL_start=3, clockwise=True, inward=False)

    Auge += start_oben + bow
    drawPath(Auge)  
    return Auge
        
#drawAuge(temp_x, temp_y)







def drawSchriftzug_H_stehenderSchwung(x, y):
        
    Schriftzug_H_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+3.75) 
    instroke = drawInstroke(*Grund_d, 1, "down")
    
    Schwung_oben = drawSchneckenzug(*Grund_d, UPPER_E, 5, HSL_size=1, HSL_start=14, clockwise=False, inward=False)


    Schwung_unten = drawSchneckenzug(*Schwung_oben.points[-1], UPPER_H, 5, HSL_size=1, HSL_start=20, clockwise=True, inward=True)
    
    
    #Endspitze = drawSchneckenzug(*Schwung_unten.points[-1], LOWER_E, 10, HSL_size=1, HSL_start=16, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.08 * i      for  i in range(0, 11)])
     

    Schriftzug_H_stehenderSchwung += instroke + Schwung_oben + Schwung_unten #+ Endspitze

    drawPath(Schriftzug_H_stehenderSchwung)

    return Schriftzug_H_stehenderSchwung

#drawSchriftzug_H_stehenderSchwung(temp_x, temp_y)








def drawSchriftzug_H_Hauptstrich_rechts(x, y):
        
    Schriftzug_H_Hauptstrich_rechts = BezierPath()     

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y+1.25) 
    #instroke = drawInstroke(*Grund_a, 2)

    Schwung_Anfang = drawSchneckenzug(*Grund_a, LOWER_A, 1, HSL_size=1, HSL_start=20, clockwise=False, inward=True)
    Schwung_Mitte = drawSchneckenzug(*Schwung_Anfang.points[-1], UPPER_H, 2, HSL_size=1, HSL_start=26, clockwise=True, inward=True)
    Einsatz = drawGrundelementF(*Schwung_Mitte.points[-1], 1.5)   
    Schwung_unten = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=1, HSL_start=38, clockwise=True, inward=True)
    outstroke = drawOutstroke(*Schwung_unten.points[-1], 0.5, "down")

    Schriftzug_H_Hauptstrich_rechts += Schwung_Anfang + Schwung_Mitte + Einsatz + Schwung_unten + outstroke
    drawPath(Schriftzug_H_Hauptstrich_rechts)
    return Schriftzug_H_Hauptstrich_rechts


#drawSchriftzug_H_Hauptstrich_rechts(temp_x, temp_y)









def drawSchriftzug_W_Bogen_links(x, y):
        
    Schriftzug_W_Bogen_links = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+4) 
    oben = drawSchneckenzug(*Grund_a, UPPER_E, 6, HSL_size=0.5, HSL_start=9, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*oben.points[-1], 1.85)
    mitte = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 6, HSL_size=2, HSL_start=28, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.01 * i      for  i in range(0, 7)])
        
    Schriftzug_W_Bogen_links = oben + Einsatz + mitte
    drawPath(Schriftzug_W_Bogen_links)

    return Schriftzug_W_Bogen_links

#drawSchriftzug_W_Bogen_links(temp_x, temp_y)





def drawSchriftzug_W_Bogen_mitte(x, y):
        
    Schriftzug_W_Bogen_mitte = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5.15, y+2.5) 
    Bogen_oben = drawSchneckenzug(*Grund_a, UPPER_B, 2, HSL_size=1, HSL_start=10, clockwise=False, inward=False)
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 1.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=1, HSL_start=20, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 0.5, "down")
    
    Schriftzug_W_Bogen_mitte = Bogen_oben + Bogen_unten + Einsatz + outstroke
    drawPath(Schriftzug_W_Bogen_mitte)
    return Schriftzug_W_Bogen_mitte

#drawSchriftzug_W_Bogen_mitte(temp_x, temp_y)
    
    
    

def drawSchriftzug_W_Bogen_rechts(x, y):
        
    Schriftzug_W_rechts = BezierPath()   

    ### UNTEN    
    Schriftzug_W_rechts_unten = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+9, y+0.5) 
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 5, HSL_size=1, HSL_start=8, clockwise=False, inward=True)
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 0.4)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=6, HSL_start=10, clockwise=True, inward=False)    
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 1.75, "down")
    Schriftzug_W_rechts_unten = Bogen_oben + Einsatz + Bogen_unten + outstroke

    ### OBEN
    Schriftzug_W_rechts_oben = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+8.5, y+2.75) 
    liegenderSchwung_links = drawSchneckenzug(*Grund_a, UPPER_E, 3, HSL_size=1, HSL_start=12, clockwise=True, inward=False)
    Einsatz = drawGrundelementD(*liegenderSchwung_links.points[-1], 0.1)
    liegenderSchwung_rechts = drawSchneckenzug(*Einsatz.points[4], LOWER_H, 3, HSL_size=4, HSL_start=6, clockwise=False, inward=False)     
    Schriftzug_W_rechts_oben = liegenderSchwung_links + Einsatz + liegenderSchwung_rechts
   
    Schriftzug_W_rechts = Schriftzug_W_rechts_unten + Schriftzug_W_rechts_oben
    drawPath(Schriftzug_W_rechts)

    return Schriftzug_W_rechts

#drawSchriftzug_W_Bogen_rechts(temp_x, temp_y)

















##################################################################################
######           Ab hier Versalien
##################################################################################


def drawSchriftzug_BG_Versalien(x, y):
                
    drawGrundelOrientMittig(A1, A2, offset, x, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)  # für J
    drawGrundelOrientMittig(A1, A2, offset, x+6, y-5) 
     
    drawGrundelOrientMittig(A1, A2, offset, x, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+3, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+6, y) 
    
    drawGrundelOrientMittig(A1, A2, offset, x+3, y+3) 


drawSchriftzug_BG_Versalien(temp_x, temp_y)






def drawKanzleiGatD_C(x, y):
   
    stehender_Schwung = drawSchriftzug_C_stehenderSchwung(x+0.25, y+1)
    pkt_Auslauf = stehender_Schwung.points[-2]
    #text("pkt_Auslauf", stehender_Schwung.points[-2])
    liegender_Schwung = drawSchriftzug_C_liegenderSchwung(x, y-5)
    Deckung = drawSchriftzug_A_DeckungOben(x-2, y+1.1)

    KanzleiGatD_C = stehender_Schwung + liegender_Schwung + Deckung
    trans_scale(KanzleiGatD_C, valueToMoveGlyph)
    return KanzleiGatD_C, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    

def drawKanzleiGatD_C_thinStroke(x, y, *, pass_from_thick=None):

    KanzleiGatD_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.1)
    
    KanzleiGatD_C_thinStroke += Auslauf + Endpunkt
    drawPath(KanzleiGatD_C_thinStroke)
    trans_thinStroke_down_left(KanzleiGatD_C_thinStroke) 
    trans_scale(KanzleiGatD_C_thinStroke, valueToMoveGlyph)
    return KanzleiGatD_C_thinStroke
    
    
          
    


def drawKanzleiGatD_D(x, y):
    x += 3
    stehender_Schwung = drawSchriftzug_D_stehenderSchwung(x-0.5,y-0.25)
    pkt_Auslauf = stehender_Schwung.points[-7]
    #text("pkt_Auslauf", stehender_Schwung.points[-7])
    stehender_Schwung_Deckung = drawSchriftzug_D_Deckung(x-2.75, y+0.5)
    liegender_Schwung = drawSchriftzug_D_liegenderSchwung(x-1.5,y)
    Hauptbogen = drawSchriftzug_D_Hauptbogen(x-3, y)

    KanzleiGatD_D = stehender_Schwung + stehender_Schwung_Deckung + liegender_Schwung + Hauptbogen
    trans_scale(KanzleiGatD_D, valueToMoveGlyph)
    return KanzleiGatD_D, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    


    
    
    

    
    
def drawKanzleiGatD_G(x, y):
    x += 2
    KanzleiGatD_G = BezierPath()

    Hauptbogen = drawSchriftzug_G_Hauptbogen(x, y)
    Hauptbogen_Deckung = drawSchriftzug_G_Deckung(x, y)
    
    Fuss = drawSchriftzug_G_Fuss(x, y)

    ### Auge
    Auge = drawAuge(x, y)    
    pkt_instrokeAuge = Auge.points[0]
    #text("pkt_instrokeAuge", pkt_instrokeAuge)
    pkt_outstrokeAuge = Auge.points[-1]
    #text("pkt_outstrokeAuge", pkt_outstrokeAuge)
    
  
    Bauch = drawSchriftzug_G_Bauch(x, y)
    
    stehender_Schwung = drawSchriftzug_G_stehenderSchwung(x+0.5, y, instrokeLen=1)
    pkt_Auslauf_unten = stehender_Schwung.points[-7]
    #text("pkt_Auslauf_unten", pkt_Auslauf_unten)
    pkt_Auslauf_oben = stehender_Schwung.points[1]
    #text("pkt_Auslauf_oben", pkt_Auslauf_oben)
    stehender_Schwung_Deckung = drawSchriftzug_D_Deckung(x-0.5, y+1.85)

    KanzleiGatD_G = Hauptbogen + Hauptbogen_Deckung + Fuss + Auge + Bauch + stehender_Schwung + stehender_Schwung_Deckung
    trans_scale(KanzleiGatD_G, valueToMoveGlyph)
    return KanzleiGatD_G, collections.namedtuple('dummy', 'pkt_instrokeAuge pkt_outstrokeAuge pkt_Auslauf_unten')(pkt_instrokeAuge, pkt_outstrokeAuge, pkt_Auslauf_unten)
    
    
    
def drawKanzleiGatD_G_thinStroke(x, y, *, pass_from_thick=None):

    KanzleiGatD_G_thinStroke = BezierPath()
    
    Auslauf_unten = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_unten, LOWER_E, 9, HSL_size=1, HSL_start=12, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.005 * i      for  i in range(0, 10)])
    Endpunkt_unten = drawThinstroke_Endpunkt(Auslauf_unten.points[-1][0]-modul_width*0.9, Auslauf_unten.points[-1][1]-modul_height*1.32)
    
 
    #### Auge
    instroke = drawInstroke(*pass_from_thick.pkt_instrokeAuge, 1)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 2, HSL_size=4, HSL_start=18, clockwise=False, inward=True)
    curve_fromAuge = drawSchneckenzug(*pass_from_thick.pkt_outstrokeAuge, LOWER_E, 2, HSL_size=2, HSL_start=4, clockwise=True, inward=False)


    move_down = Auslauf_unten + instroke + curve_toInstroke + curve_fromAuge ### didn't work. Why???
    trans_thinStroke_down_left(Auslauf_unten) 
    trans_thinStroke_down_left(instroke) 
    trans_thinStroke_down_left(curve_toInstroke) 
    trans_thinStroke_down_left(curve_fromAuge) 
        
    KanzleiGatD_G_thinStroke += Auslauf_unten + instroke + curve_toInstroke + curve_fromAuge + Endpunkt_unten  
    drawPath(KanzleiGatD_G_thinStroke)
    trans_scale(KanzleiGatD_G_thinStroke, valueToMoveGlyph)
    return KanzleiGatD_G_thinStroke
    
    
    
    




def drawKanzleiGatD_H(x, y):
    x += 3
    stehender_Schwung = drawSchriftzug_H_stehenderSchwung(x, y+0.5)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    Hauptstrich_rechts = drawSchriftzug_H_Hauptstrich_rechts(x, y)
    pkt_Einlauf = Hauptstrich_rechts.points[0]
    #text("pkt_Einlauf", pkt_Einlauf)
    pkt_Auslauf_unten = Hauptstrich_rechts.points[-2]
    #text("pkt_Auslauf_unten", pkt_Auslauf_unten)
    
    Deckung_Rechtsoben = drawSchriftzug_A_DeckungOben(x-5, y+1.25)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5.5)
    liegender_Schwung_unten_links = drawSchneckenzug(*Raute_a, UPPER_H, 3, HSL_size=1, HSL_start=6, clockwise=False, inward=False)
    liegender_Schwung_unten_rechts = drawSchneckenzug(*Raute_a, LOWER_H, 3, HSL_size=1, HSL_start=6, clockwise=False, inward=False)
    
    KanzleiGatD_H = stehender_Schwung + Hauptstrich_rechts + Deckung_Rechtsoben + liegender_Schwung_unten_links + liegender_Schwung_unten_rechts
    drawPath(KanzleiGatD_H)
    trans_scale(KanzleiGatD_H, valueToMoveGlyph)
    return KanzleiGatD_H, collections.namedtuple('dummy', 'pkt_Einlauf pkt_Auslauf pkt_Auslauf_unten')(pkt_Einlauf, pkt_Auslauf, pkt_Auslauf_unten)
    
    
def drawKanzleiGatD_H_thinStroke(x, y, *, pass_from_thick=None):

    KanzleiGatD_H_thinStroke = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Einlauf, 0.5)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 1, HSL_size=1, HSL_start=28, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    Auslauf_unten = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_unten, LOWER_E, 2, HSL_size=1.1, HSL_start=36, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 3)])
     
    KanzleiGatD_H_thinStroke += instroke + curve_toInstroke + Auslauf + Endpunkt + Auslauf_unten
    drawPath(KanzleiGatD_H_thinStroke)
    trans_thinStroke_down_left(KanzleiGatD_H_thinStroke)
    trans_scale(KanzleiGatD_H_thinStroke, valueToMoveGlyph)
    return KanzleiGatD_H_thinStroke
       



    
    
def drawKanzleiGatD_W(x, y):
    
    KanzleiGatD_W = BezierPath()

    links = drawSchriftzug_W_Bogen_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    mitte = drawSchriftzug_W_Bogen_mitte(x, y)
    pkt_Einlauf_vorher = mitte.points[0]
    #text("pkt_Einlauf_vorher", pkt_Einlauf_vorher)

    rechts = drawSchriftzug_W_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[-14]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Fuss = drawSchneckenzug(*rechts.points[20], UPPER_A, 4, HSL_size=2, HSL_start=7, clockwise=False, inward=False)
    
    KanzleiGatD_W = links + mitte + rechts + Fuss 
    drawPath(KanzleiGatD_W)
    trans_scale(KanzleiGatD_W, valueToMoveGlyph)
    return KanzleiGatD_W, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Einlauf_vorher pkt_Einlauf')(pkt_Auslauf, pkt_Einlauf_vorher, pkt_Einlauf)
    
    


def drawKanzleiGatD_W_thinStroke(x, y, *, pass_from_thick=None):

    KanzleiGatD_W_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=18, clockwise=False, inward=False)
    
    instroke = drawInstroke(*pass_from_thick.pkt_Einlauf_vorher, 0.25)
    Einlauf_vorher = drawSchneckenzug(*instroke.points[-1], UPPER_E, 2, HSL_size=2, HSL_start=18, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=18, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
        
    KanzleiGatD_W_thinStroke += Einlauf + instroke + Einlauf_vorher + Auslauf + Endpunkt
    drawPath(KanzleiGatD_W_thinStroke)
    trans_thinStroke_down_left(KanzleiGatD_W_thinStroke)
    trans_scale(KanzleiGatD_W_thinStroke, valueToMoveGlyph)
    return KanzleiGatD_W_thinStroke
    
    
    
    



    
    
  
def drawKanzleiGatD_dotStrokeUp(x, y, *, pass_from_thick=None):

    KanzleiGatD_dotStrokeUp = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Auslauf, 1, "down")
    Auslauf = drawSchneckenzug(*instroke.points[-1], LOWER_E, 8, HSL_size=1, HSL_start=11, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.6, Auslauf.points[-1][1]-modul_height*1.1)
    
    KanzleiGatD_dotStrokeUp += instroke + Auslauf + Endpunkt
    drawPath(KanzleiGatD_dotStrokeUp)
    trans_thinStroke_up_right(KanzleiGatD_dotStrokeUp)
    trans_scale(KanzleiGatD_dotStrokeUp, valueToMoveGlyph)    
    return KanzleiGatD_dotStrokeUp
    
    
def drawKanzleiGatD_dotStrokeDown(x, y, *, pass_from_thick=None):

    KanzleiGatD_dotStrokeDown = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Auslauf, 0.75)
    Auslauf = drawSchneckenzug(*instroke.points[-1], UPPER_E, 8, HSL_size=1, HSL_start=14, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.75, Auslauf.points[-1][1]-modul_height*0.05)
    
    KanzleiGatD_dotStrokeDown += instroke + Auslauf + Endpunkt
    drawPath(KanzleiGatD_dotStrokeDown)
    trans_thinStroke_down_left(KanzleiGatD_dotStrokeDown)
    trans_scale(KanzleiGatD_dotStrokeDown, valueToMoveGlyph)    
    return KanzleiGatD_dotStrokeDown
    
    
    
def drawKanzleiGatD_dotStrokeUpDown(x, y, *, pass_from_thick=None):

    KanzleiGatD_dotStrokeUpDown = BezierPath()
    
    ### UP
    instroke_up = drawInstroke(*pass_from_thick.pkt_Auslauf_up, 1, "down")
    Auslauf_up = drawSchneckenzug(*instroke_up.points[-1], LOWER_E, 8, HSL_size=1, HSL_start=11, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt_up = drawThinstroke_Endpunkt(Auslauf_up.points[-1][0]-modul_width*0.6, Auslauf_up.points[-1][1]-modul_height*1.1)
    
    ### DOWN
    instroke_down = drawInstroke(*pass_from_thick.pkt_Auslauf_down, 0.75)
    Auslauf_down = drawSchneckenzug(*instroke_down.points[-1], UPPER_E, 8, HSL_size=1, HSL_start=14, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt_down = drawThinstroke_Endpunkt(Auslauf_down.points[-1][0]-modul_width*0.75, Auslauf_down.points[-1][1]-modul_height*0.05)

    KanzleiGatD_dotStrokeUp = instroke_up + Auslauf_up + Endpunkt_up
    KanzleiGatD_dotStrokeDown = instroke_down + Auslauf_down + Endpunkt_down

    trans_thinStroke_up_right(KanzleiGatD_dotStrokeDown)
    trans_thinStroke_down_left(KanzleiGatD_dotStrokeDown)  

    KanzleiGatD_dotStrokeUpDown = KanzleiGatD_dotStrokeUp + KanzleiGatD_dotStrokeDown
    drawPath(KanzleiGatD_dotStrokeUpDown)
    trans_scale(KanzleiGatD_dotStrokeUpDown, valueToMoveGlyph)    
    return KanzleiGatD_dotStrokeUpDown
    
    
    
    
    
     
    


# ______________________________________________________    
    
    
margin_1modul = 60
margin_2modul = 120
marginStr = 30
marginRnd = 60 
margin_r=-30
margin_t=0

# ____________ ab hier in RF ____________________________
    
    
font = CurrentFont()

drawFunctions = {

    # 'C' : [ drawKanzleiGatD_C, [temp_x, temp_y], marginStr, marginStr-120 ],
    # 'D' : [ drawKanzleiGatD_D, [temp_x, temp_y], marginStr, marginRnd ],
    # 'G' : [ drawKanzleiGatD_G, [temp_x, temp_y], marginStr, marginRnd ],
    # 'H' : [ drawKanzleiGatD_H, [temp_x, temp_y], marginStr, 0 ],
    # 'W' : [ drawKanzleiGatD_W, [temp_x, temp_y], marginStr, marginStr-95 ],
    
    }



drawFunctions_thinStroke = {

    'C' : [ drawKanzleiGatD_C_thinStroke, [temp_x, temp_y] ],
    'D' : [ drawKanzleiGatD_C_thinStroke, [temp_x, temp_y] ],
    'G' : [ drawKanzleiGatD_G_thinStroke, [temp_x, temp_y] ],
    'H' : [ drawKanzleiGatD_H_thinStroke, [temp_x, temp_y] ],
    'W' : [ drawKanzleiGatD_W_thinStroke, [temp_x, temp_y] ],

    }
    
    
   
    
  
    
    
for key in drawFunctions:
    
    glyph = font[[key][0]]
    glyph.clear()
    
    foreground = glyph.getLayer("foreground")
    foreground.clear()
    background = glyph.getLayer("background")
    background.clear()
     
    function = drawFunctions[key][0]
    arguments = drawFunctions[key][1]

    output = function(*arguments)
	
    if key in drawFunctions_thinStroke:
        
        assert isinstance(output, tuple) and len(output)==2
        pass_from_thick_to_thin = output[1]
        output = output[0]
    
    # assert isinstance(output, glyphContext.GlyphBezierPath)
		
    writePathInGlyph(output, foreground)

    print("___",font[[key][0]])
    
    # ask for current status
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin   
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_before = margin_ts - margin_fg
    
    ### change value of margin
    glyph.leftMargin = drawFunctions[key][2]
    glyph.rightMargin = drawFunctions[key][3]
    
    ### ask inbetween 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_mid = margin_ts - margin_fg

    ### calculate value for movement of bg
    move_thinStroke = difference_before - difference_mid

    glyph.copyLayerToLayer('foreground', 'background')
 
   
    if key in drawFunctions_thinStroke:		
        thinstroke = glyph.getLayer("thinstroke")
        thinstroke.clear()

        thinfunction = drawFunctions_thinStroke[key][0]
        thinarguments = drawFunctions_thinStroke[key][1]
        thinoutput = thinfunction(*thinarguments, pass_from_thick=pass_from_thick_to_thin)
        
        writePathInGlyph(thinoutput, thinstroke)
        
    thinstroke = glyph.getLayer("thinstroke")
    thinstroke.translate((move_thinStroke, 0))
    
    ### ask final 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0 
        #print("(no thin line)")   
    difference_final = margin_ts - margin_fg
    
    foreground.clear()

    
    
    
    ### now draw the whole glphy with the magic tool
    ### first thick main stroke 
    guide_glyph = glyph.getLayer("background")

    w = A_A * penWidth 
    a = radians(90-alpha)
    h = penThickness

    if True:
        RectNibPen = RectNibPen_Just

    p = RectNibPen(
        glyphSet=CurrentFont(),
        angle=a,
        width=w,
        height=penThickness,
        trace=True
    )

    guide_glyph.draw(p)
    p.trace_path(glyph)


    ### now thin Stroke
    if key in drawFunctions_thinStroke:		

        guide_glyph = glyph.getLayer("thinstroke")
        w = penThickness 
        p = RectNibPen(
            glyphSet=CurrentFont(),
            angle=a,
            width=w,
            height=penThickness,
            trace=True
        )
        guide_glyph.draw(p)
        p.trace_path(glyph)

    ### keep a copy in background layer
    newLayerName = "original_math"
    glyph.layers[0].copyToLayer(newLayerName, clear=True)
    glyph.getLayer(newLayerName).leftMargin = glyph.layers[0].leftMargin
    glyph.getLayer(newLayerName).rightMargin = glyph.layers[0].rightMargin

    ### this will make it all one shape
    #glyph.removeOverlap(round=0)
    
    ### from robstevenson to remove extra points on curves
    ### function itself is in create_stuff.py
    #select_extra_points(glyph, remove=True)






