import importlib
import version_3.creation.Schriftteile_GatC
import version_3.creation.Halbboegen_GatC
import Kanzlei_Schriftzuege_GatC

importlib.reload(version_3.creation.Schriftteile_GatC)
importlib.reload(version_3.creation.Halbboegen_GatC)
importlib.reload(Kanzlei_Schriftzuege_GatC)

from version_3.creation.Schriftteile_GatC import *
from version_3.creation.Halbboegen_GatC import *
from Kanzlei_Schriftzuege_GatC import *
from version_3.creation.special_drawbot import BezierPath, drawPath


### True >>> modified code         >>> 1
### False >>> original Roßberg     >>> 0
version_mod = 1


if version_mod == True:
    print("Roßberg modifizert")
    import version_3.creation.Schriftteile_GatC_mod
    import Kanzlei_Schriftzuege_GatC_mod
    #import version_3.creation.Halbboegen_GatC_mod
    #importlib.reload(version_3.creation.Schriftteile_GatC_mod)
    importlib.reload(Kanzlei_Schriftzuege_GatC_mod)
    #importlib.reload(version_3.creation.Halbboegen_GatC_mod)
    from version_3.creation.Schriftteile_GatC_mod import *
    from Kanzlei_Schriftzuege_GatC_mod import *
    #from version_3.creation.Halbboegen_GatC_mod import *
else:
    print("Roßberg original")
    




from nibLib.pens.rectNibPen import RectNibPen
from version_3.creation.rect_nib_pen import RectNibPen as RectNibPen_Just
from math import radians
import math as m
import collections
import glyphContext



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 12
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)






# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Kanzlei, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)






# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)


stroke(.1)
strokeWidth(.1)


# _______________________________________________________  







def drawKanzleiGatC_a(x, y):      
    
    KanzleiGatC_a = BezierPath()
    Bogen_links = drawSchriftzug16(x, y, instrokeLen=1.5, outstrokeLen=2.65)
    Strich_rechts = drawSchriftzug8(x+3, y, instrokeLen=0, outstrokeLen=0.7)
    
    KanzleiGatC_a += Bogen_links + Strich_rechts
    trans_scale(KanzleiGatC_a, valueToMoveGlyph) 
    return KanzleiGatC_a
    
   
   
    
    
def drawKanzleiGatC_b(x,y):        
    
    Strich_links = drawSchriftzug11(x, y, outstrokeLen=0)
    Bogen_rechts = drawSchriftzug29(x+1, y)
    Raute_a, _, _, _ = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y-5)
    
    KanzleiGatC_b = Strich_links + Bogen_rechts
    trans_scale(KanzleiGatC_b, valueToMoveGlyph)
    return KanzleiGatC_b
    
    
    
    
    
def drawKanzleiGatC_c(x,y):
           
    Bogen_links = drawSchriftzug15(x, y)
    #Haeckchen_oben = drawSchriftteil2(*Bogen_links.points[0])
    Haeckchen_oben = drawGrundelementC(*Bogen_links.points[3])

    KanzleiGatC_c = Bogen_links + Haeckchen_oben
    trans_scale(KanzleiGatC_c, valueToMoveGlyph)
    return KanzleiGatC_c
    
 
    
def drawKanzleiGatC_ch(x, y):       
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, baseline-0.35)

    c_links = drawKanzleiGatC_c(x, y)  
    h_rechts = drawKanzleiGatC_h_decender(x+4, y, pos_stroke=True)[0]
    valueToReturn = drawKanzleiGatC_h_decender(x+4, y, pos_stroke=True)[1]
    KanzleiGatC_ch = c_links + h_rechts
    return KanzleiGatC_ch, valueToReturn
    
    


def drawKanzleiGatC_d(x, y, Endspitze=12):

    Bogen_links = drawSchriftzug16(x, y, outstrokeLen=0)
    Bogen_rechts = drawSchriftzug31(x, y) 
    
    KanzleiGatC_d = Bogen_links + Bogen_rechts
    trans_scale(KanzleiGatC_d, valueToMoveGlyph)
    return KanzleiGatC_d
    
    
    
    
    

    
def drawKanzleiGatC_e(x, y):
        
    Bogen_links = drawSchriftzug15(x, y, outstrokeLen=1.5)
    Haken_rechts = drawSchriftteil8(*Bogen_links.points[3])
    outstroke_mittig = drawOutstroke(*Haken_rechts.points[-1], 1.5, "down")
    
    KanzleiGatC_e = Bogen_links + Haken_rechts + outstroke_mittig
    trans_scale(KanzleiGatC_e, valueToMoveGlyph)
    return KanzleiGatC_e
    
    
    
    
    
    
def drawKanzleiGatC_f(x,y):
           
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)
    trans_scale(Querstrich, valueToMoveGlyph)

    KanzleiGatC_longs = drawKanzleiGatC_longs(x, y)[0]
    values_to_return = drawKanzleiGatC_longs(x, y)[1]
    KanzleiGatC_f = KanzleiGatC_longs + Querstrich
    return KanzleiGatC_f, values_to_return
    


    
    
def drawKanzleiGatC_f_f(x, y):
     
    Querstrich_left = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=5)
    #Querstrich_right = drawGrundelementB((x+1.5)*modul_width, (y+0.5)*modul_height, length=2)
    Querstrich = Querstrich_left #+ Querstrich_right 
    trans_scale(Querstrich, valueToMoveGlyph)
    
    longs_longs = drawKanzleiGatC_longs_longs(x, y, PosStroke=False)[0]
    values_to_return = drawKanzleiGatC_longs_longs(x, y)[1]
    
    KanzleiGatC_f_f = longs_longs + Querstrich
    return KanzleiGatC_f_f, values_to_return
    
    

def drawKanzleiGatC_f_t(x, y):
        
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    trans_scale(Querstrich, valueToMoveGlyph)
        
    glyph_longs_t = drawKanzleiGatC_longs_t(x, y)[0]
    values_to_return = drawKanzleiGatC_longs_t(x, y)[1]

    KanzleiGatC_f_t = glyph_longs_t + Querstrich
    return KanzleiGatC_f_t, values_to_return
    
    
    
    
def drawKanzleiGatC_f_f_t(x, y):
    
    KanzleiGatC_f_f_t = BezierPath()
       
    main_stroke_left = drawSchriftzug13(x, y, instrokeLen=1.75)
    pkt_Ausstrich = main_stroke_left.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    con = drawSchriftzug_longs_longs_con(x, y)
    main_stroke_middle = drawSchriftzug13(x+3, y,  instrokeLen=1.75, PosStroke=False)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.5, y+3)
    con_left = drawSchneckenzug(*main_stroke_middle.points[3], UPPER_E, 3, HSL_size=1, HSL_start=12, clockwise=True, inward=True)
    con_right = drawSchneckenzug(*con_left.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=10.6, clockwise=False, inward=True)
    Spitze = con_left + con_right

    glyph_t = drawSchriftzug10(x+6, y, instrokeLen=1.5, PosStroke=False)

    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=8)
    
    KanzleiGatC_f_f_t = main_stroke_left + con + main_stroke_middle + Spitze + glyph_t + Querstrich
    drawPath(KanzleiGatC_f_f_t)
    trans_scale(KanzleiGatC_f_f_t, valueToMoveGlyph)
    return KanzleiGatC_f_f_t, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    


  

def drawKanzleiGatC_g(x, y):
    
    Bogen_links = drawSchriftzug16(x, y, instrokeLen=1, outstrokeLen=2.5) 
    Strich_rechts = drawSchriftzug_g_rechts(x+3, y)
    pkt_Auslauf = Strich_rechts.points[-2]
    #text("pkt_Auslauf", pkt_Auslauf)

    KanzleiGatC_g = Bogen_links + Strich_rechts
    trans_scale(KanzleiGatC_g, valueToMoveGlyph)
    return KanzleiGatC_g, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    

def drawKanzleiGatC_g_thinStroke(x, y, *, pass_from_thick=None):
    
    KanzleiGatC_g_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 7, HSL_size=1.5, HSL_start=16.5, clockwise=True, inward=True, HSL_size_multipliers=[ 1+0.1* i      for  i in range(0, 8)])  
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.1, Auslauf.points[-1][1]-modul_height*0.89)
          
    KanzleiGatC_g_thinStroke += Auslauf + Endpunkt
    drawPath(KanzleiGatC_g_thinStroke)
    trans_thinStroke_down_left(KanzleiGatC_g_thinStroke)
    trans_scale(KanzleiGatC_g_thinStroke, valueToMoveGlyph)
    return KanzleiGatC_g_thinStroke
     
    
    
    
def drawKanzleiGatC_h(x, y):      
    
    KanzleiGatC_h = BezierPath()
    Strich_links = drawSchriftzug11(x, y)
   
    Bogen_rechts = drawSchriftzug30(x+3.5, y, "a", instrokeLen=3)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+2, baseline) 
    Fuss = drawSchriftteil7(*Raute_b)    
            
    KanzleiGatC_h = Strich_links + Bogen_rechts + Fuss
    trans_scale(KanzleiGatC_h, valueToMoveGlyph)    
    return KanzleiGatC_h 
    
    
    
def drawKanzleiGatC_h_decender(x, y, pos_stroke=True):      
    
    KanzleiGatC_h = BezierPath()
    Strich_links = drawSchriftzug11(x, y, pos_stroke=pos_stroke)
    Bogen_rechts = drawSchriftzug30(x+3.5, y, "c", instrokeLen=3, outstrokeLen=0) 
    pkt_Auslauf = Bogen_rechts.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 
            
    KanzleiGatC_h = Strich_links + Bogen_rechts
    trans_scale(KanzleiGatC_h, valueToMoveGlyph)    
    return KanzleiGatC_h, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
def drawKanzleiGatC_h_thinStroke(x, y, *, pass_from_thick=None):
    
    KanzleiGatC_h_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 2, HSL_size=4, HSL_start=24, clockwise=True, inward=False, HSL_size_multipliers=[ 1+.2 * i      for  i in range(0, 3)])
         
    KanzleiGatC_h_thinStroke += Auslauf
    drawPath(KanzleiGatC_h_thinStroke)
    trans_thinStroke_down_left(KanzleiGatC_h_thinStroke) 
    trans_scale(KanzleiGatC_h_thinStroke, valueToMoveGlyph)
    return KanzleiGatC_h_thinStroke
    
    
    
    
    
    
    
def drawKanzleiGatC_i(x,y):       

    Strich = drawSchriftzug8(x, y, instrokeLen=0.7, outstrokeLen=0.7)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x,y)
    Punkt = drawGrundelementC(Raute_a[0], Raute_a[1]+modul_height*2)
        
    KanzleiGatC_i = Strich + Punkt
    trans_scale(KanzleiGatC_i, valueToMoveGlyph)
    return KanzleiGatC_i





def drawKanzleiGatC_j(x, y):      
    x += 4
    Strich = drawSchriftzug_g_rechts(x, y, instrokeLen=0.7)
    pkt_Auslauf = Strich.points[-2]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x,y)
    Punkt = drawGrundelementC(Raute_a[0], Raute_a[1]+modul_height*2)
    
    KanzleiGatC_j = Strich + Punkt
    trans_scale(KanzleiGatC_j, valueToMoveGlyph)
    return KanzleiGatC_j, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    


def drawKanzleiGatC_k(x, y):
    
    Strich_links = drawSchriftzug11(x, y, outstrokeLen=0.7)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+1, y+1.5)
    Schleife = drawSchriftteil8(*Raute_b, instrokeLen=1, outstrokeLen=1.5)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y)
    Querstrich = drawGrundelementB(*Raute_a, 2)
    
    KanzleiGatC_k = Strich_links + Schleife + Querstrich
    trans_scale(KanzleiGatC_k, valueToMoveGlyph)
    return KanzleiGatC_k
    
    

        
        
def drawKanzleiGatC_l(x, y):    
    
    KanzleiGatC_l = drawSchriftzug11(x, y, outstrokeLen=0.7)
    
    trans_scale(KanzleiGatC_l, valueToMoveGlyph)
    return KanzleiGatC_l
    
    
    
    
       
        
        
def drawKanzleiGatC_m(x,y):
    
    Strich_links = drawSchriftzug9(x,y, instrokeLen=1)
    Strich_mitte = drawSchriftzug9(x+3, y, instrokeLen=3)
    Strich_rechts = drawSchriftzug8(x+6, y, instrokeLen=2.7, outstrokeLen=0.7)
    
    KanzleiGatC_m = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(KanzleiGatC_m, valueToMoveGlyph)
    return KanzleiGatC_m
    





def drawKanzleiGatC_n(x, y):      
    
    Strich_links = drawSchriftzug9(x, y, instrokeLen=1)
    Strich_rechts = drawSchriftzug8(x+3, y, instrokeLen=2.7, outstrokeLen=0.7)
    
    KanzleiGatC_n = Strich_links + Strich_rechts
    trans_scale(KanzleiGatC_n, valueToMoveGlyph)
    return KanzleiGatC_n
    




def drawKanzleiGatC_o(x, y):   
    
    Bogen_links = drawSchriftzug16(x, y, instrokeLen=1, outstrokeLen=1)
    Bogen_rechts = drawSchriftzug29(x+1, y, instrokeLen=1, outstrokeLen=1)
    
    KanzleiGatC_o = Bogen_links + Bogen_rechts
    trans_scale(KanzleiGatC_o, valueToMoveGlyph)
    return KanzleiGatC_o
    
    
    
    
    
def drawKanzleiGatC_p(x, y):
   
    Strich_links = drawSchriftzug7(x, y, instrokeLen=1, outstrokeLen=0)
    pkt_Ausstrich = Strich_links.points[-1]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Bogen_rechts = drawSchriftzug29(x+1, y, instrokeLen=2.7)
    
    Raute_a, _, _, _ = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)
    Signatur = drawGrundelementC(*Raute_a)

    KanzleiGatC_p = Strich_links + Bogen_rechts + Signatur
    trans_scale(KanzleiGatC_p, valueToMoveGlyph)
    return KanzleiGatC_p, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    

    
    
    
def drawKanzleiGatC_q(x, y):      
    
    Bogen_links = drawSchriftzug16(x, y, instrokeLen=1.5, outstrokeLen=2.7)
    Strich_rechts = drawSchriftzug14(x+3, y)
    pkt_Ausstrich = Strich_rechts.points[7]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    KanzleiGatC_q = Bogen_links + Strich_rechts
    trans_scale(KanzleiGatC_q, valueToMoveGlyph)
    return KanzleiGatC_q, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
    
    
def drawKanzleiGatC_r(x, y):       

    Strich = drawSchriftzug9(x, y, instrokeLen=1, outstrokeLen=0.7)

    Raute_a, _, _, _ = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+2, y)
    constroke = drawInstroke(*Raute_a, 1, "up")
    Haken = drawGrundelementC(*Raute_a)
    
    KanzleiGatC_r = Strich + constroke + Haken
    trans_scale(KanzleiGatC_r, valueToMoveGlyph)
    return KanzleiGatC_r    
    
    

def drawKanzleiGatC_rc(x, y):       
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, baseline-0.35)

    r_links = drawSchriftzug9(x, y, version="b", Fuss="long")
    
    KanzleiGatC_rc = r_links
    trans_scale(KanzleiGatC_rc, valueToMoveGlyph) 
    
    letter_c = drawKanzleiGatC_c(x+3, y)
    KanzleiGatC_rc += letter_c
    return KanzleiGatC_rc
    
    
    
    
    
    
    
        

def drawKanzleiGatC_s(x, y):
 
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.5, y-3.25)
    Bogen_links = drawHalbbogen8(*Grund_a, instrokeLen=2.45, outstrokeLen=2)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y-2.5)
    Bogen_rechts = drawHalbbogen4(*Grund_b, outstrokeLen=1.65)
    
    Raute_a, _, _, _ = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+2.5, y)
    Deckung_oben = drawGrundelementC(*Raute_a)
    
    _, _, _, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-0.3, baseline)
    Deckung_unten = drawGrundelementC(*Raute_d, 1, "unten")
    
    KanzleiGatC_s = Bogen_links + Bogen_rechts + Deckung_oben + Deckung_unten
    trans_scale(KanzleiGatC_s, valueToMoveGlyph)
    return KanzleiGatC_s
    
    
    
         
    

def drawKanzleiGatC_longs(x, y):
        
    stroke_down = drawSchriftzug13(x, y, instrokeLen=2.5)
    pkt_Ausstrich = stroke_down.points[-7]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Spitze = drawGrundelementC(*stroke_down.points[5])
    
    KanzleiGatC_longs = stroke_down + Spitze
    trans_scale(KanzleiGatC_longs, valueToMoveGlyph)
    return KanzleiGatC_longs, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
    

def drawKanzleiGatC_longs_longs(x, y, PosStroke=False):
        
    main_stroke_left = drawSchriftzug13(x, y, instrokeLen=1.75)
    pkt_Ausstrich = main_stroke_left.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    main_stroke_right = drawSchriftzug13(x+3, y, instrokeLen=2.3, PosStroke=PosStroke)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5.5, y+3.25)
    Spitze = drawGrundelementC(*Raute_b)
    
    con = drawSchriftzug_longs_longs_con(x, y)

    KanzleiGatC_longs_longs = main_stroke_left + main_stroke_right + con + Spitze
    trans_scale(KanzleiGatC_longs_longs, valueToMoveGlyph)
    return KanzleiGatC_longs_longs, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
 
    
    
    
    
    
    

def drawKanzleiGatC_longs_t(x, y):
    
    #### diese Figur ist mit Tab 43 abgestimmt, letzte Zeile “höchstens”!
    #### es gibt noch eine zweite Variante nach Tab 46, letzte Zeile “höchstens”!
    
    main_stroke_left = drawSchriftzug_ft_lig(x, y)
    pkt_Ausstrich = main_stroke_left.points[1]
    #text("pkt_Ausstrich", pkt_Ausstrich)

    KanzleiGatC_longs_t = main_stroke_left
    trans_scale(KanzleiGatC_longs_t, valueToMoveGlyph)
        
    glyph_t = drawKanzleiGatC_t(x+3, y, instrokeLen=0)
    KanzleiGatC_longs_t += glyph_t
    return KanzleiGatC_longs_t, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    


def drawKanzleiGatC_germandbls(x,y):
    #x += 1
    stroke_down = drawKanzleiGatC_longs(x, y)[0]
    trans_scale_invert(stroke_down, valueToMoveGlyph)
    pkt_Ausstrich = stroke_down.points[3]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    right = drawKanzleiGatC_z_fina(x+3, y)[0]
    trans_scale_invert(right, valueToMoveGlyph)
    pkt_Auslauf = right.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    KanzleiGatC_germandbls = stroke_down + right
    trans_scale(KanzleiGatC_germandbls, valueToMoveGlyph)
    return KanzleiGatC_germandbls, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
    
    
def drawKanzleiGatC_germandbls_thinStroke(x, y, *, pass_from_thick=None):
    
    KanzleiGatC_germandbls_Endspitze = BezierPath()   
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 3, HSL_size=2, HSL_start=24, clockwise=True, inward=False)
    
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)    
    
    KanzleiGatC_germandbls_Endspitze += Auslauf + Zierstrich
    
    drawPath(KanzleiGatC_germandbls_Endspitze)
    trans_thinStroke_down_left(KanzleiGatC_germandbls_Endspitze) 
    trans_scale(KanzleiGatC_germandbls_Endspitze, valueToMoveGlyph)
    return KanzleiGatC_germandbls_Endspitze 
    
    
    
    
    
    
    

    
def drawKanzleiGatC_t(x, y, instrokeLen=0.5):    
    
    Strich = drawSchriftzug10(x, y, instrokeLen=instrokeLen, outstrokeLen=0.7)
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)
    
    KanzleiGatC_t = Strich + Querstrich
    trans_scale(KanzleiGatC_t, valueToMoveGlyph)
    return KanzleiGatC_t
    
    
    
    

def drawKanzleiGatC_u(x, y):
    
    Strich_links = drawSchriftzug9(x, y, instrokeLen=1, outstrokeLen=2.7)
    Strich_rechts = drawSchriftzug8(x+3, y, instrokeLen=0, outstrokeLen=0.7)
    
    KanzleiGatC_u = Strich_links + Strich_rechts
    trans_scale(KanzleiGatC_u, valueToMoveGlyph)
    return KanzleiGatC_u
    
    
    
    
    

    
    
    
def drawKanzleiGatC_adieresis(x, y):
    
    Dieresis = drawDieresis(x, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_a = drawKanzleiGatC_a(x, y)     
    KanzleiGatC_adieresis = Grundschriftzug_a + Dieresis
    return KanzleiGatC_adieresis
    

def drawKanzleiGatC_odieresis(x, y):
    
    Dieresis = drawDieresis(x, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_o = drawKanzleiGatC_o(x, y)       
    KanzleiGatC_odieresis = Grundschriftzug_o + Dieresis
    return KanzleiGatC_odieresis
    

def drawKanzleiGatC_udieresis(x, y):
    
    Dieresis = drawDieresis(x, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_u = drawKanzleiGatC_u(x, y)   
    KanzleiGatC_udieresis = Grundschriftzug_u + Dieresis
    return KanzleiGatC_udieresis
    
    
    
    
    
def drawKanzleiGatC_v(x, y):
    
    Strich_links = drawSchriftzug9(x, y, instrokeLen=1, outstrokeLen=1)
    Strich_rechts = drawSchriftzug29(x+1, y, instrokeLen=2.7)
    
    KanzleiGatC_v = Strich_links + Strich_rechts
    trans_scale(KanzleiGatC_v, valueToMoveGlyph)
    return KanzleiGatC_v
    
    
    

    
def drawKanzleiGatC_w(x, y):
    
    Strich_links = drawSchriftzug9(x, y, instrokeLen=1, outstrokeLen=0.5)
    Strich_mitte = drawSchriftzug9(x+3, y, instrokeLen=3, outstrokeLen=1)
    Strich_rechts = drawSchriftzug29(x+4, y, instrokeLen=2.7)

    KanzleiGatC_w = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(KanzleiGatC_w, valueToMoveGlyph)
    return KanzleiGatC_w
    
    
    
    
    
def drawKanzleiGatC_x(x, y):
    
    Schnecke = drawSchriftzug_x_hook(x, y-5)
    trans_scale(Schnecke, valueToMoveGlyph)
    
    Hauptstrich_von_r = drawKanzleiGatC_r(x, y)
    KanzleiGatC_x = Hauptstrich_von_r + Schnecke
    return KanzleiGatC_x
    




def drawKanzleiGatC_y(x, y):
    
    Strich_links = drawSchriftzug9(x, y, instrokeLen=1, outstrokeLen=0.5)
    Bogen_rechts = drawSchriftzug30(x+3, y, "c", instrokeLen=2.7, outstrokeLen=0)  ### stimmt dieser Abstand?
    pkt_Auslauf = Bogen_rechts.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 
    
    KanzleiGatC_y = Strich_links + Bogen_rechts
    trans_scale(KanzleiGatC_y, valueToMoveGlyph)
    return KanzleiGatC_y, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
    


def drawKanzleiGatC_z_init(x ,y):
    #x += 2    
    KanzleiGatC_z_init = BezierPath()
    
    Halbbogen_oben = drawSchriftzug_z_Initial(x-0.6, y+0.25) 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1.15, y-1.25)
    Halbbogen_unten = drawSchriftzug25(*Raute_a, outstrokeLen=1.8)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-2.5, y-5)
    Deckung = drawGrundelementC(*Raute_d, 1, "unten")
    KanzleiGatC_z_init += Deckung

    KanzleiGatC_z_init += Halbbogen_oben + Halbbogen_unten
    drawPath(KanzleiGatC_z_init)
    trans_scale(KanzleiGatC_z_init, valueToMoveGlyph)
    return KanzleiGatC_z_init
    
    
    
    
def drawKanzleiGatC_z_mid(x ,y):
    #x +=2   
    KanzleiGatC_z_mid = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+0.66) 
    oben1 = drawSchneckenzug(*Grund_a, UPPER_E, 6, HSL_size=0.5, HSL_start=8, clockwise=False, inward=True)
    Einsatz = drawGrundelementC(*oben1.points[-1], 0.25)

    oben2 = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 6, HSL_size=0.5, HSL_start=6, clockwise=True, inward=False)
    outstroke = drawOutstroke(*oben2.points[-1], 0.5, "down")        
    
    Halbbogen_oben = oben1 + oben2 + outstroke + Einsatz
    
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-0.35, y-3)
    Halbbogen_unten = drawSchneckenzug(*Raute_a, UPPER_E, 8, HSL_size=2.75, HSL_start=4, clockwise=True, inward=False) 
    pkt_Auslauf = Halbbogen_unten.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    KanzleiGatC_z_mid += Halbbogen_oben + Halbbogen_unten
    drawPath(KanzleiGatC_z_mid)
    trans_scale(KanzleiGatC_z_mid, valueToMoveGlyph)
    return KanzleiGatC_z_mid, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
def drawKanzleiGatC_z_fina(x ,y):
    #x +=2   
    KanzleiGatC_z_fina = BezierPath()

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    Halbbogen_oben = drawHalbbogen4(*Raute_a, instrokeLen=1, outstrokeLen=0.5) 

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-0.8, y-3)
    Halbbogen_unten = drawSchneckenzug(*Raute_a, UPPER_E, 8, HSL_size=2.75, HSL_start=4, clockwise=True, inward=False) 
    pkt_Auslauf = Halbbogen_unten.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    KanzleiGatC_z_fina += Halbbogen_oben + Halbbogen_unten
    drawPath(KanzleiGatC_z_fina)
    trans_scale(KanzleiGatC_z_fina, valueToMoveGlyph)
    return KanzleiGatC_z_fina, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)

    
    
def drawKanzleiGatC_z_fina_thinStroke(x, y, *, pass_from_thick=None):
    
    KanzleiGatC_z_thinStroke = BezierPath()

    hook = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 8, HSL_size=1, HSL_start=18, clockwise=False, inward=True)

    KanzleiGatC_z_thinStroke += hook
    drawPath(KanzleiGatC_z_thinStroke)
    trans_thinStroke_down_left(KanzleiGatC_z_thinStroke) 
    trans_scale(KanzleiGatC_z_thinStroke, valueToMoveGlyph)
    return KanzleiGatC_z_thinStroke
    
    
    
    
    
    
    


def drawKanzleiGatC_thinstroke_Straight(x, y, *, pass_from_thick=None):     
    
    KanzleiGatC_thinstroke_Straight = BezierPath()
    
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
          
    KanzleiGatC_thinstroke_Straight +=  Zierstrich
    trans_thinStroke_down_left(KanzleiGatC_thinstroke_Straight)
    trans_scale(KanzleiGatC_thinstroke_Straight, valueToMoveGlyph)
    return KanzleiGatC_thinstroke_Straight
    
    
    
    
    
    
    
    


# _______________________________________________________ 

margin_1modul = 60
margin_2modul = 120
marginStr = 30
marginRnd = 60 
margin_r=-25+14
    

# ____________ ab hier in RF ____________________________
    
    
font = CurrentFont()

drawFunctions = {
     'a' : [ drawKanzleiGatC_a, [temp_x, temp_y], marginRnd, marginStr ],
    'b' : [ drawKanzleiGatC_b, [temp_x, temp_y], marginStr, marginRnd ],
    'c' : [ drawKanzleiGatC_c, [temp_x, temp_y], marginRnd, margin_r ],
    'd' : [ drawKanzleiGatC_d, [temp_x, temp_y, 12], marginRnd, marginRnd ],
    'e' : [ drawKanzleiGatC_e, [temp_x, temp_y], marginRnd, marginStr ],
    'f' : [ drawKanzleiGatC_f, [temp_x, temp_y], marginStr, -150 ],
    'g' : [ drawKanzleiGatC_g, [temp_x, temp_y], marginRnd, marginStr+22 ],
    'h' : [ drawKanzleiGatC_h, [temp_x, temp_y], marginStr, marginRnd ],
    'h.dec' : [ drawKanzleiGatC_h_decender, [temp_x, temp_y], marginStr, marginRnd ],
    'i' : [ drawKanzleiGatC_i, [temp_x, temp_y], marginStr, marginStr ],
     'j' : [ drawKanzleiGatC_j, [temp_x, temp_y], marginStr-9, marginStr+6 ],
    'k' : [ drawKanzleiGatC_k, [temp_x, temp_y], marginStr, marginStr-30 ],
    'l' : [ drawKanzleiGatC_l, [temp_x, temp_y], marginStr, marginStr-55],
    'm' : [ drawKanzleiGatC_m, [temp_x, temp_y], marginStr, marginStr ],
    'n' : [ drawKanzleiGatC_n, [temp_x, temp_y], marginStr, marginStr ],
    'o' : [ drawKanzleiGatC_o, [temp_x, temp_y], marginRnd, marginRnd ],
    'p' : [ drawKanzleiGatC_p, [temp_x, temp_y], marginStr, marginRnd ],
    'q' : [ drawKanzleiGatC_q, [temp_x, temp_y], marginRnd, marginStr ],
    'r' : [ drawKanzleiGatC_r, [temp_x, temp_y], marginStr, margin_r ],
    's' : [ drawKanzleiGatC_s, [temp_x, temp_y], marginStr, marginStr ],
    'longs' : [ drawKanzleiGatC_longs, [temp_x, temp_y], marginStr, -150 ],
    'germandbls' : [ drawKanzleiGatC_germandbls, [temp_x, temp_y], marginStr, marginStr-10 ],
    't' : [ drawKanzleiGatC_t, [temp_x, temp_y], marginStr, marginStr-15],
    'u' : [ drawKanzleiGatC_u, [temp_x, temp_y], marginStr, marginStr ],
    'v' : [ drawKanzleiGatC_v, [temp_x, temp_y], marginStr, marginRnd ],
    'w' : [ drawKanzleiGatC_w, [temp_x, temp_y], marginStr, marginRnd ],
    'x' : [ drawKanzleiGatC_x, [temp_x, temp_y], marginStr-117, margin_r ],
    'y' : [ drawKanzleiGatC_y, [temp_x, temp_y], marginStr, marginRnd ],
    'z' : [ drawKanzleiGatC_z_init, [temp_x, temp_y], marginRnd, marginRnd ],
    'z.init' : [ drawKanzleiGatC_z_init, [temp_x, temp_y], marginRnd, marginRnd ],
    'z.mid' : [ drawKanzleiGatC_z_mid, [temp_x, temp_y], marginStr+10, marginRnd ],
    'z.fina' : [ drawKanzleiGatC_z_fina, [temp_x, temp_y], marginStr, marginRnd ],

    # 'adieresis' : [ drawKanzleiGatC_adieresis, [temp_x, temp_y], marginRnd, marginStr ],
    # 'odieresis' : [ drawKanzleiGatC_odieresis, [temp_x, temp_y], marginRnd, marginRnd ],
    # 'udieresis' : [ drawKanzleiGatC_udieresis, [temp_x, temp_y], marginStr, marginStr ],

    
    # #### Ligatures
    
    # 'r_c' : [ drawKanzleiGatC_rc, [temp_x, temp_y], marginStr, marginStr-60 ],
    # 'longs_longs' : [ drawKanzleiGatC_longs_longs, [temp_x, temp_y, True], marginStr, -130 ],
    # 'f_f' : [ drawKanzleiGatC_f_f, [temp_x, temp_y], marginStr, -135 ],
    # 'longs_t' : [ drawKanzleiGatC_longs_t, [temp_x, temp_y], marginStr, marginStr-50 ],
    # 'f_t' : [ drawKanzleiGatC_f_t, [temp_x, temp_y], marginStr, marginStr-50 ],
    # 'f_f_t' : [ drawKanzleiGatC_f_f_t, [temp_x, temp_y], marginStr, marginStr-70 ],
    # 'c_h' : [ drawKanzleiGatC_ch, [temp_x, temp_y], marginRnd, marginRnd ]

    }






drawFunctions_thinStroke = {

    'f' : [ drawKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'g' : [ drawKanzleiGatC_g_thinStroke, [temp_x, temp_y] ],
    'h.dec' : [ drawKanzleiGatC_z_fina_thinStroke, [temp_x, temp_y] ],
    'j' : [ drawKanzleiGatC_g_thinStroke, [temp_x, temp_y] ],
    'p' : [ drawKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'q' : [ drawKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'y' : [ drawKanzleiGatC_h_thinStroke, [temp_x, temp_y] ],
    'z.mid' : [ drawKanzleiGatC_z_fina_thinStroke, [temp_x, temp_y] ],
    'z.fina' : [ drawKanzleiGatC_z_fina_thinStroke, [temp_x, temp_y] ],

    'longs' : [ drawKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'longs_longs' : [ drawKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'longs_t' : [ drawKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'f_f' : [ drawKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'f_t' : [ drawKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'f_f_t' : [ drawKanzleiGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'c_h' : [ drawKanzleiGatC_z_fina_thinStroke, [temp_x, temp_y] ],

    'germandbls' : [ drawKanzleiGatC_germandbls_thinStroke, [temp_x, temp_y] ],


    }







for key in drawFunctions:
    
    glyph = font[[key][0]]
    glyph.clear()
    
    foreground = glyph.getLayer("foreground")
    foreground.clear()
    background = glyph.getLayer("background")
    background.clear()
     
    function = drawFunctions[key][0]
    arguments = drawFunctions[key][1]

    output = function(*arguments)
	
    if key in drawFunctions_thinStroke:
        
        assert isinstance(output, tuple) and len(output)==2
        pass_from_thick_to_thin = output[1]
        output = output[0]
    
    # assert isinstance(output, glyphContext.GlyphBezierPath)
		
    writePathInGlyph(output, foreground)

    print("___",font[[key][0]])
    
    # ask for current status
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin   
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_before = margin_ts - margin_fg
    
    ### change value of margin
    glyph.leftMargin = drawFunctions[key][2]
    glyph.rightMargin = drawFunctions[key][3]
    
    ### ask inbetween 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_mid = margin_ts - margin_fg

    ### calculate value for movement of bg
    move_thinStroke = difference_before - difference_mid

    glyph.copyLayerToLayer('foreground', 'background')
 
   
    if key in drawFunctions_thinStroke:		
        thinstroke = glyph.getLayer("thinstroke")
        thinstroke.clear()

        thinfunction = drawFunctions_thinStroke[key][0]
        thinarguments = drawFunctions_thinStroke[key][1]
        thinoutput = thinfunction(*thinarguments, pass_from_thick=pass_from_thick_to_thin)
        
        writePathInGlyph(thinoutput, thinstroke)
        
    thinstroke = glyph.getLayer("thinstroke")
    thinstroke.translate((move_thinStroke, 0))
    
    ### ask final 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0 
        #print("(no thin line)")   
    difference_final = margin_ts - margin_fg
    
    foreground.clear()

    
    
    
    ### now draw the whole glphy with the magic tool
    ### first thick main stroke 
    guide_glyph = glyph.getLayer("background")

    w = A_A * penWidth 
    a = radians(90-alpha)
    h = penThickness

    if True:
        RectNibPen = RectNibPen_Just

    p = RectNibPen(
        glyphSet=CurrentFont(),
        angle=a,
        width=w,
        height=penThickness,
        trace=True
    )

    guide_glyph.draw(p)
    p.trace_path(glyph)


    ### now thin Stroke
    if key in drawFunctions_thinStroke:		

        guide_glyph = glyph.getLayer("thinstroke")
        w = penThickness 
        p = RectNibPen(
            glyphSet=CurrentFont(),
            angle=a,
            width=w,
            height=penThickness,
            trace=True
        )
        guide_glyph.draw(p)
        p.trace_path(glyph)

    ### keep a copy in background layer
    newLayerName = "original_math"
    glyph.layers[0].copyToLayer(newLayerName, clear=True)
    glyph.getLayer(newLayerName).leftMargin = glyph.layers[0].leftMargin
    glyph.getLayer(newLayerName).rightMargin = glyph.layers[0].rightMargin

    ### this will make it all one shape
    glyph.removeOverlap(round=0)
    
    ### from robstevenson to remove extra points on curves
    ### function itself is in create_stuff.py
    #select_extra_points(glyph, remove=True)




    

