import importlib
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatE
import version_3.creation.Schwuenge_GatE

importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatE)
importlib.reload(version_3.creation.Schwuenge_GatE)

from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatE import *
from version_3.creation.Schwuenge_GatE import *
#from version_3.creation.arc_path import ArcPath as BezierPath
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m




temp_x = 3
temp_y = 9


# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 14
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)




# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)





# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)











def drawSchriftzug7(x,y, instrokeLen=0.5, outstrokeLen=0.5):
    
    Schriftzug7 = BezierPath()  

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y)
    instroke = drawInstroke(*Grund_a, instrokeLen)

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, baseline)
    outstroke = drawOutstroke(*Grund_d, outstrokeLen)
    
    Schriftzug7.line(instroke.points[0], Grund_d)

    Schriftzug7 += instroke + outstroke
    drawPath(Schriftzug7)
    return Schriftzug7

#drawSchriftzug7(temp_x, temp_y)
    
    



def drawSchriftzug8(x,y, version="a", bottom="standard", instrokeLen=0.5, outstrokeLen=0.5, straightstrokeLen=6):
    
    Schriftzug8 = BezierPath()
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y)
    _, _, _, Raute_d_btm = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)
    
    if version == "a":
    
        instroke = drawInstroke(*Raute_a, instrokeLen)
        downstroke = drawGrundelementF(*Raute_a, straightstrokeLen)    
        Signatur = drawGrundelementE(*Raute_a)
        Schriftzug8 += instroke + downstroke + Signatur
    
    
    if version == "b":
        
        instroke = drawInstroke(*Raute_a, instrokeLen)
        Signatur = drawSchriftteil2(*Raute_a)
        Rundung_oben = drawSchneckenzug(*Raute_d, UPPER_D, 2, HSL_size=1, HSL_start=20, clockwise=False, inward=True)
        Rundung_unten = drawSchneckenzug(*Raute_d_btm, LOWER_H, 2, HSL_size=1, HSL_start=10, clockwise=True, inward=True)
        Schriftzug8.line(Rundung_oben.points[-1], Rundung_unten.points[-1])
        
        Schriftzug8 += instroke + Signatur + Rundung_oben + Rundung_unten


    outstroke = drawOutstroke(*Raute_d_btm, outstrokeLen)
    Schriftzug8 += outstroke
    drawPath(Schriftzug8)
    return Schriftzug8
    
#drawSchriftzug8(temp_x, temp_y, bottom="standard")           
#drawSchriftzug8(temp_x, temp_y, straightstrokeLen=5.2)           
#drawSchriftzug8(temp_x, temp_y, bottom="standard", version="b")           

    






def drawSchriftzug9(x,y, version="a", instrokeLen=0.5, outstrokeLen=0.5, Fuss="short"):
    
    Schriftzug9 = BezierPath()
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y)
    Raute_a_btm, _, _, Raute_d_btm = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)

    
    if version == "a":
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y)
        instroke = drawInstroke(*Grund_a, instrokeLen)
        downstroke = drawGrundelementF(*Grund_a, 6)
        Signatur = drawGrundelementE(*Raute_a_btm)
        outstroke = drawOutstroke(*Raute_d_btm, outstrokeLen)

        Schriftzug9 += instroke + downstroke + Signatur + outstroke
    
    
    if version == "b":
        instroke = drawInstroke(*Raute_a, instrokeLen)
        Rundung_oben = drawSchneckenzug(*Raute_a, UPPER_A, 1, HSL_size=1, HSL_start=24, clockwise=True, inward=True)
        Rundung_unten = drawSchneckenzug(*Raute_a_btm, LOWER_D, 2, HSL_size=1, HSL_start=19, clockwise=False, inward=True)
        Schriftzug9.line(Rundung_oben.points[-1], Rundung_unten.points[-1])
        
        if Fuss == "short":
            Fuss_short = drawSchriftteil1(*Raute_a_btm)
            outstroke = drawOutstroke(*Fuss_short.points[-1], outstrokeLen)
            Schriftzug9 += Fuss_short + outstroke
        if Fuss == "long":
            drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+1, baseline)
            Fuss_long = drawSchneckenzug(*Raute_a_btm, UPPER_F, 3, HSL_size=2, HSL_start=14.75, clockwise=True, inward=True)
            outstroke = drawOutstroke(*Fuss_long.points[-1], outstrokeLen)
            Schriftzug9 += Fuss_long + outstroke
        
        Schriftzug9 += instroke + Rundung_oben + Rundung_unten

        
    drawPath(Schriftzug9)
    
    return Schriftzug9
    

#drawSchriftzug9(temp_x, temp_y)           
#drawSchriftzug9(temp_x, temp_y, version="b", Fuss="short")           
#drawSchriftzug9(temp_x, temp_y, version="b", Fuss="long")       
    

    
    
    
def drawSchriftzug10(x,y, variante="a", instrokeLen=0.5, outstrokeLen=0.5, PosStroke=True):
    
    Schriftzug10 = BezierPath()      
    
    if variante == "a": height = y+2.75
    if variante == "b": height = y+4.25
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+0.8, height)
    instroke = drawInstroke(*Raute_d, instrokeLen, "down")
    Rundung_oben = drawSchneckenzug(*Raute_d, UPPER_E, 3, HSL_size=2, HSL_start=19, clockwise=False, inward=True)
    
    if PosStroke == True:
        pos_stroke = drawPosStroke(x, y)
        Schriftzug10 += pos_stroke

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)    
    outstroke = drawOutstroke(*Raute_d, outstrokeLen) 
    Schriftzug10.line(Rundung_oben.points[-1], Raute_d)

    Schriftzug10 += instroke + outstroke + Rundung_oben

    drawPath(Schriftzug10)
    return Schriftzug10
    
# drawSchriftzug10(temp_x, temp_y)       
# drawSchriftzug10(temp_x, temp_y, variante="b")           
    
    
    
    
    
    
def drawSchriftzug11(x, y, instrokeLen=0.5, outstrokeLen=0, pos_strokeLen=1):
    
    Schriftzug11 = BezierPath()      
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+0.8, y+4.25)
    instroke = drawInstroke(*Raute_d, instrokeLen, "down")
    Rundung_oben = drawSchneckenzug(*Raute_d, UPPER_E, 3, HSL_size=2, HSL_start=19, clockwise=False, inward=True)
    pos_stroke = drawPosStroke(x, y, length=pos_strokeLen) 

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)    
    outstroke = drawOutstroke(*Raute_d, outstrokeLen) 
    Schriftzug11.line(Rundung_oben.points[-1], Raute_d)
    Signatur = drawGrundelementE(*Raute_a)
    
    Schriftzug11 += instroke + pos_stroke + outstroke + Rundung_oben + Signatur

    drawPath(Schriftzug11)
    return Schriftzug11
    
#drawSchriftzug11(temp_x, temp_y)       




    
def drawSchriftzug12(x, y, version="a", outstrokeLen=0.5):
    
    Schriftzug12 = BezierPath()

    if version == "a":
        Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x+1, y+2.5)
        Bogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=3, HSL_start=27, clockwise=False, inward=True)
        Bogen_unten = drawSchneckenzug(*Bogen_oben.points[-1], UPPER_A, 4, HSL_size=5, HSL_start=30, clockwise=True, inward=True)
        Einsatz1 = drawGrundelementG(*Bogen_unten.points[-1], 0, "down")

    if version == "b":
        Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x+1, y+4)
        Bogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=2, HSL_start=26, clockwise=False, inward=True)
        Bogen_unten = drawSchneckenzug(*Bogen_oben.points[-1], UPPER_A, 3, HSL_size=4, HSL_start=37, clockwise=True, inward=True)
        Einsatz1 = drawGrundelementH(*Bogen_unten.points[-1], 0, "down")

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-0.7, y-4.65)
    _, _, _, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-0.5, y-5)
    Fuss = drawSchneckenzug(*Raute_a, UPPER_H, 2, HSL_size=10, HSL_start=28, clockwise=True, inward=True)
    outstroke = drawOutstroke(*Raute_d, outstrokeLen)

    Schriftzug12 += Bogen_oben + Bogen_unten + Fuss + outstroke
    drawPath(Schriftzug12)
    return Schriftzug12 
 
# drawSchriftzug12(temp_x, temp_y, "a", outstrokeLen=1)
# drawSchriftzug12(temp_x, temp_y, "b", outstrokeLen=0.5)
 
 
 

def drawSchriftzug13(x, y, instrokeLen=1.5, PosStroke=True):
    
    Schriftzug13 = BezierPath()  
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+0.3, y+3)

    instroke_top = drawInstroke(*Raute_d, instrokeLen, "down")
    
    # ------ Rundung oben ------
    # >>> Für Kanzlei: HSL_start von 10 auf 12.4 geändert, funktioniert perfekt :)
    Rundung_oben = drawSchneckenzug(*Raute_d, UPPER_E, 3, HSL_size=2, HSL_start=12.4, clockwise=False, inward=True)
    
    if PosStroke == True:
        # draw Modul + Raute MITTE 
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
        pos_stroke_links = drawPosStroke(x, y, length=1) 
        Schriftzug13 += pos_stroke_links 

    # draw Modul + Raute BTM
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, baseline)
    Schriftzug13.line(Rundung_oben.points[-1], Grund_d)
    
    Schriftzug13 += instroke_top + Rundung_oben 
    drawPath(Schriftzug13)
    return Schriftzug13
    
# drawSchriftzug13(temp_x, temp_y)
# drawSchriftzug13(temp_x+3, temp_y)




def drawSchriftzug_longs_longs_con(x, y):
    
    Schriftzug_longs_longs_con = BezierPath() 
  
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1.25, y+4) 
    
    connection_left = drawSchneckenzug(*Raute_a, UPPER_E, 2, HSL_size=1, HSL_start=25, clockwise=True, inward=False)
    
    connection_transition = drawSchneckenzug(*connection_left.points[-1], UPPER_G, 3, HSL_size=2, HSL_start=19.9, clockwise=True, inward=True)

    outstroke = drawGrundelementF(*connection_transition.points[-1], 1.5)

    
    Schriftzug_longs_longs_con += connection_left + connection_transition + outstroke
    drawPath(Schriftzug_longs_longs_con)
    return Schriftzug_longs_longs_con
    
#drawSchriftzug_longs_longs_con(temp_x, temp_y)



def drawSchriftzug_f_f_con(x, y):
    
    Schriftzug_f_f_con = BezierPath() 
  
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y+6)  
    
    connection_left = drawSchneckenzug(*Raute_d, UPPER_E, 5, HSL_size=1.5, HSL_start=8, clockwise=False, inward=False)
    
    Einsatz = drawGrundelementD(*connection_left.points[-1], 0.55)
    
    connection_transition = drawSchneckenzug(*Einsatz.points[0], UPPER_H, 2, HSL_size=1, HSL_start=16, clockwise=True, inward=True)

    outstroke = drawGrundelementF(*connection_transition.points[-1], 1.5)

    
    Schriftzug_f_f_con += connection_left + Einsatz + connection_transition + outstroke
    drawPath(Schriftzug_f_f_con)
    return Schriftzug_f_f_con

#drawSchriftzug_f_f_con(temp_x, temp_y)






def drawSchriftzug14(x,y, instrokeLen=0):
    
    Schriftzug14 = BezierPath()
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y)
    _, _, _, Raute_d_btm = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)
    instroke = drawInstroke(*Raute_a, instrokeLen)
    downstroke = drawGrundelementF(*Raute_a, 6)
    Signatur = drawGrundelementE(*Raute_a)

    Schriftzug14 += instroke + downstroke + Signatur
    drawPath(Schriftzug14)    
    return Schriftzug14
    
#drawSchriftzug14(temp_x, temp_y)           






 
 
 
 
    
def drawSchriftzug15(x,y, instrokeLen=0, outstrokeLen=1):
    
    Schriftzug15 = BezierPath()
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y)

        
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+ 0.6, y+0.3)
    #Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-0.5, y-0.25)
    instroke = drawInstroke(*Raute_c, instrokeLen, "down")
    Rundung_oben = drawSchneckenzug(*Raute_c, UPPER_E, 3, HSL_size=6.75, HSL_start=26, clockwise=False, inward=False)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)  
    Rundung_unten = drawSchneckenzug(*Raute_d, LOWER_A, 1, HSL_size=1, HSL_start=27, clockwise=True, inward=True)

    Schriftzug15.line(Rundung_oben.points[-1], Rundung_unten.points[-1])

    
    outstroke = drawOutstroke(*Raute_d, outstrokeLen)

    Schriftzug15 += instroke + Rundung_oben + Rundung_unten + outstroke
    drawPath(Schriftzug15)
    return Schriftzug15

# drawSchriftzug15(temp_x, temp_y)




def drawSchriftzug16(x,y, instrokeLen=1, outstrokeLen=3):
    
    Schriftzug16 = BezierPath()
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-0.5)

        
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+ 0.6, y-0.2)
    #Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-0.5, y-0.25)
    instroke = drawInstroke(*Raute_c, instrokeLen, "down")
    Rundung_oben = drawSchneckenzug(*Raute_c, UPPER_E, 3, HSL_size=6, HSL_start=25.5, clockwise=False, inward=False)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)  
    Rundung_unten = drawSchneckenzug(*Raute_d, LOWER_A, 1, HSL_size=1, HSL_start=21.5, clockwise=True, inward=True)

    Schriftzug16.line(Rundung_oben.points[-1], Rundung_unten.points[-1])

    
    outstroke = drawOutstroke(*Raute_d, outstrokeLen)

    Schriftzug16 += instroke + Rundung_oben + Rundung_unten + outstroke
    drawPath(Schriftzug16)
    return Schriftzug16

# drawSchriftzug16(temp_x, temp_y)






def drawSchriftzug24(x,y):
    
    Schriftzug24 = BezierPath()
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, baseline)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline-1)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-1, baseline-0.5)
    _, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)

    #liegenderSchwung_links = drawSchneckenzug(*Raute_a, UPPER_E, 4, HSL_size=1.5, HSL_start=10.6, clockwise=True, inward=True)
    liegenderSchwung_links = drawSchneckenzug(x, y, UPPER_E, 4, HSL_size=1.5, HSL_start=10.6, clockwise=True, inward=True)
    liegenderSchwung_rechts = drawSchneckenzug(*liegenderSchwung_links.points[-1], LOWER_A, 4, HSL_size=1.5, HSL_start=4.75, clockwise=False, inward=False)

    Schriftzug24 = liegenderSchwung_links + liegenderSchwung_rechts
    drawPath(Schriftzug24)    
    return Schriftzug24 
    
# drawSchriftzug24(temp_x, temp_y)





def drawSchriftzug25(x, y, outstrokeLen=0):
    
    Schriftzug25 = BezierPath()

    # Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-1, 
    # drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y-0.5)
    # drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y-1.5)
    # drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y-2.5)

    anfang = drawSchneckenzug(x, y, UPPER_E, 4, HSL_size=1, HSL_start=9.25, clockwise=True, inward=True)
    ende = drawSchneckenzug(*anfang.points[-1], UPPER_A, 4, HSL_size=2, HSL_start=18, clockwise=True, inward=False)
    outstroke = drawOutstroke(*ende.points[-1], outstrokeLen, "down")
    
    Schriftzug25 = anfang + ende + outstroke
    drawPath(Schriftzug25)    
    return Schriftzug25 
    
#drawSchriftzug25(temp_x, temp_y)







def drawSchriftzug26(x,y):
    
    Schriftzug26 = BezierPath()
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, baseline-0.25)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-0.2, baseline-0.85)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-0.5, baseline-0.5)
    _, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline-0.25)

    #liegenderSchwung_links = drawSchneckenzug(*Raute_a, UPPER_E, 4, HSL_size=1.25, HSL_start=8, clockwise=True, inward=True)
    liegenderSchwung_links = drawSchneckenzug(x, y, UPPER_E, 4, HSL_size=1.25, HSL_start=8, clockwise=True, inward=True)
    liegenderSchwung_rechts = drawSchneckenzug(*liegenderSchwung_links.points[-1], LOWER_A, 4, HSL_size=1, HSL_start=4, clockwise=False, inward=False)

    Schriftzug26 = liegenderSchwung_links + liegenderSchwung_rechts
    drawPath(Schriftzug26)    
    return Schriftzug26 
    
# drawSchriftzug26(temp_x, temp_y)





def drawSchriftzug27(x,y):
    
    Schriftzug27 = BezierPath()

     # Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-1, y)
    # drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y-0.5)
    # drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y-1.5)
    # drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y-2.5)
    # drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y-3)

    anfang = drawSchneckenzug(x, y, UPPER_E, 4, HSL_size=1, HSL_start=9.25, clockwise=True, inward=True)
    ende = drawSchneckenzug(*anfang.points[-1], UPPER_A, 4, HSL_size=1, HSL_start=22, clockwise=True, inward=False)

    Schriftzug27 = anfang + ende
    drawPath(Schriftzug27)    
    return Schriftzug27 
    
# drawSchriftzug27(temp_x, temp_y)








def drawSchriftzug28(x,y):
    
    Schriftzug28 = BezierPath()
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, baseline-0.25)

    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline-0.25)

    #liegenderSchwung_links = drawSchneckenzug(*Raute_a, UPPER_E, 4, HSL_size=0.5, HSL_start=4.5, clockwise=True, inward=True)
    liegenderSchwung_links = drawSchneckenzug(x, y, UPPER_E, 4, HSL_size=0.5, HSL_start=4.5, clockwise=True, inward=True)
    liegenderSchwung_rechts = drawSchneckenzug(*liegenderSchwung_links.points[-1], LOWER_A, 4, HSL_size=0.5, HSL_start=2.75, clockwise=False, inward=False)

    Schriftzug28 = liegenderSchwung_links + liegenderSchwung_rechts
    drawPath(Schriftzug28)    
    return Schriftzug28 
    
# drawSchriftzug28(temp_x, temp_y)





    
    
def drawSchriftzug29(x,y, instrokeLen=2.4, outstrokeLen=1.8):
    
    Schriftzug29 = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y-4.5)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+2, y)    

    instroke = drawInstroke(*Raute_a, instrokeLen)
    Rundung_oben = drawSchneckenzug(*Raute_a, UPPER_H, 2, HSL_size=3, HSL_start=11, clockwise=True, inward=False)
    Einsatz = drawGrundelementF(*Rundung_oben.points[-1], 1.3)
    Rundung_unten = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=3, HSL_start=18, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Rundung_unten.points[-1], outstrokeLen, "down")
    ### nur zur orientierung damit der Strich unten gut auftrifft
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x-1, baseline)
    
    Schriftzug29 += instroke + Rundung_oben + Einsatz + Rundung_unten + outstroke
    drawPath(Schriftzug29)
    return Schriftzug29

#drawSchriftzug29(temp_x, temp_y)




def drawSchriftzug30(x,y, version="a", instrokeLen=2, outstrokeLen=0.325):
    
    Schriftzug30 = BezierPath()
    
    drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline) ### pos unten outstroke
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y-4.5)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+2, y)    
    instroke = drawInstroke(*Raute_a, instrokeLen)
    Rundung_oben = drawSchneckenzug(*Raute_a, UPPER_H, 2, HSL_size=3, HSL_start=11, clockwise=True, inward=False)
    
    if version == "a": EinsatzLen = 1.8
    if version == "b": EinsatzLen = 2.8
    if version == "c": EinsatzLen = 3.8
        
    Einsatz = drawGrundelementF(*Rundung_oben.points[-1], EinsatzLen)
    Rundung_unten = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=3, HSL_start=18, clockwise=True, inward=False)
    
               
    outstroke = drawOutstroke(*Rundung_unten.points[-1], outstrokeLen, "down")

    Schriftzug30 += instroke + Rundung_oben + Einsatz + Rundung_unten + outstroke
    drawPath(Schriftzug30)
    return Schriftzug30

# drawSchriftzug30(temp_x, temp_y, "a")
# drawSchriftzug30(temp_x, temp_y, "b")
# drawSchriftzug30(temp_x, temp_y, "c")







def drawSchriftzug31(x, y, outstrokeLen=1.75):
    #x += 2
    Schriftzug31 = BezierPath()

    Grund_a, _, _, _ = drawGrundelOrient(A1, A2, offset, x, y+3.5)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, baseline)
    
    Spitze_oben = drawSchneckenzug(*Grund_a, UPPER_D, 4, HSL_size=1, HSL_start=12, clockwise=False, inward=False)
    Einsatz_oben = drawGrundelementC(*Spitze_oben.points[-1], 0)
    Rundung_oben = drawSchneckenzug(*Einsatz_oben.points[-1], UPPER_H, 2, HSL_size=2, HSL_start=26.5, clockwise=True, inward=False)
    Einsatz_mitte = drawGrundelementF(*Rundung_oben.points[-1], 0)
    Rundung_unten = drawSchneckenzug(*Einsatz_mitte.points[-1], LOWER_B, 2, HSL_size=2, HSL_start=28, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Rundung_unten.points[-1], outstrokeLen, "down")

    Schriftzug31 = Spitze_oben + Einsatz_oben + Rundung_oben + Einsatz_mitte + Rundung_unten + outstroke
    drawPath(Schriftzug31)    
    return Schriftzug31 
    
#drawSchriftzug31(temp_x, temp_y)
  
    




def drawSchriftzug32(x, y, outstrokeLen=0):
    
    Schriftzug32 = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y)
    Raute_a, Raute_b_btm, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y+1)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y-1)

    Rundung_oben = drawSchneckenzug(x, y, UPPER_E, 4, HSL_size=1.5, HSL_start=11.5, clockwise=False, inward=True)
    Rundung_unten = drawSchneckenzug(*Rundung_oben.points[-1], UPPER_A, 4, HSL_size=2, HSL_start=11, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Rundung_unten.points[-1], outstrokeLen, "down")

    Schriftzug32 = Rundung_oben + Rundung_unten + outstroke
    drawPath(Schriftzug32)    
    return Schriftzug32 
    
#drawSchriftzug32(temp_x, temp_y)
  
    
    

def drawSchriftzug33(x, y, outstrokeLen=1, EinsatzLen=0):
    
    Schriftzug33 = BezierPath()
    
    _, Raute_b, _, _ = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y+1)
    drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y)
    drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y-0.5)

    ### Rundung_oben = drawSchneckenzug(*Raute_b, UPPER_E, 4, HSL_size=1.5, HSL_start=11.5, clockwise=False, inward=True)
    Rundung_oben = drawSchneckenzug(x, y, UPPER_E, 4, HSL_size=1.5, HSL_start=11.5, clockwise=False, inward=True)
    Einsatz = drawGrundelementE(*Rundung_oben.points[-1], EinsatzLen)
    Rundung_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=2, HSL_start=8, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Rundung_unten.points[-1], outstrokeLen, "down")

    Schriftzug33 = Rundung_oben + Einsatz + Rundung_unten + outstroke
    drawPath(Schriftzug33)    
    return Schriftzug33 
    
#drawSchriftzug33(temp_x, temp_y)
  



def drawSchriftzug34(x, y, outstrokeLen=0):

    Schriftzug34 = BezierPath()
    
    Raute_a, Raute_b_btm, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y+1)


    Rundung_oben = drawSchneckenzug(x, y, UPPER_E, 4, HSL_size=0.5, HSL_start=4.55, clockwise=False, inward=True)
    Rundung_unten = drawSchneckenzug(*Rundung_oben.points[-1], UPPER_A, 4, HSL_size=0.5, HSL_start=3.25, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Rundung_unten.points[-1], outstrokeLen, "down")

    Schriftzug34 = Rundung_oben + Rundung_unten + outstroke
    drawPath(Schriftzug34)    
    return Schriftzug34 
    
#drawSchriftzug34(temp_x, temp_y, outstrokeLen=1)
  




def drawSchriftzug_g_rechts(x, y, instrokeLen=0.5, outstrokeLen=0):
    
    Schriftzug_g_rechts = BezierPath() 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y)
    instroke = drawInstroke(*Raute_a, instrokeLen)
    downstroke = drawGrundelementF(*Raute_a, 6)
    Signatur = drawGrundelementE(*Raute_a)
    Bogen_unten = drawSchneckenzug(*downstroke.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=22, clockwise=True, inward=False)

    Schriftzug_g_rechts += instroke + downstroke+Signatur + Bogen_unten
    drawPath(Schriftzug_g_rechts)
    return Schriftzug_g_rechts
    
#drawSchriftzug_g_rechts(temp_x, temp_y)  






def drawSchriftzug_x_hook(x, y):
    
    Schriftzug_x_hook = BezierPath() 
    
    Raute_a, _, _, _ = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y)
    instroke = drawInstroke(*Raute_a, 1)
    x_hook = drawSchneckenzug(*instroke.points[-1], UPPER_E, 8, HSL_size=1, HSL_start=18, clockwise=False, inward=True)
    
    Schriftzug_x_hook += instroke + x_hook
    drawPath(Schriftzug_x_hook) 
    return Schriftzug_x_hook
    
#drawSchriftzug_x_hook(temp_x, temp_y)




#### Ich brauche das doch nicht weil ich es in hier in der GattungE direkt mit einer
#### einzelnen Schneckenlinie gelöst habe. Bei Gat C ging das irgendwie nicht.
# # def drawSchriftzug_z_Initial(x, y):
        
# #     Schriftzug_z_Initial = BezierPath()

# #     Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.5, y+2) 
# #     oben1 = drawSchneckenzug(*Grund_a, UPPER_E, 6, HSL_size=0.5, HSL_start=6, clockwise=False, inward=True)
# #     Einsatz = drawGrundelementC(*oben1.points[-1], 0.5)

# #     oben2 = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 6, HSL_size=0.5, HSL_start=5, clockwise=True, inward=False)
# #     outstroke = drawOutstroke(*oben2.points[-1], 1.5, "down")        
    
# #     Schriftzug_z_Initial += oben1 + oben2 + outstroke + Einsatz
# #     drawPath(Schriftzug_z_Initial)
# #     return Schriftzug_z_Initial

# # #drawSchriftzug_z_Initial(temp_x, temp_y)



 




def drawSchriftzug_ft_lig(x, y):
    
    Schriftzug_ft_lig = BezierPath()  
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+1.25, y+3.25)    
    #drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+3.75, y+2.75)

    instroke_top = drawInstroke(*Raute_d, 1.25, "down")
    
    Rundung_oben = drawSchneckenzug(*Raute_d, UPPER_E, 3, HSL_size=2, HSL_start=24.5, clockwise=False, inward=True)
    
    # Raute MITTE 
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
    pos_stroke_links = drawPosStroke(x, y, length=1) 
      
    # Raute BTM
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, baseline)
    Schriftzug_ft_lig.line(Rundung_oben.points[-1], Grund_d)
    
    #Spitze
    con_left = drawSchneckenzug(*instroke_top.points[-1], UPPER_E, 4, HSL_size=1, HSL_start=9, clockwise=True, inward=True)
    con_right = drawSchneckenzug(*con_left.points[-1], LOWER_A, 4, HSL_size=1, HSL_start=7.4, clockwise=False, inward=True)
    Spitze = con_left + con_right
    
    Schriftzug_ft_lig += instroke_top + Rundung_oben + pos_stroke_links + Spitze
    drawPath(Schriftzug_ft_lig)
    return Schriftzug_ft_lig
    
#drawSchriftzug_ft_lig(temp_x, temp_y)



def drawDieresis(x, y):
    
    Dieresis = BezierPath() 
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y+3)
    links = drawGrundelementE(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x+2.5, y+3)
    rechts = drawGrundelementE(*Raute_a)

    Dieresis = links + rechts
    drawPath(Dieresis) 
    return Dieresis
    
#drawDieresis(temp_x, temp_y)




