import importlib
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatE

importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatE)

from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatE import *

from version_3.creation.arc_path import ArcPath as BezierPath
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m




# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 18
page_height = 14  ### soll 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)




# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 1.5

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)


# temporary to check height of numbers
#line((1*modul_width, 12.5*modul_height), (12*modul_width, 12.5*modul_height))





# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "D")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 7








      
          
def drawSchriftzug_C_DeckungOben(x, y):
        
    Schriftzug_C_DeckungOben = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+11.25, y+8.25) 
    DeckungOben_left = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1.25, HSL_start=1, clockwise=False, inward=False)
    DeckungOben_right = drawSchneckenzug(*DeckungOben_left.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=8, clockwise=False, inward=False)

    Schriftzug_C_DeckungOben  = DeckungOben_left + DeckungOben_right
    drawPath(Schriftzug_C_DeckungOben)
    return Schriftzug_C_DeckungOben

# drawSchriftzug_C_DeckungOben(temp_x, temp_y)



def drawSchriftzug_C_stehenderSchwung(x, y):
        
    Schriftzug_C_stehenderSchwung = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+8, y+5.75) 
    oben = drawSchneckenzug(*Grund_a, UPPER_E, 2, HSL_size=0, HSL_start=52, clockwise=False, inward=True)

    instroke = drawInstroke(*oben.points[0], 1, "down")
    Einsatz = drawGrundelementG(*oben.points[-1], 1.1, "down")
    mitte = drawSchneckenzug(*Einsatz.points[-1], LOWER_C, 2, HSL_size=2, HSL_start=26, clockwise=True, inward=True)
    Einsatz2 = drawOutstroke(*mitte.points[-1], 0.545, "down")
    unten = drawSchneckenzug(*Einsatz2.points[0], LOWER_E, 4, HSL_size=4, HSL_start=20.4, clockwise=True, inward=True)
    
    Schriftzug_C_stehenderSchwung += oben + instroke + Einsatz + mitte + Einsatz2 + unten
    drawPath(Schriftzug_C_stehenderSchwung)
    return Schriftzug_C_stehenderSchwung

# drawSchriftzug_C_stehenderSchwung(temp_x, temp_y)




def drawSchriftzug_C_liegenderSchwung(x, y):
        
    Schriftzug_C_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+0.25) 
    right = drawSchneckenzug(*Grund_a, LOWER_A, 4, HSL_size=2, HSL_start=24, clockwise=False, inward=False)
    outstroke = drawOutstroke(*right.points[-1], 1)

    Schriftzug_C_liegenderSchwung += right + outstroke
    drawPath(Schriftzug_C_liegenderSchwung)
    return Schriftzug_C_liegenderSchwung

#drawSchriftzug_C_liegenderSchwung(temp_x, temp_y)








      
          
def drawSchriftzug_D_DeckungOben(x, y):
        
    Schriftzug_D_DeckungOben = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+10, y+7.5) 
    DeckungOben_left = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1.25, HSL_start=1, clockwise=False, inward=False)
    DeckungOben_right = drawSchneckenzug(*DeckungOben_left.points[-1], LOWER_H, 3, HSL_size=1, HSL_start=8, clockwise=False, inward=False)

    Schriftzug_D_DeckungOben  = DeckungOben_left + DeckungOben_right
    drawPath(Schriftzug_D_DeckungOben)
    return Schriftzug_D_DeckungOben

#drawSchriftzug_D_DeckungOben(temp_x, temp_y)



def drawSchriftzug_D_stehenderSchwung(x, y):
        
    Schriftzug_D_stehenderSchwung = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+7, y+5) 
    oben = drawSchneckenzug(*Grund_a, UPPER_E, 2, HSL_size=0, HSL_start=52, clockwise=False, inward=True)

    instroke = drawInstroke(*oben.points[0], 2, "down")
    Einsatz = drawGrundelementG(*oben.points[-1], 1, "down")
    mitte = drawSchneckenzug(*Einsatz.points[-1], LOWER_C, 2, HSL_size=2, HSL_start=15, clockwise=True, inward=True)
    Einsatz2 = drawOutstroke(*mitte.points[-1], 1.1, "down")
    unten = drawSchneckenzug(*Einsatz2.points[0], LOWER_E, 3, HSL_size=4, HSL_start=16, clockwise=True, inward=True)
    
    Schriftzug_D_stehenderSchwung += oben + instroke + Einsatz + mitte + Einsatz2 + unten
    drawPath(Schriftzug_D_stehenderSchwung)
    return Schriftzug_D_stehenderSchwung

#drawSchriftzug_D_stehenderSchwung(temp_x, temp_y)




def drawSchriftzug_D_liegenderSchwung(x, y):
        
    Schriftzug_D_liegenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.25) 
    right = drawSchneckenzug(*Grund_a, LOWER_A, 4, HSL_size=1.5, HSL_start=22, clockwise=False, inward=True)
    outstroke = drawOutstroke(*right.points[-1], 1.75)
    up = drawSchneckenzug(*outstroke.points[0], LOWER_E, 8, HSL_size=3.9, HSL_start=47, clockwise=False, inward=True)
    Einsatz = drawOutstroke(*up.points[-1], 1.5, "down")
    left = drawSchneckenzug(*Einsatz.points[0], UPPER_E, 2, HSL_size=4, HSL_start=46, clockwise=False, inward=True)
    left_btm = drawSchneckenzug(*left.points[-1], UPPER_C, 6, HSL_size=2, HSL_start=16, clockwise=False, inward=True)


    Schriftzug_D_liegenderSchwung += right + outstroke + up + Einsatz + left + left_btm
    drawPath(Schriftzug_D_liegenderSchwung)
    return Schriftzug_D_liegenderSchwung

#drawSchriftzug_D_liegenderSchwung(temp_x, temp_y)










def drawSchriftzug_G_Hauptbogen(x, y):
        
    Schriftzug_G_Hauptbogen = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+0.5, y+4.5) 

    oben = drawSchneckenzug(*Grund_a, UPPER_E, 6, HSL_size=1, HSL_start=6.5, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*oben.points[-1], 1.75)
    mitte = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 6, HSL_size=2, HSL_start=26, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.01 * i      for  i in range(0, 7)])

    Schriftzug_G_Hauptbogen = oben + Einsatz + mitte
    drawPath(Schriftzug_G_Hauptbogen)
    return Schriftzug_G_Hauptbogen

#drawSchriftzug_G_Hauptbogen(temp_x, temp_y)







def drawSchriftzug_G_Deckung(x, y):
        
    Schriftzug_G_Deckung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1.8, y-3.5) 

    
    # fast wie Schriftteil 9
    HSL_size = 2
    HSL_start = 5
          
         
    G1, G2 = line_G_vonH_o_gr_s(Grund_a, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)
    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     
    
    Schriftzug_G_Deckung.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))

    
    ### zweiter Teil
    HSL_size = 0.5
    HSL_start = 5
    
    G3, G4 = line_G_vonF_o_kl_s(Grund_a, *angles, part, HSL_size, HSL_start)
    H3, H4 = line_H_vonG_o_kl(G3, *angles, part, HSL_size, HSL_start-HSL_size)
    A5, A6 = line_A_vonH_o_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*3)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*4)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    E3, E4 = line_E_vonD_u_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*6)
    
    Schriftzug_G_Deckung.arc(*drawKreisSeg(H3, HSL_start-HSL_size, angle_3, angle_2, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(A5, HSL_start-HSL_size*2, angle_2, angle_1, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(B3, HSL_start-HSL_size*3, angle_1, angle_0, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(C3, HSL_start-HSL_size*4, angle_16, angle_15, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(D3, HSL_start-HSL_size*5, angle_15, angle_14, True))
    Schriftzug_G_Deckung.arc(*drawKreisSeg(E3, HSL_start-HSL_size*6, angle_14, angle_13, True))
    


    drawPath(Schriftzug_G_Deckung)

    return Schriftzug_G_Deckung

#drawSchriftzug_G_Deckung(temp_x, temp_y)








def drawSchriftzug_G_Fuss(x, y):
        
    Schriftzug_G_Fuss = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.25, y-4.25) 
 
    links = drawSchneckenzug(*Grund_a, UPPER_H, 3, HSL_size=4, HSL_start=6, clockwise=False, inward=False)
    
    Einsatz = drawGrundelementD(*Grund_a, 1.25)
    rechts = drawSchneckenzug(*Einsatz.points[4], LOWER_H, 3, HSL_size=4, HSL_start=16, clockwise=False, inward=False)

    outstroke = drawOutstroke(*rechts.points[-1], 1)

    Schriftzug_G_Fuss = links + Einsatz + rechts + outstroke
    drawPath(Schriftzug_G_Fuss)

    return Schriftzug_G_Fuss
    
    
#drawSchriftzug_G_Fuss(temp_x, temp_y)





    
    
    
def drawSchriftzug_G_Bauch(x, y):
        
    Schriftzug_G_Bauch = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6.5, y+0.3) 
    top = drawSchneckenzug(*Grund_a, UPPER_G, 2, HSL_size=4, HSL_start=9, clockwise=False, inward=False)
    oben_rechts = drawSchneckenzug(*Grund_a, UPPER_G, 3, HSL_size=2, HSL_start=10, clockwise=True, inward=False)
    Einsatz = drawGrundelementF(*oben_rechts.points[-1], 0.75)
    unten_rechts = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=21.5, clockwise=True, inward=False)
    outstroke = drawOutstroke(*unten_rechts.points[-1], 1, "down")

    Schriftzug_G_Bauch = top + oben_rechts + Einsatz + unten_rechts + outstroke
    drawPath(Schriftzug_G_Bauch)
    return Schriftzug_G_Bauch 
    
#drawSchriftzug_G_Bauch(temp_x, temp_y)

    
    
    

def drawSchriftzug_G_stehenderSchwung(x, y, instrokeLen=0.5):
        
    Schriftzug_G_stehenderSchwung = BezierPath()   
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-0.5) 
    Einsatz = drawGrundelementD(*Grund_a, length=0)
    
    oben = drawSchneckenzug(*Grund_a, LOWER_H, 5, HSL_size=1, HSL_start=11, clockwise=True, inward=False)
    instroke = drawInstroke(*oben.points[-1], instrokeLen, "down")
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=0.5, HSL_start=12, clockwise=True, inward=False)

    Schriftzug_G_stehenderSchwung += instroke + oben + unten + Einsatz
    drawPath(Schriftzug_G_stehenderSchwung)
    return Schriftzug_G_stehenderSchwung

#drawSchriftzug_G_stehenderSchwung(temp_x, temp_y)

   



def drawAuge(x, y):
    
    Auge = BezierPath()
    
    Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.25, y+3.5)

    start_oben = drawSchneckenzug(*Raute_a, LOWER_A, 2, HSL_size=4, HSL_start=8, clockwise=False, inward=False)
    bow = drawSchneckenzug(*start_oben.points[-1], UPPER_G, 6, HSL_size=0.75, HSL_start=3, clockwise=True, inward=False)

    Auge += start_oben + bow
    drawPath(Auge)  
    return Auge
        
#drawAuge(temp_x, temp_y)









def drawSchriftzug_H_stehenderSchwung(x, y):
    x+=2
    Schriftzug_H_stehenderSchwung = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+9, y+8) 
    oben = drawSchneckenzug(*Grund_a, UPPER_E, 2, HSL_size=0, HSL_start=68, clockwise=False, inward=True)

    instroke = drawInstroke(*oben.points[0], 2, "down")
    Einsatz = drawGrundelementG(*oben.points[-1], 2, "down")
    mitte = drawSchneckenzug(*Einsatz.points[-1], LOWER_C, 2, HSL_size=2, HSL_start=52, clockwise=True, inward=True)
    Einsatz2 = drawOutstroke(*mitte.points[-1], 1.25, "down")
    unten = drawSchneckenzug(*Einsatz2.points[0], LOWER_E, 2, HSL_size=4, HSL_start=20, clockwise=True, inward=True)
    
    turn = drawSchneckenzug(*unten.points[-1], LOWER_G, 6, HSL_size=0, HSL_start=3, clockwise=True, inward=True)
    Einsatz3 = drawOutstroke(*turn.points[-1], 0.75)

    Schleife_links = drawSchneckenzug(*Einsatz3.points[0], UPPER_E, 4, HSL_size=1, HSL_start=5, clockwise=True, inward=False)
    Schleife_rechts = drawSchneckenzug(*Schleife_links.points[-1], LOWER_A, 4, HSL_size=1, HSL_start=12, clockwise=False, inward=True)
    
    Schriftzug_H_stehenderSchwung += oben + instroke + Einsatz + mitte + Einsatz2 + unten + turn + Einsatz3 + Schleife_links + Schleife_rechts
    drawPath(Schriftzug_H_stehenderSchwung)
    return Schriftzug_H_stehenderSchwung

#drawSchriftzug_H_stehenderSchwung(temp_x, temp_y)


def drawSchriftzug_H_body(x, y):
    x+=2
    Schriftzug_H_body = BezierPath()   

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+9.2, y+5.9) 
    oben = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=1, HSL_start=15, clockwise=True, inward=True)
    mitte = drawSchneckenzug(*oben.points[-1], LOWER_B, 3, HSL_size=4, HSL_start=43, clockwise=True, inward=False)
    outstroke = drawOutstroke(*mitte.points[-1], 0.75, "down")

    
    Schriftzug_H_body += oben + mitte + outstroke
    drawPath(Schriftzug_H_body)
    return Schriftzug_H_body

#drawSchriftzug_H_body(temp_x, temp_y)










def drawSchriftzug_W_Bogen_links(x, y):
        
    Schriftzug_W_Bogen_links = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+4) 
    oben = drawSchneckenzug(*Grund_a, UPPER_E, 6, HSL_size=0.5, HSL_start=9, clockwise=False, inward=False)
    Einsatz = drawGrundelementC(*oben.points[-1], 1.85)
    mitte = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 6, HSL_size=2, HSL_start=28, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.01 * i      for  i in range(0, 7)])
        
    Schriftzug_W_Bogen_links = oben + Einsatz + mitte
    drawPath(Schriftzug_W_Bogen_links)

    return Schriftzug_W_Bogen_links

#drawSchriftzug_W_Bogen_links(temp_x, temp_y)





def drawSchriftzug_W_Bogen_mitte(x, y):
        
    Schriftzug_W_Bogen_mitte = BezierPath()   
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5.15, y+2.5) 
    Bogen_oben = drawSchneckenzug(*Grund_a, UPPER_B, 2, HSL_size=1, HSL_start=10, clockwise=False, inward=False)
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 1.25)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=1, HSL_start=20, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 0.5, "down")
    
    Schriftzug_W_Bogen_mitte = Bogen_oben + Bogen_unten + Einsatz + outstroke
    drawPath(Schriftzug_W_Bogen_mitte)
    return Schriftzug_W_Bogen_mitte

#drawSchriftzug_W_Bogen_mitte(temp_x, temp_y)
    
    
    

def drawSchriftzug_W_Bogen_rechts(x, y):
        
    Schriftzug_W_rechts = BezierPath()   

    ### UNTEN    
    Schriftzug_W_rechts_unten = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+9, y+0.5) 
    Bogen_oben = drawSchneckenzug(*Grund_b, UPPER_E, 5, HSL_size=1, HSL_start=8, clockwise=False, inward=True)
    Einsatz = drawGrundelementD(*Bogen_oben.points[-1], 0.4)
    Bogen_unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=6, HSL_start=10, clockwise=True, inward=False)    
    outstroke = drawOutstroke(*Bogen_unten.points[-1], 1.75, "down")
    Schriftzug_W_rechts_unten = Bogen_oben + Einsatz + Bogen_unten + outstroke

    ### OBEN
    Schriftzug_W_rechts_oben = BezierPath()   
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+8.5, y+2.75) 
    liegenderSchwung_links = drawSchneckenzug(*Grund_a, UPPER_E, 3, HSL_size=1, HSL_start=12, clockwise=True, inward=False)
    Einsatz = drawGrundelementD(*liegenderSchwung_links.points[-1], 0.1)
    liegenderSchwung_rechts = drawSchneckenzug(*Einsatz.points[4], LOWER_H, 3, HSL_size=4, HSL_start=6, clockwise=False, inward=False)     
    Schriftzug_W_rechts_oben = liegenderSchwung_links + Einsatz + liegenderSchwung_rechts
   
    Schriftzug_W_rechts = Schriftzug_W_rechts_unten + Schriftzug_W_rechts_oben
    drawPath(Schriftzug_W_rechts)

    return Schriftzug_W_rechts

#drawSchriftzug_W_Bogen_rechts(temp_x, temp_y)










