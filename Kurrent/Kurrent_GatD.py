import importlib
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatC
import version_3.creation.Halbboegen_GatC
import Kurrent_Schriftzuege_GatC
import Kurrent_Schriftzuege_GatD        ### für Versalien

importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatC)
importlib.reload(version_3.creation.Halbboegen_GatC)
importlib.reload(Kurrent_Schriftzuege_GatC)
importlib.reload(Kurrent_Schriftzuege_GatD)

from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatC import *
from version_3.creation.Halbboegen_GatC import *
from Kurrent_Schriftzuege_GatC import *
from Kurrent_Schriftzuege_GatD import *

# from version_3.creation.arc_path import ArcPath as BezierPath
from version_3.creation.special_drawbot import BezierPath, drawPath

from nibLib.pens.rectNibPen import RectNibPen
from version_3.creation.rect_nib_pen import RectNibPen as RectNibPen_Just
from math import radians
import math as m
import collections
import glyphContext




# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 22
page_height = 18

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)






# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 1.5

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)

# temporary to check height of numbers
# line((1*modul_width, 12.5*modul_height), (12*modul_width, 12.5*modul_height))




# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 7


    
# ________________________________________________________  
    
    
    
    

def drawKurrentGatC_a(x, y):      
            
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    
    bogen_links = drawSchriftzug5(*Grund_d, height="1.25", instrokeLen=0.5)
    connection_stroke = drawOutstroke(*bogen_links.points[0], 2.1, "up")
    schleife_down_rechts = drawSchriftzug3(x+3, y)

    KurrentGatC_a = bogen_links + connection_stroke + schleife_down_rechts
    trans_scale(KurrentGatC_a, valueToMoveGlyph)
    return KurrentGatC_a
    
    
def drawKurrentGatC_adieresis(x, y):
    
    Dieresis = drawDieresis(x, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    glyph_a = drawKurrentGatC_a(x, y)
    KurrentGatC_udieresis = glyph_a + Dieresis
    return KurrentGatC_udieresis
    
    
    
    
    
    
def drawKurrentGatC_b(x,y):        
     
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    
    instroke = drawInstroke(*Grund_a, 1)
    Strich_links = drawSchriftzug10(x, y, outstrokeLen=1.6)

    #Bogen_rechts = drawSchriftteil8(*Grund_b)
    Bogen_rechts = drawSchriftzug3_loopRight(x+3, y, instrokeLen=0.5)
    
    #hook = drawSchriftzug23_KurrentKanzlei(x+3, y+1)
    #hook = drawSchriftzug_b_hook(x+3, y+1, version="0.5")
    hook = drawSchriftzug_b_hook(x+3, y+1, version="1")    ### passt mit m folgend
    #hook = drawSchriftzug_b_hook(x+3, y+1, version="1.5")

    KurrentGatC_b = instroke + Strich_links + Bogen_rechts + hook
    trans_scale(KurrentGatC_b, valueToMoveGlyph)
    return KurrentGatC_b
    
    
    



def drawKurrentGatC_c(x, y, hook=True):
    
    KurrentGatC_c = BezierPath()
           
    Bogen_links = drawSchriftzug5(x*modul_width, (y-1)*modul_height, instrokeLen=1)
    #Bogen_links = drawSchriftzug2(x+2, y)

    if hook == True:
        Haeckchen_oben = drawSchriftzug23_KurrentKanzlei(x+3, y+1.5)
        KurrentGatC_c += Haeckchen_oben
        
    KurrentGatC_c += Bogen_links 
    trans_scale(KurrentGatC_c, valueToMoveGlyph)
    return KurrentGatC_c
    


def drawKurrentGatC_c_h(x, y, version="mid"):      
    #x += -3
    glyph_c = drawSchriftzug2(x-3, y, outstrokeLen=2)
    trans_scale(glyph_c, valueToMoveGlyph)
  
    if version == "mid":
        glyph_h = drawKurrentGatC_h(x, y, version="mid")

    if version == "end":
        glyph_h = drawKurrentGatC_h(x, y, version="end")[0]
    
    values_to_return = drawKurrentGatC_h(x, y, version="end")[1]   
    KurrentGatC_c_h = glyph_c + glyph_h

    if version == "mid":
        return KurrentGatC_c_h
    
    if version == "end":
        return KurrentGatC_c_h, values_to_return
        
    



def drawKurrentGatC_d(x, y):      

    full_stroke = drawSchriftzug15(x, y)
    
    KurrentGatC_d = full_stroke
    trans_scale(KurrentGatC_d, valueToMoveGlyph)
    return KurrentGatC_d
    



def drawKurrentGatC_e(x, y, version="mid"):
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)

    stroke_left = drawSchriftzug1(x, y, "a", 1)
    con_stroke = drawConstroke(*Grund_d, "A", 2)
    
    if version == "mid":
        hook = drawGrundelementC(*con_stroke.points[-1], outstrokeLen=1)

    if version == "end":
        hook = drawSchriftteil10(*con_stroke.points[-1])

    KurrentGatC_e = stroke_left + con_stroke + hook
    trans_scale(KurrentGatC_e, valueToMoveGlyph)
    return KurrentGatC_e
    
    
    
    
       
def drawKurrentGatC_f(x,y, version="a"):
    #x += 5
    KurrentGatC_f = BezierPath()
    
    if version == "a": KurrentGatC_f = drawSchriftzug25(x, y, outstrokeLen=1)
    if version == "b": KurrentGatC_f = drawSchriftzug25(x, y, "b")

    drawPath(KurrentGatC_f)
    trans_scale(KurrentGatC_f, valueToMoveGlyph)
    return KurrentGatC_f
    
    
    
    
    
def drawKurrentGatC_f_f(x, y):
    
    KurrentGatC_f_f = BezierPath()

    left = drawSchriftzug_longs_longs(x, y)
    right = drawSchriftzug17(x+6, y, base=False)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1.5, y-1)
    #Querstrich = drawGrundelementB(*Raute_a, 6)
    Querstrich_links = drawSchneckenzug(*Raute_a, UPPER_E, 2, HSL_size=1, HSL_start=34, clockwise=True, inward=True)
    Querstrich_rechts = drawSchneckenzug(*Querstrich_links.points[-1], LOWER_G, 2, HSL_size=1, HSL_start=34, clockwise=False, inward=True)

    KurrentGatC_f_f = left + right + Querstrich_links + Querstrich_rechts
    drawPath(KurrentGatC_f_f)
    trans_scale(KurrentGatC_f_f, valueToMoveGlyph)
    return KurrentGatC_f_f
    
    
    
def drawKurrentGatC_f_t(x, y):
        
    left_f = drawSchriftzug25(x, y, "b", outstrokeLen=1)  
    right_t = drawSchriftzug14(x+8, y, outstrokeLen=2) 
    Querstrich_t = drawGrundelementC((x+6)*modul_width, (y-0.5)*modul_height)    
    
    KurrentGatC_f_t = left_f + right_t + Querstrich_t
    trans_scale(KurrentGatC_f_t, valueToMoveGlyph)
    return KurrentGatC_f_t




def drawKurrentGatC_longs_longs_t(x, y):
        
    left = drawSchriftzug_longs_longs(x, y, outstrokeLen = 0.3)
    right_t = drawSchriftzug_longs_t(x+6, y, base=False)
    
    KurrentGatC_longs_longs_t = left + right_t
    trans_scale(KurrentGatC_longs_longs_t, valueToMoveGlyph)
    return KurrentGatC_longs_longs_t
    
    
    

def drawKurrentGatC_f_f_t(x, y):
        
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.5, y-0.5)
    Querstrich = drawSchriftteil12(*Raute_a)
    trans_scale(Querstrich, valueToMoveGlyph)

    glyph_longs_longs_t = drawKurrentGatC_longs_longs_t(x, y)
    KurrentGatC_f_f_t = glyph_longs_longs_t + Querstrich
    return KurrentGatC_f_f_t
    
    
    


    
def drawKurrentGatC_g(x, y, version="mid"):
    #x += 5
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    
    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
    
    bogen_links = drawSchriftzug5(*Grund_d, height="1.25", instrokeLen=0.5)
    connection_stroke = drawOutstroke(*bogen_links.points[0], 2.1, "up")
    
    # Windung von Raute_b nach rechts
    loopRight = drawSchriftzug3_loopRight(x+3, y)

    # Wendepunkt ab Raute_b nach links
    loopLeft = drawSchriftzug3_loopLeft(x+3, y, outstrokeLen=1)
    
    if version == "mid":
        schwung = drawSchriftzug9(x+3, y, outstrokeLen=1)
    
    if version == "end":
        schwung = drawSchriftzug9(x+3, y, version="end") 
        pkt_Auslauf = schwung.points[3]
        #text("pkt_Auslauf", pkt_Auslauf)  
        
    KurrentGatC_g = bogen_links + connection_stroke + loopRight + loopLeft + schwung
    trans_scale(KurrentGatC_g, valueToMoveGlyph)

    if version == "mid":
        return KurrentGatC_g
    
    if version == "end":
        return KurrentGatC_g, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf) 
        
    
    
    
    
def drawKurrentGatC_g_thinStroke(x, y, *, pass_from_thick=None):
    
    KurrentGatC_g_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 6, HSL_size=1.5, HSL_start=22, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.2 * i      for  i in range(0, 7)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.1, Auslauf.points[-1][1]-modul_height*0.875)
          
    KurrentGatC_g_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentGatC_g_thinStroke)
    trans_thinStroke_down_left(KurrentGatC_g_thinStroke) 
    trans_scale(KurrentGatC_g_thinStroke, valueToMoveGlyph)
    return KurrentGatC_g_thinStroke
    
     
           


def drawKurrentGatC_h(x, y, version="mid"):      
    
    Strich_links = drawSchriftzug10(x, y, instrokeLen=1, outstrokeLen=3)

    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
    
    if version == "mid":
        Bogen_rechts = drawSchriftzug11(x-3, y)
    
    if version == "end":
        Bogen_rechts = drawSchriftzug12(x-3, y, "end")
        pkt_Auslauf = Bogen_rechts.points[-1]
        #text("pkt_Auslauf", pkt_Auslauf)
            
    KurrentGatC_h = Strich_links + Bogen_rechts
    trans_scale(KurrentGatC_h, valueToMoveGlyph)
    
    if version == "mid":
        return KurrentGatC_h
    
    if version == "end":
        return KurrentGatC_h, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)

    
        
    
    
        

def drawKurrentGatC_i(x, y):      

    downstroke = drawSchriftzug2(x, y, "a")
    iPunkt = drawGrundelementC((x+1.5)*modul_width, (y+4)*modul_height)

    KurrentGatC_i = downstroke + iPunkt
    trans_scale(KurrentGatC_i, valueToMoveGlyph)
    return KurrentGatC_i
        
    
        
        
        
        
    
def drawKurrentGatC_j(x, y, version="mid"):      
    #x+=4
    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
    
    if version == "mid":
        Strich = drawSchriftzug9(x, y, instrokeLen=1)

    if version == "end":
        Strich = drawSchriftzug9(x, y, instrokeLen=1, version="end") 
        pkt_Auslauf = Strich.points[-5]
        #text("pkt_Auslauf", pkt_Auslauf)
        
    Punkt = drawGrundelementC((x+1.5)*modul_width, (y+4)*modul_height)
    
    KurrentGatC_j = Strich + Punkt
    trans_scale(KurrentGatC_j, valueToMoveGlyph)
    
    if version == "mid":
        return KurrentGatC_j

    if version == "end":
        return KurrentGatC_j, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)   
    





def drawKurrentGatC_k(x, y, version="mid"):
    
    
    if version == "start":
        Strich_links = drawSchriftzug_k_start(x, y, outstrokeLen=0)

    if version == "mid":
        Strich_links = drawSchriftzug13(x, y, instrokeLen=2, outstrokeLen=0)
    
    #Positionierung Schleifenelement
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+2, y+1)

    upstroke = drawOutstroke(*Grund_a, 2)
    Element_oben = drawHalbbogen1(*upstroke.points[0])
    downstroke = drawOutstroke(*Element_oben.points[-1], 3.75, "down")
    Signatur = drawGrundelementC((x-1)*modul_width, (y-0.5)*modul_height)
    outstroke = drawOutstroke(x*modul_width, (y-1)*modul_height, 1)
   
    KurrentGatC_k = Strich_links + upstroke + Element_oben + downstroke + Signatur + outstroke
    trans_scale(KurrentGatC_k, valueToMoveGlyph)

    
    return KurrentGatC_k
    
    
    
    
    
    
        
        
def drawKurrentGatC_l(x, y):    
    
    main_stroke = drawSchriftzug10(x, y, instrokeLen=1, outstrokeLen=1)
    
    KurrentGatC_l = main_stroke
    trans_scale(KurrentGatC_l, valueToMoveGlyph)
    
    return KurrentGatC_l
    
    
    
    
    
        
        
def drawKurrentGatC_m(x, y):
    
    downstroke_left = drawSchriftzug2(x, y, "a", outstrokeLen=3)
    downstroke_middle = drawSchriftzug2(x+3, y, "a", instrokeLen=0, outstrokeLen=3)
    downstroke_right = drawSchriftzug2(x+6, y, "a", instrokeLen=0)
    
    KurrentGatC_m = downstroke_left + downstroke_middle + downstroke_right
    trans_scale(KurrentGatC_m, valueToMoveGlyph) 
    return KurrentGatC_m
    





def drawKurrentGatC_n(x, y):   
  
    downstroke_left = drawSchriftzug2(x, y, "a", outstrokeLen=3)
    downstroke_right = drawSchriftzug2(x+3, y, "a", instrokeLen=0)
    
    KurrentGatC_n = downstroke_left + downstroke_right
    trans_scale(KurrentGatC_n, valueToMoveGlyph)
    return KurrentGatC_n
    





def drawKurrentGatC_o(x, y):   
    
    #Bogen_links = drawSchriftzug5(x*modul_width, (y-1)*modul_height, "1.5", instrokeLen=0.5, outstrokeLen=2)
    Bogen_links = drawSchriftzug5(x*modul_width, (y-1)*modul_height, "1.25", instrokeLen=0.5, outstrokeLen=2)
    Bogen_rechts = drawSchriftzug3_loopRight(x+4, y)
    #hook = drawSchriftzug_b_hook(x+4, y+1, version="0.5")
    hook = drawSchriftzug_b_hook(x+4, y+1, version="1")    ### passt mit m folgend

    KurrentGatC_o = Bogen_links + Bogen_rechts + hook
    trans_scale(KurrentGatC_o, valueToMoveGlyph)
    return KurrentGatC_o
    
    
def drawKurrentGatC_odieresis(x, y):
    
    Dieresis = drawDieresis(x, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    glyph_o = drawKurrentGatC_o(x, y)
    KurrentGatC_odieresis = glyph_o + Dieresis
    return KurrentGatC_odieresis 
    
    
    

    
def drawKurrentGatC_p(x, y):
    
    Strich_links = drawSchriftzug4(x, y, version="gebogen", outstrokeLen=1.25)
    Strich_rechts = drawSchriftzug7((x+2)*modul_width, (y+0.5)*modul_height, outstrokeLen=0)
    Strich_unten = drawGrundelementH((x-1)*modul_width, (y-1)*modul_height, 6, "down")
    
    KurrentGatC_p = Strich_links + Strich_rechts + Strich_unten
    trans_scale(KurrentGatC_p, valueToMoveGlyph)
    
    return KurrentGatC_p
    
    
    
    
    
    
def drawKurrentGatC_q(x, y):      
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    
    bogen_links = drawSchriftzug5(*Grund_d, height="1.25", instrokeLen=0.5)
    con_stroke = drawOutstroke(*bogen_links.points[0], 2.1, "up")
    
    # Windung von Raute_b nach rechts
    loopRight = drawSchriftzug3_loopRight(x+3, y)

    # Wendepunkt ab Raute_b nach links
    loopLeft = drawSchriftzug3_loopLeft(x+3, y, outstrokeLen=1.05)

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3, y-0.5)
    Strich_gerade = drawGrundelementF(*Grund_d, 1.5, "up")
    Strich_unten = drawGrundelementH(*Grund_d, 6, "down")
    outstroke = drawOutstroke(*Grund_d, 1)
    
    KurrentGatC_q = bogen_links + con_stroke + loopRight + loopLeft +  Strich_gerade + Strich_unten + outstroke
    trans_scale(KurrentGatC_q, valueToMoveGlyph)    
    return KurrentGatC_q
    






def drawKurrentGatC_r(x, y):       

    Strich_links = drawSchriftzug4(x, y, distance="1.5", outstrokeLen=1.7)
    Strich_rechts = drawGrundelementC((x+1.5)*modul_width, (y+0.5)*modul_height, outstrokeLen=1)
    
    KurrentGatC_r = Strich_links + Strich_rechts
    trans_scale(KurrentGatC_r, valueToMoveGlyph)
    return KurrentGatC_r
    
    
    
    
    
    
def drawKurrentGatC_s(x, y):
    #x+=2
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y-0)
    instroke = drawInstroke(*Grund_a, 1)
    full_stroke = drawSchriftzug16(x, y) 

    KurrentGatC_s = instroke + full_stroke
    trans_scale(KurrentGatC_s, valueToMoveGlyph)
    return KurrentGatC_s
    
      
  
  
  

def drawKurrentGatC_longs(x, y):
        
    KurrentGatC_longs = drawSchriftzug17(x, y, base=True)
    
    trans_scale(KurrentGatC_longs, valueToMoveGlyph)
    return KurrentGatC_longs
    





def drawKurrentGatC_longs_longs(x, y):
        
    left = drawSchriftzug_longs_longs(x, y)
    right = drawSchriftzug17(x+6, y, base=False)
    
    KurrentGatC_longs_longs = left + right
    trans_scale(KurrentGatC_longs_longs, valueToMoveGlyph)
    return KurrentGatC_longs_longs



def drawKurrentGatC_longs_t(x, y):

    KurrentGatC_longs_t = drawSchriftzug_longs_t(x, y)
 
    trans_scale(KurrentGatC_longs_t, valueToMoveGlyph)
    return KurrentGatC_longs_t
    
    



def drawKurrentGatC_germandbls(x, y):
        
    KurrentGatC_germandbls = drawSchriftzug22(x,y)    
    pkt_Auslauf = KurrentGatC_germandbls.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    trans_scale(KurrentGatC_germandbls, valueToMoveGlyph)
    return KurrentGatC_germandbls, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
def drawKurrentGatC_germandbls_thinStroke(x, y, *, pass_from_thick=None):
    
    KurrentGatC_germandbls_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 5, HSL_size=4, HSL_start=24, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 6)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.02, Auslauf.points[-1][1]-modul_height*0.45) 
    
    KurrentGatC_germandbls_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentGatC_germandbls_thinStroke)
    trans_thinStroke_down_left(KurrentGatC_germandbls_thinStroke) 
    trans_scale(KurrentGatC_germandbls_thinStroke, valueToMoveGlyph)
    return KurrentGatC_germandbls_thinStroke
       
    

    
    
def drawKurrentGatC_t(x, y):    
    
    Strich = drawSchriftzug14(x, y, outstrokeLen=2)
    Querstrich = drawGrundelementC((x-2)*modul_width, (y-0.5)*modul_height)
    
    KurrentGatC_t = Strich + Querstrich
    trans_scale(KurrentGatC_t, valueToMoveGlyph)
    return KurrentGatC_t
    
    



def drawKurrentGatC_u(x, y):
    
    downstroke_left = drawSchriftzug2(x, y, "a", outstrokeLen=3)
    downstroke_right = drawSchriftzug2(x+3, y, "a", instrokeLen=0)
    hook = drawSchriftzug_u_hook(x+2, y+2.5)
    
    KurrentGatC_u = downstroke_left + downstroke_right + hook
    trans_scale(KurrentGatC_u, valueToMoveGlyph) 
    return KurrentGatC_u

    
def drawKurrentGatC_udieresis(x, y):
    
    downstroke_left = drawSchriftzug2(x, y, "a", outstrokeLen=3)
    downstroke_right = drawSchriftzug2(x+3, y, "a", instrokeLen=0)
    Dieresis = drawDieresis(x, y)
    
    KurrentGatC_udieresis = downstroke_left + downstroke_right + Dieresis
    trans_scale(KurrentGatC_udieresis, valueToMoveGlyph)
    return KurrentGatC_udieresis
    
    
    
    
    
    
def drawKurrentGatC_v(x, y):
    
    Strich_links = drawSchriftzug4(x, y, outstrokeLen=1.25)
    Strich_rechts = drawSchriftzug7((x+2)*modul_width, (y+0.5)*modul_height, outstrokeLen=0)
    
    KurrentGatC_v = Strich_links + Strich_rechts
    trans_scale(KurrentGatC_v, valueToMoveGlyph)
    return KurrentGatC_v
    
    

    
def drawKurrentGatC_w(x, y):
    
    Strich_links = drawSchriftzug2(x, y, "a", outstrokeLen=2)
    Strich_mitte = drawSchriftzug4(x+3, y, outstrokeLen=1.25)
    Strich_rechts = drawSchriftzug7((x+5)*modul_width, (y+0.5)*modul_height, outstrokeLen=0)
    
    KurrentGatC_w = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(KurrentGatC_w, valueToMoveGlyph)
    return KurrentGatC_w
    
    
    
    
    
def drawKurrentGatC_x(x, y):
    
    Strich_links = drawSchriftzug4(x, y, outstrokeLen=2.1)
    Strich_rechts = drawGrundelementC((x+2)*modul_width, (y+0.5)*modul_height, outstrokeLen=1)
    hook_btm = drawSchriftzug_x_hook(x-0.5, y-2)
    
    KurrentGatC_x = Strich_links + Strich_rechts + hook_btm
    trans_scale(KurrentGatC_x, valueToMoveGlyph)
    return KurrentGatC_x
    
    
    


def drawKurrentGatC_y(x, y, version="mid"): 
    
    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
        
    Strich_links = drawSchriftzug4(x, y, outstrokeLen=2.1)

    if version == "mid":
        Bogen_rechts = drawSchriftzug11(x-3, y, instrokeLen=1, outstrokeLen=1)

    if version == "end":
        Bogen_rechts = drawSchriftzug12(x-3, y, "end")
        pkt_Auslauf = Bogen_rechts.points[-1]
        #text("pkt_Auslauf", pkt_Auslauf)
                
    KurrentGatC_y = Strich_links + Bogen_rechts
    trans_scale(KurrentGatC_y, valueToMoveGlyph)

    if version == "mid":
        return KurrentGatC_y

    if version == "end":
        return KurrentGatC_y, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
    


def drawKurrentGatC_z(x, y, version="mid"):

    #x+=5
    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
    
    if version == "start":
        Element_oben = drawSchriftzug_z_top(x-3, y)
        connection = drawOutstroke(*Element_oben.points[-1], 2, "down")
        Halbbogen_unten = drawSchriftzug12(x-6, y, version="a", outstrokeLen=3)

    if version == "mid":
        Element_oben = drawGrundelementC(x*modul_width,(y+1)*modul_height, 1, instrokeLen=2)
        connection = drawOutstroke(*Element_oben.points[-1], 2, "down")
        Halbbogen_unten = drawSchriftzug12(x-6, y-0.5, version="b", outstrokeLen=4)

    if version == "end":
        Element_oben = drawGrundelementC(x*modul_width,(y+1)*modul_height, instrokeLen=2)
        connection = drawOutstroke(*Element_oben.points[-1], 2, "down")
        Halbbogen_unten = drawSchriftzug12(x-6, y-0.5, version="b_end", outstrokeLen=3)
        pkt_Auslauf = Halbbogen_unten.points[-1]
        #text("pkt_Auslauf", pkt_Auslauf)
        
    KurrentGatC_z = Element_oben + connection + Halbbogen_unten
    trans_scale(KurrentGatC_z, valueToMoveGlyph)

    if version == "start" or version == "mid":
        return KurrentGatC_z

    if version == "end":
        return KurrentGatC_z, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
        
        
    
    
    
    
    
    
    
    
    
    

#############################################################################
######     ab hier Interpunktion
#############################################################################


 
    
    
    
    
    
def drawKurrentGatC_period(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    period = drawGrundelementC(*Grund_d)
        
    KurrentGatC_period = period
    trans_scale(KurrentGatC_period, valueToMoveGlyph)
    return KurrentGatC_period
    
    
    
    
    
def drawKurrentGatC_colon(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    colon_btm = drawGrundelementC(*Grund_d)
    
    # Hilfslinie zum Kontrollieren der Richtung GG
    save()
    stroke(0,0,3)
    strokeWidth(0.001)
    line(Grund_d, (Grund_d[0]-(G2[0]-G1[0])*3, Grund_d[1]-(G2[1]-G1[1])*3))
    restore()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    colon_top = drawGrundelementC(*Grund_b)  
    
        
    KurrentGatC_colon = colon_top + colon_btm
    trans_scale(KurrentGatC_colon, valueToMoveGlyph)    
    return KurrentGatC_colon
    
    
    
    
    
def drawKurrentGatC_semicolon(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y-0.5)

    # Hilfslinie zum Kontrollieren der Richtung GG
    save()
    stroke(0,0,3)
    strokeWidth(0.001)
    line(Grund_d, (Grund_d[0]-(G2[0]-G1[0])*3, Grund_d[1]-(G2[1]-G1[1])*3))
    restore()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y)
    
    semicolon_top = drawGrundelementC(*Grund_b)  
    semicolon_btm = drawSchriftzug_semicolon_btm(x, y)  

    KurrentGatC_semicolon = semicolon_top + semicolon_btm
    trans_scale(KurrentGatC_semicolon, valueToMoveGlyph)
    return KurrentGatC_semicolon
    
    
    
    

def drawKurrentGatC_quoteright(x, y):

    quoteright = drawSchriftzug_semicolon_btm(x, y+5) 
    
    KurrentGatC_quoteright = quoteright
    trans_scale(KurrentGatC_quoteright, valueToMoveGlyph)
    return KurrentGatC_quoteright
    
    
       
    
    
def drawKurrentGatC_comma(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)

    comma = drawGrundelementG(*Grund_a, 2, "down") 

    KurrentGatC_comma = comma
    trans_scale(KurrentGatC_comma, valueToMoveGlyph)
    return KurrentGatC_comma
    
    



    
def drawKurrentGatC_endash(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1)

    endash = drawGrundelementB(*Grund_a, 2) 

    KurrentGatC_endash = endash
    trans_scale(KurrentGatC_endash, valueToMoveGlyph)
    return KurrentGatC_endash
    
    
    
    
    
def drawKurrentGatC_hyphen(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+0.75)
    hyphen_top = drawGrundelementH(*Grund_b, 2, "down") 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.25)
    hyphen_btm = drawGrundelementH(*Grund_b, 2, "down") 
    
    KurrentGatC_hyphen = hyphen_top + hyphen_btm
    trans_scale(KurrentGatC_hyphen, valueToMoveGlyph)
    return KurrentGatC_hyphen
     
    
    
    
def drawKurrentGatC_exclam(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y-0.5)
    exclam = drawGrundelementG(*Grund_a, 6.5) 
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    period = drawGrundelementC(*Grund_d)

    KurrentGatC_exclam = exclam + period
    trans_scale(KurrentGatC_exclam, valueToMoveGlyph)
    return KurrentGatC_exclam
    
    



def drawKurrentGatC_question(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1.5, y-0.5)
    period = drawGrundelementC(*Grund_d)
    question = drawSchriftzug_question(x, y) 

    KurrentGatC_question = question + period
    trans_scale(KurrentGatC_question, valueToMoveGlyph)
    return KurrentGatC_question
    
        




#############################################################################
######     ab hier Zahlen
#############################################################################


 
    
    


def drawKurrentGatC_helper(x, y):
    
    KurrentGatC_helper = BezierPath()     
 
    x -= 2 
    save()
    stroke(1,0,1)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+3, y-0.5)
    # drawGrundelOrient(A1, A2, offset, x, y+1.5)
    # drawGrundelOrient(A1, A2, offset, x, y+2.5)
    # drawGrundelOrient(A1, A2, offset, x, y+3.5)
    #drawGrundelOrient(A1, A2, offset, x, y+4.5)
    
    Grund_a_top, Grund_b_top, Grund_c, Grund_d_top = drawGrundelOrient(A1, A2, offset, x+3, y+4.5)
    drawGrundelementG(*Grund_b_top, 6, "down")

    Grund_a_2, Grund_b_2, Grund_c, Grund_d_2 = drawGrundelOrient(A1, A2, offset, x+6, y+4.5)

    drawGrundelementG(*Grund_a_2, 6, "down")
    drawGrundelementG(*Grund_b_2, 6, "down")

    line(Grund_a_top, Grund_d)

    restore()
    #KurrentGatC_helper = Bogen_links + Bogen_rechts
    trans_scale(KurrentGatC_helper, valueToMoveGlyph)
    return KurrentGatC_helper
    
    
    
    

def drawKurrentGatC_zero(x, y):
    x += 3
    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y)

    KurrentGatC_zero = Bogen_links + Bogen_rechts
    trans_scale(KurrentGatC_zero, valueToMoveGlyph)
    return KurrentGatC_zero
    
    
  
 
def drawKurrentGatC_one(x, y):
    x += 3
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.5)
    
    instroke = drawInstroke(*Grund_a, 1.5)
    stem = drawGrundelementG(*Grund_a, 6, "down")
    Signatur_oben = drawSchriftteil10(*Grund_a)
    
    KurrentGatC_one = instroke + stem + Signatur_oben
    trans_scale(KurrentGatC_one, valueToMoveGlyph)
    return KurrentGatC_one
    



def drawKurrentGatC_two(x, y):
    x += 3     
    Bogen = drawSchriftzug_two_Bogen(x, y)
    Schwung_unten = drawSchriftzug_two_Schwung (x-2, y)
    
    KurrentGatC_two = Bogen + Schwung_unten
    trans_scale(KurrentGatC_two, valueToMoveGlyph)
    return KurrentGatC_two  
    
    


def drawKurrentGatC_three(x, y):
    x += 3
    KurrentGatC_three = BezierPath()     
     
    Three_Top = drawSchriftzug_three_Top(x, y)
    Three_Schwung = drawSchriftzug_three_Bogen(x, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    KurrentGatC_three = Three_Top + Three_Schwung
    drawPath(KurrentGatC_three)
    trans_scale(KurrentGatC_three, valueToMoveGlyph)
    return KurrentGatC_three, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
def drawKurrentGatC_three_thinStroke(x, y, *, pass_from_thick=None):

    KurrentGatC_three_thinStroke = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, pass_from_thick.pkt_Auslauf[0]/modul_width, pass_from_thick.pkt_Auslauf[1]/modul_height)  
    Auslauf = drawSchneckenzug(*Grund_c, LOWER_E, 9, HSL_size=0.6, HSL_start=12, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.1)
    
    KurrentGatC_three_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentGatC_three_thinStroke)
    trans_thinStroke_down_left(KurrentGatC_three_thinStroke) 
    trans_scale(KurrentGatC_three_thinStroke, valueToMoveGlyph)
    return KurrentGatC_three_thinStroke
     
    
    
    
    
    
    
    
def drawKurrentGatC_four(x, y):
    x += 3
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.5)
    
    stem = drawSchriftzug_four(x, y, instrokeLen=0)
    stroke_down = drawGrundelementA(*Grund_a, 3.75, "down")
    stroke_hor = drawGrundelementB(*stroke_down.points[-1], 4.5)

    KurrentGatC_four = stem + stroke_down + stroke_hor
    trans_scale(KurrentGatC_four, valueToMoveGlyph)
    return KurrentGatC_four   
    
    
    
    
def drawKurrentGatC_five(x, y):
    x += 3
    KurrentGatC_five = BezierPath()
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+4.5)
     
    Five_Top = drawSchriftzug_five_Top(*Raute_a)
    Connection = drawGrundelementA(*Raute_a, 3.25, "down")
    Three_Schwung = drawSchriftzug_three_Bogen(x-2.95, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    KurrentGatC_five = Five_Top + Three_Schwung + Connection
    drawPath(KurrentGatC_five)
    trans_scale(KurrentGatC_five, valueToMoveGlyph)    
    return KurrentGatC_five, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    


def drawKurrentGatC_six(x, y):
    x += 3
    ### das waren vorher zwei Bogen, jetzt nur noch einer
    #Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    Bogen_rechts = drawSchriftzug_six_full(x, y)
    pkt_Auslauf_inner = Bogen_rechts.points[-3]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner)
    pkt_Auslauf_top = Bogen_rechts.points[0]
    #text("pkt_Auslauf_top", pkt_Auslauf_top)
    KurrentGatC_six = Bogen_rechts
    trans_scale(KurrentGatC_six, valueToMoveGlyph)
    return KurrentGatC_six, collections.namedtuple('dummy', 'pkt_Auslauf_top pkt_Auslauf_inner')(pkt_Auslauf_top, pkt_Auslauf_inner)
    

def drawKurrentGatC_six_thinStroke(x, y, *, pass_from_thick=None):

    KurrentGatC_six_thinStroke = BezierPath() 
    
    Auslauf_top = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_top, UPPER_E, 2, HSL_size=4, HSL_start=12, clockwise=True, inward=False)    
    Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, UPPER_E, 2, HSL_size=2, HSL_start=25, clockwise=False, inward=False)
        
    trans_thinStroke_down_left(Auslauf_inner) 
    trans_thinStroke_up_right(Auslauf_top)
    
    KurrentGatC_six_thinStroke += Auslauf_top + Auslauf_inner 
    drawPath(KurrentGatC_six_thinStroke)

    trans_scale(KurrentGatC_six_thinStroke, valueToMoveGlyph)
    return KurrentGatC_six_thinStroke 
        
    
    
    
    
    
def drawKurrentGatC_seven(x, y):
    x += 3
    KurrentGatC_seven = BezierPath()     
     
    Seven_Top = drawSchriftzug_seven_Top(x, y)
    Seven_Stem = drawSchriftzug_seven_Stem(x+3.68 , y)
    
    KurrentGatC_seven = Seven_Top + Seven_Stem
    drawPath(KurrentGatC_seven)
    trans_scale(KurrentGatC_seven, valueToMoveGlyph)
    return KurrentGatC_seven        
            
            


def drawKurrentGatC_eight(x, y):
    x += 3
    KurrentGatC_eight = drawSchriftzug_eight(x, y)
    trans_scale(KurrentGatC_eight, valueToMoveGlyph)  
    return KurrentGatC_eight
    
    
    

def drawKurrentGatC_nine(x, y):
    x += 3
    #Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    
    KurrentGatC_nine = drawSchriftzug_nine_full(x, y)
    pkt_Auslauf_btm = KurrentGatC_nine.points[-10]
    #text("pkt_Auslauf_btm", pkt_Auslauf_btm) 
    pkt_Auslauf_inner = KurrentGatC_nine.points[-3]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner) 
    
    trans_scale(KurrentGatC_nine, valueToMoveGlyph)
    return KurrentGatC_nine, collections.namedtuple('dummy', 'pkt_Auslauf_btm pkt_Auslauf_inner')(pkt_Auslauf_btm, pkt_Auslauf_inner)
    


def drawKurrentGatC_nine_thinStroke(x, y, *, pass_from_thick=None):

    KurrentGatC_nine_thinStroke = BezierPath() 
    
    Auslauf_btm = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_btm, LOWER_E, 2, HSL_size=4, HSL_start=22, clockwise=True, inward=True)
    Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, LOWER_E, 2, HSL_size=2, HSL_start=12, clockwise=False, inward=False)
     
    trans_thinStroke_down_left(Auslauf_btm) 
    trans_thinStroke_up_right(Auslauf_inner)
    
    KurrentGatC_nine_thinStroke += Auslauf_btm + Auslauf_inner 
    drawPath(KurrentGatC_nine_thinStroke)
    trans_scale(KurrentGatC_nine_thinStroke, valueToMoveGlyph)
    return KurrentGatC_nine_thinStroke
    
    
    
    
    







#############################################################################
######     ab hier UC
#############################################################################




def drawKurrentGatC_helperUC(x, y):
    
    KurrentGatC_helperUC = BezierPath()

    drawGrundelOrientMittig(A1, A2, offset, x, y-0.5)

    helper = (-0.5, 0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        drawGrundelOrientMittig(A1, A2, offset, x+3, y+h)
        
    drawGrundelOrientMittig(A1, A2, offset, x+6, y-0.5)
    
    helper = (-0.5, 0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        drawGrundelOrientMittig(A1, A2, offset, x+9, y+h)
        
    drawGrundelOrientMittig(A1, A2, offset, x+12, y-0.5)

    helper = (-0.5, 0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        drawGrundelOrientMittig(A1, A2, offset, x+15, y+h)
        
        
    #KurrentGatC_helperUC = Bogen_rechts
    #trans_scale(KurrentGatC_helperUC)
    
    return KurrentGatC_helperUC




def drawKurrentGatC_A(x, y):
   
    ### ThinStroke fehlt noch!!!
    
    KurrentGatC_A = BezierPath()
    _, _, _, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y=-10)
    HoldImgInPlace = drawGrundelementB(*Raute_d, -10)

    Raute_a, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+8)
    instroke = drawInstroke(*Raute_a, 2)
    Schleife_in = drawSchneckenzug(*instroke.points[0], UPPER_E, 8, HSL_size=2, HSL_start=12, clockwise=True, inward=False)
    Einsatz = drawGrundelementA(*Schleife_in.points[-1], -6.5)
    Loop_in = drawSchneckenzug(*Einsatz.points[-1], LOWER_E, 2, HSL_size=7, HSL_start=38, clockwise=True, inward=True)
    Schleife = Schleife_in + Einsatz + Loop_in


    #liegender_Schwung_in = drawGrundelementC(*stehender_Schwung_end.points[-1], 1.6)
    #liegender_Schwung = drawSchneckenzug(*liegender_Schwung_in.points[-1], LOWER_G, 2, HSL_size=4, HSL_start=60, clockwise=False, inward=True)


    
    
    KurrentGatC_A = instroke + Schleife + HoldImgInPlace
    trans_scale(KurrentGatC_A, valueToMoveGlyph)
    return KurrentGatC_A
    
    
    
    
    
    


def drawKurrentGatC_C(x, y):
   
    ### ThinStroke fehlt noch!!!
    
    KurrentGatC_C = BezierPath()
    _, _, _, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y=-10)
    HoldImgInPlace = drawGrundelementB(*Raute_d, -10)

    Raute_a, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+5.5)
    instroke = drawInstroke(*Raute_a, 1)
    stehender_Schwung_in = drawSchneckenzug(*instroke.points[-1], UPPER_E, 3, HSL_size=2, HSL_start=20, clockwise=False, inward=False)
    stehender_Schwung_mid = drawSchneckenzug(*stehender_Schwung_in.points[-1], LOWER_B, 3, HSL_size=4, HSL_start=18, clockwise=True, inward=True)
    Einsatz = drawGrundelementA(*stehender_Schwung_mid.points[-1], -1.3)
    stehender_Schwung_end = drawSchneckenzug(*Einsatz.points[-1], LOWER_E, 2, HSL_size=6, HSL_start=38, clockwise=True, inward=True)

    liegender_Schwung_in = drawGrundelementC(*stehender_Schwung_end.points[-1], 1.6)
    liegender_Schwung = drawSchneckenzug(*liegender_Schwung_in.points[-1], LOWER_G, 2, HSL_size=4, HSL_start=60, clockwise=False, inward=True)

    ### Deckung
    Raute_a, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4, y+8)
    DeckungOben_left = drawSchneckenzug(*Raute_a, UPPER_E, 5, HSL_size=1.25, HSL_start=2, clockwise=False, inward=False)
    DeckungOben_right = drawSchneckenzug(*DeckungOben_left.points[-1], LOWER_H, 3, HSL_size=12, HSL_start=4, clockwise=False, inward=False)
    
    
    KurrentGatC_C = instroke + stehender_Schwung_in + stehender_Schwung_mid + Einsatz + stehender_Schwung_end + liegender_Schwung_in + liegender_Schwung + DeckungOben_left + DeckungOben_right + HoldImgInPlace
    trans_scale(KurrentGatC_C, valueToMoveGlyph)
    return KurrentGatC_C
    
    
    
    
    

def drawKurrentGatD_C(x, y):
   
    stehender_Schwung = drawSchriftzug_C_stehenderSchwung(x, y)
    liegender_Schwung = drawSchriftzug_C_liegenderSchwung(x, y)
    Deckung = drawSchriftzug_C_DeckungOben(x, y)

    KurrentGatD_C = stehender_Schwung + liegender_Schwung + Deckung
    trans_scale(KurrentGatD_C, valueToMoveGlyph)
    return KurrentGatD_C
    
    

    


def drawKurrentGatD_D(x, y):

    stehender_Schwung = drawSchriftzug_D_stehenderSchwung(x, y)
    liegender_Schwung = drawSchriftzug_D_liegenderSchwung(x, y)
    Deckung = drawSchriftzug_D_DeckungOben(x, y)

    KurrentGatD_D = stehender_Schwung + liegender_Schwung + Deckung
    trans_scale(KurrentGatD_D, valueToMoveGlyph)
    return KurrentGatD_D
    
    

def drawKurrentGatC_E(x, y):
   
    ### ThinStroke fehlt noch!!!
    
    KurrentGatC_E = BezierPath()
    _, _, _, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y=-10)
    HoldImgInPlace = drawGrundelementB(*Raute_d, -10)

    Raute_a, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+5.5)
    instroke = drawInstroke(*Raute_a, 1)
    stehender_Schwung_in = drawSchneckenzug(*instroke.points[-1], UPPER_E, 3, HSL_size=2, HSL_start=20, clockwise=False, inward=False)
    stehender_Schwung_mid = drawSchneckenzug(*stehender_Schwung_in.points[-1], LOWER_B, 3, HSL_size=4, HSL_start=18, clockwise=True, inward=True)
    Einsatz = drawGrundelementA(*stehender_Schwung_mid.points[-1], -1.3)
    stehender_Schwung_end = drawSchneckenzug(*Einsatz.points[-1], LOWER_E, 2, HSL_size=6, HSL_start=38, clockwise=True, inward=True)

    liegender_Schwung_in = drawGrundelementC(*stehender_Schwung_end.points[-1], 1.6)
    liegender_Schwung = drawSchneckenzug(*liegender_Schwung_in.points[-1], LOWER_G, 2, HSL_size=4, HSL_start=60, clockwise=False, inward=True)

    ### Deckung
    Raute_a, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.5, y+9)
    DeckungOben_left = drawSchneckenzug(*Raute_a, UPPER_E, 5, HSL_size=2, HSL_start=4, clockwise=False, inward=False)
    Einsatz2 = drawGrundelementD(*DeckungOben_left.points[-1], 1)
    DeckungOben_right = drawSchneckenzug(*Einsatz2.points[2], UPPER_H, 4, HSL_size=1, HSL_start=12, clockwise=True, inward=True)
    Deckung = DeckungOben_left + Einsatz2 + DeckungOben_right
    
    ### Querstrich
    Raute_a, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-5 , y+2.75)
    Querstrich_left = drawSchneckenzug(*Raute_a, UPPER_E, 3, HSL_size=5, HSL_start=24, clockwise=True, inward=True)
    Querstrich_right = drawSchneckenzug(*Querstrich_left.points[-1], LOWER_H , 3, HSL_size=2, HSL_start=12, clockwise=False, inward=False)

    Querstrich = Querstrich_left + Querstrich_right

    
    KurrentGatC_E = HoldImgInPlace + instroke + stehender_Schwung_in + stehender_Schwung_mid + Einsatz + Einsatz2 + stehender_Schwung_end + liegender_Schwung_in + liegender_Schwung + Deckung + Querstrich
    trans_scale(KurrentGatC_E, valueToMoveGlyph)
    return KurrentGatC_E
    
    
    

    
    
def drawKurrentGatD_G(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.25)
    Schweif_links = drawSchneckenzug(*Grund_a, UPPER_E, 3, HSL_size=4, HSL_start=11, clockwise=True, inward=False)
    Schweif_rechts = drawSchneckenzug(*Schweif_links.points[-1], LOWER_H, 3, HSL_size=4, HSL_start=13, clockwise=False, inward=False)
    outstroke = drawOutstroke(*Schweif_rechts.points[-1], 1)
    
    up_bottom = drawSchneckenzug(*outstroke.points[0], LOWER_E, 4, HSL_size=4, HSL_start=18, clockwise=False, inward=False)
    up_top = drawSchneckenzug(*up_bottom.points[-1], LOWER_A, 4, HSL_size=4, HSL_start=18, clockwise=True, inward=False)

    Einsatz = drawOutstroke(*up_top.points[-1], 1)
    turn = drawSchneckenzug(*Einsatz.points[0], UPPER_E, 7, HSL_size=0.5, HSL_start=3, clockwise=True, inward=False)
    transition = drawSchneckenzug(*turn.points[-1], LOWER_D, 1, HSL_size=1, HSL_start=72, clockwise=True, inward=False)
    thin_left = drawOutstroke(*transition.points[-1], 5, "down")

    Schweif_mitte_links = drawSchneckenzug(*thin_left.points[0], UPPER_E, 3, HSL_size=4, HSL_start=6, clockwise=True, inward=False)
    Schweif_mitte_rechts = drawSchneckenzug(*Schweif_mitte_links.points[-1], LOWER_H, 3, HSL_size=4, HSL_start=4, clockwise=False, inward=False)
    thin_middle = drawOutstroke(*Schweif_mitte_rechts.points[-1], 7.5)

    right_top = drawSchneckenzug(*thin_middle.points[0], UPPER_E, 3, HSL_size=4, HSL_start=14, clockwise=True, inward=False)
    thin_right = drawOutstroke(*right_top.points[-1], 1.75, "down")

    s_top = drawSchneckenzug(*thin_right.points[0], UPPER_E, 3, HSL_size=4, HSL_start=12, clockwise=False, inward=False)
    Einsatz2 = drawGrundelementF(*s_top.points[-1], 0.5)
    s_bottom = drawSchneckenzug(*Einsatz2.points[-1], LOWER_B, 3, HSL_size=0, HSL_start=28, clockwise=True, inward=False)
    outstroke = drawOutstroke(*s_bottom.points[-1], 0.5, "down")
    pkt_Auslauf = outstroke.points[0]
    #text("pkt_Auslauf_oben", pkt_Auslauf_oben)

    ### Signatur
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+11, y+2.5)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.3, Signatur.points[-8][1]+modul_height*1.8
    #text("pkt_Signatur", pkt_Signatur)
    
    KurrentGatD_G = Schweif_links + Schweif_rechts + outstroke + up_bottom + up_top + Einsatz + turn + transition + thin_left + Schweif_mitte_links + Schweif_mitte_rechts + thin_middle + right_top + thin_right + s_top + Einsatz2 + s_bottom + outstroke + Signatur
    trans_scale(KurrentGatD_G, valueToMoveGlyph)
    return KurrentGatD_G, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Signatur')(pkt_Auslauf, pkt_Signatur)
    
    
    
def drawKurrentGatD_G_thinStroke(x, y, *, pass_from_thick=None):

    KurrentGatD_G_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 6, HSL_size=2.5, HSL_start=18, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.01 * i      for  i in range(0, 7)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.05, Auslauf.points[-1][1]-modul_height*0.7)
    
    KurrentGatD_G_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-3*modul_height))

    KurrentGatD_G_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentGatD_G_thinStroke)
    trans_thinStroke_down_left(KurrentGatD_G_thinStroke) 
    trans_scale(KurrentGatD_G_thinStroke, valueToMoveGlyph)
    return KurrentGatD_G_thinStroke
    
    
    
    




def drawKurrentGatD_H(x, y):
    
    stehender_Schwung = drawSchriftzug_H_stehenderSchwung(x, y+0.5)
    pkt_Auslauf_links = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)

    body = drawSchriftzug_H_body(x, y)
    pkt_Auslauf_rechts = body.points[-2]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Deckung_Rechtsoben = drawSchriftzug_D_DeckungOben(x+5, y+3.75)

    KurrentGatD_H = stehender_Schwung + Deckung_Rechtsoben + body
    drawPath(KurrentGatD_H)
    trans_scale(KurrentGatD_H, valueToMoveGlyph)
    return KurrentGatD_H, collections.namedtuple('dummy', 'pkt_Auslauf_links pkt_Auslauf_rechts')(pkt_Auslauf_links, pkt_Auslauf_rechts)
    
    
def drawKurrentGatD_H_thinStroke(x, y, *, pass_from_thick=None):

    KurrentGatD_H_thinStroke = BezierPath()
    
    ### Mittelteil
    Auslauf_links = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_links, LOWER_E, 2, HSL_size=8, HSL_start=49.75, clockwise=False, inward=False)
    Einsatz = drawGrundelementG(*Auslauf_links.points[-1], 0)
    Auslauf_continued = drawSchneckenzug(*Einsatz.points[-1], UPPER_C, 2, HSL_size=1, HSL_start=56, clockwise=True, inward=False)
    
    ### rechts
    Auslauf_rechts = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_rechts, LOWER_E, 8, HSL_size=2, HSL_start=18, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf_rechts.points[-1][0]-modul_width*0.1, Auslauf_rechts.points[-1][1]-modul_height*0.85)

    trans_thinStroke_up_right(Auslauf_links)
    trans_thinStroke_up_right(Einsatz)
    trans_thinStroke_up_right(Auslauf_continued)

    trans_thinStroke_down_left(Auslauf_rechts)
    trans_thinStroke_down_left(Endpunkt)

    KurrentGatD_H_thinStroke += Auslauf_links + Auslauf_rechts + Endpunkt + Einsatz + Auslauf_continued
    drawPath(KurrentGatD_H_thinStroke)
    trans_scale(KurrentGatD_H_thinStroke, valueToMoveGlyph)
    return KurrentGatD_H_thinStroke
       


def drawKurrentGatC_L(x, y):
   
    ### ThinStroke fehlt noch!!!
    
    KurrentGatC_L = BezierPath()
    _, _, _, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y=-8.75)
    HoldImgInPlace = drawGrundelementB(*Raute_d, -10)

    Raute_a, _, _, _ = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+6.5)
    instroke = drawGrundelementA(*Raute_a, 1.25, "down")
    
    stehender_Schwung_in = drawSchneckenzug(*instroke.points[-1], UPPER_E, 2, HSL_size=2, HSL_start=34, clockwise=False, inward=False)
    stehender_Schwung_mid = drawSchneckenzug(*stehender_Schwung_in.points[-1], UPPER_C, 1, HSL_size=4, HSL_start=38, clockwise=False, inward=True)
    Einsatz = drawGrundelementF(*stehender_Schwung_mid.points[-1], 0.25)
    stehender_Schwung_end = drawSchneckenzug(*Einsatz.points[-1], LOWER_B, 3, HSL_size=3, HSL_start=14, clockwise=True, inward=True)
    Einsatz2 = drawGrundelementA(*stehender_Schwung_end.points[-1], -1)
    stehender_Schwung_end2 = drawSchneckenzug(*Einsatz2.points[-1], LOWER_E, 2, HSL_size=3, HSL_start=24, clockwise=True, inward=True)
    stehender_Schwung = stehender_Schwung_in + stehender_Schwung_mid + Einsatz + stehender_Schwung_end + Einsatz2 + stehender_Schwung_end2
    
    liegender_Schwung_in = drawGrundelementC(*stehender_Schwung_end2.points[-1], 1.5)
    liegender_Schwung_out = drawSchneckenzug(*liegender_Schwung_in.points[-1], LOWER_G, 2, HSL_size=4, HSL_start=34, clockwise=False, inward=True)
    liegender_Schwung = liegender_Schwung_in + liegender_Schwung_out

    loop_start = drawSchneckenzug(*instroke.points[0], UPPER_E, 3, HSL_size=5, HSL_start=19, clockwise=True, inward=True)
    loop_continue = drawSchneckenzug(*loop_start.points[-1], UPPER_H, 5, HSL_size=5.5, HSL_start=0.5, clockwise=True, inward=False)
    Einsatz3 = drawGrundelementA(*loop_continue.points[-1], -1.85)
    loop_continue2 = drawSchneckenzug(*Einsatz3.points[-1], LOWER_E, 4, HSL_size=7, HSL_start=36, clockwise=True, inward=True)
    loop_fina = drawSchneckenzug(*loop_continue2.points[-1], LOWER_A, 4, HSL_size=1, HSL_start=7, clockwise=True, inward=False)

    
    loop = loop_start + loop_continue + Einsatz3 + loop_continue2 + loop_fina
    
    KurrentGatC_L = instroke + stehender_Schwung + liegender_Schwung + loop + HoldImgInPlace
    trans_scale(KurrentGatC_L, valueToMoveGlyph)
    return KurrentGatC_L
    
    



def drawKurrentGatD_W(x, y):
    
    KurrentGatD_W = BezierPath()
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y+5.4)
    
    instroke = drawInstroke(*Grund_a, 1, "down")
    Bogen_links_oben = drawSchneckenzug(*instroke.points[-1], UPPER_E, 4, HSL_size=3, HSL_start=20, clockwise=True, inward=True)
    Bogen_links_mitte = drawSchneckenzug(*Bogen_links_oben.points[-1], UPPER_A, 4, HSL_size=14, HSL_start=12, clockwise=True, inward=False)
    outstroke = drawOutstroke(*Bogen_links_mitte.points[-1], 2.5, "down")

    Bogen_links_3 = drawSchneckenzug(*outstroke.points[0], LOWER_E, 4, HSL_size=2, HSL_start=12, clockwise=True, inward=True)
    Bogen_links_4 = drawSchneckenzug(*Bogen_links_3.points[-1], LOWER_A, 7, HSL_size=1.5, HSL_start=4, clockwise=True, inward=False)
    Einsatz = drawGrundelementD(*Bogen_links_4.points[-1], 1.25)
    Bogen_links_5 = drawSchneckenzug(*Einsatz.points[4], LOWER_H, 3, HSL_size=1, HSL_start=16, clockwise=False, inward=False)

    Bogen_mitte_1 = drawSchneckenzug(*Bogen_links_5.points[-1], LOWER_E, 2, HSL_size=80, HSL_start=45, clockwise=False, inward=False)
    Bogen_mitte_2 = drawSchneckenzug(*Bogen_mitte_1.points[-1], UPPER_B, 1, HSL_size=2, HSL_start=12, clockwise=False, inward=False)
    Bogen_mitte_3 = drawSchneckenzug(*Bogen_mitte_2.points[-1], UPPER_A, 4, HSL_size=6, HSL_start=16.1, clockwise=True, inward=False)
    Einsatz2 = drawOutstroke(*Bogen_mitte_3.points[-1], 0.75, "down")
    Bogen_mitte_4 = drawSchneckenzug(*Einsatz2.points[-1], UPPER_E, 8, HSL_size=1.25, HSL_start=14, clockwise=True, inward=True) 
    Bogen_mitte_5 = drawSchneckenzug(*Bogen_mitte_4.points[-1], LOWER_E, 6, HSL_size=2, HSL_start=2, clockwise=True, inward=False)     
    
    Bogen_rechts_1 = drawSchneckenzug(*Bogen_mitte_5.points[-1], UPPER_C, 2, HSL_size=4, HSL_start=106, clockwise=True, inward=False)
    Bogen_rechts_2 = drawSchneckenzug(*Bogen_rechts_1.points[-1], UPPER_E, 4, HSL_size=3, HSL_start=21, clockwise=True, inward=True)
    Bogen_rechts_3 = drawSchneckenzug(*Bogen_rechts_2.points[-1], UPPER_A, 4, HSL_size=14, HSL_start=16, clockwise=True, inward=False)

    pkt_Auslauf = Bogen_rechts_3.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    KurrentGatD_W += instroke + Bogen_links_oben + Bogen_links_mitte + outstroke + Bogen_links_3 + Bogen_links_4 + Einsatz + Bogen_links_5
    KurrentGatD_W += Bogen_mitte_1 + Bogen_mitte_2 + Bogen_mitte_3 + Einsatz2 + Bogen_mitte_4 + Bogen_mitte_5
    KurrentGatD_W += Bogen_rechts_1 + Bogen_rechts_3 + Bogen_rechts_2
    
    drawPath(KurrentGatD_W)
    trans_scale(KurrentGatD_W, valueToMoveGlyph)
    return KurrentGatD_W, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
 
 
def drawKurrentGatD_W_thinStroke(x, y, *, pass_from_thick=None):

    KurrentGatD_W_thinStroke = BezierPath() 
    
    Endspitze = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 3, HSL_size=2, HSL_start=24, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 4)])     
    
    KurrentGatD_W_thinStroke = Endspitze 
    drawPath(KurrentGatD_W_thinStroke)
    trans_thinStroke_down_left(KurrentGatD_W_thinStroke) 
    trans_scale(KurrentGatD_W_thinStroke, valueToMoveGlyph)
    return KurrentGatD_W_thinStroke
    
    




#############################################################################
######     misc
#############################################################################

 
def drawKurrentGatC_etc(x, y):
    
    KurrentGatC_etc = drawSchriftzug_etc(x, y)
    trans_scale(KurrentGatC_etc, valueToMoveGlyph)
    return KurrentGatC_etc
    
    
    
    
def drawKurrentGatC_empty(x, y, *, pass_from_thick=None):     
    
    KurrentGatC_empty = BezierPath()
    KurrentGatC_empty.line((-100, -100), (-100, -110))
    return KurrentGatC_empty
    
    



    




# ________________________________________________________

marginStr = 30
marginRnd = 39    ### rund links
marginRnd2 = 39    ### rund rechts ### für p, v
marginCvr = -117    ## für b
marginPrblm = -30-59    ### für e, r, x weil Verbindung nicht passt
marginINT = 90    


# ____________ ab hier in RF ____________________________
    
    
font = CurrentFont()

drawFunctions = {
   #  'a' : [ drawKurrentGatC_a, [temp_x, temp_y], marginRnd, marginStr-15 ],
   #  'b' : [ drawKurrentGatC_b, [temp_x, temp_y], marginStr, marginCvr ],
   #  'c' : [ drawKurrentGatC_c, [temp_x, temp_y], marginRnd+22, marginStr-60 ],
   #  'd' : [ drawKurrentGatC_d, [temp_x, temp_y], marginRnd+22, marginStr -170],

   #  'e' : [ drawKurrentGatC_e, [temp_x, temp_y], marginStr, marginPrblm ],
   #  'e.mid' : [ drawKurrentGatC_e, [temp_x, temp_y, "mid"], marginStr, marginPrblm ],
   #  'e.end' : [ drawKurrentGatC_e, [temp_x, temp_y, "end"], marginStr, marginPrblm ],

   #  'f' : [ drawKurrentGatC_f, [temp_x, temp_y], marginStr-300, marginStr-349 ],
    
   # 'g' : [ drawKurrentGatC_g, [temp_x, temp_y], marginStr-112, marginStr ],
   # 'g.mid' : [ drawKurrentGatC_g, [temp_x, temp_y, "mid"], marginStr-112, marginStr ],
   # 'g.end' : [ drawKurrentGatC_g, [temp_x, temp_y, "end"], marginStr-62, marginStr ],
    
   #  'h' : [ drawKurrentGatC_h, [temp_x, temp_y], marginStr-91, marginStr-87],
   #  'h.mid' : [ drawKurrentGatC_h, [temp_x, temp_y, "mid"], marginStr-91, marginStr-87 ],
   #  'h.end' : [ drawKurrentGatC_h, [temp_x, temp_y, "end"], marginStr, marginStr-55 ],
   
   #  'i' : [ drawKurrentGatC_i, [temp_x, temp_y], marginStr, marginStr-150 ],
    
   #   'j' : [ drawKurrentGatC_j, [temp_x, temp_y], marginStr-267, marginStr-150 ],
   #   'j.mid' : [ drawKurrentGatC_j, [temp_x, temp_y, "mid"], marginStr-267, marginStr-150 ],
   #   'j.end' : [ drawKurrentGatC_j, [temp_x, temp_y, "end"], marginStr-226, marginStr-80 ],

   #  'k' : [ drawKurrentGatC_k, [temp_x, temp_y], marginStr, marginStr-237 ],
   #  'k.start' : [ drawKurrentGatC_k, [temp_x, temp_y, "start"], marginStr, marginStr-317 ],
   #  'k.mid' : [ drawKurrentGatC_k, [temp_x, temp_y, "mid"], marginStr, marginStr-237],
    
   #  'l' : [ drawKurrentGatC_l, [temp_x, temp_y], marginStr, marginStr-267 ],
   #  'm' : [ drawKurrentGatC_m, [temp_x, temp_y], marginStr, marginStr ],
   #  'n' : [ drawKurrentGatC_n, [temp_x, temp_y], marginStr, marginStr ],
   #  'o' : [ drawKurrentGatC_o, [temp_x, temp_y], marginRnd, marginCvr+97 ],
   #  'p' : [ drawKurrentGatC_p, [temp_x, temp_y], marginStr-300, marginRnd2 ],
   #  'q' : [ drawKurrentGatC_q, [temp_x, temp_y], marginRnd-156, marginStr ],
   #  'r' : [ drawKurrentGatC_r, [temp_x, temp_y], marginStr, marginPrblm ],
   #  's' : [ drawKurrentGatC_s, [temp_x, temp_y], marginStr, marginRnd2-274 ],
   #  'longs' : [ drawKurrentGatC_longs, [temp_x, temp_y], marginStr-300, marginStr-460 ],
   #  'germandbls' : [ drawKurrentGatC_germandbls, [temp_x, temp_y], marginStr-300, marginStr-160],
   #  't' : [ drawKurrentGatC_t, [temp_x, temp_y], marginStr, marginStr-120 ],
   #  'u' : [ drawKurrentGatC_u, [temp_x, temp_y], marginStr, marginStr-125 ],
   #  'v' : [ drawKurrentGatC_v, [temp_x, temp_y], marginStr, marginRnd2 ],
   #  'w' : [ drawKurrentGatC_w, [temp_x, temp_y], marginStr, marginRnd2 ],
   #  'x' : [ drawKurrentGatC_x, [temp_x, temp_y], marginStr-108, marginPrblm ],

   # 'y' : [ drawKurrentGatC_y, [temp_x, temp_y], marginStr-91, marginStr ],
   # 'y.mid' : [ drawKurrentGatC_y, [temp_x, temp_y, "mid"], marginStr-167+75, marginStr ],
   # 'y.end' : [ drawKurrentGatC_y, [temp_x, temp_y, "end"], marginStr, marginRnd ],

   #  'z' : [ drawKurrentGatC_z, [temp_x, temp_y, "mid"], marginStr-230, marginStr ],
   #  'z.start' : [ drawKurrentGatC_z, [temp_x, temp_y, "start"], marginStr-308, marginStr-97 ],
   #  'z.mid' : [ drawKurrentGatC_z, [temp_x, temp_y, "mid"], marginStr-231, marginStr ],
   #  'z.end' : [ drawKurrentGatC_z, [temp_x, temp_y, "end"], marginStr-120, marginStr],

   #  'adieresis' : [ drawKurrentGatC_adieresis, [temp_x, temp_y], marginRnd, marginStr-120 ],
   #  'odieresis' : [ drawKurrentGatC_odieresis, [temp_x, temp_y], marginRnd, marginStr-120 ],
   #  'udieresis' : [ drawKurrentGatC_udieresis, [temp_x, temp_y], marginStr, marginStr-120 ],

  
    
    
   #  #### Ligatures
    
    # 'e_t_c' : [ drawKurrentGatC_etc, [temp_x, temp_y], marginRnd, marginRnd ],
    # 'longs_longs' : [ drawKurrentGatC_longs_longs, [temp_x, temp_y], marginStr-300, marginStr-460 ],
    # 'f_f' : [ drawKurrentGatC_f_f, [temp_x, temp_y], marginStr-300, marginStr-412 ],
    # 'longs_t' : [ drawKurrentGatC_longs_t, [temp_x, temp_y], marginStr-300, marginStr-258 ],
    # 'f_t' : [ drawKurrentGatC_f_t, [temp_x, temp_y], marginStr-300, marginStr-120 ],
    # 'longs_longs_t' : [ drawKurrentGatC_longs_longs_t, [temp_x, temp_y], marginStr-300, marginStr-258 ],
    # 'f_f_t' : [ drawKurrentGatC_f_f_t, [temp_x, temp_y], marginStr-300, marginStr-258 ],

    # 'c_h' : [ drawKurrentGatC_c_h, [temp_x, temp_y], marginStr, marginStr-87],
    # 'c_h.mid' : [ drawKurrentGatC_c_h, [temp_x, temp_y, "mid"], marginStr, marginStr-87 ],
    # 'c_h.end' : [ drawKurrentGatC_c_h, [temp_x, temp_y, "end"], marginStr, marginStr-55 ],

    
   # #  #### Interpunction
    
    # 'period' : [ drawKurrentGatC_period, [temp_x, temp_y], marginINT, marginINT ],
    # 'colon' : [ drawKurrentGatC_colon, [temp_x, temp_y], marginINT, marginINT ],
    # 'semicolon' : [ drawKurrentGatC_semicolon, [temp_x, temp_y], marginINT, marginINT ],
    # 'quoteright' : [ drawKurrentGatC_quoteright, [temp_x, temp_y], marginINT, marginINT ],
    # 'comma' : [ drawKurrentGatC_comma, [temp_x, temp_y], marginINT, marginINT ],
    # 'endash' : [ drawKurrentGatC_endash, [temp_x, temp_y], marginINT, marginINT ],
    # 'hyphen' : [ drawKurrentGatC_hyphen, [temp_x, temp_y], marginINT, marginINT ],
    # 'exclam' : [ drawKurrentGatC_exclam, [temp_x, temp_y], marginINT, marginINT ],
    # 'question' : [ drawKurrentGatC_question, [temp_x, temp_y], marginINT+60, marginINT ],



   #  #### Numbers
      
    # 'zero' : [ drawKurrentGatC_zero, [temp_x, temp_y], marginRnd, marginRnd-90 ],
    # 'one' : [ drawKurrentGatC_one, [temp_x, temp_y], marginStr, marginStr-210 ],
    # 'two' : [ drawKurrentGatC_two, [temp_x, temp_y], marginStr, marginRnd-150 ],
    # 'three' : [ drawKurrentGatC_three, [temp_x, temp_y], marginStr+150, marginStr-180 ],
    # 'four' : [ drawKurrentGatC_four, [temp_x, temp_y], marginStr+30, marginStr-150 ],
    # 'five' : [ drawKurrentGatC_five, [temp_x, temp_y], marginStr+150, marginStr-270 ],
    # 'six' : [ drawKurrentGatC_six, [temp_x, temp_y], marginRnd, marginStr-60 ],
    # 'seven' : [ drawKurrentGatC_seven, [temp_x, temp_y], marginStr, marginStr-270 ],
    # 'eight' : [ drawKurrentGatC_eight, [temp_x, temp_y], marginRnd, marginRnd-120 ],    
    # 'nine' : [ drawKurrentGatC_nine, [temp_x, temp_y], marginStr+90, marginRnd-120 ],    

    # 'zero.osf' : [ drawKurrentGatC_zero, [temp_x, temp_y], marginRnd, marginRnd ],
    # 'one.osf' : [ drawKurrentGatC_one, [temp_x, temp_y], marginStr, marginStr-120 ],
    # 'two.osf' : [ drawKurrentGatC_two, [temp_x, temp_y], marginStr-60, marginRnd ],
    # 'three.osf' : [ drawKurrentGatC_three, [temp_x, temp_y], marginStr+90, marginStr-30 ],
    # 'four.osf' : [ drawKurrentGatC_four, [temp_x, temp_y], marginStr+60, marginStr ],
    # 'five.osf' : [ drawKurrentGatC_five, [temp_x, temp_y], marginStr+90, marginStr-120 ],
    # 'six.osf' : [ drawKurrentGatC_six, [temp_x, temp_y], marginRnd, marginStr+60 ],
    # 'seven.osf' : [ drawKurrentGatC_seven, [temp_x, temp_y], marginStr, marginStr-150 ],
    # 'eight.osf' : [ drawKurrentGatC_eight, [temp_x, temp_y], marginRnd, marginRnd ],    
    # 'nine.osf' : [ drawKurrentGatC_nine, [temp_x, temp_y], marginStr, marginRnd ],
    


    # # # # # ### Uppercase

    # 'C' : [ drawKurrentGatD_C, [temp_x, temp_y], marginRnd, -394  ],
    # 'D' : [ drawKurrentGatD_D, [temp_x, temp_y], marginRnd, -250  ],
    # 'G' : [ drawKurrentGatD_G, [temp_x, temp_y], marginRnd, -100 ],
    # 'H' : [ drawKurrentGatD_H, [temp_x, temp_y], marginRnd, -200  ],
    # 'W' : [ drawKurrentGatD_W, [temp_x, temp_y], marginRnd, -60 ],


    #'A' : [ drawKurrentGatC_A, [temp_x, temp_y], marginStr, -40  ],
    # # # # # # 'B' : [ drawKurrentGatC_B, [temp_x, temp_y] ],
    #'C' : [ drawKurrentGatC_C, [temp_x, temp_y], marginStr, -40  ],
    # # # # # # 'D' : [ drawKurrentGatD_D, [temp_x, temp_y], marginRnd, marginRnd  ],
    #'E' : [ drawKurrentGatC_E, [temp_x, temp_y], marginStr, -40  ],
    # # # # # # 'F' : [ drawKurrentGatC_F, [temp_x, temp_y] ],
    # # # # # # 'G' : [ drawKurrentGatD_G, [temp_x, temp_y], marginRnd, marginRnd  ],
    # # # # # # 'H' : [ drawKurrentGatD_H, [temp_x, temp_y], marginRnd, marginRnd  ],
    # # # # # # 'I' : [ drawKurrentGatC_I, [temp_x, temp_y] ],
    # # # # # # 'J' : [ drawKurrentGatC_J, [temp_x, temp_y] ],
    # # # # # # 'K' : [ drawKurrentGatC_K, [temp_x, temp_y] ],
    # 'L' : [ drawKurrentGatC_L, [temp_x, temp_y], marginRnd, marginRnd  ],
    # # # # # # 'M' : [ drawKurrentGatC_M, [temp_x, temp_y] ],
    # # # # # # 'N' : [ drawKurrentGatC_N, [temp_x, temp_y] ],
    # # # # # # 'O' : [ drawKurrentGatC_O, [temp_x, temp_y] ],
    # # # # # # 'P' : [ drawKurrentGatC_P, [temp_x, temp_y] ],
    # # # # # # 'Q' : [ drawKurrentGatC_Q, [temp_x, temp_y] ],
    # # # # # # 'R' : [ drawKurrentGatC_R, [temp_x, temp_y] ],
    # # # # # # 'S' : [ drawKurrentGatC_S, [temp_x, temp_y] ],
    # # # # # # 'T' : [ drawKurrentGatC_T, [temp_x, temp_y] ],
    # # # # # # 'U' : [ drawKurrentGatC_U, [temp_x, temp_y] ],
    # # # # # # 'V' : [ drawKurrentGatC_V, [temp_x, temp_y] ],
    # # # # # # 'W' : [ drawKurrentGatD_W, [temp_x, temp_y], marginRnd, marginRnd ],
    # # # # # # 'X' : [ drawKurrentGatC_X, [temp_x, temp_y] ],
    # # # # # # 'Y' : [ drawKurrentGatC_Y, [temp_x, temp_y] ],
    # # # # # # 'Z' : [ drawKurrentGatC_Z, [temp_x, temp_y] ],

    }
    
    

drawFunctions_thinStroke = {

     'g.end' : [ drawKurrentGatC_g_thinStroke, [temp_x, temp_y] ],      
     'h.end' : [ drawKurrentGatC_g_thinStroke, [temp_x, temp_y] ],
     'j.end' : [ drawKurrentGatC_g_thinStroke, [temp_x, temp_y] ],
     'y.end' : [ drawKurrentGatC_g_thinStroke, [temp_x, temp_y] ],
     'z.end' : [ drawKurrentGatC_g_thinStroke, [temp_x, temp_y] ],

     'germandbls' : [ drawKurrentGatC_germandbls_thinStroke, [temp_x, temp_y] ],

     'c_h.end' : [ drawKurrentGatC_g_thinStroke, [temp_x, temp_y] ],  


      'three' : [ drawKurrentGatC_three_thinStroke, [temp_x, temp_y] ],
      'five' : [ drawKurrentGatC_three_thinStroke, [temp_x, temp_y] ],
      'six' : [ drawKurrentGatC_six_thinStroke, [temp_x, temp_y] ],
      'nine' : [ drawKurrentGatC_nine_thinStroke, [temp_x, temp_y] ],

      'three.osf' : [ drawKurrentGatC_three_thinStroke, [temp_x, temp_y] ],
      'five.osf' : [ drawKurrentGatC_three_thinStroke, [temp_x, temp_y] ],
      'six.osf' : [ drawKurrentGatC_six_thinStroke, [temp_x, temp_y] ],
      'nine.osf' : [ drawKurrentGatC_nine_thinStroke, [temp_x, temp_y] ],

      'W' : [ drawKurrentGatD_W_thinStroke, [temp_x, temp_y] ],
      'H' : [ drawKurrentGatD_H_thinStroke, [temp_x, temp_y] ],
      'G' : [ drawKurrentGatD_G_thinStroke, [temp_x, temp_y] ],

    }
    

     
     
     
     
  
    
for key in drawFunctions:
    
    glyph = font[[key][0]]
    glyph.clear()
    
    foreground = glyph.getLayer("foreground")
    foreground.clear()
    background = glyph.getLayer("background")
    background.clear()
     
    function = drawFunctions[key][0]
    arguments = drawFunctions[key][1]

    output = function(*arguments)
	
    if key in drawFunctions_thinStroke:
        
        assert isinstance(output, tuple) and len(output)==2
        pass_from_thick_to_thin = output[1]
        output = output[0]
    
    # assert isinstance(output, glyphContext.GlyphBezierPath)
		
    writePathInGlyph(output, foreground)

    print("___",font[[key][0]])
    
    # ask for current status
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin   
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_before = margin_ts - margin_fg
    
    ### change value of margin
    glyph.leftMargin = drawFunctions[key][2]
    glyph.rightMargin = drawFunctions[key][3]
    
    ### ask inbetween 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_mid = margin_ts - margin_fg

    ### calculate value for movement of bg
    move_thinStroke = difference_before - difference_mid

    glyph.copyLayerToLayer('foreground', 'background')
 
   
    if key in drawFunctions_thinStroke:		
        thinstroke = glyph.getLayer("thinstroke")
        thinstroke.clear()

        thinfunction = drawFunctions_thinStroke[key][0]
        thinarguments = drawFunctions_thinStroke[key][1]
        thinoutput = thinfunction(*thinarguments, pass_from_thick=pass_from_thick_to_thin)
        
        writePathInGlyph(thinoutput, thinstroke)
        
    thinstroke = glyph.getLayer("thinstroke")
    thinstroke.translate((move_thinStroke, 0))
    
    ### ask final 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0 
        #print("(no thin line)")   
    difference_final = margin_ts - margin_fg
    
    foreground.clear()

    
    
    
    ### now draw the whole glphy with the magic tool
    ### first thick main stroke 
    guide_glyph = glyph.getLayer("background")

    w = A_A * penWidth 
    a = radians(90-alpha)
    h = penThickness

    if True:
        RectNibPen = RectNibPen_Just

    p = RectNibPen(
        glyphSet=CurrentFont(),
        angle=a,
        width=w,
        height=penThickness,
        trace=True
    )

    guide_glyph.draw(p)
    p.trace_path(glyph)


    ### now thin Stroke
    if key in drawFunctions_thinStroke:		

        guide_glyph = glyph.getLayer("thinstroke")
        w = penThickness 
        p = RectNibPen(
            glyphSet=CurrentFont(),
            angle=a,
            width=w,
            height=penThickness,
            trace=True
        )
        guide_glyph.draw(p)
        p.trace_path(glyph)

    ### keep a copy in background layer
    newLayerName = "original_math"
    glyph.layers[0].copyToLayer(newLayerName, clear=True)
    glyph.getLayer(newLayerName).leftMargin = glyph.layers[0].leftMargin
    glyph.getLayer(newLayerName).rightMargin = glyph.layers[0].rightMargin

    ### this will make it all one shape
    #glyph.removeOverlap(round=0)
    
    ### from robstevenson to remove extra points on curves
    ### function itself is in create_stuff.py
    #select_extra_points(glyph, remove=True)




    

    