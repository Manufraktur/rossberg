import importlib
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatE
import version_3.creation.Halbboegen_GatE
# import version_3.creation.Schwuenge_GatE
import Kurrent_Schriftzuege_GatE

importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatE)
importlib.reload(version_3.creation.Halbboegen_GatE)
# importlib.reload(version_3.creation.Schwuenge_GatE)
importlib.reload(Kurrent_Schriftzuege_GatE)

from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatE import *
from version_3.creation.Halbboegen_GatE import *
# from version_3.creation.Schwuenge_GatE import *
from Kurrent_Schriftzuege_GatE import *
from version_3.creation.special_drawbot import BezierPath, drawPath

from nibLib.pens.rectNibPen import RectNibPen
from version_3.creation.rect_nib_pen import RectNibPen as RectNibPen_Just
from math import radians
import math as m
import collections
import glyphContext




# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 18
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)







# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 1.5

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)


# temporary to check height of numbers
#line((1*modul_width, 12.5*modul_height), (12*modul_width, 12.5*modul_height))






# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 7






# ________________________________________________________





def drawKurrentGatE_a(x, y):      
            
    bogen_links = drawSchriftzug6(x, y, instrokeLen=0.5, outstrokeLen=2.6)
    schleife_down_rechts = drawSchriftzug3(x+3, y)

    KurrentGatE_a = bogen_links + schleife_down_rechts
    trans_scale(KurrentGatE_a, valueToMoveGlyph)
    return KurrentGatE_a
    
    
def drawKurrentGatE_adieresis(x, y):
    
    Dieresis = drawDieresis(x, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    glyph_a = drawKurrentGatE_a(x, y)
    KurrentGatE_udieresis = glyph_a + Dieresis
    return KurrentGatE_udieresis
    
    
    
    
    
def drawKurrentGatE_b(x,y):        
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)

    Strich_links = drawSchriftzug10(x, y,  instrokeLen=1, outstrokeLen=2.2)
    #Bogen_rechts = drawSchriftteil8(*Grund_b)
    Bogen_rechts = drawSchriftzug3_loopRight(x+3, y, instrokeLen=0.5)
    hook = drawSchriftzug_b_hook(x+3, y+1, version="1.5")    ### passt mit ? folgend
    
    KurrentGatE_b = Strich_links + Bogen_rechts + hook
    trans_scale(KurrentGatE_b, valueToMoveGlyph)
    return KurrentGatE_b
    
    
    
    


def drawKurrentGatE_c(x, y, hook=True):
           
    KurrentGatE_c = BezierPath()
           
    Bogen_links = drawSchriftzug6(x, y, version="2", instrokeLen=0.75, outstrokeLen=1.25)
    
    if hook == True:
        Haeckchen_oben = drawSchriftzug23_KurrentKanzlei(x+1.85, y+1.35)
        KurrentGatE_c += Haeckchen_oben
        
    KurrentGatE_c += Bogen_links 
    trans_scale(KurrentGatE_c, valueToMoveGlyph)
    return KurrentGatE_c
    
    
def drawKurrentGatE_c_h(x, y, version="mid"):      
    
    glyph_c = drawSchriftzug2(x-3, y, outstrokeLen=2)
    trans_scale(glyph_c, valueToMoveGlyph)
  
    if version == "mid":
        glyph_h = drawKurrentGatE_h(x, y, version="mid")
    
    if version == "end":
        glyph_h = drawKurrentGatE_h(x, y, version="end")[0]
    
    values_to_return = drawKurrentGatE_h(x, y, version="end")[1]   
    
    KurrentGatE_c_h = glyph_c + glyph_h
    
    if version == "mid":
        return KurrentGatE_c_h
    
    if version == "end":
        return KurrentGatE_c_h, values_to_return
        
            
    



def drawKurrentGatE_d(x, y):      

    full_stroke = drawSchriftzug15(x, y)
    
    KurrentGatE_d = full_stroke
    trans_scale(KurrentGatE_d, valueToMoveGlyph)
    return KurrentGatE_d
    


    


def drawKurrentGatE_e(x, y, version="mid"):
        
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    stroke_left = drawSchriftzug1(x, y, "a", instrokeLen=1) 
    con_stroke = drawOutstroke(*Grund_d, length=2)
    
    if version == "mid":
        hook = drawGrundelementE(*con_stroke.points[0], outstrokeLen=1)

    if version == "end":
        hook = drawSchriftteil10(*con_stroke.points[0])

    KurrentGatE_e = stroke_left + con_stroke + hook
    trans_scale(KurrentGatE_e, valueToMoveGlyph)
    return KurrentGatE_e
    
    
    
    
       
def drawKurrentGatE_f(x,y, version="b"):    # Kurve passt noch nicht >>> später noch lösen
    x += 1
    KurrentGatE_f = BezierPath()
    
    # draw Modul + Raute
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.5)
    
    base = drawSchriftzug1(x, y, "a")   ### Warum zeichnet er das nicht in RF???
    
    downstroke = drawGrundelementH(*Grund_a, 6, "down")
    upstroke = drawGrundelementH(*Grund_a, 4.9, "up")

    Querstrich = drawSchriftzug2(x+2, y, "b", "gebogen")
    
    # draw Modul + Raute
    helper = (0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+6, y+h)

    # Signatur
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+8.5, y+5.5)
    
    HSL_size = 3
    HSL_start = 7

    G2 = Raute_c[0]-modul_width*0.75, Raute_c[1]+modul_height/4
    G1 = G2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),     G2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    #polygon(G1, G2) 

    F1, F2 = line_F_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size)

    E1, E2 = line_E_vonF_o_gr(F1, *angles, part, HSL_size, HSL_start+HSL_size*2)     

    
    
    # Übergang Signatur zu downstroke
    HSL_size = 0
    HSL_start = 82
    
    E3 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),  E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    polygon(E1, E3)
    
    D1, D2 = line_D_vonE_o_gr(E3, *angles, part, HSL_size, HSL_start)
    KurrentGatE_f.arc(*drawKreisSeg(D1, HSL_start, angle_6, angle_5, True))

    # Path zeichnen Signatur
    # nur damit Reihenfolge für Path stimmt ....
    HSL_size = 3
    HSL_start = 7
    KurrentGatE_f.arc(*drawKreisSeg(F1, HSL_start+HSL_size, angle_5, angle_4, True))
    KurrentGatE_f.arc(*drawKreisSeg(G1, HSL_start, angle_4, angle_3, True))



    # Kurve Zwischenstück -TURN-

    HSL_size =  5
    HSL_start = 3
    
    A8 = G2
    A7 = A8[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      A8[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    polygon(A7, A8)

    B1, B2 = line_B_vonA_u_gr(A7, *angles, part, HSL_size, HSL_start+HSL_size)
    KurrentGatE_f.arc(*drawKreisSeg(A7, HSL_start, angle_1, angle_0, True))

    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    KurrentGatE_f.arc(*drawKreisSeg(B1, HSL_start+HSL_size, angle_16, angle_15, True))

    KurrentGatE_f.arc(*drawKreisSeg(C1, HSL_start+HSL_size*2, angle_15, angle_14, True))


    # Kurve unten
    HSL_size =  12
    HSL_start = 168
    
    E6 = Querstrich.points[0]
    E5 = E6[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start) , E6[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    polygon(E5, E6)
    
    D1, D2 = line_D_vonE_u_kl(E5, *angles, part, HSL_size, HSL_start-HSL_size)
    KurrentGatE_f.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_14, angle_13, True))


    drawPath(KurrentGatE_f)
    KurrentGatE_f += base + downstroke + upstroke + Querstrich
    trans_scale(KurrentGatE_f, valueToMoveGlyph)
    return KurrentGatE_f
    
    
    


    
    
def drawKurrentGatE_f_f(x, y):
    
    KurrentGatE_f_f = BezierPath()

    left = drawSchriftzug_longs_longs(x, y)
    right = drawSchriftzug17(x+6, y, base=False)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1.2, y-1.135)
    #Querstrich = drawGrundelementB(*Raute_a, 6)
    Querstrich_links = drawSchneckenzug(*Raute_a, UPPER_E, 2, HSL_size=1, HSL_start=34, clockwise=True, inward=True)
    Querstrich_rechts = drawSchneckenzug(*Querstrich_links.points[-1], LOWER_G, 2, HSL_size=1, HSL_start=34, clockwise=False, inward=True)
        
    KurrentGatE_f_f = left + right + Querstrich_links + Querstrich_rechts
    drawPath(KurrentGatE_f_f)
    trans_scale(KurrentGatE_f_f, valueToMoveGlyph)
    return KurrentGatE_f_f
    
    
    
def drawKurrentGatE_f_t(x, y):
        
    left_f = drawSchriftzug25(x, y, "b", outstrokeLen=1.5)  
    right_t = drawSchriftzug14(x+8, y, outstrokeLen=2) 
    Querstrich_t = drawGrundelementE((x+7)*modul_width, (y-1)*modul_height, pos="unten")

    KurrentGatE_f_t = left_f + right_t + Querstrich_t
    trans_scale(KurrentGatE_f_t, valueToMoveGlyph)
    return KurrentGatE_f_t



def drawKurrentGatE_f_f_t(x, y):
    #x -=5
    # left = drawSchriftzug_longs_longs(x, y)
    # right_t = drawSchriftzug_longs_t(x+6, y, base=False)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.5, y-0.5)
    Querstrich = drawSchriftteil12(*Raute_a)
    trans_scale(Querstrich, valueToMoveGlyph)

    glyph_longs_longs_t = drawKurrentGatE_longs_longs_t(x, y)
    KurrentGatE_f_f_t = glyph_longs_longs_t + Querstrich
    return KurrentGatE_f_f_t
    
    

def drawKurrentGatE_longs_longs_t(x, y):
    #x -=5
    left = drawSchriftzug_longs_longs(x, y)
    right_t = drawSchriftzug_longs_t(x+6, y, base=False)

    KurrentGatE_longs_longs_t = left + right_t
    trans_scale(KurrentGatE_longs_longs_t, valueToMoveGlyph)
    return KurrentGatE_longs_longs_t


    

    
def drawKurrentGatE_g(x, y, version="mid"):
    x += 3
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    bogen_links = drawSchriftzug6(x, y, instrokeLen=0.5, outstrokeLen=2.6)
    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
        
    # Windung von Raute_b nach rechts
    loopRight = drawSchriftzug3_loopRight(x+3, y)

    # Wendepunkt ab Raute_b nach links
    loopLeft = drawSchriftzug3_loopLeft(x+3, y)
    
    if version == "mid":
        schwung = drawSchriftzug9(x+3, y, outstrokeLen=1)
    
    if version == "end":
        schwung = drawSchriftzug9(x+3, y, version="end") 
        pkt_Auslauf = schwung.points[3]
        #text("pkt_Auslauf", pkt_Auslauf)       
    
    KurrentGatE_g = bogen_links + loopRight + loopLeft + schwung
    trans_scale(KurrentGatE_g, valueToMoveGlyph)
    
    if version == "mid":
        return KurrentGatE_g
    
    if version == "end":
        return KurrentGatE_g, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
def drawKurrentGatE_g_thinStroke(x, y, *, pass_from_thick=None):
    
    KurrentGatE_g_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 6, HSL_size=1.5, HSL_start=22, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.2 * i      for  i in range(0, 7)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.1, Auslauf.points[-1][1]-modul_height*0.875) 
         
    KurrentGatE_g_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentGatE_g_thinStroke)
    trans_thinStroke_down_left(KurrentGatE_g_thinStroke) 
    trans_scale(KurrentGatE_g_thinStroke, valueToMoveGlyph)
    return KurrentGatE_g_thinStroke
    
    
    
    
     
           


def drawKurrentGatE_h(x, y, version="mid"):      
    
    Strich_links = drawSchriftzug10(x, y, instrokeLen=1, outstrokeLen=3)

    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
    
    if version == "mid":
        Bogen_rechts = drawSchriftzug11(x-3, y)
    
    if version == "end":
        Bogen_rechts = drawSchriftzug12(x-3, y, "end")
        pkt_Auslauf = Bogen_rechts.points[-1]
        #text("pkt_Auslauf", pkt_Auslauf)
        
    KurrentGatE_h = Strich_links + Bogen_rechts
    trans_scale(KurrentGatE_h, valueToMoveGlyph)

    if version == "mid":
        return KurrentGatE_h
            
    if version == "end":
        return KurrentGatE_h, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)

    
    
    
        

def drawKurrentGatE_i(x, y):      

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)

    instroke = drawInstroke(*Grund_a, 1)
    downstroke = drawGrundelementF(*Grund_a, 1.5)
    outstroke = drawOutstroke(*downstroke.points[1], 1)
    iPunkt = drawGrundelementE(Grund_a[0]+modul_width*2.5, Grund_a[1]+modul_height*3.5)
    
    KurrentGatE_i = instroke + downstroke + outstroke + iPunkt
    trans_scale(KurrentGatE_i, valueToMoveGlyph)
    return KurrentGatE_i
        
    
        
        
        
    
def drawKurrentGatE_j(x, y, version="mid"):      
    #x+=4
    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
    
    if version == "mid":
        Strich = drawSchriftzug9(x, y, instrokeLen=1)

    if version == "end":
        Strich = drawSchriftzug9(x, y, instrokeLen=1, version="end") 
        pkt_Auslauf = Strich.points[-5]
        #text("pkt_Auslauf", pkt_Auslauf)

    Punkt = drawGrundelementE((x+1.5)*modul_width, (y+4)*modul_height)
    
    KurrentGatE_j = Strich + Punkt
    trans_scale(KurrentGatE_j, valueToMoveGlyph)
    
    if version == "mid":
        return KurrentGatE_j
    
    if version == "end":
        return KurrentGatE_j, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)





def drawKurrentGatE_k(x, y, version="mid"):
    
    if version == "start":
        Strich_links = drawSchriftzug_k_start(x, y, outstrokeLen=0)

    if version == "mid":
        Strich_links = drawSchriftzug13(x, y, instrokeLen=2, outstrokeLen=0)
    
    #Positionierung Schleifenelement
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+2, y+1)

    upstroke = drawOutstroke(*Grund_a, 2)
    Element_oben = drawHalbbogen1(*upstroke.points[0], instrokeLen=1)
    downstroke = drawOutstroke(*Element_oben.points[-1], 2.96, "down")
    Signatur = drawGrundelementE((x-0.5)*modul_width, (y-0.2)*modul_height, instrokeLen=0)
    outstroke = drawOutstroke(*Signatur.points[4], 1.5)

    KurrentGatE_k = Strich_links + upstroke + Element_oben + downstroke + Signatur + outstroke
    trans_scale(KurrentGatE_k, valueToMoveGlyph)
    return KurrentGatE_k
    
    
    
    
    
    
        
        
def drawKurrentGatE_l(x, y):    
    
    main_stroke = drawSchriftzug10(x, y, instrokeLen=1, outstrokeLen=2)
    
    KurrentGatE_l = main_stroke
    trans_scale(KurrentGatE_l, valueToMoveGlyph)
    return KurrentGatE_l
    
    
    
    
    
        
        
def drawKurrentGatE_m(x, y):
    
    downstroke_left = drawSchriftzug2(x, y, "a", outstrokeLen=3)
    downstroke_middle = drawSchriftzug2(x+3, y, "a", instrokeLen=0, outstrokeLen=3)
    downstroke_right = drawSchriftzug2(x+6, y, "a", instrokeLen=0)
    
    KurrentGatE_m = downstroke_left + downstroke_middle + downstroke_right
    trans_scale(KurrentGatE_m, valueToMoveGlyph)
    return KurrentGatE_m
    







def drawKurrentGatE_n(x, y):
  
    downstroke_left = drawSchriftzug2(x, y, "a", outstrokeLen=3)
    downstroke_right = drawSchriftzug2(x+3, y, "a", instrokeLen=0)
    
    KurrentGatE_n = downstroke_left + downstroke_right
    trans_scale(KurrentGatE_n, valueToMoveGlyph)
    return KurrentGatE_n
    





def drawKurrentGatE_o(x, y):
        
    ### hier müsste eigentlch Schriftzug 5 verwendet werden,
    ### so steht es auf S. 375 der Theorie, aber das ist zu eckig.
    ### daher Schriftzug6 in größerer Version.
    Bogen_links = drawSchriftzug6(x, y, version="2", instrokeLen=0.5, outstrokeLen=2.1)
    Bogen_rechts = drawSchriftzug3_loopRight(x+2.5, y-0.25)
    hook = drawSchriftzug_b_hook(x+2.5, y+0.75, version="1")
    
    KurrentGatE_o = Bogen_links + Bogen_rechts + hook
    trans_scale(KurrentGatE_o, valueToMoveGlyph)
    return KurrentGatE_o
    
    
def drawKurrentGatE_odieresis(x, y):
    
    Dieresis = drawDieresis(x-0.5, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    glyph_o = drawKurrentGatE_o(x, y)
    KurrentGatE_odieresis = glyph_o + Dieresis
    return KurrentGatE_odieresis 
    


    
def drawKurrentGatE_p(x, y):
        
    Strich_unten = drawGrundelementH((x-1)*modul_width, (y-1)*modul_height, 6, "down")
    trans_scale(Strich_unten, valueToMoveGlyph)

    glyph_p = drawKurrentGatE_v(x, y)
    KurrentGatE_p = glyph_p + Strich_unten
    return KurrentGatE_p
    
    
    
    
    
    
def drawKurrentGatE_q(x, y):      
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    
    bogen_links = drawSchriftzug6(x, y, instrokeLen=0.5, outstrokeLen=2.6)
    
    # Windung von Raute_b nach rechts
    loopRight = drawSchriftzug3_loopRight(x+3, y)

    # Wendepunkt ab Raute_b nach links
    loopLeft = drawSchriftzug3_loopLeft(x+3, y, outstrokeLen=0.75)
    
    Strich_gerade = drawGrundelementF((x+2)*modul_width, (y+0.5)*modul_height, 1.5)
    Strich_unten = drawGrundelementH((x+2)*modul_width, (y-1)*modul_height, 6, "down")
    outstroke = drawOutstroke(*Strich_gerade.points[-1], 1)
    
    KurrentGatE_q = bogen_links + loopRight + loopLeft + Strich_gerade + Strich_unten + outstroke
    trans_scale(KurrentGatE_q, valueToMoveGlyph)
    return KurrentGatE_q
    






def drawKurrentGatE_r(x, y):       

    Strich_links = drawSchriftzug4(x, y, "1.5", outstrokeLen=1.675)
    Strich_rechts = drawGrundelementE((x+1.5)*modul_width, (y+0.5)*modul_height, outstrokeLen=1)
    
    KurrentGatE_r = Strich_links + Strich_rechts
    trans_scale(KurrentGatE_r, valueToMoveGlyph) 
    return KurrentGatE_r
    
    
    
    
    
    
def drawKurrentGatE_s(x, y):
    
    full_stroke = drawSchriftzug16(x, y) 

    KurrentGatE_s = full_stroke
    trans_scale(KurrentGatE_s, valueToMoveGlyph)
    return KurrentGatE_s
    
      
  

    

def drawKurrentGatE_longs(x, y):
        
    KurrentGatE_longs = drawSchriftzug17(x, y, base=True)
    
    trans_scale(KurrentGatE_longs, valueToMoveGlyph)
    return KurrentGatE_longs
    
    



def drawKurrentGatE_longs_longs(x, y):
        
    left = drawSchriftzug_longs_longs(x, y)
    right = drawSchriftzug17(x+6, y, base=False)
    
    KurrentGatE_longs_longs = left + right
    trans_scale(KurrentGatE_longs_longs, valueToMoveGlyph)
    return KurrentGatE_longs_longs



def drawKurrentGatE_longs_t(x, y):
        
    KurrentGatE_longs_t = drawSchriftzug_longs_t(x, y)
    
    trans_scale(KurrentGatE_longs_t, valueToMoveGlyph)
    return KurrentGatE_longs_t
    
    
    


def drawKurrentGatE_germandbls(x, y):
        
    KurrentGatE_germandbls = drawSchriftzug22(x,y)    
    pkt_Auslauf = KurrentGatE_germandbls.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    trans_scale(KurrentGatE_germandbls, valueToMoveGlyph)
    return KurrentGatE_germandbls, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
def drawKurrentGatE_germandbls_thinStroke(x, y, *, pass_from_thick=None):
    
    KurrentGatE_germandbls_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 5, HSL_size=4, HSL_start=24, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 6)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.025, Auslauf.points[-1][1]-modul_height*0.5)
     
    KurrentGatE_germandbls_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentGatE_germandbls_thinStroke)
    trans_thinStroke_down_left(KurrentGatE_germandbls_thinStroke) 
    trans_scale(KurrentGatE_germandbls_thinStroke, valueToMoveGlyph)
    return KurrentGatE_germandbls_thinStroke
    
    
    
    
    

    
def drawKurrentGatE_t(x, y):    
    
    Strich = drawSchriftzug14(x, y, outstrokeLen=2)
    Querstrich = drawGrundelementE((x-1)*modul_width, (y-1)*modul_height, pos="unten")

    KurrentGatE_t = Strich + Querstrich
    trans_scale(KurrentGatE_t, valueToMoveGlyph)
    return KurrentGatE_t
    
    
    
    

    

def drawKurrentGatE_u(x, y):
    
    downstroke_left = drawSchriftzug2(x, y, "a", outstrokeLen=3)
    downstroke_right = drawSchriftzug2(x+3, y, "a", instrokeLen=0)
    hook = drawSchriftzug_u_hook(x+2, y+2.5)
    
    KurrentGatE_u = downstroke_left + downstroke_right + hook
    trans_scale(KurrentGatE_u, valueToMoveGlyph)
    return KurrentGatE_u
    

    
def drawKurrentGatE_udieresis(x, y):
    
    downstroke_left = drawSchriftzug2(x, y, "a", outstrokeLen=3)
    downstroke_right = drawSchriftzug2(x+3, y, "a", instrokeLen=0)
    Dieresis = drawDieresis(x, y)
    
    KurrentGatE_udieresis = downstroke_left + downstroke_right + Dieresis
    trans_scale(KurrentGatE_udieresis, valueToMoveGlyph)
    return KurrentGatE_udieresis
    
    
    
    
    
    
def drawKurrentGatE_v(x, y):
    
    Strich_links = drawSchriftzug4(x, y, version="gebogen", outstrokeLen=2)
    Strich_rechts = drawSchriftzug7((x+2)*modul_width, (y+0.5)*modul_height, outstrokeLen=0)
    
    KurrentGatE_v = Strich_links + Strich_rechts
    trans_scale(KurrentGatE_v, valueToMoveGlyph)
    return KurrentGatE_v
    
    
    

    
def drawKurrentGatE_w(x, y):
    
    Strich_links = drawSchriftzug2(x, y, "a", outstrokeLen=2)
    Strich_mitte = drawSchriftzug4(x+3, y, outstrokeLen=1.75)
    Strich_rechts = drawSchriftzug7((x+5)*modul_width, (y+0.5)*modul_height, outstrokeLen=0)
    
    KurrentGatE_w = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(KurrentGatE_w, valueToMoveGlyph)
    return KurrentGatE_w
    
    
    
    
    
def drawKurrentGatE_x(x, y):
    
    hook_btm = drawSchriftzug_x_hook(x-0.5, y)
    trans_scale(hook_btm, valueToMoveGlyph)

    glyph_r = drawKurrentGatE_r(x, y)    
    KurrentGatE_x = glyph_r + hook_btm
    return KurrentGatE_x
    
    
    



def drawKurrentGatE_y(x, y, version="mid"):
    
    Strich_links = drawSchriftzug4(x, y, outstrokeLen=1.75)

    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
    
    if version == "mid":
        Bogen_rechts = drawSchriftzug11(x-3, y, instrokeLen=1, outstrokeLen=1)

    if version == "end":
        Bogen_rechts = drawSchriftzug12(x-3, y, "end")
        pkt_Auslauf = Bogen_rechts.points[-1]
        #text("pkt_Auslauf", pkt_Auslauf)
                
    KurrentGatE_y = Strich_links + Bogen_rechts
    trans_scale(KurrentGatE_y, valueToMoveGlyph)
    
    if version == "mid":
        return KurrentGatE_y

    if version == "end":
        return KurrentGatE_y, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)





def drawKurrentGatE_z(x, y, version="mid"):
    #x +=2
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x+2, y+1)
    
    pkt_Auslauf = 0, 0
    #text("pkt_Auslauf", pkt_Auslauf)
    
    if version == "start":
        Element_oben = drawSchriftzug_z_top(x-3, y)
        connection = drawOutstroke(*Element_oben.points[-1], 0, "down")
        Halbbogen_unten = drawSchriftzug12(x-6, y, version="a", outstrokeLen=2.5)

    if version == "mid":
        Element_oben = drawGrundelementE(*Raute_a, instrokeLen=2.75)
        connection = drawOutstroke(*Element_oben.points[2], 2, "down")
        Halbbogen_unten = drawSchriftzug12(x-6, y-0.5, version="b", outstrokeLen=3.5)

    if version == "end":        
        Element_oben = drawGrundelementE(*Raute_a, instrokeLen=2.75)
        connection = drawOutstroke(*Element_oben.points[2], 2, "down")
        Halbbogen_unten = drawSchriftzug12(x-6, y-1, version="end")
        pkt_Auslauf = Halbbogen_unten.points[-1]
        #text("pkt_Auslauf", pkt_Auslauf)

    if version == "isol":
        Element_oben = drawGrundelementE(*Raute_a, instrokeLen=1)
        connection = drawOutstroke(*Element_oben.points[2], 2, "down")
        Halbbogen_unten = drawSchriftzug12(x-6, y-0.5, version="b", outstrokeLen=3.5)
               
    KurrentGatE_z = Element_oben + connection + Halbbogen_unten
    trans_scale(KurrentGatE_z, valueToMoveGlyph)
    
    if version == "start" or version == "mid" or version == "isol":
        return KurrentGatE_z
        
    if version == "end":        
        return KurrentGatE_z, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
        
        
        
          

    
    
    
    
    
    

#############################################################################
######     ab hier Interpunktion
#############################################################################


    
    
    

def drawKurrentGatE_period(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    period = drawGrundelementE(*Grund_d, 1, pos="unten")
        
    KurrentGatE_period = period
    trans_scale(KurrentGatE_period, valueToMoveGlyph)
    return KurrentGatE_period
    
    
    
    
    
def drawKurrentGatE_colon(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    colon_btm = drawGrundelementE(*Grund_d)
    
    # Hilfslinie zum Kontrollieren der Richtung GG
    save()
    stroke(0,0,3)
    strokeWidth(0.001)
    line(Grund_d, (Grund_d[0]-(G2[0]-G1[0])*3, Grund_d[1]-(G2[1]-G1[1])*3))
    restore()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    colon_top = drawGrundelementE(*Grund_b)  
    
    KurrentGatE_colon = colon_top + colon_btm
    trans_scale(KurrentGatE_colon, valueToMoveGlyph)
    return KurrentGatE_colon
    
    
    
    
def drawKurrentGatE_semicolon(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)

    # Hilfslinie zum Kontrollieren der Richtung GG
    save()
    stroke(0,0,3)
    strokeWidth(0.001)
    line(Grund_d, (Grund_d[0]-(G2[0]-G1[0])*3, Grund_d[1]-(G2[1]-G1[1])*3))
    restore()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    
    semicolon_top = drawGrundelementE(*Grund_b)  
    semicolon_btm = drawSchriftzug_semicolon_btm(x, y)  

    KurrentGatE_semicolon = semicolon_top + semicolon_btm
    trans_scale(KurrentGatE_semicolon, valueToMoveGlyph)
    return KurrentGatE_semicolon
    
    
    
    

def drawKurrentGatE_quoteright(x, y):

    quoteright = drawSchriftzug_semicolon_btm(x, y+5) 
    
    KurrentGatE_quoteright = quoteright
    trans_scale(KurrentGatE_quoteright, valueToMoveGlyph)
    return KurrentGatE_quoteright
    
    
       
    
    
def drawKurrentGatE_comma(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    comma = drawGrundelementG(*Grund_a, 2, "down") 

    KurrentGatE_comma = comma
    trans_scale(KurrentGatE_comma, valueToMoveGlyph)
    return KurrentGatE_comma
    
    



    
def drawKurrentGatE_endash(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1)
    endash = drawGrundelementB(*Grund_a, 2) 

    KurrentGatE_endash = endash
    trans_scale(KurrentGatE_endash, valueToMoveGlyph)
    return KurrentGatE_endash
    
    
    
    
    
def drawKurrentGatE_hyphen(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+0.75)
    hyphen_top = drawGrundelementH(*Grund_b, 2, "down") 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.25)
    hyphen_btm = drawGrundelementH(*Grund_b, 2, "down") 
    
    KurrentGatE_hyphen = hyphen_top + hyphen_btm
    trans_scale(KurrentGatE_hyphen, valueToMoveGlyph)
    return KurrentGatE_hyphen
     
    
    
    
def drawKurrentGatE_exclam(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1, y-0.5)
    exclam = drawGrundelementG(*Grund_a, 6.5) 
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    period = drawGrundelementE(*Grund_d)

    KurrentGatE_exclam = exclam + period
    trans_scale(KurrentGatE_exclam, valueToMoveGlyph)
    return KurrentGatE_exclam
    
    



def drawKurrentGatE_question(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    period = drawGrundelementE(*Grund_d)
    question = drawSchriftzug_question(x, y) 

    KurrentGatE_question = question + period
    trans_scale(KurrentGatE_question, valueToMoveGlyph)
    return KurrentGatE_question
    
    
    
    
    
    



#############################################################################
######     ab hier Zahlen
#############################################################################



    


def drawKurrentGatE_helper(x, y):
    
    KurrentGatE_helper = BezierPath()     
 
    x -= 2 
    save()
    stroke(1,0,1)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-0.5)
    drawGrundelOrient(A1, A2, offset, x+3, y-0.5)
    # drawGrundelOrient(A1, A2, offset, x, y+1.5)
    # drawGrundelOrient(A1, A2, offset, x, y+2.5)
    # drawGrundelOrient(A1, A2, offset, x, y+3.5)
    #drawGrundelOrient(A1, A2, offset, x, y+4.5)
    
    Grund_a_top, Grund_b_top, Grund_c, Grund_d_top = drawGrundelOrient(A1, A2, offset, x+3, y+4.5)
    drawGrundelementG(*Grund_b_top, 6, "down")

    Grund_a_2, Grund_b_2, Grund_c, Grund_d_2 = drawGrundelOrient(A1, A2, offset, x+6, y+4.5)

    drawGrundelementG(*Grund_a_2, 6, "down")
    drawGrundelementG(*Grund_b_2, 6, "down")

    line(Grund_a_top, Grund_d)

    restore()
    #KurrentGatE_helper = Bogen_links + Bogen_rechts
    trans_scale(KurrentGatE_helper, valueToMoveGlyph)
    return KurrentGatE_helper
    
    
    
    

def drawKurrentGatE_zero(x, y):
    
    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y)

    KurrentGatE_zero = Bogen_links + Bogen_rechts
    trans_scale(KurrentGatE_zero, valueToMoveGlyph)
    return KurrentGatE_zero
    
    
  
 
def drawKurrentGatE_one(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.5)
    
    instroke = drawInstroke(*Grund_a, 1.5)
    stem = drawGrundelementG(*Grund_a, 6, "down")
    Signatur_oben = drawSchriftteil10(*Grund_a)
    
    KurrentGatE_one = instroke + stem + Signatur_oben
    trans_scale(KurrentGatE_one, valueToMoveGlyph)
    return KurrentGatE_one
    



def drawKurrentGatE_two(x, y):
     
    Bogen = drawSchriftzug_two_Top(x, y)
    Stem = drawSchriftzug_two_Stem(x, y)
    Schwung_unten = drawSchriftzug_two_Schwung (x, y)
    
    KurrentGatE_two = Bogen + Stem + Schwung_unten
    trans_scale(KurrentGatE_two, valueToMoveGlyph)
    return KurrentGatE_two  
    
    
    
    
    


def drawKurrentGatE_three(x, y):
    x += 3
    KurrentGatE_three = BezierPath()     
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1, y+4)
   
    Three_Top = drawSchriftzug_three_Top(x, y)
    connection = drawGrundelementG(*Grund_b, 2.25, "down")
    Three_Schwung = drawSchriftzug_three_Bogen(x-2.15, y)
    pkt_Auslauf = Three_Schwung.points[-3]
    #text("pkt_Auslauf", pkt_Auslauf) 
    
    drawPath(KurrentGatE_three)

    KurrentGatE_three = Three_Top + connection + Three_Schwung
    trans_scale(KurrentGatE_three, valueToMoveGlyph)
    return KurrentGatE_three, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
def drawKurrentGatE_three_thinStroke(x, y, *, pass_from_thick=None):

    KurrentGatE_three_thinStroke = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, pass_from_thick.pkt_Auslauf[0]/modul_width, pass_from_thick.pkt_Auslauf[1]/modul_height)  
    Auslauf = drawSchneckenzug(*Grund_c, LOWER_E, 9, HSL_size=0.6, HSL_start=11, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    
    KurrentGatE_three_thinStroke += Auslauf + Endpunkt
    drawPath(KurrentGatE_three_thinStroke)
    trans_thinStroke_down_left(KurrentGatE_three_thinStroke) 
    trans_scale(KurrentGatE_three_thinStroke, valueToMoveGlyph)
    return KurrentGatE_three_thinStroke
    
    
    
    
    
    
def drawKurrentGatE_four(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.5)
    
    stem = drawSchriftzug_four(x, y, instrokeLen=0)
    stroke_left = drawSchriftzug_four_left(x, y)

    KurrentGatE_four = stem + stroke_left
    trans_scale(KurrentGatE_four, valueToMoveGlyph)
    return KurrentGatE_four   
    
    
    
    
def drawKurrentGatE_five(x, y):
    #x += 3
    KurrentGatE_five = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+4.5)
    Five_Top = drawSchriftzug_five_Top(*Grund_a)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.5, y+4.25)
    instroke = drawInstroke(*Grund_a, 0.5, "down")
    Connection = drawGrundelementG(*Grund_a, 2, "down")
    
    Three_Schwung = drawSchriftzug_three_Bogen(x-2.5, y)
    pkt_Auslauf = Three_Schwung.points[-3]
    #text("pkt_Auslauf", pkt_Auslauf) 
    
    KurrentGatE_five = Five_Top + instroke + Connection + Three_Schwung
    drawPath(KurrentGatE_five)
    trans_scale(KurrentGatE_five, valueToMoveGlyph)
    return KurrentGatE_five, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    


def drawKurrentGatE_six(x, y):
    #x+=2
    ### das waren vorher zwei Bogen, jetzt nur noch einer
    #Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    Bogen_rechts = drawSchriftzug_six_full(x, y)
    pkt_Auslauf_inner = Bogen_rechts.points[-3]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner)
    pkt_Auslauf_top = Bogen_rechts.points[0]
    #text("pkt_Auslauf_top", pkt_Auslauf_top)
    KurrentGatE_six = Bogen_rechts
    trans_scale(KurrentGatE_six, valueToMoveGlyph)
    return KurrentGatE_six, collections.namedtuple('dummy', 'pkt_Auslauf_top pkt_Auslauf_inner')(pkt_Auslauf_top, pkt_Auslauf_inner)
    

def drawKurrentGatE_six_thinStroke(x, y, *, pass_from_thick=None):

    KurrentGatE_six_thinStroke = BezierPath() 
    
    Auslauf_top = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_top, UPPER_E, 2, HSL_size=4, HSL_start=12, clockwise=True, inward=False)    
    Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, UPPER_E, 2, HSL_size=2, HSL_start=25, clockwise=False, inward=False)

    trans_thinStroke_down_left(Auslauf_inner) 
    trans_thinStroke_up_right(Auslauf_top)
            
    KurrentGatE_six_thinStroke += Auslauf_top + Auslauf_inner 
    drawPath(KurrentGatE_six_thinStroke)
    trans_scale(KurrentGatE_six_thinStroke, valueToMoveGlyph)
    return KurrentGatE_six_thinStroke 
    
    
    
    
    
    
    
def drawKurrentGatE_seven(x, y):
    #x +=1
    KurrentGatE_seven = BezierPath()     
     
    Seven_Top = drawSchriftzug_three_Top(x, y)
    Seven_Stem = drawSchriftzug_seven_Stem(x , y)
    
    drawPath(KurrentGatE_seven)
    KurrentGatE_seven = Seven_Top + Seven_Stem
    trans_scale(KurrentGatE_seven, valueToMoveGlyph)
    return KurrentGatE_seven        
            
            


def drawKurrentGatE_eight(x, y):

    KurrentGatE_eight = drawSchriftzug_eight(x, y)
    trans_scale(KurrentGatE_eight, valueToMoveGlyph)
    return KurrentGatE_eight
    
    



def drawKurrentGatE_nine(x, y):
    x += 2
    #Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    
    KurrentGatE_nine = drawSchriftzug_nine_full(x, y)
    pkt_Auslauf_btm = KurrentGatE_nine.points[6]
    #text("pkt_Auslauf_btm", pkt_Auslauf_btm) 
    pkt_Auslauf_inner = KurrentGatE_nine.points[-3]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner) 
    
    trans_scale(KurrentGatE_nine, valueToMoveGlyph)
    return KurrentGatE_nine, collections.namedtuple('dummy', 'pkt_Auslauf_btm pkt_Auslauf_inner')(pkt_Auslauf_btm, pkt_Auslauf_inner)
    


def drawKurrentGatE_nine_thinStroke(x, y, *, pass_from_thick=None):

    KurrentGatE_nine_thinStroke = BezierPath() 
    
    Auslauf_btm = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_btm, LOWER_E, 2, HSL_size=4, HSL_start=22, clockwise=True, inward=True)
    Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, LOWER_E, 2, HSL_size=2, HSL_start=12, clockwise=False, inward=False)

    trans_thinStroke_down_left(Auslauf_btm) 
    trans_thinStroke_up_right(Auslauf_inner)
            
    KurrentGatE_nine_thinStroke += Auslauf_btm + Auslauf_inner 
    drawPath(KurrentGatE_nine_thinStroke)
    trans_scale(KurrentGatE_nine_thinStroke, valueToMoveGlyph)
    return KurrentGatE_nine_thinStroke
    
    






#############################################################################
######     ab hier UC
#############################################################################



def drawKurrentGatE_helperUC(x, y):
    
    KurrentGatE_helperUC = BezierPath()

    drawGrundelOrientMittig(A1, A2, offset, x, y-0.5)

    helper = (-0.5, 0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        drawGrundelOrientMittig(A1, A2, offset, x+3, y+h)
        
    drawGrundelOrientMittig(A1, A2, offset, x+6, y-0.5)
    
    helper = (-0.5, 0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        drawGrundelOrientMittig(A1, A2, offset, x+9, y+h)
        
    drawGrundelOrientMittig(A1, A2, offset, x+12, y-0.5)

    helper = (-0.5, 0.5, 1.5, 2.5, 3.5, 4.5)
    for h in helper:
        drawGrundelOrientMittig(A1, A2, offset, x+15, y+h)
        
        
    #KurrentGatE_helperUC = Bogen_rechts
    #trans_scale(KurrentGatE_helperUC)
    return KurrentGatE_helperUC



def drawKurrentGatE_W(x, y):
    
    KurrentGatE_W = BezierPath()
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y+5)

    Bogen_links = drawSchneckenzug(*Grund_a, UPPER_E, 8, HSL_size=2, HSL_start=18, clockwise=True, inward=False)

    KurrentGatE_W += Bogen_links
    trans_scale(KurrentGatE_W, valueToMoveGlyph)
    return KurrentGatE_W
    
    




 
 
def drawKurrentGatE_etc(x, y):
    
    KurrentGatE_etc = drawSchriftzug_etc(x, y)
    trans_scale(KurrentGatE_etc, valueToMoveGlyph)
    return KurrentGatE_etc
    
    
 
 
    
    
def drawKurrentGatE_empty(x, y, *, pass_from_thick=None):     
    
    KurrentGatE_empty = BezierPath()
    KurrentGatE_empty.line((-100, -100), (-100, -110))
    return KurrentGatE_empty
    
    


    


    
# ________________________________________________________

marginStr = 30
marginRnd = 60    ### rund links
marginRnd2 = 60    ### rund rechts ### für p, v
marginCvr = -117    ## für b
marginPrblm = -54    ### für e, r, x weil Verbindung nicht passt
marginINT = 90    


# ____________ ab hier in RF ____________________________
    
    
font = CurrentFont()

drawFunctions = {
    #  'a' : [ drawKurrentGatE_a, [temp_x, temp_y], marginRnd, marginStr ],
    # 'b' : [ drawKurrentGatE_b, [temp_x, temp_y], marginStr, -57],
    # 'c' : [ drawKurrentGatE_c, [temp_x, temp_y], marginRnd, marginStr-55 ],
    # 'd' : [ drawKurrentGatE_d, [temp_x, temp_y], marginRnd, marginStr -190],

    # 'e' : [ drawKurrentGatE_e, [temp_x, temp_y], marginStr, marginPrblm ],
    # 'e.mid' : [ drawKurrentGatE_e, [temp_x, temp_y, "mid"], marginStr, marginPrblm ],
    # 'e.end' : [ drawKurrentGatE_e, [temp_x, temp_y, "end"], marginStr, marginPrblm ],

    # 'f' : [ drawKurrentGatE_f, [temp_x, temp_y], marginStr-300, marginStr-359 ],

    # 'g' : [ drawKurrentGatE_g, [temp_x, temp_y], marginRnd-140, marginStr ],
    # 'g.mid' : [ drawKurrentGatE_g, [temp_x, temp_y, "mid"], marginRnd-140, marginStr ],
    # 'g.end' : [ drawKurrentGatE_g, [temp_x, temp_y, "end"], marginRnd-101, marginStr ],

    # 'h' : [ drawKurrentGatE_h, [temp_x, temp_y], marginStr-91, marginStr-87],
    # 'h.mid' : [ drawKurrentGatE_h, [temp_x, temp_y, "mid"], marginStr-91, marginStr-87 ],
    # 'h.end' : [ drawKurrentGatE_h, [temp_x, temp_y, "end"], marginStr, marginStr-55 ],
   
    # 'i' : [ drawKurrentGatE_i, [temp_x, temp_y], marginStr, marginStr-115 ],

    # 'j' : [ drawKurrentGatE_j, [temp_x, temp_y], marginStr-267, marginStr-115 ],
    # 'j.mid' : [ drawKurrentGatE_j, [temp_x, temp_y, "mid"], marginStr-267, marginStr-115 ],
    # 'j.end' : [ drawKurrentGatE_j, [temp_x, temp_y, "end"], marginStr-229, marginStr-115 ],

    # 'k' : [ drawKurrentGatE_k, [temp_x, temp_y], marginStr, marginStr-244 ],
    # 'k.start' : [ drawKurrentGatE_k, [temp_x, temp_y, "start"], marginStr, marginStr-334 ],
    # 'k.mid' : [ drawKurrentGatE_k, [temp_x, temp_y, "mid"], marginStr, marginStr-241],

    # 'l' : [ drawKurrentGatE_l, [temp_x, temp_y], marginStr, marginStr-267 ],
    # 'm' : [ drawKurrentGatE_m, [temp_x, temp_y], marginStr, marginStr ],
    # 'n' : [ drawKurrentGatE_n, [temp_x, temp_y], marginStr, marginStr ],
    # 'o' : [ drawKurrentGatE_o, [temp_x, temp_y], marginRnd, marginCvr+80 ],
    # 'p' : [ drawKurrentGatE_p, [temp_x, temp_y], marginStr-300, marginRnd2 ],
    # 'q' : [ drawKurrentGatE_q, [temp_x, temp_y], marginRnd-156, marginStr ],
    # 'r' : [ drawKurrentGatE_r, [temp_x, temp_y], marginStr, marginPrblm ],
    # 's' : [ drawKurrentGatE_s, [temp_x, temp_y], marginStr, marginRnd2-274 ],
    # 'longs' : [ drawKurrentGatE_longs, [temp_x, temp_y], marginStr-300, marginStr-460 ],
    # 'germandbls' : [ drawKurrentGatE_germandbls, [temp_x, temp_y], marginStr-300, marginStr-160],
    # 't' : [ drawKurrentGatE_t, [temp_x, temp_y], marginStr, marginStr-120 ],
    # 'u' : [ drawKurrentGatE_u, [temp_x, temp_y], marginStr, marginStr-145 ],
    # 'v' : [ drawKurrentGatE_v, [temp_x, temp_y], marginStr, marginRnd2 ],
    # 'w' : [ drawKurrentGatE_w, [temp_x, temp_y], marginStr, marginRnd2 ],
    # 'x' : [ drawKurrentGatE_x, [temp_x, temp_y], marginStr-103, marginPrblm ],

    # 'y' : [ drawKurrentGatE_y, [temp_x, temp_y], marginStr-91, marginStr ],
    # 'y.mid' : [ drawKurrentGatE_y, [temp_x, temp_y, "mid"], marginStr-167+75, marginStr ],
    # 'y.end' : [ drawKurrentGatE_y, [temp_x, temp_y, "end"], marginStr, marginRnd ],

    # 'z' : [ drawKurrentGatE_z, [temp_x, temp_y, "isol"], marginStr-230, marginStr ],
    # 'z.start' : [ drawKurrentGatE_z, [temp_x, temp_y, "start"], marginStr-308, marginStr-97+18 ],
    # 'z.mid' : [ drawKurrentGatE_z, [temp_x, temp_y, "mid"], marginStr-218, marginStr ],
    # 'z.end' : [ drawKurrentGatE_z, [temp_x, temp_y, "end"], marginStr-140, marginStr],

    # 'adieresis' : [ drawKurrentGatE_adieresis, [temp_x, temp_y], marginRnd, marginStr-102 ],
    # 'odieresis' : [ drawKurrentGatE_odieresis, [temp_x, temp_y], marginRnd, marginStr-135 ],
    # 'udieresis' : [ drawKurrentGatE_udieresis, [temp_x, temp_y], marginStr, marginStr-102 ],

  
    
    
    # ### Ligatures
    
    # 'e_t_c' : [ drawKurrentGatE_etc, [temp_x, temp_y], marginRnd, marginRnd ],
    # 'longs_longs' : [ drawKurrentGatE_longs_longs, [temp_x, temp_y], marginStr-300, marginStr-460 ],
    # 'f_f' : [ drawKurrentGatE_f_f, [temp_x, temp_y], marginStr-300, marginStr-364 ],
    # 'longs_t' : [ drawKurrentGatE_longs_t, [temp_x, temp_y], marginStr-300, marginStr-240 ],
    # 'f_t' : [ drawKurrentGatE_f_t, [temp_x, temp_y], marginStr-300, marginStr-120 ],
    # 'longs_longs_t' : [ drawKurrentGatE_longs_longs_t, [temp_x, temp_y], marginStr-300, marginStr-240 ],
    # 'f_f_t' : [ drawKurrentGatE_f_f_t, [temp_x, temp_y], marginStr-300, marginStr-240 ],

    # 'c_h' : [ drawKurrentGatE_c_h, [temp_x, temp_y], marginStr, marginStr-87],
    # 'c_h.mid' : [ drawKurrentGatE_c_h, [temp_x, temp_y, "mid"], marginStr, marginStr-87 ],
    # 'c_h.end' : [ drawKurrentGatE_c_h, [temp_x, temp_y, "end"], marginStr, marginStr-55 ],

    
    # #### Interpunction
    
    # 'period' : [ drawKurrentGatE_period, [temp_x, temp_y], marginINT, marginINT ],
    # 'colon' : [ drawKurrentGatE_colon, [temp_x, temp_y], marginINT, marginINT ],
    # 'semicolon' : [ drawKurrentGatE_semicolon, [temp_x, temp_y], marginINT, marginINT ],
    # 'quoteright' : [ drawKurrentGatE_quoteright, [temp_x, temp_y], marginINT, marginINT ],
    # 'comma' : [ drawKurrentGatE_comma, [temp_x, temp_y], marginINT, marginINT ],
    # 'endash' : [ drawKurrentGatE_endash, [temp_x, temp_y], marginINT, marginINT ],
    # 'hyphen' : [ drawKurrentGatE_hyphen, [temp_x, temp_y], marginINT, marginINT ],
    # 'exclam' : [ drawKurrentGatE_exclam, [temp_x, temp_y], marginINT+40, marginINT ],
    # 'question' : [ drawKurrentGatE_question, [temp_x, temp_y], marginINT+100, marginINT ],



    # #### Numbers
    
    # 'zero' : [ drawKurrentGatE_zero, [temp_x, temp_y], marginRnd, marginRnd-150 ],
    # 'one' : [ drawKurrentGatE_one, [temp_x, temp_y], marginStr, marginStr-210 ],
    # 'two' : [ drawKurrentGatE_two, [temp_x, temp_y], marginStr, marginRnd-120 ],
    # 'three' : [ drawKurrentGatE_three, [temp_x, temp_y], marginStr+150, marginStr-90 ],
    # 'four' : [ drawKurrentGatE_four, [temp_x, temp_y], marginStr+30, marginStr-120 ],
    # 'five' : [ drawKurrentGatE_five, [temp_x, temp_y], marginStr+150, marginStr-150 ],
    # 'six' : [ drawKurrentGatE_six, [temp_x, temp_y], marginRnd, marginStr-60 ],
    # 'seven' : [ drawKurrentGatE_seven, [temp_x, temp_y], marginStr, marginStr-150 ],
    # 'eight' : [ drawKurrentGatE_eight, [temp_x, temp_y], marginRnd, marginRnd-150 ],    
    # 'nine' : [ drawKurrentGatE_nine, [temp_x, temp_y], marginStr+90, marginRnd-150 ],    

    # 'zero.osf' : [ drawKurrentGatE_zero, [temp_x, temp_y], marginRnd-15, marginRnd-30 ],
    # 'one.osf' : [ drawKurrentGatE_one, [temp_x, temp_y], marginStr, marginStr-60 ],
    # 'two.osf' : [ drawKurrentGatE_two, [temp_x, temp_y], marginStr, marginRnd-15 ],
    # 'three.osf' : [ drawKurrentGatE_three, [temp_x, temp_y], marginStr+90, marginStr+30 ],
    # 'four.osf' : [ drawKurrentGatE_four, [temp_x, temp_y], marginStr-30, marginStr ],
    # 'five.osf' : [ drawKurrentGatE_five, [temp_x, temp_y], marginStr+90, marginStr ],
    # 'six.osf' : [ drawKurrentGatE_six, [temp_x, temp_y], marginRnd, marginStr+60 ],
    # 'seven.osf' : [ drawKurrentGatE_seven, [temp_x, temp_y], marginStr+60, marginStr-30 ],
    # 'eight.osf' : [ drawKurrentGatE_eight, [temp_x, temp_y], marginRnd-15, marginRnd-18 ],    
    # 'nine.osf' : [ drawKurrentGatE_nine, [temp_x, temp_y], marginStr+30, marginRnd-30 ],   

    }
    
    

drawFunctions_thinStroke = {

     'g.end' : [ drawKurrentGatE_g_thinStroke, [temp_x, temp_y] ],
     'h.end' : [ drawKurrentGatE_g_thinStroke, [temp_x, temp_y] ],
     'j.end' : [ drawKurrentGatE_g_thinStroke, [temp_x, temp_y] ],
     'y.end' : [ drawKurrentGatE_g_thinStroke, [temp_x, temp_y] ],
     'z.end' : [ drawKurrentGatE_g_thinStroke, [temp_x, temp_y] ],

     'germandbls' : [ drawKurrentGatE_germandbls_thinStroke, [temp_x, temp_y] ],

     'c_h.end' : [ drawKurrentGatE_g_thinStroke, [temp_x, temp_y] ],  

      'three' : [ drawKurrentGatE_three_thinStroke, [temp_x, temp_y] ],
      'five' : [ drawKurrentGatE_three_thinStroke, [temp_x, temp_y] ],
      'six' : [ drawKurrentGatE_six_thinStroke, [temp_x, temp_y] ],
      'nine' : [ drawKurrentGatE_nine_thinStroke, [temp_x, temp_y] ],
  
      'three.osf' : [ drawKurrentGatE_three_thinStroke, [temp_x, temp_y] ],
      'five.osf' : [ drawKurrentGatE_three_thinStroke, [temp_x, temp_y] ],
      'six.osf' : [ drawKurrentGatE_six_thinStroke, [temp_x, temp_y] ],
      'nine.osf' : [ drawKurrentGatE_nine_thinStroke, [temp_x, temp_y] ],

    }
    

     
     
     
     
    
for key in drawFunctions:
    
    glyph = font[[key][0]]
    glyph.clear()
    
    foreground = glyph.getLayer("foreground")
    foreground.clear()
    background = glyph.getLayer("background")
    background.clear()
     
    function = drawFunctions[key][0]
    arguments = drawFunctions[key][1]

    output = function(*arguments)
	
    if key in drawFunctions_thinStroke:
        
        assert isinstance(output, tuple) and len(output)==2
        pass_from_thick_to_thin = output[1]
        output = output[0]
    
    # assert isinstance(output, glyphContext.GlyphBezierPath)
		
    writePathInGlyph(output, foreground)

    print("___",font[[key][0]])
    
    # ask for current status
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin   
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_before = margin_ts - margin_fg
    
    ### change value of margin
    glyph.leftMargin = drawFunctions[key][2]
    glyph.rightMargin = drawFunctions[key][3]
    
    ### ask inbetween 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_mid = margin_ts - margin_fg

    ### calculate value for movement of bg
    move_thinStroke = difference_before - difference_mid

    glyph.copyLayerToLayer('foreground', 'background')
 
   
    if key in drawFunctions_thinStroke:		
        thinstroke = glyph.getLayer("thinstroke")
        thinstroke.clear()

        thinfunction = drawFunctions_thinStroke[key][0]
        thinarguments = drawFunctions_thinStroke[key][1]
        thinoutput = thinfunction(*thinarguments, pass_from_thick=pass_from_thick_to_thin)
        
        writePathInGlyph(thinoutput, thinstroke)
        
    thinstroke = glyph.getLayer("thinstroke")
    thinstroke.translate((move_thinStroke, 0))
    
    ### ask final 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0 
        #print("(no thin line)")   
    difference_final = margin_ts - margin_fg
    
    foreground.clear()

    
    
    
    ### now draw the whole glphy with the magic tool
    ### first thick main stroke 
    guide_glyph = glyph.getLayer("background")

    w = A_A * penWidth 
    a = radians(90-alpha)
    h = penThickness

    if True:
        RectNibPen = RectNibPen_Just

    p = RectNibPen(
        glyphSet=CurrentFont(),
        angle=a,
        width=w,
        height=penThickness,
        trace=True
    )

    guide_glyph.draw(p)
    p.trace_path(glyph)


    ### now thin Stroke
    if key in drawFunctions_thinStroke:		

        guide_glyph = glyph.getLayer("thinstroke")
        w = penThickness 
        p = RectNibPen(
            glyphSet=CurrentFont(),
            angle=a,
            width=w,
            height=penThickness,
            trace=True
        )
        guide_glyph.draw(p)
        p.trace_path(glyph)

    ### keep a copy in background layer
    newLayerName = "original_math"
    glyph.layers[0].copyToLayer(newLayerName, clear=True)
    glyph.getLayer(newLayerName).leftMargin = glyph.layers[0].leftMargin
    glyph.getLayer(newLayerName).rightMargin = glyph.layers[0].rightMargin

    ### this will make it all one shape
    #glyph.removeOverlap(round=0)
    
    ### from robstevenson to remove extra points on curves
    ### function itself is in create_stuff.py
    #select_extra_points(glyph, remove=True)    



    
    
    
    
    
