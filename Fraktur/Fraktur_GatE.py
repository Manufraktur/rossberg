import importlib
import version_3.creation.Schwuenge_GatE
import version_3.creation.Schriftteile_GatE
import Fraktur_Schriftzuege_GatE

importlib.reload(version_3.creation.Schwuenge_GatE)
importlib.reload(version_3.creation.Schriftteile_GatE)
importlib.reload(Fraktur_Schriftzuege_GatE)

from version_3.creation.Schwuenge_GatE import *
from version_3.creation.Schriftteile_GatE import *
from Fraktur_Schriftzuege_GatE import *
from version_3.creation.special_drawbot import BezierPath, drawPath


# ___________ Bedienelement Slider _______________

Variable([
    dict(name="version", ui="Slider",
        args=dict(
            value=0,
            minValue=0,
            maxValue=1)),
    ], globals())
# ________________________________________________

### True >>> modified code         >>> 1
### False >>> original Roßberg     >>> 0
version_mod = version  # Eingabe durch Slider


if version_mod == True:
    print("Roßberg modifizert")
    import version_3.creation.Schriftteile_GatE_mod
    import Fraktur_Schriftzuege_GatE_mod
    importlib.reload(version_3.creation.Schriftteile_GatE_mod)
    importlib.reload(Fraktur_Schriftzuege_GatE_mod)
    from version_3.creation.Schriftteile_GatE_mod import *
    from Fraktur_Schriftzuege_GatE_mod import *
else:
    print("Roßberg original")
    

from nibLib.pens.rectNibPen import RectNibPen
from version_3.creation.rect_nib_pen import RectNibPen as RectNibPen_Just
from math import radians
import math as m
import collections
import glyphContext




# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 20
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)








# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)







# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)



stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 9


# ______________________________________________________







def drawFrakturGatE_a(x, y):      
    
    Bogen_links = drawSchriftzug7(x, y, instrokeLen=2.6, outstrokeLen=2.8)
    Strich_rechts = drawSchriftzug4(x+3, y, instrokeLen=0, outstrokeLen=0.8)
    
    FrakturGatE_a = Bogen_links + Strich_rechts
    trans_scale(FrakturGatE_a, valueToMoveGlyph)
    return FrakturGatE_a
        
 
 
    
def drawFrakturGatE_b(x,y):        
    
    Strich_links = drawSchriftzug5(x, y, top="ascender", outstrokeLen=0)
    Bogen_rechts = drawSchriftzug8(x+3, y, instrokeLen=2.8)
    
    FrakturGatE_b = Strich_links + Bogen_rechts
    trans_scale(FrakturGatE_b, valueToMoveGlyph)
    return FrakturGatE_b
  
    
def drawFrakturGatE_b_dot(x, y):      
    #y += -3
    glyph_b = drawFrakturGatE_b(x, y)
    trans_scale_invert(glyph_b, valueToMoveGlyph)

    pkt_Auslauf = glyph_b.points[5]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatE_b_dot = glyph_b
    trans_scale(FrakturGatE_b_dot, valueToMoveGlyph)
    return FrakturGatE_b_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    


def drawFrakturGatE_c(x,y, outstrokeLen=0.5):
           
    Bogen_links = drawSchriftzug6(x, y, instrokeLen=1.6, outstrokeLen=1)
    
    ### Raute zur Orientierung, um Häckchen zu kontrollieren (change instrokeLen of Bogen_links)
    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y)
    Haeckchen_oben = drawSchriftteil2(*Bogen_links.points[3])
    outstroke = drawOutstroke(*Haeckchen_oben.points[-1], outstrokeLen)
    
    FrakturGatE_c = Bogen_links + Haeckchen_oben + outstroke
    trans_scale(FrakturGatE_c, valueToMoveGlyph)
    return FrakturGatE_c
    
    
    

def drawFrakturGatE_d(x, y, Endspitze=11):      

    Bogen_links = drawSchriftzug6(x, y, instrokeLen=2.5)
    Bogen_rechts = drawSchwung6(x, y, Endspitze)
    
    FrakturGatE_d = Bogen_links + Bogen_rechts
    trans_scale(FrakturGatE_d, valueToMoveGlyph)
    return FrakturGatE_d
    


    


def drawFrakturGatE_e(x, y):
    
    FrakturGatE_e = BezierPath()      
    
    Bogen_links = drawSchriftzug6(x, y, instrokeLen=1.6, outstrokeLen=1.5)
    
    ### Raute zur Orientierung, um Position vom Häckchen kontrollieren (change instrokeLen of Bogen_links)
    drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y)

    Haken_rechts = drawSchriftteil8(*Bogen_links.points[3])
    outstroke_oben = drawOutstroke(*Haken_rechts.points[3], 1.9, "down")
    
    FrakturGatE_e = Bogen_links + Haken_rechts + outstroke_oben
    trans_scale(FrakturGatE_e, valueToMoveGlyph)
    return FrakturGatE_e
    
    


    
def drawFrakturGatE_f_tryBetter(x,y):
    
    ###   war ursprünglich mit langem s plus Querstrich gebaut aber nach Marcus Hilfe
    ###   mit verbundener Formal für langen Strich funktionierte es nicht mehr
    ###   daher von langem S code einfach hierher kopiert
    
    main_stroke_down = drawSchriftzug2(x, y, instrokeLen=1.9)
    pkt_Ausstrich = main_stroke_down.points[9]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y+3)
    Spitze = drawSchriftteil11(*Raute_a)

    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)

    FrakturGatE_f = main_stroke_down + Spitze + Querstrich
    trans_scale(FrakturGatE_f, valueToMoveGlyph)
    return FrakturGatE_f, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    


    
def drawFrakturGatE_f(x,y):

    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)
    trans_scale(Querstrich, valueToMoveGlyph)
    
    FrakturGatE_longs = drawFrakturGatE_longs(x, y)[0]
    values_to_return = drawFrakturGatE_longs(x, y)[1]
    FrakturGatE_f = FrakturGatE_longs + Querstrich
    return FrakturGatE_f, values_to_return






    
def drawFrakturGatE_f_f(x, y):
     
    Querstrich_left = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    Querstrich_right = drawGrundelementB((x+1.5)*modul_width, (y+0.5)*modul_height, length=2)
    Querstrich = Querstrich_left + Querstrich_right
    trans_scale(Querstrich, valueToMoveGlyph)
    
    ligature_longs_longs = drawFrakturGatE_longs_longs(x, y)[0]
    values_to_return = drawFrakturGatE_longs_longs(x, y)[1]
    
    FrakturGatE_f_f = ligature_longs_longs + Querstrich
    return FrakturGatE_f_f, values_to_return
    

        

def drawFrakturGatE_f_t(x, y):
        
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    trans_scale(Querstrich, valueToMoveGlyph)
        
    glyph_longs_t = drawFrakturGatE_longs_t(x, y)[0]
    values_to_return = drawFrakturGatE_longs_t(x, y)[1]
    
    FrakturGatE_f_t = glyph_longs_t + Querstrich
    return FrakturGatE_f_t, values_to_return
    
    
    
    
def drawFrakturGatE_f_f_t(x, y):
    
    Querstrich_links = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    Querstrich_rechts = drawGrundelementB((x+1.5)*modul_width, (y+0.5)*modul_height, length=2)

    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=2.5, ligatureLen=2.5)
    pkt_Ausstrich = main_stroke_left.points[9]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    pkt_Auslauf = main_stroke_left.points[3]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    con = drawSchriftzug_f_f_con(x, y+0.25)
    main_stroke_middle = drawSchriftzug2(x+3, y, instrokeLen=2.1, ligatureLen=2.5)
    Spitze = drawSchriftzug_f_t_con(x, y)
    
    FrakturGatE_f_f_t = main_stroke_left + con + main_stroke_middle + Spitze + Querstrich_links + Querstrich_rechts
    trans_scale(FrakturGatE_f_f_t, valueToMoveGlyph)
        
    glyph_t = drawFrakturGatE_t(x+6, y)
    FrakturGatE_f_f_t += glyph_t
    return FrakturGatE_f_f_t, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
    
    
    



    
def drawFrakturGatE_g(x, y):
    
    Bogen_links = drawSchriftzug7(x, y, instrokeLen=2.6, outstrokeLen=2.8)
    Strich_rechts = drawSchriftzug4(x+3, y, bottom="Schwung", instrokeLen=0)
    pkt_Auslauf = Strich_rechts.points[7]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatE_g = Bogen_links + Strich_rechts
    trans_scale(FrakturGatE_g, valueToMoveGlyph)
    return FrakturGatE_g, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
def drawFrakturGatE_g_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatE_g_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=13.5, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.1)
         
    FrakturGatE_g_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatE_g_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_g_thinStroke)
    trans_scale(FrakturGatE_g_thinStroke, valueToMoveGlyph)
    return FrakturGatE_g_thinStroke
    
    
    
    
    
    
     
           


def drawFrakturGatE_h(x, y):      
    
    Strich_links = drawSchriftzug5(x, y, top="ascender", outstrokeLen=0)
    Bogen_rechts = drawSchriftzug9(x+3, y, instrokeLen=2.8)
    
    FrakturGatE_h = Strich_links + Bogen_rechts
    trans_scale(FrakturGatE_h, valueToMoveGlyph)
    return FrakturGatE_h
    
    
def drawFrakturGatE_h_dot(x, y):      
    #y += -3
    glyph_h = drawFrakturGatE_h(x, y)
    trans_scale_invert(glyph_h, valueToMoveGlyph)

    pkt_Auslauf_up = glyph_h.points[5]
    #text("pkt_Auslauf_up", pkt_Auslauf_up)
    pkt_Auslauf_down = glyph_h.points[-1]
    #text("pkt_Auslauf_down", pkt_Auslauf_down)
        
    FrakturGatE_h_dot = glyph_h
    trans_scale(FrakturGatE_h_dot, valueToMoveGlyph)
    return FrakturGatE_h_dot, collections.namedtuple('dummy', 'pkt_Auslauf_up pkt_Auslauf_down')(pkt_Auslauf_up, pkt_Auslauf_down)
    
    
    
    
        

def drawFrakturGatE_i(x, y):      

    Strich = drawSchriftzug4(x, y, instrokeLen=0.8, outstrokeLen=0.8)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2)
    iPunkt = drawSchriftteil2(*Raute_a)
    
    FrakturGatE_i = Strich + iPunkt
    trans_scale(FrakturGatE_i, valueToMoveGlyph)
    return FrakturGatE_i
        
    
        
        
        
    
def drawFrakturGatE_j(x, y):      
    x += 2
    Strich = drawSchriftzug4(x, y, bottom="Schwung", instrokeLen=0.8)
    pkt_Auslauf = Strich.points[7]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2)
    Punkt = drawSchriftteil2(*Raute_a)
        
    FrakturGatE_j = Strich + Punkt
    trans_scale(FrakturGatE_j, valueToMoveGlyph)
    return FrakturGatE_j, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    





def drawFrakturGatE_k(x, y):
    
    Strich_links = drawSchriftzug5(x, y, top="ascender", outstrokeLen=0.8)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.25)
    Schleife_instroke = drawInstroke(*Grund_a, 1.5, "down")
    Schleife = drawSchriftteil8(*Schleife_instroke.points[-1])
    Schleife_outstroke = drawOutstroke(*Schleife.points[3], 1.5, "down")

    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)
    
    FrakturGatE_k = Strich_links + Schleife_instroke + Schleife + Schleife_outstroke + Querstrich
    trans_scale(FrakturGatE_k, valueToMoveGlyph)
    return FrakturGatE_k
    
def drawFrakturGatE_k_dot(x, y):      
    #y += -3
    glyph_k = drawFrakturGatE_k(x, y)
    trans_scale_invert(glyph_k, valueToMoveGlyph)

    pkt_Auslauf = glyph_k.points[5]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatE_k_dot = glyph_k
    trans_scale(FrakturGatE_k_dot, valueToMoveGlyph)
    return FrakturGatE_k_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
        
        
def drawFrakturGatE_l(x, y):    
    
    Schriftzug5 = drawSchriftzug5(x, y, top="ascender", outstrokeLen=0.8)
    
    FrakturGatE_l = Schriftzug5
    trans_scale(FrakturGatE_l, valueToMoveGlyph)
    return FrakturGatE_l
    
    
def drawFrakturGatE_l_dot(x, y):      
    #y += -3
    glyph_l = drawFrakturGatE_l(x, y)
    trans_scale_invert(glyph_l, valueToMoveGlyph)

    pkt_Auslauf = glyph_l.points[5]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatE_l_dot = glyph_l
    trans_scale(FrakturGatE_l_dot, valueToMoveGlyph)
    return FrakturGatE_l_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
        
        
def drawFrakturGatE_m(x, y):
    
    Strich_links = drawSchriftzug5(x, y, instrokeLen=0.8, outstrokeLen=0)
    
    # Raute Abstand 2. Strich Mitte
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y)
        
    Strich_mitte = drawSchriftzug5(x+3, y, instrokeLen=2.5, outstrokeLen=0)
    
    # Raute Abstand 3. Strich rechts
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y)
        
    Strich_rechts = drawSchriftzug4(x+6, y, instrokeLen=2.5, outstrokeLen=0.8)
    
    FrakturGatE_m = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(FrakturGatE_m, valueToMoveGlyph) 
    return FrakturGatE_m
    







def drawFrakturGatE_n(x, y):
      
    Strich_links = drawSchriftzug5(x, y, instrokeLen=0.8, outstrokeLen=0)
    
    # Raute Abstand 2. Strich
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y)
        
    Strich_rechts = drawSchriftzug4(x+3, y, instrokeLen=2.8, outstrokeLen=0.8)
    
    FrakturGatE_n = Strich_links + Strich_rechts
    trans_scale(FrakturGatE_n, valueToMoveGlyph)   
    return FrakturGatE_n
    





def drawFrakturGatE_o(x, y):
        
    # Weil aber bei Zusammensetzung der beiden Schrift-Züge Fig. VI. und VIII. das daraus
    # entstehende o in der Gattung C durch ihre starken Krümmungen zu viel Zwischenraum erhalten, [...] 
    # so muß hier in C jedem dieser Schrift-Züge 1/4 Bestandtheil Gerades mehr eingesetzet, 
    # und dann nur 3. halbe Breiten oder 1 1/2 Hauptmaas Zwischenraum (nehmlich nach den  
    # Grundbestandtheilen dieser gebogenen Schrift-Züge gemessen,) dazu genommen werden.    
    
    Bogen_links = drawSchriftzug6(x, y, "for o", instrokeLen=2.6, outstrokeLen=1.4)
    Bogen_rechts = drawSchriftzug8(x+3, y, "for o", instrokeLen=0, outstrokeLen=0)
    
    FrakturGatE_o = Bogen_links + Bogen_rechts
    trans_scale(FrakturGatE_o, valueToMoveGlyph)
    return FrakturGatE_o
    
    
    
    
    

    

def drawFrakturGatE_p(x, y):
    
    glyph_v = drawFrakturGatE_v(x, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    pkt_Ausstrich = Raute_d
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    FrakturGatE_p = glyph_v
    return FrakturGatE_p, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    
    
    
    
def drawFrakturGatE_q(x, y):      
    
    Bogen_links = drawSchriftzug7(x, y, instrokeLen=2.6, outstrokeLen=2.8)
    Strich_rechts = drawSchriftzug4(x+3, y, bottom="Kehlung", instrokeLen=0)
    pkt_Ausstrich = Strich_rechts.points[13]
    #text("pkt_Ausstrich", pkt_Ausstrich)

    FrakturGatE_q = Bogen_links + Strich_rechts
    trans_scale(FrakturGatE_q, valueToMoveGlyph)
    return FrakturGatE_q, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    






def drawFrakturGatE_r(x, y):       

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y)

    Strich = drawSchriftzug5(x, y, instrokeLen=0.8, outstrokeLen=0)
    Verbindung = drawInstroke(*Raute_a, 1.75)
    Haken = drawSchriftteil2(*Raute_a)
    outstroke = drawOutstroke(*Haken.points[-1])
    
    FrakturGatE_r = Strich + Verbindung + Haken + outstroke
    trans_scale(FrakturGatE_r, valueToMoveGlyph) 
    return FrakturGatE_r
    
    
    
    
def drawFrakturGatE_rc(x, y):       
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.25, baseline-0.45)

    Strich = drawSchriftzug8(x, y, Einsatz="1", instrokeLen=0.8, outstrokeLen=0)
    Fuss = drawSchriftteil11(*Raute_a)
    
    FrakturGatE_rc = Strich + Fuss
    trans_scale(FrakturGatE_rc, valueToMoveGlyph) 
    
    letter_c = drawFrakturGatE_c(x+3, y, outstrokeLen=0)
    FrakturGatE_rc += letter_c
    return FrakturGatE_rc
    
    
        
    
    
    
def drawFrakturGatE_s(x, y):
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, y-3.25)
    Bogen_oben = drawHalbbogen8(*Raute_a, instrokeLen=1.57, outstrokeLen=2.5)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y)
    Deckung_oben = drawSchriftteil2(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y-2)
    Bogen_unten = drawHalbbogen4(*Raute_a, instrokeLen=0, outstrokeLen=1.44)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    Deckung_unten = drawSchriftteil1(*Raute_a)
    
    FrakturGatE_s = Bogen_oben + Bogen_unten + Deckung_oben + Deckung_unten
    trans_scale(FrakturGatE_s, valueToMoveGlyph) 
    return FrakturGatE_s
    
      
  
    

def drawFrakturGatE_longs(x, y):
        
    main_stroke_down = drawSchriftzug2(x, y, instrokeLen=1.9)
    pkt_Ausstrich = main_stroke_down.points[9]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y+3)
    Spitze = drawSchriftteil11(*Raute_a)

    FrakturGatE_longs = main_stroke_down + Spitze
    trans_scale(FrakturGatE_longs, valueToMoveGlyph)
    return FrakturGatE_longs, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    

    
    
    

def drawFrakturGatE_longs_longs(x, y):
    #y -=5
    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=3)
    pkt_Ausstrich = main_stroke_left.points[9]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    pkt_Auslauf = main_stroke_left.points[3]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    main_stroke_right = drawSchriftzug2(x+3, y, instrokeLen=2.4)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.5, y+3.25)
    Spitze = drawSchriftteil11(*Raute_a)
    
    con = drawSchriftzug_longs_longs_con(x, y)

    FrakturGatE_longs_longs = main_stroke_left + main_stroke_right + con + Spitze
    trans_scale(FrakturGatE_longs_longs, valueToMoveGlyph)
    return FrakturGatE_longs_longs, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
    
    
def drawFrakturGatE_longs_longs_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatE_longs_longs_thinStroke = BezierPath()   

    Auslauf_oben = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 4, HSL_size=1, HSL_start=10, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 5)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf_oben.points[-1][0]-modul_width*0.5, Auslauf_oben.points[-1][1]-modul_height*0.6)
    
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)

    trans_thinStroke_down_left(Zierstrich) 
    trans_thinStroke_up_right(Auslauf_oben)    
 
    FrakturGatE_longs_longs_thinStroke += Auslauf_oben + Zierstrich + Endpunkt    
    drawPath(FrakturGatE_longs_longs_thinStroke)
    trans_scale(FrakturGatE_longs_longs_thinStroke, valueToMoveGlyph)
    return FrakturGatE_longs_longs_thinStroke   
    
    
    
    

def drawFrakturGatE_longs_t(x, y):
        
    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=1.5, ligatureLen=2.5)
    pkt_Ausstrich = main_stroke_left.points[9]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y+2.2)
    Spitze = drawSchriftteil12(*Raute_a)
    
    FrakturGatE_longs_t = main_stroke_left + Spitze
    trans_scale(FrakturGatE_longs_t, valueToMoveGlyph)
        
    glyph_t = drawFrakturGatE_t(x+3, y)
    FrakturGatE_longs_t += glyph_t
    return FrakturGatE_longs_t, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    

    


def drawFrakturGatE_germandbls(x,y):

    FrakturGatE_germandbls = BezierPath()
    
    ### linker Teil
    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=1.75, ligatureLen=2.5)
    pkt_Ausstrich_gerade = main_stroke_left.points[-1]
    #text("pkt_Ausstrich_gerade", pkt_Ausstrich_gerade)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.25, y+2.3)
    Spitze = drawSchriftteil12(*Raute_a)
           
       
    ### rechter Teil
    Halbbogen_oben = drawSchriftzug_z_Initial(x+3, y-1, version="ohneEndspitze")
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y-2.5)
    Halbbogen_unten = drawSchneckenzug(*Raute_a, UPPER_A, 4, HSL_size=2, HSL_start=22, clockwise=True, inward=True)
    instroke = drawInstroke(*Halbbogen_unten.points[0], 0.5)
    outstroke = drawOutstroke(*Halbbogen_unten.points[-1], 0.125, "down")
    pkt_Ausstrich_gebogen = outstroke.points[0]
    #text("pkt_Ausstrich_gebogen", pkt_Ausstrich_gebogen)
    
    FrakturGatE_germandbls += main_stroke_left + Spitze + Halbbogen_oben + Halbbogen_unten + instroke + outstroke
    drawPath(FrakturGatE_germandbls)
    trans_scale(FrakturGatE_germandbls, valueToMoveGlyph)
    return FrakturGatE_germandbls, collections.namedtuple('dummy', 'pkt_Ausstrich_gerade pkt_Ausstrich_gebogen')(pkt_Ausstrich_gerade, pkt_Ausstrich_gebogen)
    
    

def drawFrakturGatE_germandbls_Endspitze(x, y, *, pass_from_thick=None):
    
    FrakturGatE_germandbls_Endspitze = BezierPath()   
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Ausstrich_gebogen, UPPER_E, 7, HSL_size=1, HSL_start=13, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 8)])
    #FrakturGatE_germandbls_Endspitze.oval(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1], part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1])
    
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich_gerade, 3)
    
    FrakturGatE_germandbls_Endspitze += Auslauf + Zierstrich + Endpunkt
    
    drawPath(FrakturGatE_germandbls_Endspitze)
    trans_thinStroke_down_left(FrakturGatE_germandbls_Endspitze)
    trans_scale(FrakturGatE_germandbls_Endspitze, valueToMoveGlyph)
    return FrakturGatE_germandbls_Endspitze   
    
    
        
    
    
def drawFrakturGatE_t(x, y):    
    
    Strich = drawSchriftzug3(x, y, top="ascender", ascenderLen=2, outstrokeLen=0.8)
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)
    
    FrakturGatE_t = Strich + Querstrich
    trans_scale(FrakturGatE_t, valueToMoveGlyph)
    return FrakturGatE_t
    
    
def drawFrakturGatE_t_dot(x, y):      
    #y += -3
    glyph_t = drawFrakturGatE_t(x, y)
    trans_scale_invert(glyph_t, valueToMoveGlyph)

    pkt_Auslauf = glyph_t.points[3]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatE_t_dot = glyph_t
    trans_scale(FrakturGatE_t_dot, valueToMoveGlyph)
    return FrakturGatE_t_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
        


def drawFrakturGatE_u(x, y):
    
    Strich_links = drawSchriftzug5(x, y, instrokeLen=0.8, outstrokeLen=2.5)
    Strich_rechts = drawSchriftzug4(x+3, y, instrokeLen=0, outstrokeLen=0.8)
    
    FrakturGatE_u = Strich_links + Strich_rechts
    trans_scale(FrakturGatE_u, valueToMoveGlyph)
    return FrakturGatE_u
    
    
    
    
    
    
def drawFrakturGatE_adieresis(x, y):
    
    Dieresis = drawDieresis(x+0.75, y)   ### Position mit Tab 24 abgeglichen
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_a = drawFrakturGatE_a(x, y)
        
    FrakturGatE_adieresis = Grundschriftzug_a + Dieresis
    return FrakturGatE_adieresis
    
    
def drawFrakturGatE_odieresis(x, y):
    
    Dieresis = drawDieresis(x+0.75, y)   ### Position mit Tab 24 abgeglichen
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_o = drawFrakturGatE_o(x, y)
        
    FrakturGatE_odieresis = Grundschriftzug_o + Dieresis
    return FrakturGatE_odieresis
    

def drawFrakturGatE_udieresis(x, y):
    
    Dieresis = drawDieresis(x+0.5, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_u = drawFrakturGatE_u(x, y)
        
    FrakturGatE_udieresis = Grundschriftzug_u + Dieresis
    return FrakturGatE_udieresis
    
    
    
    
    
    
    
def drawFrakturGatE_v(x, y):
    
    Strich_links = drawSchriftzug5(x, y, instrokeLen=0.8, outstrokeLen=0)
    Strich_rechts = drawSchriftzug8(x+3, y, instrokeLen=2.5)
    
    FrakturGatE_v = Strich_links + Strich_rechts
    trans_scale(FrakturGatE_v, valueToMoveGlyph)
    return FrakturGatE_v
    
    
    

    
def drawFrakturGatE_w(x, y):
    
    Strich_links = drawSchriftzug5(x, y, instrokeLen=0.8, outstrokeLen=0)
    Strich_mitte = drawSchriftzug5(x+3, y, instrokeLen=2.5)
    Strich_rechts = drawSchriftzug8(x+6, y, instrokeLen=2.5)

    FrakturGatE_w = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(FrakturGatE_w, valueToMoveGlyph)
    return FrakturGatE_w
    
    
    
    
    
def drawFrakturGatE_x(x, y):
    
    glyph_r = drawFrakturGatE_r(x, y)
    trans_scale_invert(glyph_r, valueToMoveGlyph)  ### erstmal rückwärts skalieren um den hook zu positionieren!
    
    pkt_Ausstrich = glyph_r.points[-12]
    #text("pkt_Ausstrich", pkt_Ausstrich)

    FrakturGatE_x = glyph_r 
    trans_scale(FrakturGatE_x, valueToMoveGlyph)
    return FrakturGatE_x, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
def drawFrakturGatE_x_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatE_x_thinStroke = BezierPath()

    hook = drawSchneckenzug(*pass_from_thick.pkt_Ausstrich, UPPER_E, 15, HSL_size=1, HSL_start=18, clockwise=False, inward=True)
    Endpunkt = drawThinstroke_Endpunkt(hook.points[-1][0]-modul_width*0.65, hook.points[-1][1]-modul_height*1.1)
        
    FrakturGatE_x_thinStroke += hook + Endpunkt
    drawPath(FrakturGatE_x_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_x_thinStroke)
    trans_scale(FrakturGatE_x_thinStroke, valueToMoveGlyph)
    return FrakturGatE_x_thinStroke
    
    



def drawFrakturGatE_y(x, y):
    
    Strich_links = drawSchriftzug5(x, y, instrokeLen=0.8, outstrokeLen=0)
    Strich_rechts = drawSchriftzug9(x+3, y, instrokeLen=2.75)
    
    FrakturGatE_y = Strich_links + Strich_rechts
    trans_scale(FrakturGatE_y, valueToMoveGlyph)
    return FrakturGatE_y
    
    
def drawFrakturGatE_y_dot(x, y):      
    
    glyph_y = drawFrakturGatE_y(x, y)
    trans_scale_invert(glyph_y, valueToMoveGlyph)

    pkt_Auslauf = glyph_y.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatE_y_dot = glyph_y
    trans_scale(FrakturGatE_y_dot, valueToMoveGlyph)
    return FrakturGatE_y_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    


def drawFrakturGatE_z(x,y, style="final"):
    x +=2
    FrakturGatE_z = BezierPath()

    if style == "initial":
        Halbbogen_oben = drawSchriftzug_z_Initial(x, y)
    
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-1.5)
        Halbbogen_unten = drawSchneckenzug(*Raute_a, UPPER_A, 4, HSL_size=2, HSL_start=17.6, clockwise=True, inward=False)
        pkt_Ausstrich = Halbbogen_oben.points[1]
        #text("pkt_Auslauf", pkt_Auslauf) 
        
        instroke = drawInstroke(*Halbbogen_unten.points[0], 0.8)
        outstroke = drawOutstroke(*Halbbogen_unten.points[-1], 1.1, "down")

        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-3, y-5)
        Deckung_unten = drawSchriftteil1(*Raute_a)
        FrakturGatE_z += instroke + outstroke + Deckung_unten
        drawPath(Halbbogen_unten)


    if style == "mid":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
        Halbbogen_oben = drawSchriftzug_z_Initial(x, y-1.5)
    
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-3.25)
        Halbbogen_unten = drawHalbbogen5(*Raute_a, instrokeLen=0.8)
        pkt_Ausstrich = Halbbogen_unten.points[-1]
        #text("pkt_Auslauf", pkt_Auslauf) 
        
        
             

    if style == "final":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
        Halbbogen_oben = drawHalbbogen4(*Raute_a, instrokeLen=0.8)
    
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-3.5)
        Halbbogen_unten = drawHalbbogen5(*Raute_a, instrokeLen=0.8)
        pkt_Ausstrich = Halbbogen_unten.points[-1]
        #text("pkt_Auslauf", pkt_Auslauf) 
        
    FrakturGatE_z += Halbbogen_oben + Halbbogen_unten
    trans_scale(FrakturGatE_z, valueToMoveGlyph)
    return FrakturGatE_z, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    


def drawFrakturGatE_z_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatE_z_thinStroke = BezierPath()

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Ausstrich, LOWER_E, 10, HSL_size=1, HSL_start=12, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 11)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.4, Auslauf.points[-1][1]-modul_height*1.065)    

    FrakturGatE_z_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatE_z_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_z_thinStroke)
    trans_scale(FrakturGatE_z_thinStroke, valueToMoveGlyph)
    return FrakturGatE_z_thinStroke   
    
    
    
    
    
    
    

def drawFrakturGatE_thinstroke_Straight(x, y, *, pass_from_thick=None):     
    
    FrakturGatE_thinstroke_Straight = BezierPath()
    
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
          
    FrakturGatE_thinstroke_Straight +=  Zierstrich
    trans_thinStroke_down_left(FrakturGatE_thinstroke_Straight)
    trans_scale(FrakturGatE_thinstroke_Straight, valueToMoveGlyph)
    return FrakturGatE_thinstroke_Straight
    
    
    
    
    
    
    
    
    
    
#############################
### ab hier Interpunktion ###
#############################
    
    
    
    
def drawFrakturGatE_period(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    period = drawGrundelementE(*Raute_a)
        
    FrakturGatE_period = period
    trans_scale(FrakturGatE_period, valueToMoveGlyph) 
    return FrakturGatE_period
    
    
    
    
    
def drawFrakturGatE_colon(x, y):
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    colon_top = drawGrundelementE(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    colon_btm = drawGrundelementE(*Raute_a)  
        
    FrakturGatE_colon = colon_top + colon_btm
    trans_scale(FrakturGatE_colon, valueToMoveGlyph)
    return FrakturGatE_colon
    
    
    
    
    
def drawFrakturGatE_semicolon(x, y):
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    
    semicolon_top = drawGrundelementE(*Raute_a)
    semicolon_btm = drawSchriftzug_semicolon_btm(x-0.2, y)  

    FrakturGatE_semicolon = semicolon_top + semicolon_btm
    trans_scale(FrakturGatE_semicolon, valueToMoveGlyph)
    return FrakturGatE_semicolon
    
    
    
    

def drawFrakturGatE_quoteright(x, y):

    quoteright = drawSchriftzug_semicolon_btm(x, y+5.5) 
    
    FrakturGatE_quoteright = quoteright
    trans_scale(FrakturGatE_quoteright, valueToMoveGlyph)
    return FrakturGatE_quoteright
    
    

    
def drawFrakturGatE_quotesingle(x, y):

    FrakturGatE_quotesingle = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y)
    fake = drawGrundelementA(*Grund_c, 0)
    pkt_fake = fake.points[-1][0], fake.points[-1][1]+3*modul_height
    
    #### delete Grundelement A in RF once quotesingle is established
    #### just there because funktion doesn't work if there is no element in foreground layer
    
    FrakturGatE_quotesingle = fake
    drawPath(FrakturGatE_quotesingle)
    trans_scale(FrakturGatE_quotesingle, valueToMoveGlyph)
    return FrakturGatE_quotesingle, collections.namedtuple('dummy', 'pkt_fake')(pkt_fake)
    

def drawFrakturGatE_quotesingle_thinstroke(x, y, *, pass_from_thick=None):

    FrakturGatE_quotesingle_thinstroke = BezierPath()

    quotesingle = drawSchneckenzug(*pass_from_thick.pkt_fake, LOWER_B, 3, HSL_size=2, HSL_start=6, clockwise=True, inward=False)
    Endpunkt = drawThinstroke_Endpunkt(pass_from_thick.pkt_fake[0]-modul_width*1.11, pass_from_thick.pkt_fake[1]-modul_height*0.55)
    
    FrakturGatE_quotesingle_thinstroke += quotesingle + Endpunkt
    drawPath(FrakturGatE_quotesingle_thinstroke)
    trans_scale(FrakturGatE_quotesingle_thinstroke, valueToMoveGlyph)
    return FrakturGatE_quotesingle_thinstroke
    
    
    
    
    
    
def drawFrakturGatE_comma(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-4.5)

    comma = drawGrundelementG(*Grund_a, 2, "down") 

    FrakturGatE_comma = comma
    trans_scale(FrakturGatE_comma, valueToMoveGlyph)
    return FrakturGatE_comma
    
    



    
def drawFrakturGatE_endash(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-5.25)

    endash = drawGrundelementB(*Grund_a, 2) 

    FrakturGatE_endash = endash
    trans_scale(FrakturGatE_endash, valueToMoveGlyph)
    return FrakturGatE_endash
    
    
    
def drawFrakturGatE_emdash(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.25)

    emdash = drawGrundelementB(*Grund_a, 4) 

    FrakturGatE_emdash = emdash
    trans_scale(FrakturGatE_emdash, valueToMoveGlyph)
    return FrakturGatE_emdash
    
    
    
    
    
def drawFrakturGatE_hyphen(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.25)
    hyphen_top = drawGrundelementH(*Grund_b, 2, "down") 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.25)
    hyphen_btm = drawGrundelementH(*Grund_b, 2, "down") 
    
    FrakturGatE_hyphen = hyphen_top + hyphen_btm
    trans_scale(FrakturGatE_hyphen, valueToMoveGlyph)
    return FrakturGatE_hyphen
     
    
    
    
def drawFrakturGatE_exclam(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+3.25)
    exclam = drawGrundelementF(*Grund_a, 7) 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    period = drawGrundelementE(*Raute_a)

    FrakturGatE_exclam = exclam + period
    trans_scale(FrakturGatE_exclam, valueToMoveGlyph)
    return FrakturGatE_exclam
    
    



def drawFrakturGatE_question(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    period = drawGrundelementE(*Raute_a)
    question = drawSchriftzug_question(x, y) 

    FrakturGatE_question = question + period
    trans_scale(FrakturGatE_question, valueToMoveGlyph)
    return FrakturGatE_question
    
    
    
    
    




###################################
######     ab hier Zahlen    ######
###################################




def drawFrakturGatE_zero(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y)

    FrakturGatE_zero = Bogen_links + Bogen_rechts
    trans_scale(FrakturGatE_zero, valueToMoveGlyph)
    return FrakturGatE_zero
    
    
    
 
def drawFrakturGatE_one(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2)
    
    stem = drawSchriftzug3_Figures(x, y)

    Serife_nach_rechts = drawSchriftteil2(*Raute_a)
    Serife_nach_unten = drawSchriftteil5(*Raute_d)

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.5)
    transition = drawSchneckenzug(*Grund_a, LOWER_B, 3, HSL_size=1, HSL_start=16.25, clockwise=True, inward=True)
    pkt_Auslauf = transition.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 
    
    FrakturGatE_one = stem + Serife_nach_rechts + Serife_nach_unten + transition
    drawPath(FrakturGatE_one)
    trans_scale(FrakturGatE_one, valueToMoveGlyph)
    return FrakturGatE_one, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    

def drawFrakturGatE_one_thinStroke(x, y, *, pass_from_thick=None): 
    
    FrakturGatE_one_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    
    FrakturGatE_one_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatE_one_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_one_thinStroke)
    trans_scale(FrakturGatE_one_thinStroke, valueToMoveGlyph)
    return FrakturGatE_one_thinStroke
    
    
    
    


def drawFrakturGatE_two(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3, y+2)

    Top = drawSchriftzug_two_Top(x, y)
    Stem = drawGrundelementG(*Grund_a, length=7.35, direction="down")
    Schwung_unten = drawSchriftzug_two_Schwung (x, y)
    
    FrakturGatE_two = Top + Stem + Schwung_unten
    trans_scale(FrakturGatE_two, valueToMoveGlyph)
    return FrakturGatE_two  
    
    
def drawFrakturGatE_two_thinStroke(x, y, *, pass_from_thick=None): 
    
    FrakturGatE_two_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 12, HSL_size=1, HSL_start=18, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.001 * i      for  i in range(0, 13)])
    
    FrakturGatE_two_thinStroke += Auslauf 
    drawPath(FrakturGatE_two_thinStroke)
    trans_scale(FrakturGatE_two_thinStroke, valueToMoveGlyph)
    return FrakturGatE_two_thinStroke
    
    
    
    


def drawFrakturGatE_three(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3, y+2)
    
    Three_Top = drawSchriftzug_three_Top(x, y)
    Connection = drawGrundelementG(*Grund_a, 2.75, "down")
    Three_Schwung = drawSchriftzug_three_Bogen(x, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    FrakturGatE_three = Three_Top + Connection + Three_Schwung
    trans_scale(FrakturGatE_three, valueToMoveGlyph)
    return FrakturGatE_three, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
def drawFrakturGatE_three_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_three_thinStroke = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, pass_from_thick.pkt_Auslauf[0]/modul_width, pass_from_thick.pkt_Auslauf[1]/modul_height)  
    Auslauf = drawSchneckenzug(*Grund_c, LOWER_E, 11, HSL_size=0.8, HSL_start=15.25, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.07 * i      for  i in range(0, 12)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.9, Auslauf.points[-1][1]-modul_height*1)
    
    FrakturGatE_three_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatE_three_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_three_thinStroke)
    trans_scale(FrakturGatE_three_thinStroke, valueToMoveGlyph)
    return FrakturGatE_three_thinStroke
    
        
    
    
    
    
    
def drawFrakturGatE_four(x, y):
    
    FrakturGatE_four = BezierPath()
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-3.5, y)

    stem = drawSchriftzug3_Figures(x, y, instrokeLen=0, outstrokeLen=0.8)
    stroke_down = drawGrundelementH(*Grund_d, 2.755)
    con = drawSchneckenzug(*stroke_down.points[-1], UPPER_D, 1, HSL_size=0, HSL_start=15, clockwise=True, inward=False)
    stroke_hor = drawGrundelementB(*Grund_d, 5)

    FrakturGatE_four = stem + stroke_down + stroke_hor + con
    drawPath(FrakturGatE_four)
    trans_scale(FrakturGatE_four, valueToMoveGlyph)
    return FrakturGatE_four   
    
    
    
    
def drawFrakturGatE_five(x, y):
 
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+3, y+2)

     
    Five_Top = drawSchriftzug_five_Top(*Grund_a)
    Connection = drawGrundelementG(*Grund_a, 2.75, "down")
    Three_Schwung = drawSchriftzug_three_Bogen(x, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 
    
    FrakturGatE_five = Five_Top + Connection + Three_Schwung
    trans_scale(FrakturGatE_five, valueToMoveGlyph)
    return FrakturGatE_five, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    


def drawFrakturGatE_six(x, y):

    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    pkt_Auslauf_top = Bogen_links.points[0]
    #text("pkt_Auslauf_top", pkt_Auslauf_top) 
    
    Bogen_rechts = drawSchriftzug_six_BogenRechts(x, y)
    pkt_Auslauf_inner = Bogen_rechts.points[0]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner) 

    FrakturGatE_six = Bogen_links + Bogen_rechts
    trans_scale(FrakturGatE_six, valueToMoveGlyph)
    return FrakturGatE_six, collections.namedtuple('dummy', 'pkt_Auslauf_top pkt_Auslauf_inner')(pkt_Auslauf_top, pkt_Auslauf_inner)
    
    
def drawFrakturGatE_six_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_six_thinStroke = BezierPath() 


    ########## this to draw real line like in drawing Tab 21 #########
    # Einsatz = drawInstroke(*pass_from_thick.pkt_Auslauf_top, 1, "down")
    # Auslauf_top = drawSchneckenzug(*Einsatz.points[-1], UPPER_E, 4, HSL_size=0, HSL_start=16, clockwise=True, inward=False)
    # FrakturGatE_six_thinStroke += Einsatz 

    ##################################################################
    
    
    Auslauf_top = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_top, UPPER_E, 4, HSL_size=0, HSL_start=16, clockwise=True, inward=False)
    
    ### wegen blödem Absatz – siehe Tab21
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+2, y-0.25)
    Auslauf_inner = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=2, HSL_start=15, clockwise=False, inward=False)
    #Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, UPPER_E, 3, HSL_size=2, HSL_start=10, clockwise=False, inward=False)

    trans_thinStroke_down_left(Auslauf_inner) 
    trans_thinStroke_down_left(Auslauf_inner) ### zwei halbe = 1 ganzes verschoben siehe Tab 21
    trans_thinStroke_up_right(Auslauf_top)
    
    FrakturGatE_six_thinStroke += Auslauf_top + Auslauf_inner 
    drawPath(FrakturGatE_six_thinStroke)
    trans_scale(FrakturGatE_six_thinStroke, valueToMoveGlyph)
    return FrakturGatE_six_thinStroke
    
    
    
    
    
    
    
def drawFrakturGatE_seven(x, y):
    
    drawGrundelOrient(A1, A2, offset, x+1, baseline) ### Orientierung stroke bottom
    
    Seven_Top = drawSchriftzug_seven_Top(x, y)
    Seven_Stem = drawGrundelementG(*Seven_Top.points[-1], length=8, direction="down")
    
    FrakturGatE_seven = Seven_Top + Seven_Stem
    trans_scale(FrakturGatE_seven, valueToMoveGlyph)  
    return FrakturGatE_seven        

    


    
    

def drawFrakturGatE_eight(x, y):

    FrakturGatE_eight = drawSchriftzug_eight(x, y)
    trans_scale(FrakturGatE_eight, valueToMoveGlyph)
    return FrakturGatE_eight
    
    

    


def drawFrakturGatE_nine(x, y):

    Bogen_links = drawSchriftzug_nine_BogenLinks(x, y)
    pkt_Auslauf_inner = Bogen_links.points[-1]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner) 
    
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y, version="nine")
    pkt_Auslauf_btm = Bogen_rechts.points[-1]
    #text("pkt_Auslauf_btm", pkt_Auslauf_btm) 

    FrakturGatE_nine = Bogen_links + Bogen_rechts
    trans_scale(FrakturGatE_nine, valueToMoveGlyph)
    return FrakturGatE_nine, collections.namedtuple('dummy', 'pkt_Auslauf_btm pkt_Auslauf_inner')(pkt_Auslauf_btm, pkt_Auslauf_inner)

    
    
def drawFrakturGatE_nine_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_nine_thinStroke = BezierPath() 
    
    Auslauf_btm = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_btm, LOWER_E, 4, HSL_size=0, HSL_start=16, clockwise=True, inward=False)
    Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, LOWER_E, 4, HSL_size=6, HSL_start=9, clockwise=False, inward=False)
        
    trans_thinStroke_down_left(Auslauf_btm) 
    trans_thinStroke_up_right(Auslauf_inner)
    
    FrakturGatE_nine_thinStroke += Auslauf_btm + Auslauf_inner 
    drawPath(FrakturGatE_nine_thinStroke)
    trans_scale(FrakturGatE_nine_thinStroke, valueToMoveGlyph)
    return FrakturGatE_nine_thinStroke 
    
    
    
    
    




##################################################################################
###            Ab hier Versalien
##################################################################################




def drawSchriftzug_BG_Versalien(x, y):
                
    drawGrundelOrientMittig(A1, A2, offset, x, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)  # für J
    drawGrundelOrientMittig(A1, A2, offset, x+6, y-5) 
     
    drawGrundelOrientMittig(A1, A2, offset, x, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+3, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+6, y) 
    
    drawGrundelOrientMittig(A1, A2, offset, x+3, y+3) 


drawSchriftzug_BG_Versalien(temp_x, temp_y)








def drawFrakturGatE_A(x, y):

    
    Hauptstrich_links = drawSchriftzug_A_Hauptstrich_links(x, y)
    pkt_Ausstrich = Hauptstrich_links.points[0]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    Hauptstrich_rechts = drawSchriftzug_A_Hauptstrich_rechts(x, y)

    
    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x, y-0.25)
    Deckung_Rechtsoben = drawSchriftzug_A_DeckungOben(x, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.25, y-5.35)
    Signatur = drawSchriftteil11(*Raute_a)
    
    FrakturGatE_A = Hauptstrich_links + Hauptstrich_rechts + Deckung_LinksUnten + Deckung_Rechtsoben + Signatur
    trans_scale(FrakturGatE_A, valueToMoveGlyph)
    return FrakturGatE_A, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)


def drawFrakturGatE_A_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatE_A_thinStroke = BezierPath()

    pkt_Ausstrich = drawSchneckenzug(*pass_from_thick.pkt_Ausstrich, UPPER_E, 1, HSL_size=1, HSL_start=24, clockwise=True, inward=False)
    
    FrakturGatE_A_thinStroke += pkt_Ausstrich
    drawPath(FrakturGatE_A_thinStroke)
    trans_thinStroke_up_right(FrakturGatE_A_thinStroke)
    trans_scale(FrakturGatE_A_thinStroke, valueToMoveGlyph)
    return FrakturGatE_A_thinStroke





    
def drawFrakturGatE_Adieresis(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-1) 

    Einsatz = drawGrundelementE(*Grund_a, length=0.5)
    oben = drawSchneckenzug(*Grund_a, LOWER_A, 4, HSL_size=1, HSL_start=15, clockwise=True, inward=False)
    instroke = drawInstroke(*oben.points[-1], 0.75, "down")
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_A, 4, HSL_size=1, HSL_start=14, clockwise=True, inward=True)
    stehender_Schwung = instroke + oben + Einsatz + unten
    
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    ###
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1.2, y-4.7 ) 
    links = drawSchneckenzug(*Grund_a, UPPER_A, 4, HSL_size=1, HSL_start=4, clockwise=False, inward=False)  
    Einsatz = drawGrundelementE(*Grund_a, 0.75)
    rechts = drawSchneckenzug(*Einsatz.points[-4], LOWER_A, 4, HSL_size=1.5, HSL_start=4, clockwise=False, inward=False)
    outstroke = drawOutstroke(*rechts.points[-1], 1.7)
    upstroke = drawSchneckenzug(*outstroke.points[0], LOWER_E, 3, HSL_size=1, HSL_start=20.5, clockwise=False, inward=False)
    liegender_Schwung = links + Einsatz + rechts + outstroke + upstroke
    
    ###
    Deckung_Rechtsoben = drawSchriftzug_A_DeckungOben(x-3.7, y-0.85)
    instroke = drawGrundelementA(*Deckung_Rechtsoben.points[-1], -0.74)
    curve_top = drawSchneckenzug(*instroke.points[-1], UPPER_E, 3, HSL_size=1, HSL_start=9, clockwise=False, inward=False)
    straight_down = drawGrundelementF(*curve_top.points[-1], 4.5)
    curve_btm = drawSchneckenzug(*straight_down.points[-1], UPPER_B, 2, HSL_size=1, HSL_start=16, clockwise=False, inward=False)
    outstroke = drawGrundelementA(*curve_btm.points[-1], 0.5)
    Rechte_Seite = Deckung_Rechtsoben + instroke + curve_top + straight_down + curve_btm + outstroke
    
    Dieresis = drawDieresis(x+1.5, y+3.25)

    FrakturGatE_Adieresis = stehender_Schwung + liegender_Schwung + Rechte_Seite + Dieresis
    trans_scale(FrakturGatE_Adieresis, valueToMoveGlyph)
    return FrakturGatE_Adieresis, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    









def drawFrakturGatE_B(x, y):

    FrakturGatE_B = BezierPath()

    Hauptbogen = drawSchriftzug_B_Hauptbogen(x, y)
    pkt_Auslauf = Hauptbogen.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5.5, y+2.75)

    if version_mod == 0:  
        middleElement = drawSchriftteil2(*Grund_a)
        pkt_instrokeAuge = middleElement.points[0]
        #text("pkt_instrokeAuge", middleElement.points[0])
        Auge = drawSchriftteil8(*middleElement.points[-1])
        pkt_outstrokeAuge = Auge.points[3]
        #text("pkt_outstrokeAuge", Auge.points[-1])
        FrakturGatE_B += middleElement
        
    if version_mod == 1:    
        Auge = drawAuge(*Grund_a)    
        pkt_instrokeAuge = Auge.points[0]
        #text("pkt_instrokeAuge", pkt_instrokeAuge)
        pkt_outstrokeAuge = Auge.points[-1]
        #text("pkt_outstrokeAuge", pkt_outstrokeAuge)
        
    Bauch = drawSchriftzug_B_Bauch(x, y)
    Fuss = drawSchneckenzug(*Bauch.points[-2], UPPER_A, 4, HSL_size=2, HSL_start=16, clockwise=False, inward=True)
    
    FrakturGatE_B += Hauptbogen + Fuss + Auge + Bauch
    drawPath(FrakturGatE_B)
    trans_scale(FrakturGatE_B, valueToMoveGlyph)
    return FrakturGatE_B, collections.namedtuple('dummy', 'pkt_Auslauf pkt_instrokeAuge pkt_outstrokeAuge')(pkt_Auslauf, pkt_instrokeAuge, pkt_outstrokeAuge)
    

def drawFrakturGatE_B_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatE_B_thinStroke = BezierPath()
        
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    
    #### Auge
    instroke = drawInstroke(*pass_from_thick.pkt_instrokeAuge, 0.75)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 1, HSL_size=1, HSL_start=22, clockwise=False, inward=False)
    curve_fromAuge = drawSchneckenzug(*pass_from_thick.pkt_outstrokeAuge, LOWER_E, 1, HSL_size=2, HSL_start=8, clockwise=True, inward=False)

    FrakturGatE_B_thinStroke += Auslauf + instroke + curve_toInstroke + curve_fromAuge + Endpunkt
    drawPath(FrakturGatE_B_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_B_thinStroke) 
    trans_scale(FrakturGatE_B_thinStroke, valueToMoveGlyph)
    return FrakturGatE_B_thinStroke
    
    




def drawFrakturGatE_C(x, y, version="Substantival"):
    x+=3   
    stehender_Schwung = drawSchriftzug_C_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[3]
    #text("pkt_Auslauf", pkt_Auslauf)
    liegender_Schwung = drawSchriftzug_C_liegenderSchwung(x, y)
    Deckung = drawSchriftzug_A_DeckungOben(x-4.5, y+0.25)

    FrakturGatE_C = stehender_Schwung + liegender_Schwung + Deckung

    if version == "Substantival":
        trans_scale(FrakturGatE_C, valueToMoveGlyph)
    if version == "Versal":
        trans_scale(FrakturGatE_C, valueToMoveGlyph)
        FrakturGatE_C.scale(1.25)
    if version == "Initial":
        trans_scale(FrakturGatE_C, valueToMoveGlyph)
        FrakturGatE_C.scale(1.75)        
    if version == "Capital":
        trans_scale(FrakturGatE_C, valueToMoveGlyph)
        FrakturGatE_C.scale(2.5)

    return FrakturGatE_C, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    

def drawFrakturGatE_C_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)    

    FrakturGatE_C_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatE_C_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_C_thinStroke) 
    trans_scale(FrakturGatE_C_thinStroke, valueToMoveGlyph)
    return FrakturGatE_C_thinStroke
    
    

def drawFrakturGatE_C_thinStroke_Versal(x, y, *, pass_from_thick=None):

    FrakturGatE_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    
    position = Auslauf.points[-1][0]-2.5, Auslauf.points[-1][1]-5.8
    amount = 15
    radius = part*7
    
    i = 0.4
    for i in range(amount):
        FrakturGatE_C_thinStroke.oval(position[0]+i*0.2, position[1]+i*0.2, radius-i*0.4, radius-i*0.4)
        i += 0.4
    FrakturGatE_C_thinStroke += Auslauf
    drawPath(FrakturGatE_C_thinStroke)
    FrakturGatE_C_thinStroke.translate(-(modul_width*0.5)+1, -(modul_height*0.25-0.5))
    trans_scale(FrakturGatE_C_thinStroke, valueToMoveGlyph)    
    FrakturGatE_C_thinStroke.scale(1.25)
    return FrakturGatE_C_thinStroke


def drawFrakturGatE_C_thinStroke_Initial(x, y, *, pass_from_thick=None):

    FrakturGatE_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    
    position = Auslauf.points[-1][0]-2, Auslauf.points[-1][1]-4.15
    amount = 15
    radius = part*5
    
    i = 0.4
    for i in range(amount):
        FrakturGatE_C_thinStroke.oval(position[0]+i*0.2, position[1]+i*0.2, radius-i*0.4, radius-i*0.4)
        i += 0.4
        
    FrakturGatE_C_thinStroke += Auslauf
    drawPath(FrakturGatE_C_thinStroke)
    FrakturGatE_C_thinStroke.translate(-(modul_width*0.5)+1.5, -(modul_height*0.25-0.75))
    trans_scale(FrakturGatE_C_thinStroke, valueToMoveGlyph)    
    FrakturGatE_C_thinStroke.scale(1.75)
    return FrakturGatE_C_thinStroke
        
        
def drawFrakturGatE_C_thinStroke_Capital(x, y, *, pass_from_thick=None):

    FrakturGatE_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    
    position = Auslauf.points[-1][0]-2, Auslauf.points[-1][1]-4.15
    amount = 20
    radius = part*5
    
    i = 0.3
    for i in range(amount):
        FrakturGatE_C_thinStroke.oval(position[0]+i*0.15, position[1]+i*0.15, radius-i*0.3, radius-i*0.3)
        i += 0.3

    FrakturGatE_C_thinStroke += Auslauf
    drawPath(FrakturGatE_C_thinStroke)
    FrakturGatE_C_thinStroke.translate(-(modul_width*0.5)+2, -(modul_height*0.25-1))
    trans_scale(FrakturGatE_C_thinStroke, valueToMoveGlyph)    
    FrakturGatE_C_thinStroke.scale(2.5)
    return FrakturGatE_C_thinStroke
    
    
    
    
    
    
    
    
    
    



def drawFrakturGatE_D(x, y):
   
    stehender_Schwung = drawSchriftzug_D_stehenderSchwung(x+2.9,y-0.9)
    pkt_Auslauf = stehender_Schwung.points[5]
    #text("pkt_Auslauf", stehender_Schwung.points[-3])
    
    stehender_Schwung_Deckung = drawSchriftzug_D_Deckung(x+4.5, y)
    liegender_Schwung = drawSchriftzug_D_liegenderSchwung(x,y)
    Hauptbogen = drawSchriftzug_D_Hauptbogen(x, y)

    FrakturGatE_D = stehender_Schwung + stehender_Schwung_Deckung + liegender_Schwung + Hauptbogen
    
    trans_scale(FrakturGatE_D, valueToMoveGlyph)
    return FrakturGatE_D, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    

def drawFrakturGatE_E(x, y):
    x += 2
    stehender_Schwung = drawSchriftzug_C_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[3]
    #text("pkt_Auslauf", pkt_Auslauf)
    liegender_Schwung = drawSchriftzug_C_liegenderSchwung(x, y)
    Deckung = drawSchriftzug_A_DeckungOben(x-4.5, y+0.25)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2.25, y-0.5)
    Signatur = drawSchriftteil11(*Raute_a)
    
    FrakturGatE_E = stehender_Schwung + liegender_Schwung + Deckung + Signatur
    trans_scale(FrakturGatE_E, valueToMoveGlyph)
    return FrakturGatE_E, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
        
def drawFrakturGatE_F(x, y):
    
    liegender_Schwung = drawSchriftzug_F_liegenderSchwung(x, y)
    stehender_Schwung = drawSchriftzug_F_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.25, y-5.5)
    Deckung_left = drawSchriftteil9(*Raute_a)
    Deckung_btm = drawSchneckenzug(*Raute_a, UPPER_A, 3, HSL_size=1, HSL_start=5, clockwise=True, inward=False)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5.5, y+0.75)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.35, Signatur.points[-8][1]+modul_height*1.5
    #text("pkt_Signatur", pkt_Signatur)
    
    FrakturGatE_F = liegender_Schwung + stehender_Schwung + Deckung_left + Deckung_btm + Signatur
    drawPath(FrakturGatE_F)
    trans_scale(FrakturGatE_F, valueToMoveGlyph)
    return FrakturGatE_F, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Signatur')(pkt_Auslauf, pkt_Signatur)
 
    
def drawFrakturGatE_F_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_F_thinStroke = BezierPath()
    
    FrakturGatE_F_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.5*modul_height))
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 1, HSL_size=1, HSL_start=13.5, clockwise=True, inward=False)

    FrakturGatE_F_thinStroke += Auslauf
    drawPath(FrakturGatE_F_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_F_thinStroke) 
    trans_scale(FrakturGatE_F_thinStroke, valueToMoveGlyph)
    return FrakturGatE_F_thinStroke 


  

    


def drawFrakturGatE_G(x, y):
    
    FrakturGatE_G = BezierPath()

    Hauptbogen = drawSchriftzug_G_Hauptbogen(x, y)
    Hauptbogen_Deckung = drawSchriftzug_G_Deckung(x, y)
    
    Fuss = drawSchriftzug_G_Fuss(x, y)

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5.5, y+3.25)
    
    if version_mod == 0:  
        middleElement = drawSchriftteil2(*Grund_a)
        pkt_instrokeAuge = middleElement.points[0]
        #text("pkt_instrokeAuge", middleElement.points[0])
        Auge = drawSchriftteil8(*middleElement.points[-1])
        pkt_outstrokeAuge = Auge.points[3]
        #text("pkt_outstrokeAuge", Auge.points[-1])
        FrakturGatE_G += middleElement
        
    if version_mod == 1:    
        Auge = drawAuge(*Grund_a)    
        pkt_instrokeAuge = Auge.points[0]
        #text("pkt_instrokeAuge", pkt_instrokeAuge)
        pkt_outstrokeAuge = Auge.points[-1]
        #text("pkt_outstrokeAuge", pkt_outstrokeAuge)
        
        
    Bauch = drawSchriftzug_G_Bauch(x, y)
    
    stehender_Schwung = drawSchriftzug_G_stehenderSchwung(x,y, instrokeLen=1.5)
    pkt_Auslauf_unten = stehender_Schwung.points[3]
    #text("pkt_Auslauf_unten", pkt_Auslauf_unten)
    pkt_Auslauf_oben = stehender_Schwung.points[-1]
    #text("pkt_Auslauf_oben", pkt_Auslauf_oben)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.75, y-3)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.35, Signatur.points[-8][1]+modul_height*1.65
    #text("pkt_Signatur", pkt_Signatur)

    FrakturGatE_G += Hauptbogen + Hauptbogen_Deckung + Fuss + Auge + Bauch + stehender_Schwung + Signatur
    trans_scale(FrakturGatE_G, valueToMoveGlyph)
    return FrakturGatE_G, collections.namedtuple('dummy', 'pkt_instrokeAuge pkt_outstrokeAuge pkt_Auslauf_unten pkt_Auslauf_oben pkt_Signatur')(pkt_instrokeAuge, pkt_outstrokeAuge, pkt_Auslauf_unten, pkt_Auslauf_oben, pkt_Signatur)
    
    
def drawFrakturGatE_G_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_G_thinStroke = BezierPath()
    
    Auslauf_unten = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_unten, LOWER_E, 9, HSL_size=1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt_unten = drawThinstroke_Endpunkt(Auslauf_unten.points[-1][0]-modul_width*0.5, Auslauf_unten.points[-1][1]-modul_height*1.1)
    
    Auslauf_oben = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_oben, LOWER_E, 4, HSL_size=1, HSL_start=10, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 5)])
    Endpunkt_oben = drawThinstroke_Endpunkt(Auslauf_oben.points[-1][0]-modul_width*1.05, Auslauf_oben.points[-1][1]-modul_height*0.85)
        
    #### Auge
    instroke = drawInstroke(*pass_from_thick.pkt_instrokeAuge, 0.75)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 1, HSL_size=1, HSL_start=22, clockwise=False, inward=False)
    curve_fromAuge = drawSchneckenzug(*pass_from_thick.pkt_outstrokeAuge, LOWER_E, 1, HSL_size=2, HSL_start=8, clockwise=True, inward=False)

    FrakturGatE_G_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.5*modul_height))
    
    trans_thinStroke_down_left(Auslauf_unten) 
    trans_thinStroke_down_left(Endpunkt_unten) 
    trans_thinStroke_down_left(instroke) 
    trans_thinStroke_down_left(curve_toInstroke) 
    trans_thinStroke_down_left(curve_fromAuge) 
    trans_thinStroke_down_left(FrakturGatE_G_thinStroke) 
    trans_thinStroke_up_right(Auslauf_oben)
    trans_thinStroke_up_right(Endpunkt_oben)
    
    FrakturGatE_G_thinStroke += Auslauf_unten + Auslauf_oben + instroke + curve_toInstroke + curve_fromAuge + Endpunkt_unten + Endpunkt_oben
    drawPath(FrakturGatE_G_thinStroke)    
    trans_scale(FrakturGatE_G_thinStroke, valueToMoveGlyph)
    return FrakturGatE_G_thinStroke
    
    
    
    


def drawFrakturGatE_H(x, y):

    
    stehender_Schwung = drawSchriftzug_H_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Deckung_Rechtsoben = drawSchriftzug_A_DeckungOben(x-5.15, y+0.15)

    Hauptstrich_rechts = drawSchriftzug_H_Hauptstrich_rechts(x, y)
    pkt_Einlauf = Hauptstrich_rechts.points[0]
    #text("pkt_Einlauf", pkt_Einlauf)
    pkt_Auslauf_unten = Hauptstrich_rechts.points[-2]
    #text("pkt_Auslauf_unten", pkt_Auslauf_unten)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-0.5, y-5.65)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)
    
    
    FrakturGatE_H = stehender_Schwung + Hauptstrich_rechts + Deckung_Rechtsoben + liegender_Schwung_unten
    trans_scale(FrakturGatE_H, valueToMoveGlyph)
    return FrakturGatE_H, collections.namedtuple('dummy', 'pkt_Einlauf pkt_Auslauf pkt_Auslauf_unten')(pkt_Einlauf, pkt_Auslauf, pkt_Auslauf_unten)
    
    
def drawFrakturGatE_H_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_H_thinStroke = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Einlauf, 0.75)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 1, HSL_size=1, HSL_start=28, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    Auslauf_unten = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_unten, LOWER_E, 3, HSL_size=1.1, HSL_start=20, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 4)])
     
    FrakturGatE_H_thinStroke += instroke + curve_toInstroke + Auslauf + Endpunkt + Auslauf_unten
    drawPath(FrakturGatE_H_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_H_thinStroke)
    trans_scale(FrakturGatE_H_thinStroke, valueToMoveGlyph)
    return FrakturGatE_H_thinStroke
    
    
    
        


def drawFrakturGatE_I(x, y):

    liegender_Schwung = drawSchriftzug_I_liegenderSchwung(x, y)
    stehender_Schwung = drawSchriftzug_I_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.25, y-5.5)
    Deckung_left = drawSchriftteil9(*Raute_a)
    Deckung_btm = drawSchriftteil8(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1.25, y-0.5)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.35, Signatur.points[-8][1]+modul_height*1.6
    #text("pkt_Signatur", pkt_Signatur)
    
    FrakturGatE_I = liegender_Schwung + stehender_Schwung + Deckung_left + Deckung_btm + Signatur
    trans_scale(FrakturGatE_I, valueToMoveGlyph)
    return FrakturGatE_I, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Signatur')(pkt_Auslauf, pkt_Signatur)
    
def drawFrakturGatE_I_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_I_thinStroke = BezierPath()
    
    FrakturGatE_I_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.5*modul_height))
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 1, HSL_size=1, HSL_start=20, clockwise=True, inward=False)

    FrakturGatE_I_thinStroke += Auslauf
    drawPath(FrakturGatE_I_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_I_thinStroke) 
    trans_scale(FrakturGatE_I_thinStroke, valueToMoveGlyph)
    return FrakturGatE_I_thinStroke 
    
    
    
    
    
       
def drawFrakturGatE_J(x, y):

    liegender_Schwung = drawSchriftzug_J_liegenderSchwung(x, y)
    stehender_Schwung = drawSchriftzug_J_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1.25, y-1)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.35, Signatur.points[-8][1]+modul_height*1.6
    #text("pkt_Signatur", pkt_Signatur)
    
    FrakturGatE_J = liegender_Schwung + stehender_Schwung + Signatur
    trans_scale(FrakturGatE_J, valueToMoveGlyph)
    return FrakturGatE_J, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Signatur')(pkt_Auslauf, pkt_Signatur)
    
       
def drawFrakturGatE_J_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_J_thinStroke = BezierPath()
    
    FrakturGatE_J_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.5*modul_height))
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.25, HSL_start=20, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)    
    
    FrakturGatE_J_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatE_J_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_J_thinStroke) 
    trans_scale(FrakturGatE_J_thinStroke, valueToMoveGlyph)
    return FrakturGatE_J_thinStroke 
    
    
       




def drawFrakturGatE_K(x, y):
    x += 2
    stehender_Schwung = drawSchriftzug_K_stehenderSchwung(x, y)

    ### Deckung oben
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3, y+2.25)
    Einsatz_oben = drawGrundelementE(*Grund_a, 0.5, "unten")
    Deckung_oben_top = drawHalbbogen9(*Einsatz_oben.points[-1], outstrokeLen=0)
    Deckung_oben_btm = drawHalbbogen3(*Grund_a, instrokeLen=0)
    Deckung_oben = Einsatz_oben + Deckung_oben_top + Deckung_oben_btm

    ### Deckung unten
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-3, y-5.5)
    Einsatz_unten = drawGrundelementE(*Grund_a, 0.5, "unten")
    Deckung_unten_top = drawHalbbogen9(*Einsatz_unten.points[-1], outstrokeLen=0)
    Deckung_unten_btm = drawHalbbogen1(*Grund_a, instrokeLen=0)
    Deckung_unten = Einsatz_unten + Deckung_unten_top + Deckung_unten_btm
    
    ### Schwung unten
    liegender_Schwung_unten = drawSchriftzug_S_liegenderSchwung(x+2, y, instrokeLen=0.5)
    
    ### Querstrich
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-1.25, y-2)
    Querstrich = drawGrundelementB(*Grund_a, 3.25)
    
    ### Auge
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1, y+1.5)
    instroke = drawInstroke(*Grund_a, 1)
    middleElement = drawGrundelementE(*Grund_a, 0.4)
    Auge = drawSchriftteil8(*middleElement.points[4])
    Auge_komplett = instroke + middleElement + Auge
    
    FrakturGatE_K = stehender_Schwung + Deckung_oben + Deckung_unten + liegender_Schwung_unten + Querstrich + Auge_komplett
    trans_scale(FrakturGatE_K, valueToMoveGlyph)
    return FrakturGatE_K
    
    
    



   

def drawFrakturGatE_L(x, y):

    stehender_Schwung = drawSchriftzug_L_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[3]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y+2.5)
    Einsatz = drawGrundelementE(*Grund_a, 0.25, "unten")
    Einsatz2 = drawGrundelementE(*Grund_a, 0.25)

    Deckung_top = drawSchneckenzug(*Einsatz.points[-1], LOWER_A, 4, HSL_size=0.5, HSL_start=7, clockwise=True, inward=False)
    Deckung_btm = drawSchneckenzug(*Einsatz2.points[4], UPPER_A, 4, HSL_size=0.5, HSL_start=10.25, clockwise=True, inward=False)

    Fuss = drawSchriftzug_L_Fuss(x-3, y)

    FrakturGatE_L = stehender_Schwung + Einsatz + Einsatz2 + Deckung_top + Deckung_btm + Fuss
    drawPath(FrakturGatE_L)
    trans_scale(FrakturGatE_L, valueToMoveGlyph)
    return FrakturGatE_L, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
  




    
    

    
def drawFrakturGatE_M(x, y):

    links = drawSchriftzug_M_Bogen_links(x, y)
    mitte = drawSchriftzug_M_Bogen_mitte(x, y)
    rechts = drawSchriftzug_M_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[14]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x+3, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+8.05, y-5.35)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    FrakturGatE_M = links + mitte + rechts + Deckung_LinksUnten + liegender_Schwung_unten
    trans_scale(FrakturGatE_M, valueToMoveGlyph)
    return FrakturGatE_M, collections.namedtuple('dummy', 'pkt_Einlauf')(pkt_Einlauf)
    
    

def drawFrakturGatE_M_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_M_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)
    pkt_Einlauf_vorher = pass_from_thick.pkt_Einlauf[0]-modul_width*3.15, pass_from_thick.pkt_Einlauf[1]
    Einlauf_vorher = drawSchneckenzug(*pkt_Einlauf_vorher, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)

    FrakturGatE_M_thinStroke += Einlauf + Einlauf_vorher
    drawPath(FrakturGatE_M_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_M_thinStroke) 
    trans_scale(FrakturGatE_M_thinStroke, valueToMoveGlyph)
    return FrakturGatE_M_thinStroke
    
    
    
    
    
    

def drawFrakturGatE_N(x, y):

    links = drawSchriftzug_M_Bogen_links(x, y)
    rechts = drawSchriftzug_M_Bogen_rechts(x-3, y)
    pkt_Einlauf = rechts.points[14]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x+3, y)

    FrakturGatE_N = links + rechts + Deckung_LinksUnten 
    trans_scale(FrakturGatE_N, valueToMoveGlyph)
    return FrakturGatE_N, collections.namedtuple('dummy', 'pkt_Einlauf')(pkt_Einlauf)
    
def drawFrakturGatE_N_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_N_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)

    FrakturGatE_N_thinStroke += Einlauf
    drawPath(FrakturGatE_N_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_N_thinStroke) 
    trans_scale(FrakturGatE_N_thinStroke, valueToMoveGlyph)
    return FrakturGatE_N_thinStroke
    
    
       
       


    
def drawFrakturGatE_O(x, y):

    stehender_Schwung = drawSchriftzug_O_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)

    liegender_Schwung = drawSchriftzug_O_liegenderSchwung(x, y)
    Bogen_rechts = drawSchriftzug_O_BogenRechts(x, y)

    FrakturGatE_O = stehender_Schwung + liegender_Schwung + Bogen_rechts
    trans_scale(FrakturGatE_O, valueToMoveGlyph)
    return FrakturGatE_O, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)

    
    
    
def drawFrakturGatE_Odieresis(x, y):

    stehender_Schwung = drawSchriftzug_O_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    liegender_Schwung = drawSchriftzug_O_liegenderSchwung(x, y)
    Bogen_rechts = drawSchriftzug_O_BogenRechts(x, y)

    Dieresis = drawDieresis(x+0.5, y+3.5)

    FrakturGatE_Odieresis = stehender_Schwung + liegender_Schwung + Bogen_rechts + Dieresis
    trans_scale(FrakturGatE_Odieresis, valueToMoveGlyph)
    return FrakturGatE_Odieresis, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
    
    
    
def drawFrakturGatE_P(x, y):

    links = drawSchriftzug_P_Bogen_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    rechts = drawSchriftzug_P_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[19]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Deckung = drawGrundelementB(x+2.5*modul_width, y+1.25*modul_height, 3.75)
    #drawSchriftzug_W_Fuss(x-3, y)

    FrakturGatE_P = links + rechts + Deckung 
    trans_scale(FrakturGatE_P, valueToMoveGlyph)
    return FrakturGatE_P, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Einlauf')(pkt_Auslauf, pkt_Einlauf)
    
    
def drawFrakturGatE_P_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_P_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=1, HSL_start=24, clockwise=False, inward=False)
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
        
    FrakturGatE_P_thinStroke += Einlauf + Auslauf + Endpunkt
    drawPath(FrakturGatE_P_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_P_thinStroke)
    trans_scale(FrakturGatE_P_thinStroke, valueToMoveGlyph)
    return FrakturGatE_P_thinStroke
    
    
    
     

    

def drawFrakturGatE_Q(x, y):
    x += 2
    stehender_Schwung = drawSchriftzug_O_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)

    liegender_Schwung = drawSchriftzug_O_liegenderSchwung(x, y)
    Bogen_rechts = drawSchriftzug_O_BogenRechts(x, y)

    liegender_SchwungQ = drawSchriftzug_Q_liegenderSchwung(x, y)
    
    FrakturGatE_Q = stehender_Schwung + liegender_Schwung + Bogen_rechts + liegender_SchwungQ
    trans_scale(FrakturGatE_Q, valueToMoveGlyph)
    return FrakturGatE_Q, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)    
    
    
    


    
    
def drawFrakturGatE_R(x, y):
   
    FrakturGatE_R = BezierPath()

    Hauptbogen = drawSchriftzug_R_Hauptbogen(x, y)
    Hauptbogen_Deckung = drawSchriftzug_R_Deckung(x, y)
    
    Fuss = drawSchriftzug_R_Fuss(x, y)

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5.5, y+2.75)
    
    if version_mod == 0:  
        middleElement = drawSchriftteil2(*Grund_a)
        pkt_instrokeAuge = middleElement.points[0]
        #text("pkt_instrokeAuge", middleElement.points[0])
        Auge = drawSchriftteil8(*middleElement.points[-1])
        pkt_outstrokeAuge = Auge.points[3]
        #text("pkt_outstrokeAuge", Auge.points[-1])
        FrakturGatE_R += middleElement
        
    if version_mod == 1:    
        Auge = drawAuge(*Grund_a)    
        pkt_instrokeAuge = Auge.points[0]
        #text("pkt_instrokeAuge", pkt_instrokeAuge)
        pkt_outstrokeAuge = Auge.points[-1]
        #text("pkt_outstrokeAuge", pkt_outstrokeAuge)
        
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1.75, y-1.625)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.5, Signatur.points[-8][1]+modul_height*1.6
    #text("pkt_Signatur", pkt_Signatur)

    FrakturGatE_R += Hauptbogen + Hauptbogen_Deckung + Fuss + Auge + Signatur
    trans_scale(FrakturGatE_R, valueToMoveGlyph)
    return FrakturGatE_R, collections.namedtuple('dummy', 'pkt_instrokeAuge pkt_outstrokeAuge pkt_Signatur')(pkt_instrokeAuge, pkt_outstrokeAuge, pkt_Signatur)
    
    
def drawFrakturGatE_R_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatE_R_thinStroke = BezierPath()
    
    FrakturGatE_R_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.5*modul_height))
    
    #### Auge
    instroke = drawInstroke(*pass_from_thick.pkt_instrokeAuge, 0.75)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 1, HSL_size=1, HSL_start=22, clockwise=False, inward=False)
    curve_fromAuge = drawSchneckenzug(*pass_from_thick.pkt_outstrokeAuge, LOWER_E, 1, HSL_size=2, HSL_start=8, clockwise=True, inward=False)

    FrakturGatE_R_thinStroke += instroke + curve_toInstroke + curve_fromAuge
    drawPath(FrakturGatE_R_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_R_thinStroke) 
    trans_scale(FrakturGatE_R_thinStroke, valueToMoveGlyph)
    return FrakturGatE_R_thinStroke
    
    
    
    
    
    

def drawFrakturGatE_S(x, y):

    stehender_Schwung = drawSchriftzug_S_stehenderSchwung(x, y)

    ### Deckung oben
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3, y+2.5)
    Einsatz = drawGrundelementE(*Grund_a, 0.5, "unten")
    Deckung_top = drawHalbbogen9(*Einsatz.points[-1], outstrokeLen=0)
    Deckung_btm = drawHalbbogen2(*Grund_a, instrokeLen=0)
    
    ### Schwung unten
    liegender_Schwung_unten = drawSchriftzug_S_liegenderSchwung(x, y)

    FrakturGatE_S = stehender_Schwung + liegender_Schwung_unten + Deckung_top + Deckung_btm + Einsatz
    trans_scale(FrakturGatE_S, valueToMoveGlyph)
    return FrakturGatE_S
    
    
    
    
    
           
def drawFrakturGatE_T(x, y):

    stehender_Schwung = drawSchriftzug_T_stehenderSchwung(x, y)
    
    ### Schwung liegend oben
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+4.5, y+2.75)
    Schwung_top_left = drawSchriftteil9(*Grund_a)
    Schwung_top_Einsatz = drawGrundelementE(*Grund_a, 1, "oben")
    Schwung_top_right = drawSchriftteil10(*Schwung_top_Einsatz.points[4])
    
    Fuss = drawSchriftzug_L_Fuss(x, y)

    FrakturGatE_T = stehender_Schwung + Schwung_top_left + Schwung_top_Einsatz + Schwung_top_right + Fuss
    trans_scale(FrakturGatE_T, valueToMoveGlyph)
    return FrakturGatE_T
    





def drawFrakturGatE_U(x, y):
    x += 3
    FrakturGatE_U = BezierPath()
    
    links = drawSchriftzug_U_Hauptstrich_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    rechts = drawSchriftzug_U_Hauptstrich_rechts(x, y)
    pkt_transition = rechts.points[13]
    #text("pkt_transition", pkt_transition)

    FrakturGatE_U = links + rechts 
    trans_scale(FrakturGatE_U, valueToMoveGlyph)
    return FrakturGatE_U, collections.namedtuple('dummy', 'pkt_Auslauf pkt_transition')(pkt_Auslauf, pkt_transition)   
    
    
def drawFrakturGatE_U_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_U_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=13, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.8, Auslauf.points[-1][1]-modul_height*1.3)
            
    Transition = drawSchneckenzug(*pass_from_thick.pkt_transition, LOWER_E, 2, HSL_size=17, HSL_start=40.25, clockwise=False, inward=False)

    trans_thinStroke_down_left(Auslauf) 
    trans_thinStroke_up_right(Transition)
    
    FrakturGatE_U_thinStroke += Auslauf + Transition + Endpunkt
    drawPath(FrakturGatE_U_thinStroke)
    trans_scale(FrakturGatE_U_thinStroke, valueToMoveGlyph)
    return FrakturGatE_U_thinStroke
    
    
     

def drawFrakturGatE_Udieresis(x, y):

    FrakturGatE_Udieresis = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+2) 
    Hauptbogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=3, HSL_start=14, clockwise=True, inward=False)
    Hauptbogen_unten = drawSchneckenzug(*Hauptbogen_oben.points[-1], LOWER_B, 3, HSL_size=10, HSL_start=40, clockwise=True, inward=True)
    Hauptbogen = Hauptbogen_oben + Hauptbogen_unten
    
    pkt_Auslauf = Hauptbogen.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    
    ###
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+1.75, y-5 ) 
    links = drawSchneckenzug(*Grund_a, UPPER_A, 4, HSL_size=1, HSL_start=2.6, clockwise=False, inward=False)  
    Einsatz = drawGrundelementE(*Grund_a, 0.25)
    rechts = drawSchneckenzug(*Einsatz.points[-4], LOWER_A, 4, HSL_size=2, HSL_start=4, clockwise=False, inward=False)
    outstroke = drawOutstroke(*rechts.points[-1], 1.7)
    upstroke = drawSchneckenzug(*outstroke.points[0], LOWER_E, 3, HSL_size=1, HSL_start=20, clockwise=False, inward=False)
    liegender_Schwung = links + Einsatz + rechts + outstroke + upstroke
    
    
    ###
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.6, y+2)    
    stem = drawSchriftzug3_Figures(x+6.6, y, instrokeLen=0)
    serife_nach_rechts = drawSchriftteil2(*Raute_a)
    serife_nach_unten = drawSchriftteil5(*Raute_d)
    rechts = stem + serife_nach_rechts + serife_nach_unten
    
    Dieresis = drawDieresis(x+5, y+3)

    FrakturGatE_Udieresis = rechts + Hauptbogen + Dieresis + liegender_Schwung
    trans_scale(FrakturGatE_Udieresis, valueToMoveGlyph)
    return FrakturGatE_Udieresis, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    


def drawFrakturGatE_V(x, y):

    FrakturGatE_V = BezierPath()
    
    links = drawSchriftzug_V_Bogen_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    rechts = drawSchriftzug_V_Bogen_rechts(x, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5.6, y-5.4)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    FrakturGatE_V = links + rechts + liegender_Schwung_unten 
    trans_scale(FrakturGatE_V, valueToMoveGlyph)
    return FrakturGatE_V, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    



    
def drawFrakturGatE_W(x, y):

    links = drawSchriftzug_W_Bogen_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    mitte = drawSchriftzug_W_Bogen_mitte(x, y)
    rechts = drawSchriftzug_W_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[-2]
    #text("pkt_Einlauf", pkt_Einlauf) 

    Fuss = drawSchneckenzug(*rechts.points[17], UPPER_A, 4, HSL_size=2, HSL_start=14, clockwise=False, inward=True)

    FrakturGatE_W = links + mitte + rechts + Fuss 
    drawPath(FrakturGatE_W)
    trans_scale(FrakturGatE_W, valueToMoveGlyph)
    return FrakturGatE_W, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Einlauf')(pkt_Auslauf, pkt_Einlauf)
    
    
def drawFrakturGatE_W_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_W_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=18, clockwise=False, inward=False)
    pkt_Einlauf_vorher = pass_from_thick.pkt_Einlauf[0]-modul_width*3.15, pass_from_thick.pkt_Einlauf[1]
    Einlauf_vorher = drawSchneckenzug(*pkt_Einlauf_vorher, UPPER_E, 2, HSL_size=2, HSL_start=18, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
        
    FrakturGatE_W_thinStroke += Einlauf + Einlauf_vorher + Auslauf + Endpunkt
    drawPath(FrakturGatE_W_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_W_thinStroke) 
    trans_scale(FrakturGatE_W_thinStroke, valueToMoveGlyph)
    return FrakturGatE_W_thinStroke
    
    
    
    
    
       
    
def drawFrakturGatE_X(x, y):

    Hauptbogen_Links = drawSchriftzug_R_Hauptbogen(x, y)
    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x, y)
    
    Hauptbogen_Rechts = drawSchriftzug_X_BogenRechts(x, y)
    Deckung_Rechtsoben = drawSchriftzug_X_DeckungOben(x, y)

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1.5, y-2)
    Querstrich = drawGrundelementB(*Grund_a, 3.5)
    
    FrakturGatE_X = Hauptbogen_Links + Deckung_LinksUnten + Hauptbogen_Rechts + Deckung_Rechtsoben + Querstrich
    trans_scale(FrakturGatE_X, valueToMoveGlyph)
    return FrakturGatE_X
    
    
    


def drawFrakturGatE_Y(x, y):

    FrakturGatE_Y = BezierPath()
    
    links = drawSchriftzug_Y_Bogen_links(x, y)
    rechts = drawSchriftzug_Y_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[-9]
    #text("pkt_Einlauf", pkt_Einlauf)
    pkt_Auslauf = rechts.points[17]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+4, y-5.5)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5.5, y-5.5)
    #liegender_Schwung_unten = drawSchriftteil12(*Raute_a)
    #liegender_Schwung_unten = drawSchriftzug_L_Fuss(x+2.95, y)
    ### Schwung unten als Alternatve
    liegender_Schwung_unten = drawSchriftzug_S_liegenderSchwung(x+6.875, y, outstrokeLen=0)

    FrakturGatE_Y = links + rechts + liegender_Schwung_unten
    trans_scale(FrakturGatE_Y, valueToMoveGlyph)
    return FrakturGatE_Y, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Einlauf')(pkt_Auslauf, pkt_Einlauf)
    
def drawFrakturGatE_Y_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_Y_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=4, HSL_start=20, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 8, HSL_size=1, HSL_start=14, clockwise=False, inward=True, HSL_size_multipliers=[ 1 + 0.075 * i  for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.7, Auslauf.points[-1][1]-modul_height*0.05)

    FrakturGatE_Y_thinStroke += Einlauf + Auslauf + Endpunkt
    drawPath(FrakturGatE_Y_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_Y_thinStroke) 
    trans_scale(FrakturGatE_Y_thinStroke, valueToMoveGlyph)
    return FrakturGatE_Y_thinStroke
    
    
    

    
    
def drawFrakturGatE_Z(x, y):

    FrakturGatE_Z = BezierPath()
    
    oben = drawSchriftzug_Z_Bogen_oben(x, y)
    outstroke = drawOutstroke(*oben.points[-1], 0.25, "down")
    pkt_Auslauf_oben = outstroke.points[0]
    #text("pkt_Auslauf_oben", pkt_Auslauf_oben)
    
    unten = drawSchriftzug_Z_Bogen_unten(x, y)
    pkt_Auslauf_unten = unten.points[-1]
    #text("pkt_Auslauf_unten", pkt_Auslauf_unten)
    
    Deckung_unten = drawSchriftzug_R_Deckung(x, y)

    FrakturGatE_Z = oben + outstroke + unten + Deckung_unten 
    trans_scale(FrakturGatE_Z, valueToMoveGlyph)
    return FrakturGatE_Z, collections.namedtuple('dummy', 'pkt_Auslauf_oben pkt_Auslauf_unten')(pkt_Auslauf_oben, pkt_Auslauf_unten)
    

def drawFrakturGatE_Z_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatE_Z_thinStroke = BezierPath()

    Auslauf_oben = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_oben, LOWER_E, 8, HSL_size=1, HSL_start=12, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf_oben.points[-1][0]-modul_width*0.25, Auslauf_oben.points[-1][1]-modul_height*1.04)
    
    Auslauf_unten = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_unten, LOWER_E, 1, HSL_size=1, HSL_start=20, clockwise=True, inward=True)
    
    FrakturGatE_Z_thinStroke += Auslauf_oben + Endpunkt + Auslauf_unten
    drawPath(FrakturGatE_Z_thinStroke)
    trans_thinStroke_down_left(FrakturGatE_Z_thinStroke) 
    trans_scale(FrakturGatE_Z_thinStroke, valueToMoveGlyph)
    return FrakturGatE_Z_thinStroke




def drawFrakturGatE_dotStrokeUp(x, y, *, pass_from_thick=None):

    FrakturGatE_dotStrokeUp = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Auslauf, 1, "down")
    Auslauf = drawSchneckenzug(*instroke.points[-1], LOWER_E, 8, HSL_size=1, HSL_start=11, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.6, Auslauf.points[-1][1]-modul_height*1.1)
    
    FrakturGatE_dotStrokeUp += instroke + Auslauf + Endpunkt
    drawPath(FrakturGatE_dotStrokeUp)
    trans_thinStroke_up_right(FrakturGatE_dotStrokeUp)
    trans_scale(FrakturGatE_dotStrokeUp, valueToMoveGlyph)    
    return FrakturGatE_dotStrokeUp
    
    
def drawFrakturGatE_dotStrokeDown(x, y, *, pass_from_thick=None):

    FrakturGatE_dotStrokeDown = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Auslauf, 0.75)
    Auslauf = drawSchneckenzug(*instroke.points[-1], UPPER_E, 8, HSL_size=1, HSL_start=14, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.75, Auslauf.points[-1][1]-modul_height*0.05)
    
    FrakturGatE_dotStrokeDown += instroke + Auslauf + Endpunkt
    drawPath(FrakturGatE_dotStrokeDown)
    trans_thinStroke_down_left(FrakturGatE_dotStrokeDown)
    trans_scale(FrakturGatE_dotStrokeDown, valueToMoveGlyph)    
    return FrakturGatE_dotStrokeDown
    
    
    
def drawFrakturGatE_dotStrokeUpDown(x, y, *, pass_from_thick=None):

    FrakturGatE_dotStrokeUpDown = BezierPath()
    
    ### UP
    instroke_up = drawInstroke(*pass_from_thick.pkt_Auslauf_up, 1, "down")
    Auslauf_up = drawSchneckenzug(*instroke_up.points[-1], LOWER_E, 8, HSL_size=1, HSL_start=11, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt_up = drawThinstroke_Endpunkt(Auslauf_up.points[-1][0]-modul_width*0.6, Auslauf_up.points[-1][1]-modul_height*1.1)
    
    ### DOWN
    instroke_down = drawInstroke(*pass_from_thick.pkt_Auslauf_down, 0.75)
    Auslauf_down = drawSchneckenzug(*instroke_down.points[-1], UPPER_E, 8, HSL_size=1, HSL_start=14, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt_down = drawThinstroke_Endpunkt(Auslauf_down.points[-1][0]-modul_width*0.75, Auslauf_down.points[-1][1]-modul_height*0.05)

    FrakturGatE_dotStrokeUp = instroke_up + Auslauf_up + Endpunkt_up
    FrakturGatE_dotStrokeDown = instroke_down + Auslauf_down + Endpunkt_down

    trans_thinStroke_up_right(FrakturGatE_dotStrokeDown)
    trans_thinStroke_down_left(FrakturGatE_dotStrokeDown)  

    FrakturGatE_dotStrokeUpDown = FrakturGatE_dotStrokeUp + FrakturGatE_dotStrokeDown
    drawPath(FrakturGatE_dotStrokeUpDown)
    trans_scale(FrakturGatE_dotStrokeUpDown, valueToMoveGlyph)    
    return FrakturGatE_dotStrokeUpDown
    
    
    
    
    
    




    

# ________________________________________________________
  

margin_1modul = 60
margin_2modul = 120
marginStr = 30   ####48
marginRnd = 66   ###72 
margin_r=-11
margin_g= 67  ### für j
flush = 0  
  
# ____________ ab hier in RF ____________________________
  
    
    
font = CurrentFont()

drawFunctions = {
   # 'a' : [ drawFrakturGatE_a, [temp_x, temp_y], marginRnd, marginStr ],
   # 'b' : [ drawFrakturGatE_b, [temp_x, temp_y], marginStr, marginRnd ],
   #  'c' : [ drawFrakturGatE_c, [temp_x, temp_y], marginRnd, margin_r ],
   #  'd' : [ drawFrakturGatE_d, [temp_x, temp_y, 11], marginRnd, marginRnd ],
   #  'e' : [ drawFrakturGatE_e, [temp_x, temp_y], marginRnd, marginStr ],
   #  'f' : [ drawFrakturGatE_f, [temp_x, temp_y], marginStr, -123 ],
   #  'g' : [ drawFrakturGatE_g, [temp_x, temp_y], marginRnd, margin_g ],
   #  'h' : [ drawFrakturGatE_h, [temp_x, temp_y], marginStr, marginRnd ],
   #  'i' : [ drawFrakturGatE_i, [temp_x, temp_y], marginStr, marginStr ],
   # 'j' : [ drawFrakturGatE_j, [temp_x, temp_y], marginStr-12, margin_g ],
   #  'k' : [ drawFrakturGatE_k, [temp_x, temp_y], marginStr, marginStr-30 ],
   # 'l' : [ drawFrakturGatE_l, [temp_x, temp_y], marginStr, marginStr-12],
   #  'm' : [ drawFrakturGatE_m, [temp_x, temp_y], marginStr, marginStr ],
   #   'n' : [ drawFrakturGatE_n, [temp_x, temp_y], marginStr, marginStr ],
   #  'o' : [ drawFrakturGatE_o, [temp_x, temp_y], marginRnd, marginRnd ],
   #  'p' : [ drawFrakturGatE_p, [temp_x, temp_y], marginStr, marginRnd ],
   #  'q' : [ drawFrakturGatE_q, [temp_x, temp_y], marginRnd, margin_g+11 ],
   #  'r' : [ drawFrakturGatE_r, [temp_x, temp_y], marginStr, margin_r ],
   #  's' : [ drawFrakturGatE_s, [temp_x, temp_y], marginStr, marginStr ],
   #  'longs' : [ drawFrakturGatE_longs, [temp_x, temp_y], marginStr, -153+32 ],
   #  'germandbls' : [ drawFrakturGatE_germandbls, [temp_x, temp_y], marginStr, marginStr-30 ],
    
   #  't' : [ drawFrakturGatE_t, [temp_x, temp_y], marginStr, marginStr-30],
   #  'u' : [ drawFrakturGatE_u, [temp_x, temp_y], marginStr, marginStr ],
   #  'v' : [ drawFrakturGatE_v, [temp_x, temp_y], marginStr, marginRnd ],
   #  'w' : [ drawFrakturGatE_w, [temp_x, temp_y], marginStr, marginRnd ],
   #  'x' : [ drawFrakturGatE_x, [temp_x, temp_y], marginStr, margin_r ],
   #  'y' : [ drawFrakturGatE_y, [temp_x, temp_y], marginStr, marginRnd ],
   #  'z' : [ drawFrakturGatE_z, [temp_x, temp_y, "initial"], marginRnd, marginRnd-30 ],
   #  'z.init' : [ drawFrakturGatE_z, [temp_x, temp_y, "initial"], marginRnd, marginRnd-30 ],
   #  'z.mid' : [ drawFrakturGatE_z, [temp_x, temp_y, "mid"], marginStr-10, marginRnd-20 ],
   #  'z.fina' : [ drawFrakturGatE_z, [temp_x, temp_y, "final"], marginStr, marginRnd ],
     
   #  'adieresis' : [ drawFrakturGatE_adieresis, [temp_x, temp_y], marginRnd, marginStr ],
   #  'odieresis' : [ drawFrakturGatE_odieresis, [temp_x, temp_y], marginRnd, marginRnd ],
   #  'udieresis' : [ drawFrakturGatE_udieresis, [temp_x, temp_y], marginStr, marginStr ],

   #  'b.ss01' : [ drawFrakturGatE_b_dot, [temp_x, temp_y], marginStr, marginRnd ],
   #  'h.ss01' : [ drawFrakturGatE_h_dot, [temp_x, temp_y], marginStr, marginRnd ],
   #  'k.ss01' : [ drawFrakturGatE_k_dot, [temp_x, temp_y], marginStr, marginRnd-66 ],
   #  'l.ss01' : [ drawFrakturGatE_l_dot, [temp_x, temp_y], marginStr, marginRnd-48 ],
   #  't.ss01' : [ drawFrakturGatE_t_dot, [temp_x, temp_y], marginStr, marginRnd-66 ],
   #  'y.ss01' : [ drawFrakturGatE_y_dot, [temp_x, temp_y], marginStr, marginRnd ],
         
   #  #### Ligatures
    
   #  'r_c' : [ drawFrakturGatE_rc, [temp_x, temp_y], marginStr+30, margin_r+30 ],
   #  'longs_longs' : [ drawFrakturGatE_longs_longs, [temp_x, temp_y], marginStr-12, -180+39 ],
   #  'f_f' : [ drawFrakturGatE_f_f, [temp_x, temp_y], marginStr, -123-28 ],
   #  'longs_t' : [ drawFrakturGatE_longs_t, [temp_x, temp_y], marginStr-12, marginStr-30 ],
   #  'f_t' : [ drawFrakturGatE_f_t, [temp_x, temp_y], marginStr, marginStr-30 ],
   #  'f_f_t' : [ drawFrakturGatE_f_f_t, [temp_x, temp_y], marginStr, marginStr-30 ],
    
    
   #  ### Interpunction  
       
   #  'period' : [ drawFrakturGatE_period, [temp_x, temp_y], margin_2modul, margin_2modul ],
   #  'colon' : [ drawFrakturGatE_colon, [temp_x, temp_y], margin_2modul, margin_2modul ],
   #  'semicolon' : [ drawFrakturGatE_semicolon, [temp_x, temp_y], margin_2modul, margin_2modul ],
   #  'quoteright' : [ drawFrakturGatE_quoteright, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'quotesingle' : [ drawFrakturGatE_quotesingle, [temp_x, temp_y], margin_2modul, flush ],
   #  'comma' : [ drawFrakturGatE_comma, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'endash' : [ drawFrakturGatE_endash, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'emdash' : [ drawFrakturGatE_emdash, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'hyphen' : [ drawFrakturGatE_hyphen, [temp_x, temp_y], margin_1modul, margin_1modul ],
   #  'exclam' : [ drawFrakturGatE_exclam, [temp_x, temp_y], margin_2modul, margin_2modul ],
   #  'question' : [ drawFrakturGatE_question, [temp_x, temp_y], margin_2modul, margin_2modul ],
    
    
   #  ### Numbers
    
   #  'zero' : [ drawFrakturGatE_zero, [temp_x, temp_y], marginRnd, marginRnd-60 ],
   #  'one' : [ drawFrakturGatE_one, [temp_x, temp_y], marginStr+140, marginStr ],
   #  'two' : [ drawFrakturGatE_two, [temp_x, temp_y], marginStr+18, marginStr ],
   #  'three' : [ drawFrakturGatE_three, [temp_x, temp_y], marginStr+60, marginStr-30 ],
   #  'four' : [ drawFrakturGatE_four, [temp_x+3, temp_y], 0, marginStr-30 ],
   #  'five' : [ drawFrakturGatE_five, [temp_x, temp_y], marginStr+120, marginStr-90 ],
   #  'six' : [ drawFrakturGatE_six, [temp_x, temp_y], marginRnd, marginStr ],
   #  'seven' : [ drawFrakturGatE_seven, [temp_x, temp_y], marginStr, -45 ],
   #  'eight' : [ drawFrakturGatE_eight, [temp_x, temp_y], marginRnd, marginRnd-30 ],    
   #  'nine' : [ drawFrakturGatE_nine, [temp_x, temp_y], marginRnd, marginStr-15 ],     
    
    
    
   #  #### Uppercase

   #  'A' : [ drawFrakturGatE_A, [temp_x, temp_y], marginStr, marginStr-90 ],
   #  'B' : [ drawFrakturGatE_B, [temp_x, temp_y], marginStr, marginRnd ],
   #  'C' : [ drawFrakturGatE_C, [temp_x, temp_y], marginStr, marginStr-120 ],
   #  'D' : [ drawFrakturGatE_D, [temp_x, temp_y], marginStr, marginRnd ],
   #  'E' : [ drawFrakturGatE_E, [temp_x, temp_y], marginStr, marginStr-90 ],
   #  'F' : [ drawFrakturGatE_F, [temp_x, temp_y], marginStr, marginStr-90 ],
   #  'G' : [ drawFrakturGatE_G, [temp_x, temp_y], marginStr, marginRnd ],
   #  'H' : [ drawFrakturGatE_H, [temp_x, temp_y], marginStr+60, 30 ],
   #  'I' : [ drawFrakturGatE_I, [temp_x, temp_y], marginStr, marginStr-30],
   #  'J' : [ drawFrakturGatE_J, [temp_x, temp_y], marginStr, marginRnd-100],
   #  'K' : [ drawFrakturGatE_K, [temp_x, temp_y], marginRnd, marginStr-60 ],
   #  'L' : [ drawFrakturGatE_L, [temp_x, temp_y], marginStr+60, marginStr-30 ],
   #  'M' : [ drawFrakturGatE_M, [temp_x, temp_y], marginStr, marginStr-90 ],
   #  'N' : [ drawFrakturGatE_N, [temp_x, temp_y], marginStr, marginStr-90 ],
   #  'O' : [ drawFrakturGatE_O, [temp_x, temp_y], marginStr+60, marginStr+30 ],
   #  'P' : [ drawFrakturGatE_P, [temp_x, temp_y], marginStr+30, marginStr-120 ],
   #  'Q' : [ drawFrakturGatE_Q, [temp_x, temp_y], marginStr, marginRnd ],
   #  'R' : [ drawFrakturGatE_R, [temp_x, temp_y], marginStr, marginStr ],
   #  'S' : [ drawFrakturGatE_S, [temp_x, temp_y], marginStr, marginRnd-30 ],
   #  'T' : [ drawFrakturGatE_T, [temp_x, temp_y], marginStr, marginStr-90 ],
   #  'U' : [ drawFrakturGatE_U, [temp_x, temp_y], marginStr, marginStr+10 ],
   #  'V' : [ drawFrakturGatE_V, [temp_x, temp_y], marginStr, marginStr-45 ],
   #  'W' : [ drawFrakturGatE_W, [temp_x, temp_y], marginStr, marginStr-125 ],
   #  'X' : [ drawFrakturGatE_X, [temp_x, temp_y], marginStr, marginStr-90 ],
   #  'Y' : [ drawFrakturGatE_Y, [temp_x, temp_y], marginStr, marginStr-125 ],
   #  'Z' : [ drawFrakturGatE_Z, [temp_x, temp_y], marginStr, marginRnd ],

   #  'C.ss02' : [ drawFrakturGatE_C, [temp_x, temp_y, "Versal"], marginStr, marginStr-125 ],
   #  'C.ss03' : [ drawFrakturGatE_C, [temp_x, temp_y, "Initial"], marginStr, marginStr-150 ],
   #  'C.ss04' : [ drawFrakturGatE_C, [temp_x, temp_y, "Capital"], marginStr, marginStr-200 ],
    
    # 'Adieresis' : [ drawFrakturGatE_Adieresis, [temp_x, temp_y], marginStr, marginStr ],
    # 'Odieresis' : [ drawFrakturGatE_Odieresis, [temp_x, temp_y], marginStr+60, marginStr+30 ],
    # 'Udieresis' : [ drawFrakturGatE_Udieresis, [temp_x, temp_y], marginStr, marginStr ],
    
    }
    



drawFunctions_thinStroke = {

    'f' : [ drawFrakturGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'g' : [ drawFrakturGatE_g_thinStroke, [temp_x, temp_y] ],
    'j' : [ drawFrakturGatE_g_thinStroke, [temp_x, temp_y] ],
    'p' : [ drawFrakturGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'q' : [ drawFrakturGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'x' : [ drawFrakturGatE_x_thinStroke, [temp_x, temp_y] ],
    'z' : [ drawFrakturGatE_z_thinStroke, [temp_x, temp_y] ],
    'z.init' : [ drawFrakturGatE_z_thinStroke, [temp_x, temp_y] ],
    'z.mid' : [ drawFrakturGatE_x_thinStroke, [temp_x, temp_y] ],
    'z.fina' : [ drawFrakturGatE_x_thinStroke, [temp_x, temp_y] ],

    'b.ss01' : [ drawFrakturGatE_dotStrokeUp, [temp_x, temp_y] ],
    'h.ss01' : [ drawFrakturGatE_dotStrokeUpDown, [temp_x, temp_y] ],
    'k.ss01' : [ drawFrakturGatE_dotStrokeUp, [temp_x, temp_y] ],
    'l.ss01' : [ drawFrakturGatE_dotStrokeUp, [temp_x, temp_y] ],
    't.ss01' : [ drawFrakturGatE_dotStrokeUp, [temp_x, temp_y] ],
    'y.ss01' : [ drawFrakturGatE_dotStrokeDown, [temp_x, temp_y] ],
    
    'longs' : [ drawFrakturGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'longs_longs' : [ drawFrakturGatE_longs_longs_thinStroke, [temp_x, temp_y] ],
    'longs_t' : [ drawFrakturGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'f_f' : [ drawFrakturGatE_longs_longs_thinStroke, [temp_x, temp_y] ],
    'f_t' : [ drawFrakturGatE_thinstroke_Straight, [temp_x, temp_y] ],
    'f_f_t' : [ drawFrakturGatE_longs_longs_thinStroke, [temp_x, temp_y] ],

    'germandbls' : [ drawFrakturGatE_germandbls_Endspitze, [temp_x, temp_y] ],
    'quotesingle' : [ drawFrakturGatE_quotesingle_thinstroke, [temp_x, temp_y] ],  


    'A' : [ drawFrakturGatE_A_thinStroke, [temp_x, temp_y] ],
    'B' : [ drawFrakturGatE_B_thinStroke, [temp_x, temp_y] ],
    'C' : [ drawFrakturGatE_C_thinStroke, [temp_x, temp_y] ],
    'C.ss02' : [ drawFrakturGatE_C_thinStroke_Versal, [temp_x, temp_y] ],
    'C.ss03' : [ drawFrakturGatE_C_thinStroke_Initial, [temp_x, temp_y] ],
    'C.ss04' : [ drawFrakturGatE_C_thinStroke_Capital, [temp_x, temp_y] ],
    'D' : [ drawFrakturGatE_C_thinStroke, [temp_x, temp_y] ],
    'E' : [ drawFrakturGatE_C_thinStroke, [temp_x, temp_y] ],
    'F' : [ drawFrakturGatE_F_thinStroke, [temp_x, temp_y] ],
    'G' : [ drawFrakturGatE_G_thinStroke, [temp_x, temp_y] ],
    'H' : [ drawFrakturGatE_H_thinStroke, [temp_x, temp_y] ],
    'I' : [ drawFrakturGatE_F_thinStroke, [temp_x, temp_y] ],
    'J' : [ drawFrakturGatE_J_thinStroke, [temp_x, temp_y] ],
    'L' : [ drawFrakturGatE_C_thinStroke, [temp_x, temp_y] ],
    'M' : [ drawFrakturGatE_M_thinStroke, [temp_x, temp_y] ],
    'N' : [ drawFrakturGatE_N_thinStroke, [temp_x, temp_y] ],
    'O' : [ drawFrakturGatE_C_thinStroke, [temp_x, temp_y] ],
    'P' : [ drawFrakturGatE_P_thinStroke, [temp_x, temp_y] ],
    'Q' : [ drawFrakturGatE_C_thinStroke, [temp_x, temp_y] ],
    'R' : [ drawFrakturGatE_R_thinStroke, [temp_x, temp_y] ],
    'U' : [ drawFrakturGatE_U_thinStroke, [temp_x, temp_y] ],
    'V' : [ drawFrakturGatE_C_thinStroke, [temp_x, temp_y] ],
    'W' : [ drawFrakturGatE_W_thinStroke, [temp_x, temp_y] ],
    'Y' : [ drawFrakturGatE_Y_thinStroke, [temp_x, temp_y] ],
    'Z' : [ drawFrakturGatE_Z_thinStroke, [temp_x, temp_y] ],

    'Adieresis' : [ drawFrakturGatE_C_thinStroke, [temp_x, temp_y] ],
    'Odieresis' : [ drawFrakturGatE_C_thinStroke, [temp_x, temp_y] ],
    'Udieresis' : [ drawFrakturGatE_C_thinStroke, [temp_x, temp_y] ],
    
    'one' : [ drawFrakturGatE_one_thinStroke, [temp_x, temp_y] ],
    'three' : [ drawFrakturGatE_three_thinStroke, [temp_x, temp_y] ],
    'five' : [ drawFrakturGatE_three_thinStroke, [temp_x, temp_y] ],
    'six' : [ drawFrakturGatE_six_thinStroke, [temp_x, temp_y] ],
    'nine' : [ drawFrakturGatE_nine_thinStroke, [temp_x, temp_y] ],
    
    }
    
    
    
    
    
    
    


for key in drawFunctions:
    
    glyph = font[[key][0]]
    glyph.clear()
    
    foreground = glyph.getLayer("foreground")
    foreground.clear()
    background = glyph.getLayer("background")
    background.clear()
     
    function = drawFunctions[key][0]
    arguments = drawFunctions[key][1]

    output = function(*arguments)
	
    if key in drawFunctions_thinStroke:
        
        assert isinstance(output, tuple) and len(output)==2
        pass_from_thick_to_thin = output[1]
        output = output[0]
    
    # assert isinstance(output, glyphContext.GlyphBezierPath)
		
    writePathInGlyph(output, foreground)

    print("___",font[[key][0]])
    
    # ask for current status
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin   
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_before = margin_ts - margin_fg
    
    ### change value of margin
    glyph.leftMargin = drawFunctions[key][2]
    glyph.rightMargin = drawFunctions[key][3]
    
    ### ask inbetween 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_mid = margin_ts - margin_fg

    ### calculate value for movement of bg
    move_thinStroke = difference_before - difference_mid

    glyph.copyLayerToLayer('foreground', 'background')
 
   
    if key in drawFunctions_thinStroke:		
        thinstroke = glyph.getLayer("thinstroke")
        thinstroke.clear()

        thinfunction = drawFunctions_thinStroke[key][0]
        thinarguments = drawFunctions_thinStroke[key][1]
        thinoutput = thinfunction(*thinarguments, pass_from_thick=pass_from_thick_to_thin)
        
        writePathInGlyph(thinoutput, thinstroke)
        
    thinstroke = glyph.getLayer("thinstroke")
    thinstroke.translate((move_thinStroke, 0))
    
    ### ask final 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0 
        #print("(no thin line)")   
    difference_final = margin_ts - margin_fg
    
    foreground.clear()

    
    
    
    ### now draw the whole glphy with the magic tool
    ### first thick main stroke 
    guide_glyph = glyph.getLayer("background")

    w = A_A * penWidth 
    a = radians(90-alpha)
    h = penThickness

    if True:
        RectNibPen = RectNibPen_Just

    p = RectNibPen(
        glyphSet=CurrentFont(),
        angle=a,
        width=w,
        height=penThickness,
        trace=True
    )

    guide_glyph.draw(p)
    p.trace_path(glyph)


    ### now thin Stroke
    if key in drawFunctions_thinStroke:		

        guide_glyph = glyph.getLayer("thinstroke")
        w = penThickness 
        p = RectNibPen(
            glyphSet=CurrentFont(),
            angle=a,
            width=w,
            height=penThickness,
            trace=True
        )
        guide_glyph.draw(p)
        p.trace_path(glyph)

    ### keep a copy in background layer
    newLayerName = "original_math"
    glyph.layers[0].copyToLayer(newLayerName, clear=True)
    glyph.getLayer(newLayerName).leftMargin = glyph.layers[0].leftMargin
    glyph.getLayer(newLayerName).rightMargin = glyph.layers[0].rightMargin

    ### this will make it all one shape
    #glyph.removeOverlap(round=0)
    
    ### from robstevenson to remove extra points on curves
    ### function itself is in create_stuff.py
    #select_extra_points(glyph, remove=True)



    
    
    
    
    
    
    
    
    

    