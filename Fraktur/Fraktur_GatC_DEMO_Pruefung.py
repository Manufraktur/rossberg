import importlib
import version_3.creation.Schriftteile_GatC
import version_3.creation.Schwuenge_GatC
import Fraktur_Schriftzuege_GatC

importlib.reload(version_3.creation.Schriftteile_GatC)
importlib.reload(version_3.creation.Schwuenge_GatC)
importlib.reload(Fraktur_Schriftzuege_GatC)

from version_3.creation.Schriftteile_GatC import *
from version_3.creation.Schwuenge_GatC import *
from Fraktur_Schriftzuege_GatC import *

from version_3.creation.special_drawbot import BezierPath, drawPath


# ___________ Bedienelement Slider _______________

# Variable([
#     dict(name="version", ui="Slider",
#         args=dict(
#             value=0,
#             minValue=0,
#             maxValue=1)),
#     ], globals())
# ________________________________________________

### True >>> modified code         >>> 1
### False >>> original Roßberg     >>> 0
version_mod = True  # Eingabe durch Slider

if version_mod == True:
    #print("Roßberg modifizert")
    import version_3.creation.Schriftteile_GatC_mod
    import Fraktur_Schriftzuege_GatC_mod
    importlib.reload(version_3.creation.Schriftteile_GatC_mod)
    importlib.reload(Fraktur_Schriftzuege_GatC_mod)
    from version_3.creation.Schriftteile_GatC_mod import *
    from Fraktur_Schriftzuege_GatC_mod import *
else:
    print("Roßberg original")
    

from nibLib.pens.rectNibPen import RectNibPen
from version_3.creation.rect_nib_pen import RectNibPen as RectNibPen_Just
from math import radians
import math as m
import collections
import glyphContext






# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 20
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)








# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)
    




# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)



stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 9



# ________________________________________________________


  

def drawFrakturGatC_a(x, y):      
    
    Bogen_links = drawSchriftzug7(x, y, outstrokeLen=2.4)
    Strich_rechts = drawSchriftzug4(x+3, y, instrokeLen=0)
    
    FrakturGatC_a = Bogen_links + Strich_rechts
    trans_scale(FrakturGatC_a, valueToMoveGlyph)
    return FrakturGatC_a
 


    
    
def drawFrakturGatC_b(x,y):        
    
    Strich_links = drawSchriftzug5(x, y, top="ascender")
    Bogen_rechts = drawSchriftzug8(x+3, y, instrokeLen=2.5)
    
    FrakturGatC_b = Strich_links + Bogen_rechts
    trans_scale(FrakturGatC_b, valueToMoveGlyph)
    return FrakturGatC_b
    
    
def drawFrakturGatC_b_dot(x, y):      
    #y += -3
    glyph_b = drawFrakturGatC_b(x, y)
    trans_scale_invert(glyph_b, valueToMoveGlyph)

    pkt_Auslauf = glyph_b.points[5]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatC_b_dot = glyph_b
    trans_scale(FrakturGatC_b_dot, valueToMoveGlyph)
    return FrakturGatC_b_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
    


def drawFrakturGatC_c(x,y, outstrokeLen=0.5):
           
    Bogen_links = drawSchriftzug6(x, y, outstrokeLen=1)
    Haeckchen_oben = drawSchriftteil2(*Bogen_links.points[0])
    outstroke = drawOutstroke(*Haeckchen_oben.points[-1], outstrokeLen)

    FrakturGatC_c = Bogen_links + Haeckchen_oben + outstroke
    trans_scale(FrakturGatC_c, valueToMoveGlyph)
    return FrakturGatC_c
    
    
    



def drawFrakturGatC_d(x, y, Endspitze=12):      

    Bogen_links = drawSchriftzug6(x, y)
    Bogen_rechts = drawSchwung6(x, y)
    
    FrakturGatC_d = Bogen_links + Bogen_rechts
    trans_scale(FrakturGatC_d, valueToMoveGlyph)
    return FrakturGatC_d
    

 


def drawFrakturGatC_e(x, y):
    
    FrakturGatC_e = BezierPath()      
    
    Bogen_links = drawSchriftzug6(x, y, outstrokeLen=1.5)
    Haken_rechts = drawSchriftteil8(*Bogen_links.points[0])
    outstroke_oben = drawOutstroke(*Haken_rechts.points[-1], 1.5, "down")

    FrakturGatC_e = Bogen_links + Haken_rechts + outstroke_oben 
    trans_scale(FrakturGatC_e, valueToMoveGlyph)
    return FrakturGatC_e
    
    
       

 
    
def drawFrakturGatC_f(x,y):

    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    trans_scale(Querstrich, valueToMoveGlyph)

    FrakturGatC_longs = drawFrakturGatC_longs(x, y)[0]
    values_to_return = drawFrakturGatC_longs(x, y)[1]
    FrakturGatC_f = FrakturGatC_longs + Querstrich
    return FrakturGatC_f, values_to_return
    
    
    
    
    
    
def drawFrakturGatC_f_f(x, y):
    
    Querstrich_left = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    Querstrich_right = drawGrundelementB((x+1.5)*modul_width, (y+0.5)*modul_height, length=2)
    Querstrich = Querstrich_left + Querstrich_right
    trans_scale(Querstrich, valueToMoveGlyph)
    
    ligature_longs_longs = drawFrakturGatC_longs_longs(x, y)[0]
    values_to_return = drawFrakturGatC_longs_longs(x, y)[1]
    
    FrakturGatC_f_f = ligature_longs_longs + Querstrich
    return FrakturGatC_f_f, values_to_return
    
     
     
     
     

def drawFrakturGatC_f_t(x, y):
        
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    trans_scale(Querstrich, valueToMoveGlyph)
        
    glyph_longs_t = drawFrakturGatC_longs_t(x, y)[0]
    values_to_return = drawFrakturGatC_longs_t(x, y)[1]
    
    FrakturGatC_f_t = glyph_longs_t + Querstrich
    return FrakturGatC_f_t, values_to_return 
    

    
    
def drawFrakturGatC_f_f_t(x, y):
    
    Querstrich_links = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, length=2)
    Querstrich_rechts = drawGrundelementB((x+1.5)*modul_width, (y+0.5)*modul_height, length=2)

    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=2, ligatureLen=2)
    pkt_Ausstrich = main_stroke_left.points[9]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    pkt_Auslauf = main_stroke_left.points[-7]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    con = drawSchriftzug_f_f_con(x, y+0.25)
    main_stroke_middle = drawSchriftzug2(x+3, y, instrokeLen=1, ligatureLen=2)

    Spitze = drawSchriftzug_f_t_con(x, y)
    
    FrakturGatC_f_f_t = main_stroke_left + con + main_stroke_middle + Spitze + Querstrich_links + Querstrich_rechts
    trans_scale(FrakturGatC_f_f_t, valueToMoveGlyph)
        
    glyph_t = drawFrakturGatC_t(x+6, y)
    FrakturGatC_f_f_t += glyph_t
    
    return FrakturGatC_f_f_t, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
        

    
    

    
def drawFrakturGatC_g(x, y):

    Bogen_links = drawSchriftzug7(x, y, outstrokeLen=2.5) 
    Strich_rechts = drawSchriftzug4(x+3, y, bottom="Schwung")
    pkt_Auslauf = Strich_rechts.points[7]
    #text("pkt_Auslauf", pkt_Auslauf)

    FrakturGatC_g = Bogen_links + Strich_rechts
    trans_scale(FrakturGatC_g, valueToMoveGlyph)
    return FrakturGatC_g, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    

    
def drawFrakturGatC_g_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_g_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.1)
    
    FrakturGatC_g_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatC_g_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_g_thinStroke)
    trans_scale(FrakturGatC_g_thinStroke, valueToMoveGlyph)    
    return FrakturGatC_g_thinStroke
    








def drawFrakturGatC_h(x, y):      
    
    Strich_links = drawSchriftzug5(x, y, top="ascender", outstrokeLen=0)
    Bogen_rechts = drawSchriftzug9(x+3, y)
    
    FrakturGatC_h = Strich_links + Bogen_rechts
    trans_scale(FrakturGatC_h, valueToMoveGlyph)
    return FrakturGatC_h
    
    
def drawFrakturGatC_h_dot(x, y):      
    #y += -3
    glyph_h = drawFrakturGatC_h(x, y)
    trans_scale_invert(glyph_h, valueToMoveGlyph)

    pkt_Auslauf_up = glyph_h.points[5]
    #text("pkt_Auslauf_up", pkt_Auslauf_up)
    pkt_Auslauf_down = glyph_h.points[-1]
    #text("pkt_Auslauf_down", pkt_Auslauf_down)
        
    FrakturGatC_h_dot = glyph_h
    trans_scale(FrakturGatC_h_dot, valueToMoveGlyph)
    return FrakturGatC_h_dot, collections.namedtuple('dummy', 'pkt_Auslauf_up pkt_Auslauf_down')(pkt_Auslauf_up, pkt_Auslauf_down)
    
    

    
    
    
    
        

def drawFrakturGatC_i(x, y):      
    
    #FrakturGatC_i = BezierPath()
    Strich = drawSchriftzug4(x, y)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2)
    iPunkt = drawSchriftteil2(*Raute_a)
    
    FrakturGatC_i = Strich + iPunkt
    trans_scale(FrakturGatC_i, valueToMoveGlyph)
    return FrakturGatC_i
        
    

        
    
def drawFrakturGatC_j(x, y):      
    
    x  += 3
    Strich = drawSchriftzug4(x, y, bottom="Schwung")
    pkt_Auslauf = Strich.points[7]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2)
    Punkt = drawSchriftteil2(*Raute_a)
    
    FrakturGatC_j = Strich + Punkt
    trans_scale(FrakturGatC_j, valueToMoveGlyph)
    return FrakturGatC_j, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    




def drawFrakturGatC_k(x, y):
    
    Strich_links = drawSchriftzug5(x, y, top="ascender", outstrokeLen=1)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.25)

    Schleife_instroke = drawInstroke(*Grund_a, 1.5, "down")
    Schleife = drawSchriftteil8(*Schleife_instroke.points[-1])
    Schleife_outstroke = drawOutstroke(*Schleife.points[-1], 1.5, "down")

    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)
    
    FrakturGatC_k = Strich_links + Schleife_instroke + Schleife + Schleife_outstroke + Querstrich
    trans_scale(FrakturGatC_k, valueToMoveGlyph)
    return FrakturGatC_k
    
    
def drawFrakturGatC_k_dot(x, y):      
    #y += -3
    glyph_k = drawFrakturGatC_k(x, y)
    trans_scale_invert(glyph_k, valueToMoveGlyph)

    pkt_Auslauf = glyph_k.points[5]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatC_k_dot = glyph_k
    trans_scale(FrakturGatC_k_dot, valueToMoveGlyph)
    return FrakturGatC_k_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
        
        
def drawFrakturGatC_l(x, y):    
    
    Schriftzug5 = drawSchriftzug5(x, y, top="ascender", outstrokeLen=0.5)
    
    FrakturGatC_l = Schriftzug5
    trans_scale(FrakturGatC_l, valueToMoveGlyph)
    return FrakturGatC_l
    
    
def drawFrakturGatC_l_dot(x, y):      
    #y += -3
    glyph_l = drawFrakturGatC_l(x, y)
    trans_scale_invert(glyph_l, valueToMoveGlyph)

    pkt_Auslauf = glyph_l.points[5]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatC_l_dot = glyph_l
    trans_scale(FrakturGatC_l_dot, valueToMoveGlyph)
    return FrakturGatC_l_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
        
        
def drawFrakturGatC_m(x, y):
    
    Strich_links = drawSchriftzug5(x, y, outstrokeLen=0)
    
    # Raute Abstand 2. Strich Mitte
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y)
        
    Strich_mitte = drawSchriftzug5(x+3, y, instrokeLen=2.5, outstrokeLen=0)
    
    # Raute Abstand 3. Strich rechts
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y)
        
    Strich_rechts = drawSchriftzug4(x+6, y, instrokeLen=2.5)
    
    FrakturGatC_m = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(FrakturGatC_m, valueToMoveGlyph) 
    return FrakturGatC_m
    







def drawFrakturGatC_n(x, y):

    FrakturGatC_n = BezierPath()
    
    Strich_links = drawSchriftzug5(x, y, outstrokeLen=0)

    # Raute Abstand 2. Strich
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y)
    Strich_rechts = drawSchriftzug4(x+3, y, instrokeLen=2.5)

    FrakturGatC_n = Strich_links + Strich_rechts
    trans_scale(FrakturGatC_n, valueToMoveGlyph)    
    return FrakturGatC_n
    





def drawFrakturGatC_o(x, y):
        
    # Weil aber bei Zusammensetzung der beiden Schrift-Züge Fig. VI. und VIII. das daraus
    # entstehende o in der Gattung C durch ihre starken Krümmungen zu viel Zwischenraum erhalten, [...] 
    # so muß hier in C jedem dieser Schrift-Züge 1/4 Bestandtheil Gerades mehr eingesetzet, 
    # und dann nur 3. halbe Breiten oder 1 1/2 Hauptmaas Zwischenraum (nehmlich nach den  
    # Grundbestandtheilen dieser gebogenen Schrift-Züge gemessen,) dazu genommen werden.    
    
    Bogen_links = drawSchriftzug6(x, y, "for o", outstrokeLen=0)
    Bogen_rechts = drawSchriftzug8(x+2.5, y, "for o", outstrokeLen=1.225)
    
    FrakturGatC_o = Bogen_links + Bogen_rechts
    trans_scale(FrakturGatC_o, valueToMoveGlyph) 
    return FrakturGatC_o
    
    
    
    

    
def drawFrakturGatC_p(x, y):
        
    # Weil aber bei Zusammensetzung der beiden Schrift-Züge Fig. VI. und VIII. das daraus
    # entstehende o in der Gattung C durch ihre starken Krümmungen zu viel Zwischenraum erhalten, [...] 
    # so muß hier in C jedem dieser Schrift-Züge 1/4 Bestandtheil Gerades mehr eingesetzet, 
    # und dann nur 3. halbe Breiten oder 1 1/2 Hauptmaas Zwischenraum (nehmlich nach den  
    # Grundbestandtheilen dieser gebogenen Schrift-Züge gemessen,) dazu genommen werden.
   
    Strich_links = drawSchriftzug3(x, y, bottom="Kehlung")
    pkt_Ausstrich = Strich_links.points[-1]
    
    Bogen_rechts = drawSchriftzug8(x+3, y, instrokeLen=2.5)
    
    # Positionierung Signatur
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, baseline)
    Signatur = drawSchriftteil1(*Raute_d, "CCW")
        
    FrakturGatC_p = Strich_links + Signatur + Bogen_rechts
    trans_scale(FrakturGatC_p, valueToMoveGlyph) 
    return FrakturGatC_p, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    


    
    
def drawFrakturGatC_q(x, y):      
    
    Bogen_links = drawSchriftzug7(x, y)
    Strich_rechts = drawSchriftzug4(x+3, y, bottom="Kehlung")
    pkt_Ausstrich = Strich_rechts.points[13]
    #text("pkt_Ausstrich", pkt_Ausstrich)
  
    FrakturGatC_q = Bogen_links + Strich_rechts
    trans_scale(FrakturGatC_q, valueToMoveGlyph) 
    return FrakturGatC_q, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)






def drawFrakturGatC_r(x, y):       
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y)

    Strich = drawSchriftzug5(x, y, outstrokeLen=0)
    Verbindung = drawInstroke(*Raute_a, 1.5)
    Haken = drawSchriftteil2(*Raute_a)
    outstroke = drawOutstroke(*Haken.points[3])

    FrakturGatC_r = Strich + Verbindung + Haken + outstroke
    trans_scale(FrakturGatC_r, valueToMoveGlyph) 
    return FrakturGatC_r
    

    

def drawFrakturGatC_rc(x, y):       
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, baseline-0.35)

    Strich = drawSchriftzug8(x, y, Einsatz="1", outstrokeLen=0)
    Fuss = drawSchriftteil11(*Raute_a)
    
    FrakturGatC_rc = Strich + Fuss
    trans_scale(FrakturGatC_rc, valueToMoveGlyph) 
    
    letter_c = drawFrakturGatC_c(x+3, y, outstrokeLen=0)
    FrakturGatC_rc += letter_c
    return FrakturGatC_rc
    
    
    
    
    
    
    
def drawFrakturGatC_s(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-3)
    Bogen_oben = drawHalbbogen8(*Raute_a, instrokeLen=1.72, outstrokeLen=2)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y)
    Deckung_oben = drawSchriftteil2(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2, y-2)
    Bogen_unten = drawHalbbogen4(*Raute_a, outstrokeLen=1.67)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1, y-5)
    Deckung_unten = drawSchriftteil1(*Raute_d)
    
    FrakturGatC_s = Bogen_oben + Bogen_unten + Deckung_oben + Deckung_unten
    trans_scale(FrakturGatC_s, valueToMoveGlyph) 
    return FrakturGatC_s
    
      
  

    

def drawFrakturGatC_longs(x, y):
        
    main_stroke_down = drawSchriftzug2(x, y, instrokeLen=1.5)
    pkt_Ausstrich = main_stroke_down.points[9]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.55, y+3.26)
    Spitze = drawSchriftteil11(*Raute_a)

    FrakturGatC_longs = main_stroke_down + Spitze
    trans_scale(FrakturGatC_longs, valueToMoveGlyph)
    return FrakturGatC_longs, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    
    

    

def drawFrakturGatC_longs_longs(x, y):
        
    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=2.5)
    pkt_Ausstrich = main_stroke_left.points[9]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    pkt_Auslauf = main_stroke_left.points[-7]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    main_stroke_right = drawSchriftzug2(x+3, y, instrokeLen=1.5)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+6.5, y+3.235)
    Spitze = drawSchriftteil11(*Raute_a)
    con = drawSchriftzug_longs_longs_con(x, y)

    FrakturGatC_longs_longs = main_stroke_left + main_stroke_right + con + Spitze
    trans_scale(FrakturGatC_longs_longs, valueToMoveGlyph)
    return FrakturGatC_longs_longs, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
    

def drawFrakturGatC_longs_longs_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatC_longs_longs_thinStroke = BezierPath()   

    Auslauf_oben = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 4, HSL_size=1, HSL_start=10, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 5)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf_oben.points[-1][0]-modul_width*0.5, Auslauf_oben.points[-1][1]-modul_height*0.6)

    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
    trans_thinStroke_down_left(Zierstrich) 
    trans_thinStroke_up_right(Auslauf_oben)
        
    FrakturGatC_longs_longs_thinStroke += Auslauf_oben + Zierstrich + Endpunkt

    drawPath(FrakturGatC_longs_longs_thinStroke)
    trans_scale(FrakturGatC_longs_longs_thinStroke, valueToMoveGlyph)
    return FrakturGatC_longs_longs_thinStroke   
    
    
    
    
    
    


def drawFrakturGatC_longs_t(x, y):
        
    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=1, ligatureLen=2)
    pkt_Ausstrich = main_stroke_left.points[9]
    #text("pkt_Ausstrich", pkt_Ausstrich)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.5, y+2.515)
    Spitze = drawSchriftteil12(*Raute_a)
    
    FrakturGatC_longs_t = main_stroke_left + Spitze
    trans_scale(FrakturGatC_longs_t, valueToMoveGlyph)
        
    glyph_t = drawFrakturGatC_t(x+3, y)
    FrakturGatC_longs_t += glyph_t
    return FrakturGatC_longs_t, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    

def drawFrakturGatC_germandbls(x,y):
    
    FrakturGatC_germandbls = BezierPath()

    main_stroke_left = drawSchriftzug2(x, y, instrokeLen=1, ligatureLen=2)
    pkt_Ausstrich = main_stroke_left.points[9]
    #text("pkt_Ausstrich", pkt_Ausstrich)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3.5, y+2.5)
    Spitze = drawSchriftteil12(*Raute_a)
           
       
    ### rechter Teil        
    Halbbogen_oben = drawSchriftzug_z_Initial(x+2.5, y-1)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y-2.5)
    Halbbogen_unten = drawSchneckenzug(*Raute_a, UPPER_G, 6, HSL_size=1, HSL_start=10, clockwise=True, inward=False)
    instroke = drawInstroke(*Halbbogen_unten.points[0], 0.5)
    outstroke = drawOutstroke(*Halbbogen_unten.points[-1], 0.25, "down")
    
    pkt_Auslauf = outstroke.points[0]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatC_germandbls += main_stroke_left + Spitze + Halbbogen_oben + Halbbogen_unten + instroke + outstroke

    drawPath(FrakturGatC_germandbls)
    trans_scale(FrakturGatC_germandbls, valueToMoveGlyph)
    return FrakturGatC_germandbls, collections.namedtuple('dummy', 'pkt_Ausstrich pkt_Auslauf')(pkt_Ausstrich, pkt_Auslauf)
    
    
    
    

def drawFrakturGatC_germandbls_Endspitze(x, y, *, pass_from_thick=None):
    
    FrakturGatC_germandbls_Endspitze = BezierPath()   
    
    Endspitze = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 7, HSL_size=1, HSL_start=13, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 8)])
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
    #FrakturGatC_germandbls_Endspitze.oval(Endspitze.points[-1][0]-modul_width*0.5, Endspitze.points[-1][1]-modul_height*0.015, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Endspitze.points[-1][0]-modul_width*0.5, Endspitze.points[-1][1]-modul_height*0.015)

    FrakturGatC_germandbls_Endspitze += Endspitze + Zierstrich + Endpunkt   
    drawPath(FrakturGatC_germandbls_Endspitze)
    trans_thinStroke_down_left(FrakturGatC_germandbls_Endspitze)
    trans_scale(FrakturGatC_germandbls_Endspitze, valueToMoveGlyph)
    return FrakturGatC_germandbls_Endspitze   
      
    
    
    
    
def drawFrakturGatC_t(x, y):    
    
    Strich = drawSchriftzug3(x, y, top="ascender")
    Querstrich = drawGrundelementB((x-1.5)*modul_width, (y+0.5)*modul_height, 2)
    
    FrakturGatC_t = Strich + Querstrich
    trans_scale(FrakturGatC_t, valueToMoveGlyph)
    return FrakturGatC_t
    

def drawFrakturGatC_t_dot(x, y):      
    #y += -3
    glyph_t = drawFrakturGatC_t(x, y)
    trans_scale_invert(glyph_t, valueToMoveGlyph)

    pkt_Auslauf = glyph_t.points[5]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatC_t_dot = glyph_t
    trans_scale(FrakturGatC_t_dot, valueToMoveGlyph)
    return FrakturGatC_t_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
    
    
    

def drawFrakturGatC_u(x, y):
    
    Strich_links = drawSchriftzug5(x, y, outstrokeLen=2.5)
    Strich_rechts = drawSchriftzug4(x+3, y, instrokeLen=0)
    
    FrakturGatC_u = Strich_links + Strich_rechts
    trans_scale(FrakturGatC_u, valueToMoveGlyph)
    return FrakturGatC_u
    
    
    
    
    
    
def drawFrakturGatC_adieresis(x, y):
    
    Dieresis = drawDieresis(x+0.5, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_a = drawFrakturGatC_a(x, y)
        
    FrakturGatC_adieresis = Grundschriftzug_a + Dieresis
    return FrakturGatC_adieresis
    

def drawFrakturGatC_odieresis(x, y):
    
    Dieresis = drawDieresis(x+0.5, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_o = drawFrakturGatC_o(x, y)
        
    FrakturGatC_odieresis = Grundschriftzug_o + Dieresis
    return FrakturGatC_odieresis
    

def drawFrakturGatC_udieresis(x, y):
    
    Dieresis = drawDieresis(x+0.5, y)
    trans_scale(Dieresis, valueToMoveGlyph)
    
    Grundschriftzug_u = drawFrakturGatC_u(x, y)
        
    FrakturGatC_udieresis = Grundschriftzug_u + Dieresis
    return FrakturGatC_udieresis
    
    
    
    
    
    
def drawFrakturGatC_v(x, y):
    
    Strich_links = drawSchriftzug5(x, y, outstrokeLen=0)
    Strich_rechts = drawSchriftzug8(x+3, y, instrokeLen=2.5)
    
    FrakturGatC_v = Strich_links + Strich_rechts
    trans_scale(FrakturGatC_v, valueToMoveGlyph)
    return FrakturGatC_v
    
    
    

    
def drawFrakturGatC_w(x, y):
    
    Strich_links = drawSchriftzug5(x, y, outstrokeLen=0)
    Strich_mitte = drawSchriftzug5(x+3, y, instrokeLen=2.5)
    Strich_rechts = drawSchriftzug8(x+6, y, instrokeLen=2.5)

    FrakturGatC_w = Strich_links + Strich_mitte + Strich_rechts
    trans_scale(FrakturGatC_w, valueToMoveGlyph)
    return FrakturGatC_w
    
    
    
    
    
def drawFrakturGatC_x(x, y):
    
    glyph_r = drawFrakturGatC_r(x, y)
    trans_scale_invert(glyph_r, valueToMoveGlyph)  ### erstmal rückwärts skalieren um den hook zu positionieren!
    
    pkt_Ausstrich = glyph_r.points[27]
    #text("pkt_Ausstrich", pkt_Ausstrich)

    FrakturGatC_x = glyph_r 
    trans_scale(FrakturGatC_x, valueToMoveGlyph)
    return FrakturGatC_x, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)

    
    
    
def drawFrakturGatC_x_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatC_x_thinStroke = BezierPath()

    hook = drawSchneckenzug(*pass_from_thick.pkt_Ausstrich, UPPER_E, 15, HSL_size=1, HSL_start=17, clockwise=False, inward=True)
    Endpunkt = drawThinstroke_Endpunkt(hook.points[-1][0]-modul_width*0.7, hook.points[-1][1]-modul_height*1.075)

    FrakturGatC_x_thinStroke += hook + Endpunkt
    drawPath(FrakturGatC_x_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_x_thinStroke)
    trans_scale(FrakturGatC_x_thinStroke, valueToMoveGlyph)
    return FrakturGatC_x_thinStroke
      

    
    
    



def drawFrakturGatC_y(x, y):
    
    Strich_links = drawSchriftzug5(x, y, outstrokeLen=0)
    Strich_rechts = drawSchriftzug9(x+3, y, instrokeLen=2.5)
    
    FrakturGatC_y = Strich_links + Strich_rechts
    trans_scale(FrakturGatC_y, valueToMoveGlyph)
    return FrakturGatC_y
    

def drawFrakturGatC_y_dot(x, y):      
    
    glyph_y = drawFrakturGatC_y(x, y)
    trans_scale_invert(glyph_y, valueToMoveGlyph)

    pkt_Auslauf = glyph_y.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    FrakturGatC_y_dot = glyph_y
    trans_scale(FrakturGatC_y_dot, valueToMoveGlyph)
    return FrakturGatC_y_dot, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    


def drawFrakturGatC_z(x,y, style="final"):
    #x +=3    
    FrakturGatC_z = BezierPath()
        
    if style == "initial":
        Halbbogen_oben = drawSchriftzug_z_Initial(x, y)
        pkt_Ausstrich = Halbbogen_oben.points[-1]
        #text("pkt_Auslauf", pkt_Auslauf) 
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, y-1.5)
        Halbbogen_unten = drawSchneckenzug(*Raute_a, UPPER_G, 6, HSL_size=1, HSL_start=10, clockwise=True, inward=False)
        instroke = drawInstroke(*Halbbogen_unten.points[0], 0.5)
        outstroke = drawOutstroke(*Halbbogen_unten.points[-1], 1.75, "down")

        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-2.5, y-5)
        Deckung_unten = drawSchriftteil1(*Raute_d)
        FrakturGatC_z += instroke + outstroke + Deckung_unten
    
        drawPath(Halbbogen_unten)


    if style == "mid":
        Halbbogen_oben = drawSchriftzug_z_Initial(x-0.5, y-1.75)
        pkt_Ausstrich = Halbbogen_oben.points[-1]
        #text("pkt_Auslauf", pkt_Auslauf) 
        
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, y-5.5)
        Halbbogen_unten = drawSchneckenzug(*Raute_a, UPPER_G, 6, HSL_size=1, HSL_start=10, clockwise=True, inward=False)
        instroke = drawInstroke(*Halbbogen_unten.points[0], 0.5)
        outstroke = drawOutstroke(*Halbbogen_unten.points[-1], 1.75, "down")

        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-3.25)
        Halbbogen_unten = drawHalbbogen5(*Raute_a)
        pkt_Ausstrich = Halbbogen_unten.points[-1]
        #text("pkt_Ausstrich", pkt_Ausstrich) 
        drawPath(Halbbogen_unten)
        
        
   
    if style == "final":
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
        Halbbogen_oben = drawHalbbogen4(*Raute_a)
    
        Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-3.5)
        Halbbogen_unten = drawHalbbogen5(*Raute_a)
        pkt_Ausstrich = Halbbogen_unten.points[-1]
        #text("pkt_Ausstrich", pkt_Ausstrich) 


    FrakturGatC_z += Halbbogen_oben + Halbbogen_unten
    trans_scale(FrakturGatC_z, valueToMoveGlyph)
    return FrakturGatC_z, collections.namedtuple('dummy', 'pkt_Ausstrich')(pkt_Ausstrich)
    
    

def drawFrakturGatC_z_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatC_z_thinStroke = BezierPath()

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Ausstrich, LOWER_E, 10, HSL_size=1, HSL_start=12, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 11)])
    #FrakturGatC_z_thinStroke.oval(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.05, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.45, Auslauf.points[-1][1]-modul_height*1.05)

    FrakturGatC_z_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatC_z_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_z_thinStroke)
    trans_scale(FrakturGatC_z_thinStroke, valueToMoveGlyph)
    return FrakturGatC_z_thinStroke      
        







def drawFrakturGatC_thinstroke_Straight(x, y, *, pass_from_thick=None):     
    
    FrakturGatC_thinstroke_Straight = BezierPath()
    
    Zierstrich = drawThinLineBtm(*pass_from_thick.pkt_Ausstrich, 3)
          
    FrakturGatC_thinstroke_Straight +=  Zierstrich
    trans_thinStroke_down_left(FrakturGatC_thinstroke_Straight)
    trans_scale(FrakturGatC_thinstroke_Straight, valueToMoveGlyph)
    return FrakturGatC_thinstroke_Straight
    
    

    
    
    
    
    
    
    
###################################
###### ab hier Interpunktion ######
###################################
    
    
    
    
def drawFrakturGatC_period(x, y):
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    period = drawGrundelementC(*Raute_a)
        
    FrakturGatC_period = period
    trans_scale(FrakturGatC_period, valueToMoveGlyph)
    return FrakturGatC_period
    
    
    
    
    
def drawFrakturGatC_colon(x, y):
        
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    colon_top = drawGrundelementC(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)    
    colon_btm = drawGrundelementC(*Raute_a)  
            
    FrakturGatC_colon = colon_top + colon_btm
    trans_scale(FrakturGatC_colon, valueToMoveGlyph)    
    return FrakturGatC_colon
    
    
    
    
    
def drawFrakturGatC_semicolon(x, y):
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)    
    semicolon_top = drawGrundelementC(*Raute_a)      
    semicolon_btm = drawSchriftzug_semicolon_btm(x-0.5, baseline+0.25)  

    FrakturGatC_semicolon = semicolon_top + semicolon_btm
    trans_scale(FrakturGatC_semicolon, valueToMoveGlyph)    
    return FrakturGatC_semicolon
    
    
    
    

def drawFrakturGatC_quoteright(x, y):

    quoteright = drawSchriftzug_semicolon_btm(x, y+0.75) 
    
    FrakturGatC_quoteright = quoteright
    trans_scale(FrakturGatC_quoteright, valueToMoveGlyph)    
    return FrakturGatC_quoteright
    




    
def drawFrakturGatC_quotesingle(x, y):

    FrakturGatC_quotesingle = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+1.5)
    fake = drawGrundelementA(Grund_c[0], Grund_c[1]-10, 0)   ### this has to be deleted manually
    pkt_fake = Grund_c
    
    FrakturGatC_quotesingle = fake
    drawPath(FrakturGatC_quotesingle)
    trans_scale(FrakturGatC_quotesingle, valueToMoveGlyph)
    return FrakturGatC_quotesingle, collections.namedtuple('dummy', 'pkt_fake')(pkt_fake)
    

def drawFrakturGatC_quotesingle_thinstroke(x, y, *, pass_from_thick=None):

    FrakturGatC_quotesingle_thinstroke = BezierPath()

    quotesingle = drawSchneckenzug(*pass_from_thick.pkt_fake, LOWER_B, 3, HSL_size=2, HSL_start=6, clockwise=True, inward=False)
    Endpunkt = drawThinstroke_Endpunkt(pass_from_thick.pkt_fake[0]-modul_width*1.11, pass_from_thick.pkt_fake[1]-modul_height*0.55)

    FrakturGatC_quotesingle_thinstroke += quotesingle + Endpunkt
    drawPath(FrakturGatC_quotesingle_thinstroke)
    trans_scale(FrakturGatC_quotesingle_thinstroke, valueToMoveGlyph)
    return FrakturGatC_quotesingle_thinstroke
    
    
    
    
    
    
def drawFrakturGatC_comma(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-4.5)
    comma = drawGrundelementG(*Grund_a, 2, "down") 

    FrakturGatC_comma = comma
    trans_scale(FrakturGatC_comma, valueToMoveGlyph)    
    return FrakturGatC_comma
    
    



    
def drawFrakturGatC_endash(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-5.25)

    endash = drawGrundelementB(*Grund_a, 2) 

    FrakturGatC_endash = endash
    trans_scale(FrakturGatC_endash, valueToMoveGlyph)
    return FrakturGatC_endash
    
    

def drawFrakturGatC_emdash(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.25)

    emdash = drawGrundelementB(*Grund_a, 4) 

    FrakturGatC_emdash = emdash
    trans_scale(FrakturGatC_emdash, valueToMoveGlyph)
    return FrakturGatC_emdash
     
    
    
    
def drawFrakturGatC_hyphen(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-1.25)
    hyphen_top = drawGrundelementH(*Grund_b, 2, "down") 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.25)
    hyphen_btm = drawGrundelementH(*Grund_b, 2, "down") 
    
    FrakturGatC_hyphen = hyphen_top + hyphen_btm
    trans_scale(FrakturGatC_hyphen, valueToMoveGlyph)
    return FrakturGatC_hyphen
     
    
    
    
def drawFrakturGatC_exclam(x, y):
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+3.25)
    exclam = drawGrundelementF(*Grund_a, 7) 
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    period = drawGrundelementC(*Raute_a)

    FrakturGatC_exclam = exclam + period
    trans_scale(FrakturGatC_exclam, valueToMoveGlyph)
    return FrakturGatC_exclam
    
    



def drawFrakturGatC_question(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    period = drawGrundelementC(*Raute_a)

    question = drawSchriftzug_question(x, y) 

    FrakturGatC_question = question + period
    trans_scale(FrakturGatC_question, valueToMoveGlyph)
    return FrakturGatC_question
    








###################################
######     ab hier Zahlen    ######
###################################




def drawFrakturGatC_zero(x, y):

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y)

    FrakturGatC_zero = Bogen_links + Bogen_rechts
    trans_scale(FrakturGatC_zero, valueToMoveGlyph)
    return FrakturGatC_zero
    





    
    
 
def drawFrakturGatC_one(x, y): 
    x += 3
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y+2)
    
    stem = drawSchriftzug3_Figures(x, y)

    Serife_nach_rechts = drawSchriftteil2(*Raute_a)
    Serife_nach_unten = drawSchriftteil5(*Raute_d)
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-3.5)
    transition = drawSchneckenzug(*Grund_a, LOWER_B, 3, HSL_size=1, HSL_start=16.25, clockwise=True, inward=True)
    pkt_Auslauf = transition.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 
        
    FrakturGatC_one = stem + Serife_nach_rechts + Serife_nach_unten + transition
    drawPath(FrakturGatC_one)
    trans_scale(FrakturGatC_one, valueToMoveGlyph)
    return FrakturGatC_one, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)


def drawFrakturGatC_one_thinStroke(x, y, *, pass_from_thick=None): 
    
    FrakturGatC_one_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    #FrakturGatC_one_thinStroke.oval(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1, part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    
    FrakturGatC_one_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatC_one_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_one_thinStroke)
    trans_scale(FrakturGatC_one_thinStroke, valueToMoveGlyph)
    return FrakturGatC_one_thinStroke
    
    
    




def drawFrakturGatC_two(x, y):
     
    Bogen = drawSchriftzug_two_Bogen(x, y)
    pkt_Auslauf = Bogen.points[0]
    #text("pkt_Auslauf", pkt_Auslauf) 
    
    Schwung_unten = drawSchriftzug_two_Schwung (x, y)
    
    FrakturGatC_two = Bogen + Schwung_unten    
    trans_scale(FrakturGatC_two, valueToMoveGlyph)
    return FrakturGatC_two, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
def drawFrakturGatC_two_thinStroke(x, y, *, pass_from_thick=None): 
    
    FrakturGatC_two_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 12, HSL_size=1, HSL_start=18, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.001 * i      for  i in range(0, 13)])

    FrakturGatC_two_thinStroke += Auslauf 
    drawPath(FrakturGatC_two_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_two_thinStroke)
    trans_scale(FrakturGatC_two_thinStroke, valueToMoveGlyph)
    return FrakturGatC_two_thinStroke
    
    
    
    
    
    
    


def drawFrakturGatC_three(x, y):

    FrakturGatC_three = BezierPath()
         
    Three_Top = drawSchriftzug_three_Top(x, y)
    Three_Schwung = drawSchriftzug_three_Bogen(x, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    con = drawGrundelementA(*Three_Top.points[-1], 4, "down")
    
    drawPath(FrakturGatC_three)
    FrakturGatC_three = Three_Top + Three_Schwung + con
    trans_scale(FrakturGatC_three, valueToMoveGlyph)
    return FrakturGatC_three, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
def drawFrakturGatC_three_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_three_thinStroke = BezierPath()
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, pass_from_thick.pkt_Auslauf[0]/modul_width, pass_from_thick.pkt_Auslauf[1]/modul_height)  
    Auslauf = drawSchneckenzug(*Grund_c, LOWER_E, 11, HSL_size=1, HSL_start=18, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 12)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.9, Auslauf.points[-1][1]-modul_height*1)    
    #FrakturGatC_three_thinStroke.oval(Auslauf.points[-1][0]-modul_width*0.9, Auslauf.points[-1][1]-modul_height*1, part*8, part*8)
    
    FrakturGatC_three_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatC_three_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_three_thinStroke) 
    trans_scale(FrakturGatC_three_thinStroke, valueToMoveGlyph)
    return FrakturGatC_three_thinStroke
    
    
    
    
    
    
def drawFrakturGatC_four(x, y):
    
    x+=5
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-3.5, y)

    stem = drawSchriftzug3_Figures(x, y, instrokeLen=0)
    stroke_down = drawGrundelementH(*stem.points[0], 3, "down")
    stroke_hor = drawGrundelementB(*Grund_d, 5)

    FrakturGatC_four = stem + stroke_down + stroke_hor
    trans_scale(FrakturGatC_four, valueToMoveGlyph)
    return FrakturGatC_four   
    
    
    
    
def drawFrakturGatC_five(x, y):

    FrakturGatC_five = BezierPath()
       
    Five_Top = drawSchriftzug_five_Top(x, y)
    Three_Schwung = drawSchriftzug_three_Bogen(x, y)
    pkt_Auslauf = Three_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf) 

    con = drawGrundelementA(*Five_Top.points[0], 4, "down")
        
    FrakturGatC_five = Five_Top + Three_Schwung + con
    drawPath(FrakturGatC_five)
    trans_scale(FrakturGatC_five, valueToMoveGlyph)
    return FrakturGatC_five, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
    


def drawFrakturGatC_six(x, y):
    
    Bogen_links = drawSchriftzug_zero_BogenLinks(x, y)
    pkt_Auslauf_top = Bogen_links.points[0]
    #text("pkt_Auslauf_top", pkt_Auslauf_top) 
    
    Bogen_rechts = drawSchriftzug_six_BogenRechts(x, y)
    pkt_Auslauf_inner = Bogen_rechts.points[0]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner) 

    FrakturGatC_six = Bogen_links + Bogen_rechts
    trans_scale(FrakturGatC_six, valueToMoveGlyph)
    return FrakturGatC_six, collections.namedtuple('dummy', 'pkt_Auslauf_top pkt_Auslauf_inner')(pkt_Auslauf_top, pkt_Auslauf_inner)
    
    
def drawFrakturGatC_six_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_six_thinStroke = BezierPath() 

    
    ########## this to draw real line like in drawing Tab 21 #########
    # Einsatz = drawInstroke(*pass_from_thick.pkt_Auslauf_top, 1, "down")
    # Auslauf_top = drawSchneckenzug(*Einsatz.points[-1], UPPER_E, 4, HSL_size=0, HSL_start=16, clockwise=True, inward=False)
    # FrakturGatC_six_thinStroke += Einsatz 

    ##################################################################
    
    
    Auslauf_top = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_top, UPPER_E, 4, HSL_size=0, HSL_start=16, clockwise=True, inward=False)
    
    ### wegen blödem Absatz – siehe Tab21
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+2, y-0.25)
    Auslauf_inner = drawSchneckenzug(*Grund_a, UPPER_E, 4, HSL_size=2, HSL_start=15, clockwise=False, inward=False)
    #Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, UPPER_E, 3, HSL_size=2, HSL_start=10, clockwise=False, inward=False)

    trans_thinStroke_down_left(Auslauf_inner) 
    trans_thinStroke_down_left(Auslauf_inner) ### zwei halbe = 1 ganzes verschoben siehe Tab 21
    trans_thinStroke_up_right(Auslauf_top)
        
    FrakturGatC_six_thinStroke += Auslauf_top + Auslauf_inner 
    drawPath(FrakturGatC_six_thinStroke)
    trans_scale(FrakturGatC_six_thinStroke, valueToMoveGlyph)
    return FrakturGatC_six_thinStroke 
    
    





    
    
def drawFrakturGatC_seven(x, y):

    FrakturGatC_seven = BezierPath()

    Seven_Top = drawSchriftzug_three_Top(x-1, y)
    pkt_Stem = Seven_Top.points[-1]
    #text("pkt_Stem", pkt_Stem) 
        
    FrakturGatC_seven = Seven_Top
    drawPath(FrakturGatC_seven)
    trans_scale(FrakturGatC_seven, valueToMoveGlyph)
    return FrakturGatC_seven, collections.namedtuple('dummy', 'pkt_Stem')(pkt_Stem)
        
    
def drawFrakturGatC_seven_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_seven_thinStroke = BezierPath() 

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+4, y+2)
    
    stem_top = drawSchneckenzug(*pass_from_thick.pkt_Stem, UPPER_E, 4, HSL_size=14, HSL_start=76, clockwise=False, inward=True)
    stem_btm = drawSchneckenzug(*stem_top.points[-1], LOWER_A, 3, HSL_size=4, HSL_start=16.7, clockwise=False, inward=True)
    
    #FrakturGatC_seven_thinStroke.oval(stem_btm.points[-1][0]-modul_width*0.55, stem_btm.points[-1][1], part*8, part*8)
    Endpunkt = drawThinstroke_Endpunkt(stem_btm.points[-1][0]-modul_width*0.55, stem_btm.points[-1][1])
    
    FrakturGatC_seven_thinStroke += stem_top + stem_btm + Endpunkt
    drawPath(FrakturGatC_seven_thinStroke)
    trans_scale(FrakturGatC_seven_thinStroke, valueToMoveGlyph)
    return FrakturGatC_seven_thinStroke        







def drawFrakturGatC_eight(x, y):

    FrakturGatC_eight = drawSchriftzug_eight(x, y)
    trans_scale(FrakturGatC_eight, valueToMoveGlyph)
    return FrakturGatC_eight
    
    
    
    
    


def drawFrakturGatC_nine(x, y):
    
    Bogen_links = drawSchriftzug_nine_BogenLinks(x, y)
    pkt_Auslauf_inner = Bogen_links.points[-1]
    #text("pkt_Auslauf_inner", pkt_Auslauf_inner) 
    
    Bogen_rechts = drawSchriftzug_zero_BogenRechts(x, y, version="nine")
    pkt_Auslauf_btm = Bogen_rechts.points[-1]
    #text("pkt_Auslauf_btm", pkt_Auslauf_btm) 
    
    FrakturGatC_nine = Bogen_links + Bogen_rechts
    trans_scale(FrakturGatC_nine, valueToMoveGlyph)
    return FrakturGatC_nine, collections.namedtuple('dummy', 'pkt_Auslauf_btm pkt_Auslauf_inner')(pkt_Auslauf_btm, pkt_Auslauf_inner)


def drawFrakturGatC_nine_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_nine_thinStroke = BezierPath() 
    
    Auslauf_btm = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_btm, LOWER_E, 4, HSL_size=0, HSL_start=16, clockwise=True, inward=False)
    Auslauf_inner = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_inner, LOWER_E, 4, HSL_size=6, HSL_start=9, clockwise=False, inward=False)
        
    trans_thinStroke_down_left(Auslauf_btm) 
    trans_thinStroke_up_right(Auslauf_inner)
    
    FrakturGatC_nine_thinStroke += Auslauf_btm + Auslauf_inner 
    drawPath(FrakturGatC_nine_thinStroke)
    trans_scale(FrakturGatC_nine_thinStroke, valueToMoveGlyph)
    return FrakturGatC_nine_thinStroke 
    
    
    
    
    





##################################################################################
###            Ab hier Versalien
##################################################################################


def drawSchriftzug_BG_Versalien(x, y):
                
    drawGrundelOrientMittig(A1, A2, offset, x, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)
    drawGrundelOrientMittig(A1, A2, offset, x+3, y-5)  # für J
    drawGrundelOrientMittig(A1, A2, offset, x+6, y-5) 
     
    drawGrundelOrientMittig(A1, A2, offset, x, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+3, y) 
    drawGrundelOrientMittig(A1, A2, offset, x+6, y) 
    
    drawGrundelOrientMittig(A1, A2, offset, x+3, y+3) 


# drawSchriftzug_BG_Versalien(temp_x, temp_y)





def drawFrakturGatC_A(x, y):

    Hauptstrich_links = drawSchriftzug_A_Hauptstrich_links(x, y)
    Hauptstrich_rechts = drawSchriftzug_A_Hauptstrich_rechts(x, y)

    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x, y)
    Deckung_Rechtsoben = drawSchriftzug_A_DeckungOben(x, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.5, y-5.35)
    Signatur = drawSchriftteil11(*Raute_a)
    
    FrakturGatC_A = Hauptstrich_links + Hauptstrich_rechts + Deckung_LinksUnten + Deckung_Rechtsoben + Signatur
    trans_scale(FrakturGatC_A, valueToMoveGlyph)
    return FrakturGatC_A



    
def drawFrakturGatC_Adieresis(x, y):

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y-1) 

    Einsatz = drawGrundelementD(*Grund_a, length=0.5)
    oben = drawSchneckenzug(*Grund_a, LOWER_H, 5, HSL_size=1, HSL_start=10, clockwise=True, inward=False)
    instroke = drawInstroke(*oben.points[-1], 0.75, "down")
    unten = drawSchneckenzug(*Einsatz.points[4], UPPER_H, 5, HSL_size=1, HSL_start=14, clockwise=True, inward=True)
    stehender_Schwung = instroke + oben + Einsatz + unten
    
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    ###
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.65, y-5.05) 
    
    links = drawSchneckenzug(*Grund_a, UPPER_H, 3, HSL_size=1, HSL_start=10, clockwise=False, inward=False)  
    Einsatz = drawGrundelementD(*Grund_a, 0.25)
    rechts = drawSchneckenzug(*Einsatz.points[-4], LOWER_H, 3, HSL_size=1, HSL_start=9, clockwise=False, inward=False)
    outstroke = drawOutstroke(*rechts.points[-1], 1.7)
    upstroke = drawSchneckenzug(*outstroke.points[0], LOWER_E, 3, HSL_size=1, HSL_start=14, clockwise=False, inward=False)
    liegender_Schwung = links + Einsatz + rechts + outstroke + upstroke
    
    ###
    Deckung_Rechtsoben = drawSchriftzug_A_DeckungOben(x-3.6, y-1.1)
    instroke = drawGrundelementA(*Deckung_Rechtsoben.points[-1], -0.71)
    curve_top = drawSchneckenzug(*instroke.points[-1], UPPER_E, 3, HSL_size=1, HSL_start=10, clockwise=False, inward=False)
    straight_down = drawGrundelementF(*curve_top.points[-1], 4.725)
    curve_btm = drawSchneckenzug(*straight_down.points[-1], UPPER_B, 3, HSL_size=1, HSL_start=10, clockwise=False, inward=False)
    outstroke = drawGrundelementA(*curve_btm.points[-1], 0.5)
    Rechte_Seite = Deckung_Rechtsoben + instroke + curve_top + straight_down + curve_btm + outstroke
    
    Dieresis = drawDieresis(x+1.5, y+3.25)

    FrakturGatC_Adieresis = stehender_Schwung + liegender_Schwung + Rechte_Seite + Dieresis
    trans_scale(FrakturGatC_Adieresis, valueToMoveGlyph)
    return FrakturGatC_Adieresis, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    


        
    
def drawFrakturGatC_B(x, y):
    #x += 5  
    FrakturGatC_B = BezierPath() 

    Hauptbogen = drawSchriftzug_B_Hauptbogen(x, y)
    pkt_Auslauf = Hauptbogen.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)

    ### Auge
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5, y+2.5)   

    if version_mod == 0:  
        middleElement = drawSchriftteil2(*Grund_a)
        pkt_instrokeAuge = middleElement.points[0]
        #text("pkt_instrokeAuge", middleElement.points[0])
        Auge = drawSchriftteil8(*middleElement.points[-1])
        pkt_outstrokeAuge = Auge.points[-1]
        #text("pkt_outstrokeAuge", Auge.points[-1])
        FrakturGatC_B += middleElement
        
    if version_mod == 1:    
        Auge = drawAuge(*Grund_a)    
        pkt_instrokeAuge = Auge.points[0]
        #text("pkt_instrokeAuge", pkt_instrokeAuge)
        pkt_outstrokeAuge = Auge.points[-1]
        #text("pkt_outstrokeAuge", pkt_outstrokeAuge)


    Bauch = drawSchriftzug_B_Bauch(x, y)
    Fuss = drawSchneckenzug(*Bauch.points[-2], UPPER_H, 3, HSL_size=3, HSL_start=24, clockwise=False, inward=True)

    FrakturGatC_B += Hauptbogen + Fuss + Auge + Bauch
    drawPath(FrakturGatC_B)
    trans_scale(FrakturGatC_B, valueToMoveGlyph)      
    return FrakturGatC_B, collections.namedtuple('dummy', 'pkt_Auslauf pkt_instrokeAuge pkt_outstrokeAuge')(pkt_Auslauf, pkt_instrokeAuge, pkt_outstrokeAuge)

    
def drawFrakturGatC_B_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatC_B_thinStroke = BezierPath()
        
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)

    #### Auge
    instroke = drawInstroke(*pass_from_thick.pkt_instrokeAuge, 0.25)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 2, HSL_size=4, HSL_start=16, clockwise=False, inward=True)
    curve_fromAuge = drawSchneckenzug(*pass_from_thick.pkt_outstrokeAuge, LOWER_E, 1, HSL_size=2, HSL_start=10, clockwise=True, inward=False)

    FrakturGatC_B_thinStroke += Auslauf + instroke + curve_toInstroke + curve_fromAuge + Endpunkt
    drawPath(FrakturGatC_B_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_B_thinStroke) 
    trans_scale(FrakturGatC_B_thinStroke, valueToMoveGlyph)
    return FrakturGatC_B_thinStroke
    




def drawFrakturGatC_C(x, y, version="Substantival"):
   
    stehender_Schwung = drawSchriftzug_C_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-2]
    #text("pkt_Auslauf", stehender_Schwung.points[-2])
    liegender_Schwung = drawSchriftzug_C_liegenderSchwung(x, y)
    Deckung = drawSchriftzug_A_DeckungOben(x-2.5, y+0.25)

    FrakturGatC_C = stehender_Schwung + liegender_Schwung + Deckung
    
    if version == "Substantival":
        trans_scale(FrakturGatC_C, valueToMoveGlyph)
    if version == "Versal":
        trans_scale(FrakturGatC_C, valueToMoveGlyph)
        FrakturGatC_C.scale(1.25)
    if version == "Initial":
        trans_scale(FrakturGatC_C, valueToMoveGlyph)
        FrakturGatC_C.scale(1.75)        
    if version == "Capital":
        trans_scale(FrakturGatC_C, valueToMoveGlyph)
        FrakturGatC_C.scale(2.5)
    
    return FrakturGatC_C, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)


def drawFrakturGatC_C_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)  

    FrakturGatC_C_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatC_C_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_C_thinStroke) 
    trans_scale(FrakturGatC_C_thinStroke, valueToMoveGlyph)    
    return FrakturGatC_C_thinStroke
    

def drawFrakturGatC_C_thinStroke_Versal(x, y, *, pass_from_thick=None):

    FrakturGatC_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    
    position = Auslauf.points[-1][0]-2.5, Auslauf.points[-1][1]-5.8
    amount = 15
    radius = part*7
    
    i = 0.4
    for i in range(amount):
        FrakturGatC_C_thinStroke.oval(position[0]+i*0.2, position[1]+i*0.2, radius-i*0.4, radius-i*0.4)
        i += 0.4
    FrakturGatC_C_thinStroke += Auslauf
    drawPath(FrakturGatC_C_thinStroke)
    FrakturGatC_C_thinStroke.translate(-(modul_width*0.5)+1, -(modul_height*0.25-0.5))
    trans_scale(FrakturGatC_C_thinStroke, valueToMoveGlyph)    
    FrakturGatC_C_thinStroke.scale(1.25)
    return FrakturGatC_C_thinStroke


def drawFrakturGatC_C_thinStroke_Initial(x, y, *, pass_from_thick=None):

    FrakturGatC_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    
    position = Auslauf.points[-1][0]-2, Auslauf.points[-1][1]-4.15
    amount = 15
    radius = part*5
    
    i = 0.4
    for i in range(amount):
        FrakturGatC_C_thinStroke.oval(position[0]+i*0.2, position[1]+i*0.2, radius-i*0.4, radius-i*0.4)
        i += 0.4
        
    FrakturGatC_C_thinStroke += Auslauf
    drawPath(FrakturGatC_C_thinStroke)
    FrakturGatC_C_thinStroke.translate(-(modul_width*0.5)+1.5, -(modul_height*0.25-0.75))
    trans_scale(FrakturGatC_C_thinStroke, valueToMoveGlyph)    
    FrakturGatC_C_thinStroke.scale(1.75)
    return FrakturGatC_C_thinStroke
        
        
def drawFrakturGatC_C_thinStroke_Capital(x, y, *, pass_from_thick=None):

    FrakturGatC_C_thinStroke = BezierPath()
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1, HSL_start=14, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    
    position = Auslauf.points[-1][0]-2, Auslauf.points[-1][1]-4.15
    amount = 20
    radius = part*5
    
    i = 0.3
    for i in range(amount):
        FrakturGatC_C_thinStroke.oval(position[0]+i*0.15, position[1]+i*0.15, radius-i*0.3, radius-i*0.3)
        i += 0.3

    FrakturGatC_C_thinStroke += Auslauf
    drawPath(FrakturGatC_C_thinStroke)
    FrakturGatC_C_thinStroke.translate(-(modul_width*0.5)+2, -(modul_height*0.25-1))
    trans_scale(FrakturGatC_C_thinStroke, valueToMoveGlyph)    
    FrakturGatC_C_thinStroke.scale(2.5)
    return FrakturGatC_C_thinStroke
    
    
    
    
          
    


def drawFrakturGatC_D(x, y):
   
    stehender_Schwung = drawSchriftzug_G_stehenderSchwung(x,y-1)
    pkt_Auslauf = stehender_Schwung.points[-3]
    #text("pkt_Auslauf", stehender_Schwung.points[-3])
    stehender_Schwung_Deckung = drawSchriftzug_D_Deckung(x-3, y)
    liegender_Schwung = drawSchriftzug_D_liegenderSchwung(x-3,y)
    Hauptbogen = drawSchriftzug_D_Hauptbogen(x-3, y)

    FrakturGatC_D = stehender_Schwung + stehender_Schwung_Deckung + liegender_Schwung + Hauptbogen
    trans_scale(FrakturGatC_D, valueToMoveGlyph)
    return FrakturGatC_D, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    


    

def drawFrakturGatC_E(x, y):
   
    stehender_Schwung = drawSchriftzug_E_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-2]
    #text("pkt_Auslauf", stehender_Schwung.points[-2])

    liegender_Schwung = drawSchriftzug_E_liegenderSchwung(x, y)

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x+4.5, y+3.5)  
    Deckung_top = drawSchneckenzug(*Grund_a, LOWER_G, 6, HSL_size=0.5, HSL_start=6, clockwise=True, inward=True)
    Einsatz = drawGrundelementC(*Grund_a, 0.5)
    Deckung_btm = drawSchneckenzug(*Einsatz.points[-1], UPPER_G, 6, HSL_size=0.5, HSL_start=9, clockwise=True, inward=True)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+4.5, y-0.25)
    Signatur = drawSchriftteil11(*Raute_a)

    FrakturGatC_E = stehender_Schwung + liegender_Schwung + Deckung_top + Einsatz + Deckung_btm + Signatur
    drawPath(FrakturGatC_E)
    trans_scale(FrakturGatC_E, valueToMoveGlyph)
    return FrakturGatC_E, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    



    
      
   
def drawFrakturGatC_F(x, y):
    x += 3
    liegender_Schwung = drawSchriftzug_F_liegenderSchwung(x, y)
    stehender_Schwung = drawSchriftzug_F_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-9]
    #text("pkt_Auslauf", pkt_Auslauf)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-2.5, y-5.25)
    Deckung_left = drawSchriftteil9(*Raute_a, direction="CW")
    Deckung_btm = drawSchriftteil8(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+2.5, y+0.75)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.5, Signatur.points[-8][1]+modul_height*1.25
    #text("pkt_Signatur", pkt_Signatur)
    
    FrakturGatC_F = liegender_Schwung + stehender_Schwung + Deckung_left + Deckung_btm + Signatur
    trans_scale(FrakturGatC_F, valueToMoveGlyph)
    return FrakturGatC_F, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Signatur')(pkt_Auslauf, pkt_Signatur)
    
    
def drawFrakturGatC_F_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_F_thinStroke = BezierPath()
    
    FrakturGatC_F_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.25*modul_height))
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 1, HSL_size=1, HSL_start=28, clockwise=True, inward=False)

    FrakturGatC_F_thinStroke += Auslauf
    drawPath(FrakturGatC_F_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_F_thinStroke) 
    trans_scale(FrakturGatC_F_thinStroke, valueToMoveGlyph)
    return FrakturGatC_F_thinStroke 



    
    
def drawFrakturGatC_G(x, y):

    FrakturGatC_G = BezierPath()

    Hauptbogen = drawSchriftzug_G_Hauptbogen(x, y)
    Hauptbogen_Deckung = drawSchriftzug_G_Deckung(x, y)
    
    Fuss = drawSchriftzug_G_Fuss(x, y)

    ### Auge
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5, y+3)
    
    if version_mod == 0:  
        middleElement = drawSchriftteil2(*Grund_a)
        pkt_instrokeAuge = middleElement.points[0]
        #text("pkt_instrokeAuge", middleElement.points[0])
        Auge = drawSchriftteil8(*middleElement.points[-1])
        pkt_outstrokeAuge = Auge.points[-1]
        #text("pkt_outstrokeAuge", Auge.points[-1])
        FrakturGatC_G += middleElement
        
    if version_mod == 1:    
        Auge = drawAuge(*Grund_a)    
        pkt_instrokeAuge = Auge.points[0]
        #text("pkt_instrokeAuge", pkt_instrokeAuge)
        pkt_outstrokeAuge = Auge.points[-1]
        #text("pkt_outstrokeAuge", pkt_outstrokeAuge)
        

    Bauch = drawSchriftzug_G_Bauch(x, y)
    
    stehender_Schwung = drawSchriftzug_G_stehenderSchwung(x, y, instrokeLen=1.5)
    pkt_Auslauf_unten = stehender_Schwung.points[-3]
    #text("pkt_Auslauf_unten", pkt_Auslauf_unten)
    pkt_Auslauf_oben = stehender_Schwung.points[-1]
    #text("pkt_Auslauf_oben", pkt_Auslauf_oben)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5.25, y-3)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.4, Signatur.points[-8][1]+modul_height*1.25
    #text("pkt_Signatur", pkt_Signatur)

    FrakturGatC_G += Hauptbogen + Hauptbogen_Deckung + Fuss + Auge + Bauch + stehender_Schwung + Signatur
    trans_scale(FrakturGatC_G, valueToMoveGlyph)
    return FrakturGatC_G, collections.namedtuple('dummy', 'pkt_instrokeAuge pkt_outstrokeAuge pkt_Auslauf_unten pkt_Auslauf_oben pkt_Signatur')(pkt_instrokeAuge, pkt_outstrokeAuge, pkt_Auslauf_unten, pkt_Auslauf_oben, pkt_Signatur)
    
    
    
def drawFrakturGatC_G_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_G_thinStroke = BezierPath()
    
    Auslauf_unten = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_unten, LOWER_E, 9, HSL_size=1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt_unten = drawThinstroke_Endpunkt(Auslauf_unten.points[-1][0]-modul_width*1, Auslauf_unten.points[-1][1]-modul_height*1.35)
    
    Auslauf_oben = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_oben, LOWER_E, 4, HSL_size=1, HSL_start=10, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 5)])
    Endpunkt_oben = drawThinstroke_Endpunkt(Auslauf_oben.points[-1][0]-modul_width*0.5, Auslauf_oben.points[-1][1]-modul_height*0.6)
 
    #### Auge
    instroke = drawInstroke(*pass_from_thick.pkt_instrokeAuge, 0.25)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 2, HSL_size=4, HSL_start=16, clockwise=False, inward=True)
    curve_fromAuge = drawSchneckenzug(*pass_from_thick.pkt_outstrokeAuge, LOWER_E, 1, HSL_size=2, HSL_start=10, clockwise=True, inward=False)

    FrakturGatC_G_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.25*modul_height))

    move_down = Auslauf_unten + instroke + curve_toInstroke + curve_fromAuge ### didn't work. Why???
    trans_thinStroke_down_left(Auslauf_unten) 
    trans_thinStroke_down_left(instroke) 
    trans_thinStroke_down_left(curve_toInstroke) 
    trans_thinStroke_down_left(curve_fromAuge) 
    trans_thinStroke_down_left(FrakturGatC_G_thinStroke) 
    trans_thinStroke_up_right(Auslauf_oben)
        
    FrakturGatC_G_thinStroke += Auslauf_unten + Auslauf_oben + instroke + curve_toInstroke + curve_fromAuge + Endpunkt_unten + Endpunkt_oben  
    drawPath(FrakturGatC_G_thinStroke)
    trans_scale(FrakturGatC_G_thinStroke, valueToMoveGlyph)
    return FrakturGatC_G_thinStroke
    
    
    
    




def drawFrakturGatC_H(x, y):
    
    x += 3
    
    stehender_Schwung = drawSchriftzug_H_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    Hauptstrich_rechts = drawSchriftzug_H_Hauptstrich_rechts(x, y)
    pkt_Einlauf = Hauptstrich_rechts.points[0]
    #text("pkt_Einlauf", pkt_Einlauf)
    pkt_Auslauf_unten = Hauptstrich_rechts.points[-2]
    #text("pkt_Auslauf_unten", pkt_Auslauf_unten)
    
    Deckung_Rechtsoben = drawSchriftzug_A_DeckungOben(x-1.25-3.5, y+0.25)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5.5)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)
    
    FrakturGatC_H = stehender_Schwung + Hauptstrich_rechts + Deckung_Rechtsoben + liegender_Schwung_unten
    
    trans_scale(FrakturGatC_H, valueToMoveGlyph)
    return FrakturGatC_H, collections.namedtuple('dummy', 'pkt_Einlauf pkt_Auslauf pkt_Auslauf_unten')(pkt_Einlauf, pkt_Auslauf, pkt_Auslauf_unten)
    
    
def drawFrakturGatC_H_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_H_thinStroke = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Einlauf, 0.5)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 1, HSL_size=1, HSL_start=28, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    Auslauf_unten = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_unten, LOWER_E, 3, HSL_size=1.1, HSL_start=20, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 4)])
     
    FrakturGatC_H_thinStroke += instroke + curve_toInstroke + Auslauf + Endpunkt + Auslauf_unten
    drawPath(FrakturGatC_H_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_H_thinStroke)
    trans_scale(FrakturGatC_H_thinStroke, valueToMoveGlyph)
    return FrakturGatC_H_thinStroke
       

    
    
    


def drawFrakturGatC_I(x, y):

    liegender_Schwung = drawSchriftzug_I_liegenderSchwung(x, y)
    stehender_Schwung = drawSchriftzug_I_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+0.5, y-5.25)
    Deckung_left = drawSchriftteil9(*Raute_a, direction="CW")
    Deckung_btm = drawSchriftteil8(*Raute_a)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1.5, y-0.25)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.5, Signatur.points[-8][1]+modul_height*1.25
    #text("pkt_Signatur", pkt_Signatur)
    
    FrakturGatC_I = liegender_Schwung + stehender_Schwung + Deckung_left + Deckung_btm + Signatur
    trans_scale(FrakturGatC_I, valueToMoveGlyph)
    return FrakturGatC_I, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Signatur')(pkt_Auslauf, pkt_Signatur)
        
       
def drawFrakturGatC_I_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_I_thinStroke = BezierPath()
    
    FrakturGatC_I_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.5*modul_height))
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 1, HSL_size=1, HSL_start=28, clockwise=True, inward=False)

    FrakturGatC_I_thinStroke += Auslauf
    drawPath(FrakturGatC_I_thinStroke)
    trans_scale(FrakturGatC_I_thinStroke, valueToMoveGlyph)
    return FrakturGatC_I_thinStroke 
    
    
    
    


def drawFrakturGatC_J(x, y):

    #x += 5
    liegender_Schwung = drawSchriftzug_J_liegenderSchwung(x, y)
    stehender_Schwung = drawSchriftzug_J_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x-1.5, y-0.75)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.5, Signatur.points[-8][1]+modul_height*1.25
    #text("pkt_Signatur", pkt_Signatur)
    
    FrakturGatC_J = liegender_Schwung + stehender_Schwung + Signatur
    trans_scale(FrakturGatC_J, valueToMoveGlyph)
    return FrakturGatC_J, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Signatur')(pkt_Auslauf, pkt_Signatur)
    
    
def drawFrakturGatC_J_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_J_thinStroke = BezierPath()
    
    FrakturGatC_J_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.25*modul_height))
    
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.25, HSL_start=20, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    
    FrakturGatC_J_thinStroke += Auslauf + Endpunkt
    drawPath(FrakturGatC_J_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_J_thinStroke) 
    trans_scale(FrakturGatC_J_thinStroke, valueToMoveGlyph)
    return FrakturGatC_J_thinStroke 
    
    




def drawFrakturGatC_K(x, y):
    x += 3
    stehender_Schwung = drawSchriftzug_K_stehenderSchwung(x, y)

    ### Deckung oben
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+3.25, y+2.75)
    Einsatz = drawGrundelementC(*Grund_a, 0.25, "unten")
    Deckung_oben_top = drawHalbbogen9(*Einsatz.points[4], outstrokeLen=0)
    Deckung_oben_btm = drawHalbbogen4(*Grund_a, instrokeLen=0)
    Deckung_oben = Einsatz + Deckung_oben_top + Deckung_oben_btm


    ### Deckung unten
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x-3, y-5.5)
    Einsatz = drawGrundelementC(*Grund_a, 0.5, "unten")
    Deckung_unten_top = drawHalbbogen9(*Einsatz.points[4], outstrokeLen=0)
    Deckung_unten_btm = drawHalbbogen1(*Grund_a, instrokeLen=0)
    Deckung_unten = Einsatz + Deckung_unten_top + Deckung_unten_btm
    
    ### Schwung unten
    liegender_Schwung_unten = drawSchriftzug_S_liegenderSchwung(x-1.25, y)
    outstroke = drawOutstroke(*liegender_Schwung_unten.points[0], 0.3, "down")
    
    ### Querstrich
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x-0.8, y-2)
    Querstrich = drawGrundelementB(*Grund_a, 3)
    
    ### Auge
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1, y+1.5)
    instroke = drawInstroke(*Grund_a, 1)
    middleElement = drawGrundelementC(*Grund_a, 0.4)
    Auge = drawSchriftteil8(*middleElement.points[-1])
    Auge_all = instroke + middleElement + Auge
    
    FrakturGatC_K = stehender_Schwung + liegender_Schwung_unten + Deckung_oben + Deckung_unten + Querstrich + Auge_all + outstroke
    trans_scale(FrakturGatC_K, valueToMoveGlyph)
    return FrakturGatC_K
    
    
    
    
    
    

def drawFrakturGatC_L(x, y):

    stehender_Schwung = drawSchriftzug_L_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-9]
    #text("pkt_Auslauf", pkt_Auslauf)
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+2, y+2.5)
    Einsatz = drawGrundelementC(*Grund_a, 0.25, "unten")
    Deckung_top = drawHalbbogen9(*Einsatz.points[4], outstrokeLen=0)
    Deckung_btm = drawHalbbogen3(*Grund_a, instrokeLen=0)
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5.35)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    FrakturGatC_L = stehender_Schwung + Einsatz + Deckung_top + Deckung_btm + liegender_Schwung_unten
    trans_scale(FrakturGatC_L, valueToMoveGlyph)
    return FrakturGatC_L, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    
    
def drawFrakturGatC_M(x, y):

    links = drawSchriftzug_M_Bogen_links(x, y)
    mitte = drawSchriftzug_M_Bogen_mitte(x, y)
    rechts = drawSchriftzug_M_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[14]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x+3, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+8.75, y-5.35)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    FrakturGatC_M = links + mitte + rechts + Deckung_LinksUnten + liegender_Schwung_unten
    trans_scale(FrakturGatC_M, valueToMoveGlyph)
    return FrakturGatC_M, collections.namedtuple('dummy', 'pkt_Einlauf')(pkt_Einlauf)
    

    
def drawFrakturGatC_M_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_M_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)

    pkt_Einlauf = pass_from_thick.pkt_Einlauf[0]- 3*modul_width, pass_from_thick.pkt_Einlauf[1]

    Einlauf_davor = drawSchneckenzug(*pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)
    
    FrakturGatC_M_thinStroke += Einlauf + Einlauf_davor
    drawPath(FrakturGatC_M_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_M_thinStroke) 
    trans_scale(FrakturGatC_M_thinStroke, valueToMoveGlyph)
    return FrakturGatC_M_thinStroke
       
    
    
    
    
def drawFrakturGatC_N(x, y):

    links = drawSchriftzug_M_Bogen_links(x, y)
    rechts = drawSchriftzug_M_Bogen_rechts(x-3, y)
    pkt_Einlauf = rechts.points[14]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x+3, y)

    FrakturGatC_N = links + rechts + Deckung_LinksUnten 
    trans_scale(FrakturGatC_N, valueToMoveGlyph)
    return FrakturGatC_N, collections.namedtuple('dummy', 'pkt_Einlauf')(pkt_Einlauf)
    

def drawFrakturGatC_N_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_N_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)

    FrakturGatC_N_thinStroke += Einlauf
    drawPath(FrakturGatC_N_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_N_thinStroke) 
    trans_scale(FrakturGatC_N_thinStroke, valueToMoveGlyph)
    return FrakturGatC_N_thinStroke
    
    
     
    
    

    
def drawFrakturGatC_O(x, y):

    stehender_Schwung = drawSchriftzug_O_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-9]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    liegender_Schwung = drawSchriftzug_O_liegenderSchwung(x, y)
    Bogen_rechts = drawSchriftzug_O_BogenRechts(x, y)

    FrakturGatC_O = stehender_Schwung + liegender_Schwung + Bogen_rechts
    trans_scale(FrakturGatC_O, valueToMoveGlyph)
    return FrakturGatC_O, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)

    
    
def drawFrakturGatC_Odieresis(x, y):

    stehender_Schwung = drawSchriftzug_O_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    liegender_Schwung = drawSchriftzug_O_liegenderSchwung(x, y)
    Bogen_rechts = drawSchriftzug_O_BogenRechts(x, y)

    Dieresis = drawDieresis(x+0.5, y+3.5)

    FrakturGatC_Odieresis = stehender_Schwung + liegender_Schwung + Bogen_rechts + Dieresis
    trans_scale(FrakturGatC_Odieresis, valueToMoveGlyph)
    return FrakturGatC_Odieresis, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)

    
    
    
    
    
    
def drawFrakturGatC_P(x, y):

    links = drawSchriftzug_P_Bogen_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    rechts = drawSchriftzug_P_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[22]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Deckung = drawGrundelementB(x+4*modul_width, y+1.25*modul_height, 3.75)
    #drawSchriftzug_R_Deckung(x+4.5, y+0.25)

    FrakturGatC_P = links + rechts + Deckung 
    trans_scale(FrakturGatC_P, valueToMoveGlyph)
    return FrakturGatC_P, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Einlauf')(pkt_Auslauf, pkt_Einlauf)
    

def drawFrakturGatC_P_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_P_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=18, clockwise=False, inward=False)
    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
    
    FrakturGatC_P_thinStroke += Einlauf + Auslauf + Endpunkt
    drawPath(FrakturGatC_P_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_P_thinStroke)
    trans_scale(FrakturGatC_P_thinStroke, valueToMoveGlyph)
    return FrakturGatC_P_thinStroke
    
    
    
    

def drawFrakturGatC_Q(x, y):
    
    ''' dieser obere Teil ist von O kopiert'''
    stehender_Schwung = drawSchriftzug_O_stehenderSchwung(x, y)
    pkt_Auslauf = stehender_Schwung.points[-9]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    liegender_Schwung = drawSchriftzug_O_liegenderSchwung(x, y)
    Bogen_rechts = drawSchriftzug_O_BogenRechts(x, y)

    FrakturGatC_O = stehender_Schwung + liegender_Schwung + Bogen_rechts
    
    ''' ab hier neue Form'''
    liegender_Schwung = drawSchriftzug_Q_liegenderSchwung(x-2, y)

    FrakturGatC_Q = FrakturGatC_O + liegender_Schwung
    trans_scale(FrakturGatC_Q, valueToMoveGlyph)
    return FrakturGatC_Q, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    



    
def drawFrakturGatC_R(x, y):

    FrakturGatC_R = BezierPath()

    Hauptbogen = drawSchriftzug_R_Hauptbogen(x, y)
    Hauptbogen_Deckung = drawSchriftzug_R_Deckung(x, y)
    
    Fuss = drawSchriftzug_R_Fuss(x, y)
    
    ### Auge
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+5, y+2.5)
    
    if version_mod == 0:  
        middleElement = drawSchriftteil2(*Grund_a)
        pkt_instrokeAuge = middleElement.points[0]
        #text("pkt_instrokeAuge", middleElement.points[0])
        Auge = drawSchriftteil8(*middleElement.points[-1])
        pkt_outstrokeAuge = Auge.points[-1]
        #text("pkt_outstrokeAuge", Auge.points[-1])
        FrakturGatC_R += middleElement
        
    if version_mod == 1:    
        Auge = drawAuge(*Grund_a)    
        pkt_instrokeAuge = Auge.points[0]
        #text("pkt_instrokeAuge", pkt_instrokeAuge)
        pkt_outstrokeAuge = Auge.points[-1]
        #text("pkt_outstrokeAuge", pkt_outstrokeAuge)
        
        
    
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+1.75, y-1.5)
    Signatur = drawSchriftteil11(*Raute_a)
    pkt_Signatur = Signatur.points[-8][0]+modul_width*0.5, Signatur.points[-8][1]+modul_height*1.4
    #text("pkt_Signatur", pkt_Signatur)

    FrakturGatC_R += Hauptbogen + Hauptbogen_Deckung + Fuss + Auge + Signatur
    trans_scale(FrakturGatC_R, valueToMoveGlyph)
    return FrakturGatC_R, collections.namedtuple('dummy', 'pkt_instrokeAuge pkt_outstrokeAuge pkt_Signatur')(pkt_instrokeAuge, pkt_outstrokeAuge, pkt_Signatur)
    
    
def drawFrakturGatC_R_thinStroke(x, y, *, pass_from_thick=None):
    
    FrakturGatC_R_thinStroke = BezierPath()
    
    FrakturGatC_R_thinStroke.line(pass_from_thick.pkt_Signatur, (pass_from_thick.pkt_Signatur[0], pass_from_thick.pkt_Signatur[1]-2.5*modul_height))
    
    #### Auge
    instroke = drawInstroke(*pass_from_thick.pkt_instrokeAuge, 0.25)
    curve_toInstroke = drawSchneckenzug(*instroke.points[-1], UPPER_E, 2, HSL_size=4, HSL_start=16, clockwise=False, inward=True)
    curve_fromAuge = drawSchneckenzug(*pass_from_thick.pkt_outstrokeAuge, LOWER_E, 1, HSL_size=2, HSL_start=10, clockwise=True, inward=False)

    FrakturGatC_R_thinStroke += instroke + curve_toInstroke + curve_fromAuge
    drawPath(FrakturGatC_R_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_R_thinStroke)
    trans_scale(FrakturGatC_R_thinStroke, valueToMoveGlyph)
    return FrakturGatC_R_thinStroke
    
    
    
    
    


def drawFrakturGatC_S(x, y):

    
    stehender_Schwung = drawSchriftzug_S_stehenderSchwung(x, y)

    ### Deckung oben
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+6, y+3)
    Einsatz = drawGrundelementC(*Grund_a, 0.25, "unten")
    Deckung_top = drawHalbbogen9(*Einsatz.points[4], outstrokeLen=0)
    Deckung_btm = drawHalbbogen3(*Grund_a, instrokeLen=0)
 
    liegender_Schwung_unten = drawSchriftzug_S_liegenderSchwung(x, y, outstrokeLen=0.7)

    FrakturGatC_S = stehender_Schwung + Einsatz + Deckung_top +  Deckung_btm + liegender_Schwung_unten
    trans_scale(FrakturGatC_S, valueToMoveGlyph)
    return FrakturGatC_S
    
    
    
     
    
def drawFrakturGatC_T(x, y):
    
    stehender_Schwung = drawSchriftzug_T_stehenderSchwung(x, y)
    liegender_Schwung = drawSchriftzug_T_liegenderSchwung(x, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+3, y-5.35)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    FrakturGatC_T = stehender_Schwung + liegender_Schwung + liegender_Schwung_unten
    trans_scale(FrakturGatC_T, valueToMoveGlyph)
    return FrakturGatC_T
    
    




def drawFrakturGatC_U(x, y):

    FrakturGatC_U = BezierPath()
    
    links = drawSchriftzug_U_Hauptstrich_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    rechts = drawSchriftzug_U_Hauptstrich_rechts(x, y)
    
    FrakturGatC_U = links + rechts 
    trans_scale(FrakturGatC_U, valueToMoveGlyph)
    return FrakturGatC_U, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    

def drawFrakturGatC_Udieresis(x, y):

    FrakturGatC_Udieresis = BezierPath()

    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x, y+1.6) 
    Hauptbogen_oben = drawSchneckenzug(*Grund_a, UPPER_E, 5, HSL_size=2, HSL_start=28, clockwise=True, inward=True)
    Hauptbogen_unten = drawSchneckenzug(*Hauptbogen_oben.points[-1], LOWER_B, 3, HSL_size=2, HSL_start=26, clockwise=True, inward=True)
    Hauptbogen = Hauptbogen_oben + Hauptbogen_unten
    
    pkt_Auslauf = Hauptbogen.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    rechts = drawSchriftzug_U_Hauptstrich_rechts(x+2.9, y)
    Dieresis = drawDieresis(x+5, y+3)

    FrakturGatC_Udieresis = rechts + Hauptbogen + Dieresis
    trans_scale(FrakturGatC_Udieresis, valueToMoveGlyph)
    return FrakturGatC_Udieresis, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    
    
    

    
    
    



def drawFrakturGatC_V(x, y):

    FrakturGatC_V = BezierPath()
    
    links = drawSchriftzug_V_Bogen_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    rechts = drawSchriftzug_V_Bogen_rechts(x, y)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5.5, y-5.5)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    FrakturGatC_V = links + rechts + liegender_Schwung_unten 
    trans_scale(FrakturGatC_V, valueToMoveGlyph)
    return FrakturGatC_V, collections.namedtuple('dummy', 'pkt_Auslauf')(pkt_Auslauf)
    
    
    

    
    
def drawFrakturGatC_W(x, y):
    
    FrakturGatC_W = BezierPath()

    links = drawSchriftzug_W_Bogen_links(x, y)
    pkt_Auslauf = links.points[-1]
    #text("pkt_Auslauf", pkt_Auslauf)
    
    mitte = drawSchriftzug_W_Bogen_mitte(x, y)
    rechts = drawSchriftzug_W_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[22]
    #text("pkt_Einlauf", pkt_Einlauf)
    
    Fuss = drawSchneckenzug(*rechts.points[20], UPPER_H, 3, HSL_size=4, HSL_start=11, clockwise=False, inward=False)
    
    FrakturGatC_W = links + mitte + rechts + Fuss 
    drawPath(FrakturGatC_W)
    trans_scale(FrakturGatC_W, valueToMoveGlyph)
    return FrakturGatC_W, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Einlauf')(pkt_Auslauf, pkt_Einlauf)
    
    


def drawFrakturGatC_W_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_W_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)
    pkt_Einlauf_vorher = pass_from_thick.pkt_Einlauf[0]-modul_width*3.75, pass_from_thick.pkt_Einlauf[1]
    Einlauf_vorher = drawSchneckenzug(*pkt_Einlauf_vorher, UPPER_E, 1, HSL_size=2, HSL_start=28, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, LOWER_E, 9, HSL_size=1.1, HSL_start=15, clockwise=True, inward=True, HSL_size_multipliers=[ 1 + 0.04 * i  for  i in range(0, 10)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.5, Auslauf.points[-1][1]-modul_height*1.1)
        
    FrakturGatC_W_thinStroke += Einlauf + Einlauf_vorher + Auslauf + Endpunkt
    drawPath(FrakturGatC_W_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_W_thinStroke)
    trans_scale(FrakturGatC_W_thinStroke, valueToMoveGlyph)
    return FrakturGatC_W_thinStroke
    
    
    
    
 
    

def drawFrakturGatC_X(x, y):

    Hauptbogen_Links = drawSchriftzug_R_Hauptbogen(x, y)
    Deckung_LinksUnten = drawSchriftzug_R_Deckung(x, y)
    
    Hauptbogen_Rechts = drawSchriftzug_X_BogenRechts(x, y)
    Deckung_Rechtsoben = drawSchriftzug_X_DeckungOben(x, y)

    ### Querstrich
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrientMittig(A1, A2, offset, x+1.5, y-2)
    Querstrich = drawGrundelementB(*Grund_a, 3.5)
    
    FrakturGatC_X = Hauptbogen_Links + Deckung_LinksUnten + Hauptbogen_Rechts + Deckung_Rechtsoben + Querstrich
    trans_scale(FrakturGatC_X, valueToMoveGlyph)
    return FrakturGatC_X
    
    
    
      

def drawFrakturGatC_Y(x, y):

    FrakturGatC_Y = BezierPath()
    
    links = drawSchriftzug_Y_Bogen_links(x, y)
    rechts = drawSchriftzug_Y_Bogen_rechts(x, y)
    pkt_Einlauf = rechts.points[22]
    #text("pkt_Einlauf", pkt_Einlauf)
    pkt_Auslauf = rechts.points[20]
    #text("pkt_Auslauf", pkt_Auslauf)

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x+5.45, y-5.5)
    liegender_Schwung_unten = drawSchriftteil12(*Raute_a)

    FrakturGatC_Y = links + rechts + liegender_Schwung_unten 
    trans_scale(FrakturGatC_Y, valueToMoveGlyph)
    return FrakturGatC_Y, collections.namedtuple('dummy', 'pkt_Auslauf pkt_Einlauf')(pkt_Auslauf, pkt_Einlauf)
   

def drawFrakturGatC_Y_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_Y_thinStroke = BezierPath()
    
    Einlauf = drawSchneckenzug(*pass_from_thick.pkt_Einlauf, UPPER_E, 2, HSL_size=2, HSL_start=20, clockwise=False, inward=False)

    Auslauf = drawSchneckenzug(*pass_from_thick.pkt_Auslauf, UPPER_E, 8, HSL_size=1, HSL_start=14, clockwise=False, inward=True, HSL_size_multipliers=[ 1 + 0.075 * i  for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.75, Auslauf.points[-1][1]-modul_height*0.04)
        
    FrakturGatC_Y_thinStroke += Einlauf + Auslauf + Endpunkt
    drawPath(FrakturGatC_Y_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_Y_thinStroke)
    trans_scale(FrakturGatC_Y_thinStroke, valueToMoveGlyph)
    return FrakturGatC_Y_thinStroke
    
    
    
    
    
    
    
    
def drawFrakturGatC_Z(x, y):

    FrakturGatC_Z = BezierPath()
    
    oben = drawSchriftzug_Z_Bogen_oben(x, y)
    outstroke = drawOutstroke(*oben.points[-1], 0.25, "down")
    pkt_Auslauf_oben = outstroke.points[0]
    #text("pkt_Auslauf_oben", pkt_Auslauf_oben)
    
    unten = drawSchriftzug_Z_Bogen_unten(x, y)
    pkt_Auslauf_unten = unten.points[-3]
    #text("pkt_Auslauf_unten", pkt_Auslauf_unten)
    
    Deckung_unten = drawSchriftzug_R_Deckung(x, y)

    FrakturGatC_Z = oben + unten + Deckung_unten + outstroke
    trans_scale(FrakturGatC_Z, valueToMoveGlyph)
    return FrakturGatC_Z, collections.namedtuple('dummy', 'pkt_Auslauf_oben pkt_Auslauf_unten')(pkt_Auslauf_oben, pkt_Auslauf_unten)
    
    
def drawFrakturGatC_Z_thinStroke(x, y, *, pass_from_thick=None):

    FrakturGatC_Z_thinStroke = BezierPath()

    Auslauf_oben = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_oben, LOWER_E, 8, HSL_size=1, HSL_start=12, clockwise=True, inward=True, HSL_size_multipliers=[ 1+.025 * i      for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf_oben.points[-1][0]-modul_width*0.25, Auslauf_oben.points[-1][1]-modul_height*1.04)
    
    Auslauf_unten = drawSchneckenzug(*pass_from_thick.pkt_Auslauf_unten, LOWER_E, 1, HSL_size=1, HSL_start=20, clockwise=True, inward=True)
    
    FrakturGatC_Z_thinStroke += Auslauf_oben + Auslauf_unten + Endpunkt
    drawPath(FrakturGatC_Z_thinStroke)
    trans_thinStroke_down_left(FrakturGatC_Z_thinStroke)
    trans_scale(FrakturGatC_Z_thinStroke, valueToMoveGlyph)
    return FrakturGatC_Z_thinStroke




def drawFrakturGatC_dotStrokeUp(x, y, *, pass_from_thick=None):

    FrakturGatC_dotStrokeUp = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Auslauf, 1, "down")
    Auslauf = drawSchneckenzug(*instroke.points[-1], LOWER_E, 8, HSL_size=1, HSL_start=11, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.6, Auslauf.points[-1][1]-modul_height*1.1)
    
    FrakturGatC_dotStrokeUp += instroke + Auslauf + Endpunkt
    drawPath(FrakturGatC_dotStrokeUp)
    trans_thinStroke_up_right(FrakturGatC_dotStrokeUp)
    trans_scale(FrakturGatC_dotStrokeUp, valueToMoveGlyph)    
    return FrakturGatC_dotStrokeUp
    
    
def drawFrakturGatC_dotStrokeDown(x, y, *, pass_from_thick=None):

    FrakturGatC_dotStrokeDown = BezierPath()
    
    instroke = drawInstroke(*pass_from_thick.pkt_Auslauf, 0.75)
    Auslauf = drawSchneckenzug(*instroke.points[-1], UPPER_E, 8, HSL_size=1, HSL_start=14, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt = drawThinstroke_Endpunkt(Auslauf.points[-1][0]-modul_width*0.75, Auslauf.points[-1][1]-modul_height*0.05)
    
    FrakturGatC_dotStrokeDown += instroke + Auslauf + Endpunkt
    drawPath(FrakturGatC_dotStrokeDown)
    trans_thinStroke_down_left(FrakturGatC_dotStrokeDown)
    trans_scale(FrakturGatC_dotStrokeDown, valueToMoveGlyph)    
    return FrakturGatC_dotStrokeDown
    
    
    
def drawFrakturGatC_dotStrokeUpDown(x, y, *, pass_from_thick=None):

    FrakturGatC_dotStrokeUpDown = BezierPath()
    
    ### UP
    instroke_up = drawInstroke(*pass_from_thick.pkt_Auslauf_up, 1, "down")
    Auslauf_up = drawSchneckenzug(*instroke_up.points[-1], LOWER_E, 8, HSL_size=1, HSL_start=11, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt_up = drawThinstroke_Endpunkt(Auslauf_up.points[-1][0]-modul_width*0.6, Auslauf_up.points[-1][1]-modul_height*1.1)
    
    ### DOWN
    instroke_down = drawInstroke(*pass_from_thick.pkt_Auslauf_down, 0.75)
    Auslauf_down = drawSchneckenzug(*instroke_down.points[-1], UPPER_E, 8, HSL_size=1, HSL_start=14, clockwise=False, inward=True, HSL_size_multipliers=[ 1+.05 * i      for  i in range(0, 9)])
    Endpunkt_down = drawThinstroke_Endpunkt(Auslauf_down.points[-1][0]-modul_width*0.75, Auslauf_down.points[-1][1]-modul_height*0.05)

    FrakturGatC_dotStrokeUp = instroke_up + Auslauf_up + Endpunkt_up
    FrakturGatC_dotStrokeDown = instroke_down + Auslauf_down + Endpunkt_down

    trans_thinStroke_up_right(FrakturGatC_dotStrokeDown)
    trans_thinStroke_down_left(FrakturGatC_dotStrokeDown)  

    FrakturGatC_dotStrokeUpDown = FrakturGatC_dotStrokeUp + FrakturGatC_dotStrokeDown
    drawPath(FrakturGatC_dotStrokeUpDown)
    trans_scale(FrakturGatC_dotStrokeUpDown, valueToMoveGlyph)    
    return FrakturGatC_dotStrokeUpDown
    
    
    
    
    
     
    


# ______________________________________________________    
    
    
margin_1modul = 60
margin_2modul = 120
marginStr = 30
marginRnd = 60 
margin_r=-30
margin_t=0

# ____________ ab hier in RF ____________________________
    
    
font = CurrentFont()

drawFunctions = {

  #   'a' : [ drawFrakturGatC_a, [temp_x, temp_y], marginRnd, marginStr ],
  #   'b' : [ drawFrakturGatC_b, [temp_x, temp_y], marginStr, marginRnd ],
  #   'c' : [ drawFrakturGatC_c, [temp_x, temp_y], marginRnd, margin_r ],
  #   'd' : [ drawFrakturGatC_d, [temp_x, temp_y, 12], marginRnd, marginRnd ],
  #   'e' : [ drawFrakturGatC_e, [temp_x, temp_y], marginRnd, marginStr ],
  #   'f' : [ drawFrakturGatC_f, [temp_x, temp_y], marginStr, -153 ],
  #   'g' : [ drawFrakturGatC_g, [temp_x, temp_y], marginRnd, marginStr+22 ],
  #   'h' : [ drawFrakturGatC_h, [temp_x, temp_y], marginStr, marginRnd ],
  #   'i' : [ drawFrakturGatC_i, [temp_x, temp_y], marginStr, marginStr ],
  #   'j' : [ drawFrakturGatC_j, [temp_x, temp_y], marginStr, marginStr ],
  #   'k' : [ drawFrakturGatC_k, [temp_x, temp_y], marginStr, marginStr-30 ],
  #   'l' : [ drawFrakturGatC_l, [temp_x, temp_y], marginStr, marginStr-30],
  #   'm' : [ drawFrakturGatC_m, [temp_x, temp_y], marginStr, marginStr ],
  #   'n' : [ drawFrakturGatC_n, [temp_x, temp_y], marginStr, marginStr ],
  #   'o' : [ drawFrakturGatC_o, [temp_x, temp_y], marginRnd, marginRnd ],
  #   'p' : [ drawFrakturGatC_p, [temp_x, temp_y], marginStr, marginRnd ],
  #   'q' : [ drawFrakturGatC_q, [temp_x, temp_y], marginRnd, marginStr ],
  #  'r' : [ drawFrakturGatC_r, [temp_x, temp_y], marginStr, margin_r ],
  #   's' : [ drawFrakturGatC_s, [temp_x, temp_y], marginStr, marginStr ],
  #   'longs' : [ drawFrakturGatC_longs, [temp_x, temp_y], marginStr, -153 ],
  #   'germandbls' : [ drawFrakturGatC_germandbls, [temp_x, temp_y], marginStr, marginStr-30 ],
  #   't' : [ drawFrakturGatC_t, [temp_x, temp_y], marginStr, margin_t],
  #   'u' : [ drawFrakturGatC_u, [temp_x, temp_y], marginStr, marginStr ],
  #   'v' : [ drawFrakturGatC_v, [temp_x, temp_y], marginStr, marginRnd ],
  #   'w' : [ drawFrakturGatC_w, [temp_x, temp_y], marginStr, marginRnd ],
  #   'x' : [ drawFrakturGatC_x, [temp_x, temp_y], marginStr, margin_r ],
  #   'y' : [ drawFrakturGatC_y, [temp_x, temp_y], marginStr, marginRnd ],
  #   'z' : [ drawFrakturGatC_z, [temp_x, temp_y, "initial"], marginRnd, marginRnd ],
  #   'z.init' : [ drawFrakturGatC_z, [temp_x, temp_y, "initial"], marginRnd, marginRnd ],
     # 'z.mid' : [ drawFrakturGatC_z, [temp_x, temp_y, "mid"], marginStr+21, marginRnd ],
     # 'z.fina' : [ drawFrakturGatC_z, [temp_x, temp_y, "final"], marginStr+18, marginRnd ],

  #   'adieresis' : [ drawFrakturGatC_adieresis, [temp_x, temp_y], marginRnd, marginStr ],
  #   'odieresis' : [ drawFrakturGatC_odieresis, [temp_x, temp_y], marginRnd, marginRnd ],
  #   'udieresis' : [ drawFrakturGatC_udieresis, [temp_x, temp_y], marginStr, marginStr ],

  #    'b.ss01' : [ drawFrakturGatC_b_dot, [temp_x, temp_y], marginStr, marginRnd ],
  #    'h.ss01' : [ drawFrakturGatC_h_dot, [temp_x, temp_y], marginStr, marginRnd ],
  #    'k.ss01' : [ drawFrakturGatC_k_dot, [temp_x, temp_y], marginStr, marginRnd-61 ],
  #    'l.ss01' : [ drawFrakturGatC_l_dot, [temp_x, temp_y], marginStr, marginRnd-61 ],
  #    't.ss01' : [ drawFrakturGatC_t_dot, [temp_x, temp_y], marginStr, marginRnd-61 ],
  #    'y.ss01' : [ drawFrakturGatC_y_dot, [temp_x, temp_y], marginStr, marginRnd ],


  #   #### Ligatures
    
  # 'r_c' : [ drawFrakturGatC_rc, [temp_x, temp_y], marginStr, marginStr-60 ],
  #  'longs_longs' : [ drawFrakturGatC_longs_longs, [temp_x, temp_y], marginStr-30, -153 ],
  #  'f_f' : [ drawFrakturGatC_f_f, [temp_x, temp_y], marginStr-30, -153 ],
  #  'longs_t' : [ drawFrakturGatC_longs_t, [temp_x, temp_y], marginStr, margin_t ],
  #  'f_t' : [ drawFrakturGatC_f_t, [temp_x, temp_y], marginStr, margin_t ],
  #  'f_f_t' : [ drawFrakturGatC_f_f_t, [temp_x, temp_y], marginStr, margin_t-56 ],


    
  #   ##### Interpunction
    
  #   'period' : [ drawFrakturGatC_period, [temp_x, temp_y], margin_1modul, margin_1modul ],
  #   'colon' : [ drawFrakturGatC_colon, [temp_x, temp_y], margin_1modul, margin_1modul ],
  #   'semicolon' : [ drawFrakturGatC_semicolon, [temp_x, temp_y], margin_1modul, margin_1modul ],
  #   'quoteright' : [ drawFrakturGatC_quoteright, [temp_x, temp_y], margin_1modul, margin_1modul ],
  #   'quotesingle' : [ drawFrakturGatC_quotesingle, [temp_x, temp_y], margin_1modul, margin_1modul ],
  #   'comma' : [ drawFrakturGatC_comma, [temp_x, temp_y], margin_1modul, margin_1modul ],
  #   'endash' : [ drawFrakturGatC_endash, [temp_x, temp_y], margin_1modul, margin_1modul ],
  #   'emdash' : [ drawFrakturGatC_emdash, [temp_x, temp_y], margin_1modul, margin_1modul ],
  #   'hyphen' : [ drawFrakturGatC_hyphen, [temp_x, temp_y], margin_1modul, margin_1modul ],
  #   'exclam' : [ drawFrakturGatC_exclam, [temp_x, temp_y], margin_1modul, margin_1modul ],
  #   'question' : [ drawFrakturGatC_question, [temp_x, temp_y], margin_1modul, margin_1modul ],


  #   ## Numbers
    
  #   'zero' : [ drawFrakturGatC_zero, [temp_x, temp_y], marginRnd, marginRnd-60 ],
  #   'one' : [ drawFrakturGatC_one, [temp_x, temp_y], marginStr+140, marginStr ],
  #   'two' : [ drawFrakturGatC_two, [temp_x, temp_y], marginStr+18, marginStr ],
  #   'three' : [ drawFrakturGatC_three, [temp_x, temp_y], marginStr+60, marginStr-60 ],
  #   'four' : [ drawFrakturGatC_four, [temp_x, temp_y], marginStr, marginStr-15 ],
  #   'five' : [ drawFrakturGatC_five, [temp_x, temp_y], marginStr+120, marginStr-210 ],
  #   'six' : [ drawFrakturGatC_six, [temp_x, temp_y], marginRnd, marginStr ],
  #   'seven' : [ drawFrakturGatC_seven, [temp_x, temp_y], marginStr, -120 ],
  #   'eight' : [ drawFrakturGatC_eight, [temp_x, temp_y], marginRnd, marginRnd-15 ],    
  #   'nine' : [ drawFrakturGatC_nine, [temp_x, temp_y], marginRnd, marginStr ],    



  #   ##### Uppercase
    
  #   'A' : [ drawFrakturGatC_A, [temp_x, temp_y], marginStr, marginStr-90 ],
  #   'B' : [ drawFrakturGatC_B, [temp_x, temp_y], marginStr, marginRnd ],
  #   'C' : [ drawFrakturGatC_C, [temp_x, temp_y], marginStr, marginStr-120 ],
  #   'D' : [ drawFrakturGatC_D, [temp_x, temp_y], marginStr, marginRnd ],
  #   'E' : [ drawFrakturGatC_E, [temp_x, temp_y], marginStr, marginStr-60 ],
  #   'F' : [ drawFrakturGatC_F, [temp_x, temp_y], marginStr, marginStr-90 ],
  #   'G' : [ drawFrakturGatC_G, [temp_x, temp_y], marginStr, marginRnd ],
  #   'H' : [ drawFrakturGatC_H, [temp_x, temp_y], marginStr, 30 ],
  #   'I' : [ drawFrakturGatC_I, [temp_x, temp_y], marginStr, marginStr-60],
  #   'J' : [ drawFrakturGatC_J, [temp_x, temp_y], marginStr, marginRnd-130],
  #   'K' : [ drawFrakturGatC_K, [temp_x, temp_y], marginRnd, marginStr-60 ],
  #   'L' : [ drawFrakturGatC_L, [temp_x, temp_y], marginStr+60, marginStr-60 ],
  #   'M' : [ drawFrakturGatC_M, [temp_x, temp_y], marginStr, marginStr-90 ],
  #   'N' : [ drawFrakturGatC_N, [temp_x, temp_y], marginStr, marginStr-90 ],
  #   'O' : [ drawFrakturGatC_O, [temp_x, temp_y], marginStr+60, marginStr+30 ],
  #   'P' : [ drawFrakturGatC_P, [temp_x, temp_y], marginStr+30, marginStr-90 ],
  #   'Q' : [ drawFrakturGatC_Q, [temp_x, temp_y], marginStr, marginStr ],
  #   'R' : [ drawFrakturGatC_R, [temp_x, temp_y], marginStr, marginStr ],
  #   'S' : [ drawFrakturGatC_S, [temp_x, temp_y], marginStr, marginRnd ],
  #   'T' : [ drawFrakturGatC_T, [temp_x, temp_y], marginStr, marginStr-120 ],
  #   'U' : [ drawFrakturGatC_U, [temp_x, temp_y], marginStr, marginStr+10 ],
  #   'V' : [ drawFrakturGatC_V, [temp_x, temp_y], marginStr, marginStr ],
  #   'W' : [ drawFrakturGatC_W, [temp_x, temp_y], marginStr, marginStr-95 ],
  #   'X' : [ drawFrakturGatC_X, [temp_x, temp_y], marginStr, marginStr-90 ],
  #   'Y' : [ drawFrakturGatC_Y, [temp_x, temp_y], marginStr, marginStr-95 ],
  #   'Z' : [ drawFrakturGatC_Z, [temp_x, temp_y], marginStr, marginRnd ],

  #   'C.ss02' : [ drawFrakturGatC_C, [temp_x, temp_y, "Versal"], marginStr, marginStr-150 ],
  #   'C.ss03' : [ drawFrakturGatC_C, [temp_x, temp_y, "Initial"], marginStr, marginStr-200 ],
  #   'C.ss04' : [ drawFrakturGatC_C, [temp_x, temp_y, "Capital"], marginStr, marginStr-225 ],

    # 'Adieresis' : [ drawFrakturGatC_Adieresis, [temp_x, temp_y], marginStr, marginStr ],
    # 'Odieresis' : [ drawFrakturGatC_Odieresis, [temp_x, temp_y], marginStr+60, marginStr+30 ],
    # 'Udieresis' : [ drawFrakturGatC_Udieresis, [temp_x, temp_y], marginStr, marginStr ],


    }



drawFunctions_thinStroke = {

    'f' : [ drawFrakturGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'g' : [ drawFrakturGatC_g_thinStroke, [temp_x, temp_y] ],
    'j' : [ drawFrakturGatC_g_thinStroke, [temp_x, temp_y] ],
    'p' : [ drawFrakturGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'q' : [ drawFrakturGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'x' : [ drawFrakturGatC_x_thinStroke, [temp_x, temp_y] ],
    'z' : [ drawFrakturGatC_z_thinStroke, [temp_x, temp_y] ],
    'z.init' : [ drawFrakturGatC_z_thinStroke, [temp_x, temp_y] ],
    'z.mid' : [ drawFrakturGatC_x_thinStroke, [temp_x, temp_y] ],
    'z.fina' : [ drawFrakturGatC_x_thinStroke, [temp_x, temp_y] ],

    'b.ss01' : [ drawFrakturGatC_dotStrokeUp, [temp_x, temp_y] ],
    'h.ss01' : [ drawFrakturGatC_dotStrokeUpDown, [temp_x, temp_y] ],
    'k.ss01' : [ drawFrakturGatC_dotStrokeUp, [temp_x, temp_y] ],
    'l.ss01' : [ drawFrakturGatC_dotStrokeUp, [temp_x, temp_y] ],
    't.ss01' : [ drawFrakturGatC_dotStrokeUp, [temp_x, temp_y] ],
    'y.ss01' : [ drawFrakturGatC_dotStrokeDown, [temp_x, temp_y] ],

    'longs' : [ drawFrakturGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'longs_longs' : [ drawFrakturGatC_longs_longs_thinStroke, [temp_x, temp_y] ],
    'longs_t' : [ drawFrakturGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'f_f' : [ drawFrakturGatC_longs_longs_thinStroke, [temp_x, temp_y] ],
    'f_t' : [ drawFrakturGatC_thinstroke_Straight, [temp_x, temp_y] ],
    'f_f_t' : [ drawFrakturGatC_longs_longs_thinStroke, [temp_x, temp_y] ],
    
    'germandbls' : [ drawFrakturGatC_germandbls_Endspitze, [temp_x, temp_y] ],
    'quotesingle' : [ drawFrakturGatC_quotesingle_thinstroke, [temp_x, temp_y] ],  


    'B' : [ drawFrakturGatC_B_thinStroke, [temp_x, temp_y] ],
    'C' : [ drawFrakturGatC_C_thinStroke, [temp_x, temp_y] ],
    'C.ss02' : [ drawFrakturGatC_C_thinStroke_Versal, [temp_x, temp_y] ],
    'C.ss03' : [ drawFrakturGatC_C_thinStroke_Initial, [temp_x, temp_y] ],
    'C.ss04' : [ drawFrakturGatC_C_thinStroke_Capital, [temp_x, temp_y] ],
    'D' : [ drawFrakturGatC_C_thinStroke, [temp_x, temp_y] ],
    'E' : [ drawFrakturGatC_C_thinStroke, [temp_x, temp_y] ],
    'F' : [ drawFrakturGatC_F_thinStroke, [temp_x, temp_y] ],
    'G' : [ drawFrakturGatC_G_thinStroke, [temp_x, temp_y] ],
    'H' : [ drawFrakturGatC_H_thinStroke, [temp_x, temp_y] ],
    'I' : [ drawFrakturGatC_F_thinStroke, [temp_x, temp_y] ],
    'J' : [ drawFrakturGatC_J_thinStroke, [temp_x, temp_y] ],
    'L' : [ drawFrakturGatC_C_thinStroke, [temp_x, temp_y] ],
    'M' : [ drawFrakturGatC_M_thinStroke, [temp_x, temp_y] ],
    'N' : [ drawFrakturGatC_N_thinStroke, [temp_x, temp_y] ],
    'O' : [ drawFrakturGatC_C_thinStroke, [temp_x, temp_y] ],
    'P' : [ drawFrakturGatC_P_thinStroke, [temp_x, temp_y] ],
    'Q' : [ drawFrakturGatC_C_thinStroke, [temp_x, temp_y] ],
    'R' : [ drawFrakturGatC_R_thinStroke, [temp_x, temp_y] ],
    'U' : [ drawFrakturGatC_C_thinStroke, [temp_x, temp_y] ],
    'V' : [ drawFrakturGatC_C_thinStroke, [temp_x, temp_y] ],
    'W' : [ drawFrakturGatC_W_thinStroke, [temp_x, temp_y] ],
    'Y' : [ drawFrakturGatC_Y_thinStroke, [temp_x, temp_y] ],
    'Z' : [ drawFrakturGatC_Z_thinStroke, [temp_x, temp_y] ],

    'Adieresis' : [ drawFrakturGatC_C_thinStroke, [temp_x, temp_y] ],
    'Odieresis' : [ drawFrakturGatC_C_thinStroke, [temp_x, temp_y] ],
    'Udieresis' : [ drawFrakturGatC_C_thinStroke, [temp_x, temp_y] ],

    'one' : [ drawFrakturGatC_one_thinStroke, [temp_x, temp_y] ],
    'two' : [ drawFrakturGatC_two_thinStroke, [temp_x, temp_y] ],
    'three' : [ drawFrakturGatC_three_thinStroke, [temp_x, temp_y] ],
    'five' : [ drawFrakturGatC_three_thinStroke, [temp_x, temp_y] ],
    'six' : [ drawFrakturGatC_six_thinStroke, [temp_x, temp_y] ],
    'seven' : [ drawFrakturGatC_seven_thinStroke, [temp_x, temp_y] ],
    'nine' : [ drawFrakturGatC_nine_thinStroke, [temp_x, temp_y] ],


    }
    
    
   
    
  
    
    
for key in drawFunctions:
    
    glyph = font[[key][0]]
    glyph.clear()
    
    foreground = glyph.getLayer("foreground")
    foreground.clear()
    background = glyph.getLayer("background")
    background.clear()
     
    function = drawFunctions[key][0]
    arguments = drawFunctions[key][1]

    output = function(*arguments)
	
    if key in drawFunctions_thinStroke:
        
        assert isinstance(output, tuple) and len(output)==2
        pass_from_thick_to_thin = output[1]
        output = output[0]
    
    # assert isinstance(output, glyphContext.GlyphBezierPath)
		
    writePathInGlyph(output, foreground)

    print("___",font[[key][0]])
    
    # ask for current status
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin   
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_before = margin_ts - margin_fg
    
    ### change value of margin
    glyph.leftMargin = drawFunctions[key][2]
    glyph.rightMargin = drawFunctions[key][3]
    
    ### ask inbetween 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_mid = margin_ts - margin_fg

    ### calculate value for movement of bg
    move_thinStroke = difference_before - difference_mid

    glyph.copyLayerToLayer('foreground', 'background')
 
   
    if key in drawFunctions_thinStroke:		
        thinstroke = glyph.getLayer("thinstroke")
        thinstroke.clear()

        thinfunction = drawFunctions_thinStroke[key][0]
        thinarguments = drawFunctions_thinStroke[key][1]
        thinoutput = thinfunction(*thinarguments, pass_from_thick=pass_from_thick_to_thin)
        
        writePathInGlyph(thinoutput, thinstroke)
        
    thinstroke = glyph.getLayer("thinstroke")
    thinstroke.translate((move_thinStroke, 0))
    
    ### ask final 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0 
        #print("(no thin line)")   
    difference_final = margin_ts - margin_fg
    
    foreground.clear()

    
    
    
    ### now draw the whole glphy with the magic tool
    ### first thick main stroke 
    guide_glyph = glyph.getLayer("background")

    w = A_A * penWidth 
    a = radians(90-alpha)
    h = penThickness

    if True:
        RectNibPen = RectNibPen_Just

    p = RectNibPen(
        glyphSet=CurrentFont(),
        angle=a,
        width=w,
        height=penThickness,
        trace=True
    )

    guide_glyph.draw(p)
    p.trace_path(glyph)


    ### now thin Stroke
    if key in drawFunctions_thinStroke:		

        guide_glyph = glyph.getLayer("thinstroke")
        w = penThickness 
        p = RectNibPen(
            glyphSet=CurrentFont(),
            angle=a,
            width=w,
            height=penThickness,
            trace=True
        )
        guide_glyph.draw(p)
        p.trace_path(glyph)

    ### keep a copy in background layer
    newLayerName = "original_math"
    glyph.layers[0].copyToLayer(newLayerName, clear=True)
    glyph.getLayer(newLayerName).leftMargin = glyph.layers[0].leftMargin
    glyph.getLayer(newLayerName).rightMargin = glyph.layers[0].rightMargin

    ### this will make it all one shape
    #glyph.removeOverlap(round=0)
    
    ### from robstevenson to remove extra points on curves
    ### function itself is in create_stuff.py
    #select_extra_points(glyph, remove=True)




    
    
    
    
    
    
    
    
    
    
    