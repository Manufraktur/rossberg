import importlib
import version_3.creation.Schriftteile_GatC
import version_3.creation.Schwuenge_GatC

importlib.reload(version_3.creation.Schriftteile_GatC)
importlib.reload(version_3.creation.Schwuenge_GatC)

from version_3.creation.Schriftteile_GatC import *
from version_3.creation.Schwuenge_GatC import *

from version_3.creation.special_drawbot import BezierPath, drawPath



from nibLib.pens.rectNibPen import RectNibPen
from version_3.creation.rect_nib_pen import RectNibPen as RectNibPen_Just
from math import radians
import math as m
import collections
import glyphContext






# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 150
page_height = 150

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)








# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 0


# Hintergrund
#backgroundGrid(page_width_cal, page_height_cal, x_height)
    




# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)



stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 9



# ________________________________________________________


  

leftStartPOS = 60
basePOS = 300
xheightPOS = 540
ascenderPOS = 720
descenderPOS = 60
radiusSerif = 40
penAngle = 450

leftStart = 240

text("base", (leftStartPOS, basePOS))
text("xheight", (leftStartPOS, xheightPOS))
text("asc", (leftStartPOS, ascenderPOS))
text("desc", (leftStartPOS, descenderPOS))

xheight = xheightPOS - basePOS


modul_width = 60
modul_height = 60



def drawFrakturGatC_o(x, y):
        
    glyph_o = BezierPath()
    
    glyph_o.oval(leftStart, basePOS, xheight, xheight)

    drawPath(glyph_o)
    return glyph_o
    

def drawFrakturGatC_d(x, y):
        
    glyph_o = BezierPath()
    
    
    #glyph_o = newPath()
    #moveTo((20,20))
    glyph_o.line((leftStart-modul_width/2, ascenderPOS-modul_height/2), (leftStart, ascenderPOS))
    glyph_o.line((leftStart, ascenderPOS), (leftStart, basePOS+modul_height))
    #glyph_o.arc((leftStart-modul_width+radiusSerif*2, ascenderPOS-modul_height-radiusSerif), modul_width, 140, 320, True)

    #glyph_o.arcTo((10, 50), (40, 50), 10)
    glyph_o.arc((leftStart+modul_width, basePOS+modul_height), modul_width, 180, 320, False)

    
    glyph_o.oval(leftStart, basePOS, xheight, xheight)
    #rundungIn = arc((leftStart, xheight-radiusSerif), radiusSerif, 0, 130, False)
    #straightDown = line(*rundungIn.points[-1], leftStart)

    # rundungIn = 


    #glyph_o = first #+ straightDown
    drawPath(glyph_o)
    return glyph_o
    
    
    
    
    
def drawFrakturGatC_f(x, y):
        
    glyph_o = BezierPath()
    
    #glyph_o.oval(leftStart, basePOS, xheight, xheight)
    glyph_o.line((10,10), (20,30))

    drawPath(glyph_o)
    return glyph_o
    
    
    
    
    
        


# ______________________________________________________    
    
    
margin_1modul = 60
margin_2modul = 120
marginStr = 30
marginRnd = 60 
margin_r=-30
margin_t=0

# ____________ ab hier in RF ____________________________
    
    
font = CurrentFont()

drawFunctions = {

  # 'a' : [ drawFrakturGatC_a, [temp_x, temp_y], marginRnd, marginStr ],
  #   'b' : [ drawFrakturGatC_b, [temp_x, temp_y], marginStr, marginRnd ],
  #   'c' : [ drawFrakturGatC_c, [temp_x, temp_y], marginRnd, margin_r ],
     'd' : [ drawFrakturGatC_d, [temp_x, temp_y], marginRnd, marginRnd ],
  #   'e' : [ drawFrakturGatC_e, [temp_x, temp_y], marginRnd, marginStr ],
  #   'f' : [ drawFrakturGatC_f, [temp_x, temp_y], 40, 40 ],
  #   'g' : [ drawFrakturGatC_g, [temp_x, temp_y], marginRnd, marginStr+22 ],
  #   'h' : [ drawFrakturGatC_h, [temp_x, temp_y], marginStr, marginRnd ],
  #   'i' : [ drawFrakturGatC_i, [temp_x, temp_y], marginStr, marginStr ],
  #   'j' : [ drawFrakturGatC_j, [temp_x, temp_y], marginStr, marginStr ],
  #   'k' : [ drawFrakturGatC_k, [temp_x, temp_y], marginStr, marginStr-30 ],
  #   'l' : [ drawFrakturGatC_l, [temp_x, temp_y], marginStr, marginStr-30],
  #   'm' : [ drawFrakturGatC_m, [temp_x, temp_y], marginStr, marginStr ],
  #   'n' : [ drawFrakturGatC_n, [temp_x, temp_y], marginStr, marginStr ],
  #   'o' : [ drawFrakturGatC_o, [temp_x, temp_y], marginRnd, marginRnd ],
  #   'p' : [ drawFrakturGatC_p, [temp_x, temp_y], marginStr, marginRnd ],
  #   'q' : [ drawFrakturGatC_q, [temp_x, temp_y], marginRnd, marginStr ],
  #  'r' : [ drawFrakturGatC_r, [temp_x, temp_y], marginStr, margin_r ],
  #   's' : [ drawFrakturGatC_s, [temp_x, temp_y], marginStr, marginStr ],
  #   'longs' : [ drawFrakturGatC_longs, [temp_x, temp_y], marginStr, -153 ],
  #   'germandbls' : [ drawFrakturGatC_germandbls, [temp_x, temp_y], marginStr, marginStr-30 ],
  #   't' : [ drawFrakturGatC_t, [temp_x, temp_y], marginStr, margin_t],
  #   'u' : [ drawFrakturGatC_u, [temp_x, temp_y], marginStr, marginStr ],
  #   'v' : [ drawFrakturGatC_v, [temp_x, temp_y], marginStr, marginRnd ],
  #   'w' : [ drawFrakturGatC_w, [temp_x, temp_y], marginStr, marginRnd ],
  #   'x' : [ drawFrakturGatC_x, [temp_x, temp_y], marginStr, margin_r ],
  #   'y' : [ drawFrakturGatC_y, [temp_x, temp_y], marginStr, marginRnd ],
  #   'z' : [ drawFrakturGatC_z, [temp_x, temp_y, "initial"], marginRnd, marginRnd ],
  #   'z.init' : [ drawFrakturGatC_z, [temp_x, temp_y, "initial"], marginRnd, marginRnd ],
     # 'z.mid' : [ drawFrakturGatC_z, [temp_x, temp_y, "mid"], marginStr+21, marginRnd ],
     # 'z.fina' : [ drawFrakturGatC_z, [temp_x, temp_y, "final"], marginStr+18, marginRnd ],

  #   'adieresis' : [ drawFrakturGatC_adieresis, [temp_x, temp_y], marginRnd, marginStr ],
  #   'odieresis' : [ drawFrakturGatC_odieresis, [temp_x, temp_y], marginRnd, marginRnd ],
  #   'udieresis' : [ drawFrakturGatC_udieresis, [temp_x, temp_y], marginStr, marginStr ],

  #    'b.ss01' : [ drawFrakturGatC_b_dot, [temp_x, temp_y], marginStr, marginRnd ],
  #    'h.ss01' : [ drawFrakturGatC_h_dot, [temp_x, temp_y], marginStr, marginRnd ],
  #    'k.ss01' : [ drawFrakturGatC_k_dot, [temp_x, temp_y], marginStr, marginRnd-61 ],
  #    'l.ss01' : [ drawFrakturGatC_l_dot, [temp_x, temp_y], marginStr, marginRnd-61 ],
  #    't.ss01' : [ drawFrakturGatC_t_dot, [temp_x, temp_y], marginStr, marginRnd-61 ],
  #    'y.ss01' : [ drawFrakturGatC_y_dot, [temp_x, temp_y], marginStr, marginRnd ],


    }


   
    
    
     
    
for key in drawFunctions:
    
    glyph = font[[key][0]]
    glyph.clear()
    
    foreground = glyph.getLayer("foreground")
    foreground.clear()
    background = glyph.getLayer("background")
    background.clear()
     
    function = drawFunctions[key][0]
    arguments = drawFunctions[key][1]

    output = function(*arguments)

		
    writePathInGlyph(output, foreground)

    print("___",font[[key][0]])
    
    # ask for current status
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin   
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_before = margin_ts - margin_fg
    
    ### change value of margin
    glyph.leftMargin = drawFunctions[key][2]
    glyph.rightMargin = drawFunctions[key][3]
    
    ### ask inbetween 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0
    difference_mid = margin_ts - margin_fg

    ### calculate value for movement of bg
    move_thinStroke = difference_before - difference_mid

    glyph.copyLayerToLayer('foreground', 'background')
 
  
    
    ### ask final 
    margin_fg = glyph.leftMargin
    margin_ts = glyph.getLayer("thinstroke").leftMargin
    if not bool(margin_ts): margin_ts = 0
    if not bool(margin_fg): margin_fg = 0 
        #print("(no thin line)")   
    difference_final = margin_ts - margin_fg
    
    foreground.clear()

    
    
    
    ### now draw the whole glphy with the magic tool
    ### first thick main stroke 
    guide_glyph = glyph.getLayer("background")

    w = 30
    a = 0.5
    #h = 1
    penThickness = 1
    
    if True:
        RectNibPen = RectNibPen_Just

    p = RectNibPen(
        glyphSet=CurrentFont(),
        angle=a,
        width=w,
        height=penThickness,
        trace=True
    )


    guide_glyph.draw(p)
    p.trace_path(glyph)

    #glyph.scale(8)



    ### keep a copy in background layer
    newLayerName = "original_math"
    glyph.layers[0].copyToLayer(newLayerName, clear=True)
    glyph.getLayer(newLayerName).leftMargin = glyph.layers[0].leftMargin
    glyph.getLayer(newLayerName).rightMargin = glyph.layers[0].rightMargin
    
    
    
    #glyph.scale(8)
    glyph.translate((0,-270))

    ### this will make it all one shape
    #glyph.removeOverlap(round=0)
    
    ### from robstevenson to remove extra points on curves
    ### function itself is in create_stuff.py
    #select_extra_points(glyph, remove=True)




    