import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatC
import version_3.creation.Halbboegen_GatC

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatC)
importlib.reload(version_3.creation.Halbboegen_GatC)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatC import *
from version_3.creation.Halbboegen_GatC import *


import math as m




# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 10
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(1)




# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 5

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)





# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 8.5





def drawSchriftzug9(x, y, version="a", instroke=False):
    
    Schriftzug9 = BezierPath()
    
    #Raute oben
    Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)

    if instroke == True:
        instroke = drawInstroke(*Raute_a_top, 1)
        Schriftzug9 += instroke

    con_stroke_in = drawGrundelementH(*Raute_a_top, 1.5, "down")


    #Raute unten
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)

   
        
    if version == "a":

        downstroke = drawGrundelementF(Raute_d[0], Raute_d[1]+4.5*modul_height, 2.73)
        bend_btm = drawSchriftteil6(*Raute_a, "1")
        Signatur = drawSchriftzug5(x, y-3.5, "b", outstroke=False)

        #Raute oben Abstand definieren
        Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+3.5, y)
        Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+2.5, y-0.5)

        instroke_conElement = drawInstroke(*Raute_a_top, 1, "down")
        
        #oberer Teil der Kurve
        HSL_size = 22.8
        HSL_start = 76

        E2 = instroke_conElement.points[0]
        E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),  E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
        polygon(E1, E2)

        D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
        C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        

        #unterer Teil der Kurve
        HSL_size = 8
        HSL_start = 21.6
        
        B4 = B2
        B3 = B4[0] + HSL_start*part,     B4[1]
        polygon(B1, B2)
        
        A3, A4 = line_A_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size)
        H3, H4 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*2)
        
        Schriftzug9.arc(*drawKreisSeg(H3, HSL_start-HSL_size*2, angle_10, angle_9, True))
        Schriftzug9.arc(*drawKreisSeg(A3, HSL_start-HSL_size, angle_9, angle_8, True))

        Schriftzug9.arc(*drawKreisSeg(B1, 7.6, angle_8, angle_7, True))
        Schriftzug9.arc(*drawKreisSeg(C1, 30.4, angle_7, angle_6, True))
        Schriftzug9.arc(*drawKreisSeg(D1, 53.2, angle_6, angle_5, True))
        

        Schriftzug9 += instroke_conElement + downstroke + bend_btm + Signatur



    if version == "b":

        downstroke = drawGrundelementF(Raute_d[0], Raute_d[1] + 4.5*modul_height, 4.5)
                
        
        #Raute oben Abstand definieren
        Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+3.5, y)
        Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+2.5, y-0.5)

        instroke_conElement = drawInstroke(*Raute_a_top, 1, "down")
        
        #oberer Teil der Kurve
        HSL_size = 24
        HSL_start = 80      

        E2 = instroke_conElement.points[0]
        E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),  E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
        polygon(E1, E2)

        D1, D2 = line_D_vonE_o_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
        C1, C2 = line_C_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
        B1, B2 = line_B_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
        
        
        #unterer Teil der Kurve
        HSL_size = 1.7
        HSL_start = 10
        
        B4 = B2
        B3 = B4[0] + HSL_start*part,     B4[1]
        polygon(B1, B2)
        
        A3, A4 = line_A_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size)
        H3, H4 = line_H_vonA_u_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*2)
        G3, G4 = line_G_vonH_u_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size*3)

        Schriftzug9.arc(*drawKreisSeg(G3, HSL_start-HSL_size*3, angle_11, angle_10, True))
        Schriftzug9.arc(*drawKreisSeg(H3, HSL_start-HSL_size*2, angle_10, angle_9, True))
        Schriftzug9.arc(*drawKreisSeg(A3, HSL_start-HSL_size, angle_9, angle_8, True))

        Schriftzug9.arc(*drawKreisSeg(B1, 8, angle_8, angle_7, True))
        Schriftzug9.arc(*drawKreisSeg(C1, 8+24, angle_7, angle_6, True))
        Schriftzug9.arc(*drawKreisSeg(D1, 8+24+24, angle_6, angle_5, True))
        
        
        Schriftzug9 += downstroke + instroke_conElement
        
    Schriftzug9 += con_stroke_in

    drawPath(Schriftzug9)
    
    return Schriftzug9


#drawSchriftzug9(temp_x, temp_y, "a")        #, instroke=True
#drawSchriftzug9(temp_x, temp_y, "b", instroke=False)        #, instroke=True









def drawSchriftzug9_Lisa(x, y):
    
    Schriftzug9_Lisa = BezierPath()
    
    #Rauten
    Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)

    downstroke = drawGrundelementF(Raute_d[0], Raute_d[1] + 4.5*modul_height, 4.5)
    
    #Raute oben Abstand definieren
    #Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+3.5, y)
    Raute_a_top, Raute_b, Raute_c, Raute_d_top = drawRauteOrientKurTop(offset, offsetDir, x+2.5, y-0.5)

    text("A2", Raute_d)
    text("hier soll E2", Raute_a_top)
        
    #von unten hinauf konstruiert, bei A beginnend bis E
    HSL_size = 4
    HSL_start = 14

    A1, A2 = line_A_vonH_u_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    text("E2", E2)


    drawPath(Schriftzug9_Lisa)
    
    return Schriftzug9_Lisa


#drawSchriftzug9_Lisa(temp_x, temp_y) 






def drawSchriftzug9_Lisa(x, y):
    
    Schriftzug9_Lisa = BezierPath()
    
    #Rauten
    Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x, y)
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurBtm(offset, offsetDir, x, y-3.5)

    downstroke = drawGrundelementF(Raute_d[0], Raute_d[1] + 4.5*modul_height, 4.5)
    
    #Raute oben Abstand definieren
    #Raute_a_top, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+3.5, y)
    Raute_a_top, Raute_b, Raute_c, Raute_d_top = drawRauteOrientKurTop(offset, offsetDir, x+2.5, y-0.5)

    text("A2", Raute_d)
    text("hier soll E2", Raute_a_top)
        
    E2 = Raute_a_top   
    A2 = Raute_d
        
    #E2 = 24, 55.5
    #A2 = 9, 30

    print("E2", E2)
    print("A2", A2)

    cos1psin4 = m.cos(m.radians(angle_seg_1)) + m.sin(m.radians(angle_seg_4))
    cos4msin1 = m.cos(m.radians(angle_seg_4)) - m.sin(m.radians(angle_seg_1))
    
    star1 = 4*m.sin(m.radians(angle_seg_1)) - m.sin(m.radians(angle_seg_1)) - m.cos(m.radians(epsilon)) - m.cos(m.radians(angle_seg_4)) - 1
    star2 = - 4*m.cos(m.radians(angle_seg_1)) + m.cos(m.radians(angle_seg_1)) + m.sin(m.radians(epsilon)) + m.sin(m.radians(angle_seg_4))

    HSL_size_part = ( (E2[1] - A2[1])/cos1psin4 - (E2[0] - A2[0])/cos4msin1 ) / ( star1/cos4msin1 - star2/cos1psin4 )
    print("HSL_size_part", HSL_size_part)

    length_part = (E2[0] - A2[0] + HSL_size_part*star1)/cos4msin1
    length_part2 = (E2[1] - A2[1] + HSL_size_part*star2)/cos1psin4
    print("length_part", length_part)
    print("length_part2", length_part2)
    length = length_part/part
    print("length", length)

    HSL_size = HSL_size_part/part
    print("HSL_size", HSL_size)

    HSL_start_part = length_part #+ 4*HSL_size_part
    print("HSL_start_part", HSL_start_part)

    #Test
    A1_0 = A2[0] + m.cos(m.radians(angle_seg_4)) * (part*length)
    B1_0 = A1_0 + (part*HSL_size)
    C1_0 = B1_0 + m.cos(m.radians(angle_seg_4)) * (part*HSL_size)
    D1_0 = C1_0 + m.cos(m.radians(epsilon)) * (part*HSL_size)
    E1_0 = D1_0 + m.sin(m.radians(angle_seg_1)) * (part*HSL_size) #sin?
    E2_0 = E1_0 - m.sin(m.radians(angle_seg_1)) * (part*length + 4*part*HSL_size)

    A1_1 = A2[1] + m.sin(m.radians(angle_seg_4)) * (part*length)
    B1_1 = A1_1
    C1_1 = B1_1 - m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    D1_1 = C1_1 - m.sin(m.radians(epsilon)) * (part*HSL_size)
    E1_1 = D1_1 - m.cos(m.radians(angle_seg_1)) * (part*HSL_size) #cos?
    E2_1 = E1_1 + m.cos(m.radians(angle_seg_1)) * (part*length + 4*part*HSL_size)

    A1 = A1_0, A1_1
    B1 = B1_0, B1_1
    C1 = C1_0, C1_1
    D1 = D1_0, D1_1
    E1 = E1_0, E1_1
    E2 = E2_0, E2_1

    print("A1[0]",A1)
    print("B1[0]",B1)
    print("C1[0]",C1)
    print("D1[0]",D1)
    print("E1[0]",E1)
    print("E2[0]",E2)
    
    # Diese eine Zeile habe ich noch dazu weil sonst Fehlermeldung.
    HSL_start = HSL_start_part/part

    text("E2 ________________vorher", E2)
    
    A1, A2 = line_A_vonH_u_gr_s(Raute_d, *angles, part, HSL_size, HSL_start)
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size)
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    
    text("E2 nachher", E2)


    drawPath(Schriftzug9_Lisa)
    
    return Schriftzug9_Lisa


drawSchriftzug9_Lisa(temp_x, temp_y) 





