import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatC

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatC)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatC import *


import math as m
from drawBot import *



"""
11. Mai 2020
Bd 1, Tab 5

Es stimmt: Um auf die gleiche Höhe zu kommen, muss man für links und rechts
andere Werte benutzen. Das liegt daran, dass die Schneckenlinie bei E startet, 
also nicht “symmetrisch” ist und daher einen weiteren Weg zurücklegen muss um  
auf die parallele Linie unten zu kommen, wenn sie nach rechts konstruiert wird. 
Nach links erreicht sie die paralelle Linie unten schneller, 
                                        d.h. der Wert fängt höher an.

<<< 17.5   vs. 17.1 >>>     bei 4 Modulen Höhe
<<< 20.7   vs. 20.3 >>>     bei 5 Modulen Höhe

"""





temp_x = 5 #* modul_width
temp_y = 9 #* modul_height



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 10
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.5)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)
  

    

# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)






# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)







def drawHSL_left_4(x, y):
    
    HSL_left_4 = BezierPath()

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-4) 
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+0.25)
    drawGrundelOrient(A1, A2, offset, x, y-0.75)
    drawGrundelOrient(A1, A2, offset, x, y-1.75)
    #drawGrundelOrient(A1, A2, offset, x, y-2.75)
    
    Linie_oben_1 = drawInstroke(*Grund_a, 3)
    Linie_oben_2 = drawOutstroke(*Grund_a, 4)
    
    HSL_size = 1
    HSL_start = 17.5



    E1, E2 = line_E_vonF_u_kl_s(Raute_b, *angles, part, HSL_size, HSL_start)
    D1, D2 = line_D_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    H1, H2 = line_H_vonA_o_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    G1, G2 = line_G_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    F1, F2 = line_F_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonF_o_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*8)
    D3, D4 = line_D_vonE_o_kl(E3, *angles, part, HSL_size, HSL_start-HSL_size*9)
    C3, C4 = line_C_vonD_o_kl(D3, *angles, part, HSL_size, HSL_start-HSL_size*10)
    B3, B4 = line_B_vonC_o_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*11)
    A5, A6 = line_A_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*12)
    H3, H4 = line_H_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*13)
    G3, G4 = line_G_vonH_u_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size*14)
    F3, F4 = line_F_vonG_u_kl(G3, *angles, part, HSL_size, HSL_start-HSL_size*15)

    HSL_left_4.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_13, angle_14))
    HSL_left_4.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_14, angle_15))
    HSL_left_4.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_15, angle_16))
    HSL_left_4.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_0, angle_1))
    HSL_left_4.arc(*drawKreisSeg(H1, HSL_start-HSL_size*5, angle_1, angle_2))
    HSL_left_4.arc(*drawKreisSeg(G1, HSL_start-HSL_size*6, angle_2, angle_3))
    HSL_left_4.arc(*drawKreisSeg(F1, HSL_start-HSL_size*7, angle_3, angle_4))
    HSL_left_4.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_4, angle_5))
    HSL_left_4.arc(*drawKreisSeg(D3, HSL_start-HSL_size*9, angle_5, angle_6))
    HSL_left_4.arc(*drawKreisSeg(C3, HSL_start-HSL_size*10, angle_6, angle_7))
    HSL_left_4.arc(*drawKreisSeg(B3, HSL_start-HSL_size*11, angle_7, angle_8))
    HSL_left_4.arc(*drawKreisSeg(A5, HSL_start-HSL_size*12, angle_8, angle_9))
    HSL_left_4.arc(*drawKreisSeg(H3, HSL_start-HSL_size*13, angle_9, angle_10))
    HSL_left_4.arc(*drawKreisSeg(G3, HSL_start-HSL_size*14, angle_10, angle_11))
    HSL_left_4.arc(*drawKreisSeg(F3, HSL_start-HSL_size*15, angle_11, angle_12))
    HSL_left_4.arc(*drawKreisSeg(E1, HSL_start-HSL_size*16, angle_12, angle_13))

    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-2.75)    
    
    Linie_unten_1 = drawInstroke(*Grund_d, 2)
    Linie_unten_2 = drawOutstroke(*Grund_d, 5)
    
    drawPath(HSL_left_4)
        
    return HSL_left_4
    
    
drawHSL_left_4(temp_x, temp_y)






def drawHSL_right_4(x, y):
    
    HSL_right_4 = BezierPath()

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-4) 
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y+0.25)
    drawGrundelOrient(A1, A2, offset, x, y-0.75)
    drawGrundelOrient(A1, A2, offset, x, y-1.75)
    #drawGrundelOrient(A1, A2, offset, x, y-2.75)
    
    Linie_oben_1 = drawInstroke(*Grund_a, 3)
    Linie_oben_2 = drawOutstroke(*Grund_a, 4)
    
    HSL_size = 1
    HSL_start = 17.1
        
    E1, E2 = line_E_vonD_u_kl_s(Raute_b, *angles, part, HSL_size, HSL_start)
    F1, F2 = line_F_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    G1, G2 = line_G_vonF_u_kl(F1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    H1, H2 = line_H_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    A3, A4 = line_A_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    B1, B2 = line_B_vonA_o_kl(A3, *angles, part, HSL_size, HSL_start-HSL_size*5)
    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    E3, E4 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*8)
    F3, F4 = line_F_vonE_o_kl(E3, *angles, part, HSL_size, HSL_start-HSL_size*9)
    G3, G4 = line_G_vonF_o_kl(F3, *angles, part, HSL_size, HSL_start-HSL_size*10)
    H3, H4 = line_H_vonG_o_kl(G3, *angles, part, HSL_size, HSL_start-HSL_size*11)
    A5, A6 = line_A_vonH_o_kl(H3, *angles, part, HSL_size, HSL_start-HSL_size*12)
    B3, B4 = line_B_vonA_u_kl(A5, *angles, part, HSL_size, HSL_start-HSL_size*13)
    C3, C4 = line_C_vonB_u_kl(B3, *angles, part, HSL_size, HSL_start-HSL_size*14)
    D3, D4 = line_D_vonC_u_kl(C3, *angles, part, HSL_size, HSL_start-HSL_size*15)

    
    HSL_right_4.arc(*drawKreisSeg(F1, HSL_start-HSL_size, angle_13, angle_12, True))
    HSL_right_4.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_12, angle_11, True))
    HSL_right_4.arc(*drawKreisSeg(H1, HSL_start-HSL_size*3, angle_11, angle_10, True))
    HSL_right_4.arc(*drawKreisSeg(A3, HSL_start-HSL_size*4, angle_10, angle_9, True))
    HSL_right_4.arc(*drawKreisSeg(B1, HSL_start-HSL_size*5, angle_9, angle_8, True))
    HSL_right_4.arc(*drawKreisSeg(C1, HSL_start-HSL_size*6, angle_8, angle_7, True))
    HSL_right_4.arc(*drawKreisSeg(D1, HSL_start-HSL_size*7, angle_7, angle_6, True))
    HSL_right_4.arc(*drawKreisSeg(E3, HSL_start-HSL_size*8, angle_6, angle_5, True))
    HSL_right_4.arc(*drawKreisSeg(F3, HSL_start-HSL_size*9, angle_5, angle_4, True))
    HSL_right_4.arc(*drawKreisSeg(G3, HSL_start-HSL_size*10, angle_4, angle_3, True))
    HSL_right_4.arc(*drawKreisSeg(H3, HSL_start-HSL_size*11, angle_3, angle_2, True))
    HSL_right_4.arc(*drawKreisSeg(A5, HSL_start-HSL_size*12, angle_2, angle_1, True))
    HSL_right_4.arc(*drawKreisSeg(B3, HSL_start-HSL_size*13, angle_1, angle_0, True))
    HSL_right_4.arc(*drawKreisSeg(C3, HSL_start-HSL_size*14, angle_16, angle_15, True))
    HSL_right_4.arc(*drawKreisSeg(D3, HSL_start-HSL_size*15, angle_15, angle_14, True))
    HSL_right_4.arc(*drawKreisSeg(E1, HSL_start-HSL_size*16, angle_14, angle_13, True))
    
    
    Grund_a, Grund_b, Grund_c, Grund_d = drawGrundelOrient(A1, A2, offset, x, y-2.75)    
    
    Linie_unten_1 = drawInstroke(*Grund_d, 2)
    Linie_unten_2 = drawOutstroke(*Grund_d, 5)
    
    drawPath(HSL_right_4)
        
    return HSL_right_4
    


drawHSL_right_4(temp_x, temp_y)


