import importlib
import version_3.creation.Grundelemente
import version_3.creation.Schriftteile_GatC

importlib.reload(version_3.creation.Grundelemente)
importlib.reload(version_3.creation.Schriftteile_GatC)

from version_3.creation.Grundelemente import *
from version_3.creation.Schriftteile_GatC import *

from version_3.creation.arc_path import ArcPath as BezierPath
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m




# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 29
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)



def backgroundGrid(page_width_cal, page_height_cal, x_height=6):
    
    save()
    stroke(0.8)
    strokeWidth(.001)
    
    # Horizontale Linien
    
    if x_height == 6:   ### Fraktur, Kanzlei = 6

        baseline = modul_height * 4
        
        line_pos_hor = 0
        while line_pos_hor < page_height_cal:
            line((0, line_pos_hor), (page_width_cal, line_pos_hor))
            line_pos_hor += modul_height
    
        whitespace = modul_height
        valueToMoveGlyph = 3.75
    
    
    elif x_height == 5:  ### Kurrentkanzlei = 5
        
        baseline = modul_height * 4.5

        line_pos_hor = [0.5, 1, 5, 5.5, 6, 8, 8.5, 9, 13, 13.5]
        for lph in line_pos_hor:
            line((0, modul_height*lph), (page_width_cal, modul_height*lph))
        
        whitespace = modul_height/2
        valueToMoveGlyph = 4.25

        
    
    elif x_height == 1.5:  ### Kurrent = 1.5
        
        baseline = modul_height * 6
        
        line_pos_hor = [0.5, 6.5, 7, 7.5, 8, 13.5]
        for lph in line_pos_hor:
            line((0, modul_height*lph), (page_width_cal, modul_height*lph))
        
        whitespace = 0
        valueToMoveGlyph = 5.75


    else:
        print("Please enter correct x-height: \nFraktur, Kanzlei = 6\nKurrentkanzlei = 5\nKurrent = 2")
        valueToMoveGlyph = 0
    
    # Linien dicker -- hervorgehoben zur Orientierung
    stroke(0.5)
    #strokeWidth(.075)
    
    # Grundline
    #line((0, baseline), (page_width_cal, baseline))

    # x-Höhe
    line((0, baseline+x_height*modul_height), (page_width_cal, baseline+x_height*modul_height))
    
    
    # Vertikale Linien
    stroke(0.8)
    strokeWidth(.001)
    
    line_pos_ver = 0
    while line_pos_ver < page_width_cal:
        line((line_pos_ver, whitespace), (line_pos_ver, page_height_cal-whitespace))
        line_pos_ver += modul_width
        
    restore()

    baseline = baseline/modul_height 
    return baseline, valueToMoveGlyph





# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
baseline, valueToMoveGlyph = backgroundGrid(page_width_cal, page_height_cal, x_height)

# temporary to check height of numbers
#line((1*modul_width, 12.5*modul_height), (12*modul_width, 12.5*modul_height))





# ______________ Modul, Raute, Offset __________________  

# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)

stroke(.1)
strokeWidth(.1)
    
    
temp_x = 3
temp_y = 7





def drawHSL_toCompare(x, y):
    
    drawHSL_toCompare = BezierPath() 
    
    x = 20
    y = 60
    
    loop = drawSchneckenzug(x, y, UPPER_E, 12, HSL_size=1, HSL_start=10, clockwise=True, inward=False)

    drawHSL_toCompare = loop
    drawPath(drawHSL_toCompare)  
    return drawHSL_toCompare
    
drawHSL_toCompare(temp_x, temp_y)



######### HSL_size gets bigger  ##########################################


# def drawHSL_toCompare(x, y):
    
#     drawHSL_toCompare = BezierPath() 
    
#     x = 70
#     y = 60
    
#     loop = drawSchneckenzug(x, y, UPPER_E, 12, HSL_size=2, HSL_start=10, clockwise=True, inward=False)
    
#     drawHSL_toCompare = loop
#     drawPath(drawHSL_toCompare)  
#     return drawHSL_toCompare
    
# drawHSL_toCompare(temp_x, temp_y)



# def drawHSL_toCompare(x, y):
    
#     drawHSL_toCompare = BezierPath() 
    
#     x = 140
#     y = 60
    
#     loop = drawSchneckenzug(x, y, UPPER_E, 12, HSL_size=4, HSL_start=10, clockwise=True, inward=False)
    
#     drawHSL_toCompare = loop
#     drawPath(drawHSL_toCompare)  
#     return drawHSL_toCompare
    
# drawHSL_toCompare(temp_x, temp_y)

# saveImage("~/Desktop/SL_size gets bigger.PDF")


######### HSL_size = 0  ##########################################


# def drawHSL_toCompare(x, y):
    
#     drawHSL_toCompare = BezierPath() 
    
#     x = 70
#     y = 60
    
#     loop = drawSchneckenzug(x, y, UPPER_E, 12, HSL_size=0, HSL_start=10, clockwise=True, inward=False)
    
#     drawHSL_toCompare = loop
#     drawPath(drawHSL_toCompare)  
#     return drawHSL_toCompare
    
# drawHSL_toCompare(temp_x, temp_y)



# def drawHSL_toCompare(x, y):
    
#     drawHSL_toCompare = BezierPath() 
    
#     x = 125
#     y = 60
    
#     loop = drawSchneckenzug(x, y, UPPER_E, 16, HSL_size=0, HSL_start=10, clockwise=True, inward=False)
    
#     drawHSL_toCompare = loop
#     drawPath(drawHSL_toCompare)  
#     return drawHSL_toCompare
    
# drawHSL_toCompare(temp_x, temp_y)


# saveImage("~/Desktop/HSL_size_equals_zero.PDF")




######### HSL_start gets bigger  ##########################################

# def drawHSL_toCompare(x, y):
    
#     drawHSL_toCompare = BezierPath() 
    
#     x = 65
#     y = 60
    
#     loop = drawSchneckenzug(x, y, UPPER_E, 12, HSL_size=1, HSL_start=20, clockwise=True, inward=False)
    
#     drawHSL_toCompare = loop
#     drawPath(drawHSL_toCompare)  
#     return drawHSL_toCompare
    
# drawHSL_toCompare(temp_x, temp_y)




# def drawHSL_toCompare(x, y):
    
#     drawHSL_toCompare = BezierPath() 
    
#     x = 125
#     y = 60
    
#     loop = drawSchneckenzug(x, y, UPPER_E, 12, HSL_size=1, HSL_start=30, clockwise=True, inward=False)
    
#     drawHSL_toCompare = loop
#     drawPath(drawHSL_toCompare)  
#     return drawHSL_toCompare
    
# drawHSL_toCompare(temp_x, temp_y)


# saveImage("~/Desktop/HSL_start gets bigger.PDF")




######### HSL variations  ##########################################

# def drawHSL_toCompare(x, y):
    
#     drawHSL_toCompare = BezierPath() 
    
#     x = 60
#     y = 60
    
#     loop = drawSchneckenzug(x, y, UPPER_E, 12, HSL_size=1, HSL_start=10, clockwise=False, inward=False)

#     drawHSL_toCompare = loop
#     drawPath(drawHSL_toCompare)  
#     return drawHSL_toCompare
    
# drawHSL_toCompare(temp_x, temp_y)


# def drawHSL_toCompare(x, y):
    
#     drawHSL_toCompare = BezierPath() 
    
#     x = 110
#     y = 60
    
#     loop = drawSchneckenzug(x, y, UPPER_E, 10, HSL_size=1, HSL_start=10, clockwise=True, inward=True)

#     drawHSL_toCompare = loop
#     drawPath(drawHSL_toCompare)  
#     return drawHSL_toCompare
    
# drawHSL_toCompare(temp_x, temp_y)


# def drawHSL_toCompare(x, y):
    
#     drawHSL_toCompare = BezierPath() 
    
#     x = 150
#     y = 60
    
#     loop = drawSchneckenzug(x, y, UPPER_E, 10, HSL_size=1, HSL_start=10, clockwise=False, inward=True)

#     drawHSL_toCompare = loop
#     drawPath(drawHSL_toCompare)  
#     return drawHSL_toCompare
    
# drawHSL_toCompare(temp_x, temp_y)



# saveImage("~/Desktop/HSL_variations.PDF")
   