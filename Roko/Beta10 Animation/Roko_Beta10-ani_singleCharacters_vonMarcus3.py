'''animate individual characters in a variable font'''

fontName = 'Roko_Beta10_Animation-50_Con00'
#txt = 'Technik \nund \nSchrift-\ngestaltung'
txt = 'Technik \nund \nSchriftgestaltung'



# variable font range 
variations = listFontVariations(fontName)
#variations = { 'x': {'minValue':0, 'maxValue':1000}}
axis = list(variations.keys())[0]
minW = variations[axis]['minValue']
maxW = variations[axis]['maxValue']

W1 = [ minW + round((maxW - minW)/(len(txt)-1) * i) for i in range(len(txt)) ]
W2 = W1.copy()
W2.reverse()
W = W1[:-1] + W2[:-1]
steps = len(W)


### switch for slower animation
for off_ in range(2*steps):
    off = off_//2
#for off in range(steps):

    newPage(1400, 800)
    fill(1)
    rect(0,0,width(),height())

    save()

    T = FormattedString()
    T.fontSize(140)
    T.font(fontName)

    for j, char in enumerate(txt):
        pos = (off + j) % steps
        T.append(char, fontVariations={axis:W[pos]})
        
        
        
    restore()
    text(T, (width() / 2, height() / 4*2.65), align='center')
    
    ### to have single images
    #saveImage(f'hellohello_{axis}{pos}.png')

### to have the animation in same folder
saveImage(f'animation_{fontName}.gif')












