# -------------------------------
# EDIT THESE VARIABLES
# -------------------------------
# The size of the page
w, h = 1200, 490
text_string = 'Technik und Schriftgestaltung'
font_name = 'Roko_Beta10_Animation-50_Con00'

lines = 1
total_frames = 100
total_duration = 10 # in seconds
# -------------------------------

# Count the number of characters
chars = len(text_string)
# Calculate the font size
font_size = 75
# Get and store the minimum and maximum wdth values
min_var = listFontVariations(font_name)['wdth']['minValue']
max_var = listFontVariations(font_name)['wdth']['maxValue']
# Calculate the duration of each frame
frame_duration = total_duration / total_frames

# For each frame...
for frame in range(total_frames):

    # Create a new page and set its duration
    newPage(w, h)
    frameDuration(frame_duration)

    # Draw the background rectangle
    #fill(bg_r/255, bg_g/255, bg_b/255)
    fill(1)
    rect(0,0,w,h)
    
    # Calculate the vertical position of the first line
    y = (h - (lines * font_size)) / 2
    y = y + w/50

    # Start each frame in a different wdth value
    frame_start = (2 * pi) * frame / total_frames

    # For each line...
    for line in range(lines):
        
        # Start each LINE in a different wdth value
        step_start = frame_start - (pi * (line))
        
        # Create a new empty text object and set its properties
        txt = FormattedString()
        #txt.fill(fg_r/255, fg_g/255, fg_b/255)
        txt.fill(0)
        txt.font(font_name)
        txt.fontSize(font_size)
        #txt.openTypeFeatures(ss01=True)

        # For each character
        for char_index in range(chars):
            
            # Calculate the wdth value
            inst_step = pi * (char_index / (chars -1))
            curr_step = (cos(step_start - inst_step) + 1) / 2
            wdth_value = min_var + curr_step * (max_var - min_var)
            
            # Add the character to the text object
            txt.append(
                text_string[char_index], 
                fontVariations=dict(wdth=wdth_value)
                )
        
        # After all characters were added 
        # to the text object, get its dimensions
        text_width, text_height = textSize(txt)

        # Calculate the horizontal position required 
        # to center this line on the page
        x = (w - text_width) /2
        
        # Finally draw this line of text
        text(txt, (x, y))

        # Move the y coordinate up for the next line
        y = y + font_size
    
# Save the animation as a gif
saveImage("Animation.gif")