import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m




temp_x = modul_width * 3
temp_y = modul_height * 9



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 5
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)

    

# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)


drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)+2)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)+1)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height))

drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-1)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-2)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-3)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-3.5)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-4)


# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)
    
strokeWidth(.1)






def drawHalbbogen1(x, y,  instrokeLen=0.5, outstrokeLen=0):
    
    Halbbogen1 = BezierPath()
        
    # Fig. 1. 
    # ist ein Halbbogen nach einem Bestandtheil nach der ersten Hauptschneckenlinie 
    # mit einem Radio von 2. Part, bis zu Ende mit 7. Part construiert.
    # 1. HSL; G>E; 2>7
    
    instroke = drawInstroke(x, y, instrokeLen)
    
    HSL_size = 1
    HSL_start = 2
   
    G1, G2 = line_G_vonF_o_gr_s((x, y), *angles, part, HSL_size, HSL_start)
        
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+1)
    Halbbogen1.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_2, True))
    
    A1, A2 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+2)
    Halbbogen1.arc(*drawKreisSeg(H1, HSL_start+1, angle_2, angle_1, True))
    
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+3)
    Halbbogen1.arc(*drawKreisSeg(A1, HSL_start+2, angle_1, angle_0, True))

    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+4)
    Halbbogen1.arc(*drawKreisSeg(B1, HSL_start+3, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+5)
    Halbbogen1.arc(*drawKreisSeg(C1, HSL_start+4, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+6)
    Halbbogen1.arc(*drawKreisSeg(D1, HSL_start+5, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E2, outstrokeLen, "down")

    Halbbogen1 += instroke + outstroke
    drawPath(Halbbogen1)    
    return Halbbogen1
   
   
#drawHalbbogen1(temp_x, temp_y)
   
   
   
   
   



    

def drawHalbbogen2(x, y, instrokeLen=0.5, outstrokeLen=0):
    
    Halbbogen2 = BezierPath()
        
    instroke = drawInstroke(x, y, instrokeLen)
        
    # Fig. 2.
    # ist ein Halbbogen 1 1/2 Bestandtheil der ersten Hauptschneckenlinie enthaltend, 
    # mit einem veränderlichen Radio von 4. bis 9. Part construiret.
    # 1. HSL; E>G; 4>9

    HSL_size = 1
    HSL_start = 4
        
    G1, G2 = line_G_vonF_o_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+1)
    Halbbogen2.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_2, True))
    
    A1, A2 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+2)
    Halbbogen2.arc(*drawKreisSeg(H1, HSL_start+1, angle_2, angle_1, True))
    
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+3)
    Halbbogen2.arc(*drawKreisSeg(A1, HSL_start+2, angle_1, angle_0, True))
    
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+4)
    Halbbogen2.arc(*drawKreisSeg(B1, HSL_start+3, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+5)
    Halbbogen2.arc(*drawKreisSeg(C1, HSL_start+4, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+6)
    Halbbogen2.arc(*drawKreisSeg(D1, HSL_start+5, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E2, outstrokeLen, "down")

    Halbbogen2 += instroke + outstroke
    drawPath(Halbbogen2) 
    return Halbbogen2


#drawHalbbogen2(temp_x, temp_y)






def drawHalbbogen3(x, y, instrokeLen=0.5, outstrokeLen=0):
    
    Halbbogen3 = BezierPath()

    instroke = drawInstroke(x, y, instrokeLen)
                
    # Fig. 3.
    # ist ein Halbbogen aus 2. Bestandtheilen, nach eben derselben Schneckenlinie, 
    # durch einen 6. bis 11. Part wachsenden Radium construiret.
    # 1. HSL; E>G; 6>11

    HSL_size = 1
    HSL_start = 6
        
    G1, G2 = line_G_vonF_o_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+1)
    Halbbogen3.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_2, True))
    
    A1, A2 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+2)
    Halbbogen3.arc(*drawKreisSeg(H1, HSL_start+1, angle_2, angle_1, True))
    
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+3)
    Halbbogen3.arc(*drawKreisSeg(A1, HSL_start+2, angle_1, angle_0, True))
    
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+4)
    Halbbogen3.arc(*drawKreisSeg(B1, HSL_start+3, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+5)
    Halbbogen3.arc(*drawKreisSeg(C1, HSL_start+4, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+6)
    Halbbogen3.arc(*drawKreisSeg(D1, HSL_start+5, angle_14, angle_13, True))
    
    outstroke = drawOutstroke(*E2, outstrokeLen, "down")

    Halbbogen3 += instroke + outstroke
    drawPath(Halbbogen3) 
    return Halbbogen3
    
    
#drawHalbbogen3(temp_x, temp_y)

    

    
    
def drawHalbbogen4(x, y, instrokeLen=0.5, outstrokeLen=0):
    
    Halbbogen4 = BezierPath()

    
    instroke = drawInstroke(x, y, instrokeLen)
            
    # Fig. 4.
    # ist ein Halbbogen aus 2 1/2 Bestandtheil, nach einer mit 1 1/4 Part Vieleck-Maases construierten Schneckenlinie, 
    # von 8. bis mit 14 1/2 Part construiret.
    # >>> ich komme hier nur auf 14.25!
    # 1.25 HSL; E>G; 8>14.25

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    
    ### Rossberg gibt 8 an, mit 7.55 würde es genau passen :/
    #bogen = drawSchneckenzug(x, y , UPPER_G, 6, HSL_size=1.25, HSL_start=7.55, clockwise=True, inward=False)
    bogen = drawSchneckenzug(x, y , UPPER_G, 6, HSL_size=1.25, HSL_start=8, clockwise=True, inward=False)
   
    outstroke = drawOutstroke(*bogen.points[-1], outstrokeLen, "down")

    Halbbogen4 += instroke + bogen + outstroke
    drawPath(Halbbogen4)
    return Halbbogen4


#drawHalbbogen4(temp_x, temp_y)
    
    
    
    
    

def drawHalbbogen5(x, y, instrokeLen=0.5, outstrokeLen=0):
    
    Halbbogen5 = BezierPath()


    instroke = drawInstroke(x, y, instrokeLen)
            
    # Fig. 5.
    # ist ein Halbbogen aus 3. Bestandtheilen, nach einer mit 1 1/2 Part Vieleck-Maases 
    # construierten Schneckenlinie, von 9. bis mit 16 Part construiret.
    # 1.5 HSL; E>G; 9>16

    HSL_size = 1.5   # 1 1/2 Hauptschneckenlinie
    HSL_start = 10
        
    G1, G2 = line_G_vonF_o_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    H1, H2 = line_H_vonG_o_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*1)
    Halbbogen5.arc(*drawKreisSeg(G1, HSL_start, angle_3, angle_2, True))
    
    A1, A2 = line_A_vonH_o_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen5.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_2, angle_1, True))
    
    B1, B2 = line_B_vonA_u_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen5.arc(*drawKreisSeg(A1, HSL_start+HSL_size*2, angle_1, angle_0, True))
    
    C1, C2 = line_C_vonB_u_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen5.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_16, angle_15, True))
    
    D1, D2 = line_D_vonC_u_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Halbbogen5.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_15, angle_14, True))
    
    E1, E2 = line_E_vonD_u_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Halbbogen5.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_14, angle_13, True))

    outstroke = drawOutstroke(*E2, outstrokeLen, "down")

    Halbbogen5 += instroke + outstroke
    drawPath(Halbbogen5)
    return Halbbogen5

#drawHalbbogen5(temp_x, temp_y)
    
    
    
    
    
def drawHalbbogen6(x, y, instrokeLen=0, outstrokeLen=0.5):
    
    Halbbogen6 = BezierPath()
        
    # siehe Fig. 5.

    HSL_size = 1.5
    HSL_start = 10
        
    G1, G2 = line_G_vonF_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)

    H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*1)
    Halbbogen6.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_10, True))
    
    A1, A2 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen6.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_9, True))
    
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen6.arc(*drawKreisSeg(A1, HSL_start+HSL_size*2, angle_9, angle_8, True))
    
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen6.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Halbbogen6.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Halbbogen6.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_6, angle_5, True))


    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(x, y, outstrokeLen)
    
    Halbbogen6 += instroke + outstroke
    drawPath(Halbbogen6)
    return Halbbogen6
    
#drawHalbbogen6(temp_x, temp_y)

    
    
          
    
def drawHalbbogen7(x, y, instrokeLen=0, outstrokeLen=0.5):
    
    Halbbogen7 = BezierPath()
        
    # siehe Fig. 4.

    HSL_size = 1.25 
    HSL_start = 8
        
    G1, G2 = line_G_vonF_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*1)
    Halbbogen7.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_10, True))
    
    A1, A2 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen7.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_9, True))
    
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen7.arc(*drawKreisSeg(A1, HSL_start+HSL_size*2, angle_9, angle_8, True))
    
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen7.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Halbbogen7.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Halbbogen7.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_6, angle_5, True))


    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(x, y, outstrokeLen)
    
    Halbbogen7 += instroke + outstroke
    drawPath(Halbbogen7)  
    return Halbbogen7
        
#drawHalbbogen7(temp_x, temp_y)  




def drawHalbbogen8(x, y, instrokeLen=0, outstrokeLen=0.5):
    
    Halbbogen8 = BezierPath()
        
    # siehe Fig. 3.
    
    HSL_size = 1     # 1. Hauptschneckenlinie
    HSL_start = 6
    
    G1, G2 = line_G_vonF_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+1)
    Halbbogen8.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_10, True))
    
    A1, A2 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+2)
    Halbbogen8.arc(*drawKreisSeg(H1, HSL_start+1, angle_10, angle_9, True))
    
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+3)
    Halbbogen8.arc(*drawKreisSeg(A1, HSL_start+2, angle_9, angle_8, True))
    
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+4)
    Halbbogen8.arc(*drawKreisSeg(B1, HSL_start+3, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+5)
    Halbbogen8.arc(*drawKreisSeg(C1, HSL_start+4, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+6)
    Halbbogen8.arc(*drawKreisSeg(D1, HSL_start+5, angle_6, angle_5, True))


    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(x, y, outstrokeLen)
    
    Halbbogen8 += instroke + outstroke
    drawPath(Halbbogen8)
    return Halbbogen8
    
#drawHalbbogen8(temp_x, temp_y, instrokeLen=0)  

    
    

def drawHalbbogen9(x, y, instrokeLen=0, outstrokeLen=0.5):
    
    Halbbogen9 = BezierPath()
        
    # siehe Fig. 2.

    HSL_size = 1 
    HSL_start = 4
        
    G1, G2 = line_G_vonF_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+1)
    Halbbogen9.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_10, True))
    
    A1, A2 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+2)
    Halbbogen9.arc(*drawKreisSeg(H1, HSL_start+1, angle_10, angle_9, True))
    
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+3)
    Halbbogen9.arc(*drawKreisSeg(A1, HSL_start+2, angle_9, angle_8, True))
    
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+4)
    Halbbogen9.arc(*drawKreisSeg(B1, HSL_start+3, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+5)
    Halbbogen9.arc(*drawKreisSeg(C1, HSL_start+4, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+6)
    Halbbogen9.arc(*drawKreisSeg(D1, HSL_start+5, angle_6, angle_5, True))


    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(x, y, outstrokeLen)
    
    Halbbogen9 += instroke + outstroke
    drawPath(Halbbogen9)   
    return Halbbogen9

#drawHalbbogen9(temp_x, temp_y)  





def drawHalbbogen10(x, y, StartSize=2, instrokeLen=0, outstrokeLen=0.5):
    
    Halbbogen10 = BezierPath()
        
    # siehe Fig. 1. 

    HSL_size = 1   
    HSL_start = StartSize      ### Hier gabe es vorher eine version 10b mit HSL_start=3
        
    G1, G2 = line_G_vonF_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*1)
    Halbbogen10.arc(*drawKreisSeg(G1, HSL_start*HSL_size, angle_11, angle_10, True))
    
    A1, A2 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen10.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_9, True))
    
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen10.arc(*drawKreisSeg(A1, HSL_start+HSL_size*2, angle_9, angle_8, True))
    
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen10.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Halbbogen10.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Halbbogen10.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_6, angle_5, True))


    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(x, y, outstrokeLen)
    
    Halbbogen10 += instroke + outstroke
    drawPath(Halbbogen10)     
    return Halbbogen10

#drawHalbbogen10(temp_x, temp_y)  








    