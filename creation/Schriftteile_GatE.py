import importlib
import version_3.creation.Grundelemente

importlib.reload(version_3.creation.Grundelemente)

from version_3.creation.Grundelemente import *
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m



temp_x = 2 * modul_width
temp_y = 9 * modul_height



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 5
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.5)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)
  

    

# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
x_height = 6

# Hintergrund
baseline = backgroundGrid(page_width_cal, page_height_cal, x_height)





# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)






def drawHauptbestandteil(x, y):
    
    Hauptbestandteil = BezierPath()

    A2 = x, y          #Raute_a
    A1 = A2[0]+offsetDir[0], A2[1]-offsetDir[1] 
    Hauptbestandteil.polygon(A1, A2)
    
    drawPath(Hauptbestandteil)     
    return Hauptbestandteil
    
# drawHauptbestandteil(temp_x, temp_y)






def drawSchriftteil1(x, y, direction="CW"):
    
    Schriftteil1 = BezierPath()
    
    # E in No. I.
    # nach einer Schneckenlinie mit 12. Part von der Linie A angefangen und bei B mit 6. Part geendiget.
    # _. HSL, A>B, 12>6  

    # Im Text steht zwar „von Linie A angefangen“ aber in der Zeichnung  
    # ist von Linie H angefangen, und nur so funktioniert es auch.
    # 6. HSL, H>B, 12>6
    
    if direction == "CW":
        Schriftteil1 = drawSchneckenzug(x, y , UPPER_H, 2, HSL_size=6, HSL_start=18, clockwise=True, inward=True) 
        
    if direction == "CCW":
        #Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x/modul_width, y/modul_height)
        Schriftteil1 = drawSchneckenzug(x, y , LOWER_B, 2, HSL_size=6, HSL_start=6, clockwise=False, inward=False)
    
    drawPath(Schriftteil1) 
    return Schriftteil1
    
# drawSchriftteil1(temp_x, temp_y, direction="CW") 
# drawSchriftteil1(temp_x, temp_y, direction="CCW") 

 
 
 
    
    
def drawSchriftteil2(x, y):
    
    #Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x/modul_width+1, y/modul_height-1)

    Schriftteil2 = BezierPath()
    
    Schriftteil2 = drawSchneckenzug(x, y , UPPER_B, 2, HSL_size=6, HSL_start=6, clockwise=False, inward=False)

    drawPath(Schriftteil2)
    return Schriftteil2
    
# drawSchriftteil2(temp_x, temp_y)  
    
    




def drawSchriftteil3(x, y, version="standard"):
    
    Schriftteil3 = BezierPath()
    
    # E in No. 3.
    # nach einem Bogen von A bis B mit 15. Part.    
    # A>B, 15

    if version == "standard":
        HSL_size = 0
        HSL_start = 15
    
    if version == "0.75":
        HSL_size = 2
        HSL_start = 26
    
    Schriftteil3 = drawSchneckenzug(x, y , UPPER_A, 1, HSL_size, HSL_start, clockwise=True, inward=True)
    
    drawPath(Schriftteil3)
    return Schriftteil3
    
# drawSchriftteil3(temp_x, temp_y, "0.75")




    
def drawSchriftteil4(x, y, version="standard"):
    
    Schriftteil4 = BezierPath()
   
    if version == "standard": 
        HSL_size = 0
        HSL_start = 15
            
    if version == "1": 
        HSL_size = 0
        HSL_start = 26
    
    Schriftteil4 = drawSchneckenzug(x, y , LOWER_A, 1, HSL_size, HSL_start, clockwise=True, inward=True)

    drawPath(Schriftteil4)
    return Schriftteil4
    
# drawSchriftteil4(temp_x, temp_y, "1")    
   
    




def drawSchriftteil5(x, y, version="standard"):
    
    Schriftteil5 = BezierPath()
    
    # E in No. 5. 
    # mit einem Bogenmaase und 7. Part von der Theilinie C bis zu B construiret.
    # 1. HSL, C>B, 7
    # >>> Wenn man 13.5 anstatt 7 nimmt, passt es!

    if version == "standard":     
        Schriftteil5 = drawSchneckenzug(x, y , UPPER_C, 1, HSL_size=0, HSL_start=13.5, clockwise=False, inward=True)
        
    if version == "1":
        Schriftteil5 = drawSchneckenzug(x, y , UPPER_C, 1, HSL_size=0, HSL_start=26, clockwise=False, inward=True)      
        
    if version == "fakeGatC":   
        # angelegt für Schriftzug 2 in Kurrent GatE
        # Wert mit Vergleich über Photoshop Tab 55 abgestimmt
        Schriftteil5 = drawSchneckenzug(x, y , UPPER_E, 3, HSL_size=5, HSL_start=21.55, clockwise=False, inward=True)

    drawPath(Schriftteil5)
    return Schriftteil5
    
# drawSchriftteil5(temp_x, temp_y, "standard")    
# drawSchriftteil5(temp_x, temp_y, "1")  
# drawSchriftteil5(temp_x, temp_y, "fakeGatC")
    
    
    
    
    
    
    
    
def drawSchriftteil6(x, y, version="standard"):
    
    Schriftteil6 = BezierPath()   
    
    if version == "standard":
        Schriftteil6 = drawSchneckenzug(x, y , LOWER_C, 1, HSL_size=0, HSL_start=15, clockwise=False, inward=True)       
        
    if version == "1":
        Schriftteil6 = drawSchneckenzug(x, y , LOWER_C, 1, HSL_size=0, HSL_start=27, clockwise=False, inward=True)       
    
    if version == "fakeGatC":
        # angelegt für Schriftzug 2 in Kurrent GatE
        # Wert mit Vergleich über Photoshop Tab 55 abgestimmt
        Schriftteil6 = drawSchneckenzug(x, y , LOWER_E, 3, HSL_size=5, HSL_start=21.55, clockwise=False, inward=True)

    drawPath(Schriftteil6)
    return Schriftteil6
    

# drawSchriftteil6(temp_x, temp_y, "standard")    
# drawSchriftteil6(temp_x, temp_y, "1")  
# drawSchriftteil6(temp_x, temp_y, "fakeGatC")    




def drawSchriftteil7(x, y):
    
    Schriftteil7 = BezierPath() 
    
    # E in No.7. 
    # ist gleichfalls eine aus einem Bogenmaase bestehende Linie aus e nach der Richtlinie C, 
    # ingleichen die Linie aus d mit dem nehmliche Maase nach der Hauptlinie gezogen, 
    # welche sich beide in c durchschneiden. 
    # Aus diesem Durchschnitte c ist der Bogen ed durch den Radium ce entstanden. 

    Schriftteil7 = drawSchneckenzug(x, y , LOWER_A, 2, HSL_size=0, HSL_start=8, clockwise=True, inward=True)
    
    drawPath(Schriftteil7)      
    return Schriftteil7

# drawSchriftteil7(temp_x, temp_y)    
   
    
    
    
    
def drawSchriftteil8(x, y, instrokeLen=0, outstrokeLen=0):
    
    Schriftteil8 = BezierPath() 

    bogen = drawSchneckenzug(x, y , UPPER_A, 2, HSL_size=0, HSL_start=8, clockwise=True, inward=True)

    
    instroke = drawInstroke(*bogen.points[0], instrokeLen)
    outstroke = drawOutstroke(*bogen.points[-1], outstrokeLen, "down")
    
    Schriftteil8 += bogen + instroke + outstroke
    drawPath(Schriftteil8)
    return Schriftteil8

# drawSchriftteil8(temp_x, temp_y) 
# drawSchriftteil8(temp_x, temp_y, instrokeLen=1, outstrokeLen=1) 




def drawSchriftteil9(x, y, direction="CCW"):
    
    Schriftteil9 = BezierPath() 
    
    # zu E No. 9. 
    # die Spitze eines Bogens der zweiten Hauptschneckenlinie, 
    # welche mit 10. Part angefangen und auf der Hauptlinie mit 4. Part geendiget ist.
    # 2. HSL, E>A, 10>4
    
    if direction == "CW":
        Schriftteil9 = drawSchneckenzug(x, y, UPPER_E, 4, HSL_size=2, HSL_start=12, clockwise=True, inward=True) 
    
    if direction == "CCW":
        Schriftteil9 = drawSchneckenzug(x, y, UPPER_A, 4, HSL_size=2, HSL_start=4, clockwise=False, inward=False) 
        
    drawPath(Schriftteil9)
    return Schriftteil9
    
    
# drawSchriftteil9(temp_x, temp_x, direction="CW")
# drawSchriftteil9(temp_x, temp_x, direction="CCW")

    
    
    
    
    
def drawSchriftteil10(x, y, direction="CCW"):
    
    Schriftteil10 = BezierPath() 

    if direction == "CW":
        Schriftteil10 = drawSchneckenzug(x, y, LOWER_E, 4, HSL_size=2, HSL_start=12, clockwise=True, inward=True) 
    
    if direction == "CCW":
        Schriftteil10 = drawSchneckenzug(x, y, LOWER_A, 4, HSL_size=2, HSL_start=4, clockwise=False, inward=False) 
     
    drawPath(Schriftteil10)
    return Schriftteil10
    
    
# drawSchriftteil10(temp_x, temp_x, direction="CW")
# drawSchriftteil10(temp_x, temp_x, direction="CCW")



    

def drawSchriftteil11(x, y):
    
    Schriftteil11 = BezierPath() 
    
    # Bei E No. 11. und 12. 
    # sind ebenfalls solche Anfangsspitzen, aber bis zur Hauptlinie, 
    # und zwar zu No. 11 vom 5. bis 2. Part, 
    # und zu No. 12. von 7. bis 3. Part construiret, 
    # und bei beiden eine dergleichen Spitze umgewendet angesetzt.
    # 11:  E>A; 5>2
    
    ### Ich habe eine andere Lösung gewählt >>> in der Mitte anfangen, 
    ### für bessere Passung und einfachere Positionierung (weil als Deckung verwendet)

    nach_links = drawSchneckenzug(x, y, UPPER_A, 4, HSL_size=1, HSL_start=2, clockwise=False, inward=False)
    nach_rechts = drawSchneckenzug(x, y, LOWER_A, 4, HSL_size=1, HSL_start=2, clockwise=False, inward=False)  

    Schriftteil11 = nach_links + nach_rechts
    
    drawPath(Schriftteil11)
    return Schriftteil11
    
    
# drawSchriftteil11(temp_x, temp_x)  

    
    
    
    
    
    
    

def drawSchriftteil12(x, y, outstrokeLen=0):
    
    Schriftteil12 = BezierPath() 
    
    # Bei E No. 11. und 12. 
    # sind ebenfalls solche Anfangsspitzen, aber bis zur Hauptlinie, 
    # und zwar zu No. 11 vom 5. bis 2. Part, 
    # und zu No. 12. von 7. bis 3. Part construiret, 
    # und bei beiden eine dergleichen Spitze umgewendet angesetzt.
    # 12:  E>A; 7>4
    
    ### Ich habe eine andere Lösung gewählt >>> in der Mitte anfangen, 
    ### für bessere Passung und einfachere Positionierung (weil als Deckung verwendet)
    
    links = drawSchneckenzug(x, y, UPPER_A, 4, HSL_size=1, HSL_start=4, clockwise=False, inward=False)
    rechts = drawSchneckenzug(x, y, LOWER_A, 4, HSL_size=1, HSL_start=4, clockwise=False, inward=False)
    outstroke = drawOutstroke(*rechts.points[-1], outstrokeLen)
    
    Schriftteil12 += links + rechts + outstroke
    drawPath(Schriftteil12)
        
    return Schriftteil12
    
# drawSchriftteil12(temp_x, temp_y)








