import importlib
import version_3.creation.Grundelemente

importlib.reload(version_3.creation.Grundelemente)

from version_3.creation.Grundelemente import *
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m



temp_x = 2 * modul_width
temp_y = 9 * modul_height





# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 5
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.5)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)
  

    

# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
x_height = 6

# Hintergrund
baseline = backgroundGrid(page_width_cal, page_height_cal, x_height)





# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height, "E")

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)








def drawSchriftteil1(x, y, direction="CW"):
    
    Schriftteil1 = BezierPath()
    
    # E in No. I.
    # nach einer Schneckenlinie mit 12. Part von der Linie A angefangen und bei B mit 6. Part geendiget.
    # _. HSL, A>B, 12>6  

    # Im Text steht zwar „von Linie A angefangen“ aber in der Zeichnung  
    # ist von Linie H angefangen, und nur so funktioniert es auch.
    # 6. HSL, H>B, 12>6
    
    if direction == "CW":
        Schriftteil1 = drawSchneckenzug(x, y , UPPER_H, 2, HSL_size=6, HSL_start=17.9, clockwise=True, inward=True) 
        
    if direction == "CCW":
        #Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x/modul_width, y/modul_height)
        Schriftteil1 = drawSchneckenzug(x, y , LOWER_B, 2, HSL_size=6, HSL_start=6, clockwise=False, inward=False)
    
    drawPath(Schriftteil1) 
    return Schriftteil1
    
# drawSchriftteil1(temp_x, temp_y, direction="CW") 
# drawSchriftteil1(temp_x, temp_y, direction="CCW") 
 
 
 
 
    





    
    

def drawSchriftteil3(x, y, version="standard"):
    
    Schriftteil3 = BezierPath()
    
    # E in No. 3.
    # nach einem Bogen von A bis B mit 15. Part.    
    # A>B, 15

    if version == "standard":
        HSL_size = 0
        HSL_start = 13.5
    
    if version == "0.75":
        HSL_size = 2
        HSL_start = 26
    
    Schriftteil3 = drawSchneckenzug(x, y , UPPER_A, 1, HSL_size, HSL_start, clockwise=True, inward=True)
    
    drawPath(Schriftteil3)
    return Schriftteil3
    
# drawSchriftteil3(temp_x, temp_y, "0.75")





    
    
def drawSchriftteil4(x, y, version="standard"):
    
    Schriftteil4 = BezierPath()
   
    if version == "standard": 
        HSL_size = 0
        HSL_start = 13.5
            
    if version == "1": 
        HSL_size = 0
        HSL_start = 26
    
    Schriftteil4 = drawSchneckenzug(x, y , LOWER_A, 1, HSL_size, HSL_start, clockwise=True, inward=True)

    drawPath(Schriftteil4)
    return Schriftteil4
    
# drawSchriftteil4(temp_x, temp_y, "1")   
   
    


    
    
    
    
    
    
    
def drawSchriftteil6(x, y, version="standard"):
    
    Schriftteil6 = BezierPath()   
    
    if version == "standard":
        Schriftteil6 = drawSchneckenzug(x, y , LOWER_C, 1, HSL_size=0, HSL_start=13.5, clockwise=False, inward=True)       
        
    if version == "1":
        Schriftteil6 = drawSchneckenzug(x, y , LOWER_C, 1, HSL_size=0, HSL_start=27, clockwise=False, inward=True)       
    
    if version == "fakeGatC":
        # angelegt für Schriftzug 2 in Kurrent GatE
        # Wert mit Vergleich über Photoshop Tab 55 abgestimmt
        Schriftteil6 = drawSchneckenzug(x, y , LOWER_E, 3, HSL_size=5, HSL_start=21.55, clockwise=False, inward=True)

    drawPath(Schriftteil6)
    return Schriftteil6
    

# drawSchriftteil6(temp_x, temp_y, "standard")    
# drawSchriftteil6(temp_x, temp_y, "1")  
# drawSchriftteil6(temp_x, temp_y, "fakeGatC")  
    







