from .arc_path import ArcPath as BezierPath
from drawBot import drawPath as _original_drawPath


__all__ = ["BezierPath", "drawPath"]


def drawPath(path=None):
    if path is not None and isinstance(path, BezierPath):
        path = path.copy()
    _original_drawPath(path)
