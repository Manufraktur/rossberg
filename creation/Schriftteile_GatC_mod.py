import importlib
import version_3.creation.Grundelemente

importlib.reload(version_3.creation.Grundelemente)

from version_3.creation.Grundelemente import *
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m





temp_x = 2 * modul_width        # war 2
temp_y = 9 * modul_height       # war 9



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 5
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.5)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)
  

    

# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)






# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)







def drawSchriftteil1(x, y, direction="CCW"):
    
    Schriftteil1 = BezierPath()
        
    if direction == "CW":
        drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+1.5, temp_y/modul_height-0.5)
        modified = drawSchneckenzug(x, y, UPPER_F, 1, HSL_size=3.5, HSL_start=9.7, clockwise=True, inward=False)
        regular = drawSchneckenzug(*modified.points[-1], UPPER_G, 2, HSL_size=4, HSL_start=12, clockwise=True, inward=True)    
   
    if direction == "CCW":
        drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height)
        regular = drawSchneckenzug(x, y, UPPER_A, 2, HSL_size=4, HSL_start=4, clockwise=False, inward=False)    
        modified = drawSchneckenzug(*regular.points[-1], UPPER_G, 1, HSL_size=3.5, HSL_start=9.7, clockwise=False, inward=False)
        
    Schriftteil1 += regular + modified
    drawPath(Schriftteil1)    
    return Schriftteil1
    
#drawSchriftteil1(temp_x, temp_y, direction="CW")
#drawSchriftteil1(temp_x, temp_y, direction="CCW")






 
 
def drawSchriftteil2(x, y):
    
    Schriftteil2 = BezierPath()

    Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height-1)

    regular = drawSchneckenzug(x, y, LOWER_A, 2, HSL_size=4.5, HSL_start=4, clockwise=False, inward=False)
    modified = drawSchneckenzug(*regular.points[-1], LOWER_G, 1, HSL_size=5, HSL_start=9.35, clockwise=False, inward=False)

    Schriftteil2 += regular + modified
    drawPath(Schriftteil2)  
    return Schriftteil2
        
#drawSchriftteil2(temp_x, temp_y)
    
    
    




def drawSchriftteil3(x, y, version="standard"):
    
    Schriftteil3 = BezierPath()
    
    # C in No. 3.
    # nach der zweiten Hauptschneckenlinie von G bis B rechts 
    # mit 8. Part angefangen und mit 4. Part geschlossen. (Th1, S. 36)
    # 2. HSL, G>B, 8>4

    #drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height-1)
        
    if version == "standard":
        Schriftteil3 = drawSchneckenzug(x, y, UPPER_G, 3, HSL_size=2, HSL_start=9.9, clockwise=True, inward=True) 
        
    if version == "0.75":
        Schriftteil3 = drawSchneckenzug(x, y, UPPER_G, 3, HSL_size=1, HSL_start=10, clockwise=True, inward=True) 

    if version == "1":
        Schriftteil3 = drawSchneckenzug(x, y, UPPER_G, 3, HSL_size=4, HSL_start=19.85, clockwise=True, inward=True) 
    
    drawPath(Schriftteil3)
    return Schriftteil3
    
    
#drawSchriftteil3(temp_x, temp_y, "standard")
# drawSchriftteil3(temp_x, temp_y, "0.75")
# drawSchriftteil3(temp_x, temp_y, "1")



 







   
def drawSchriftteil5(x, y, version="standard"):
    
    Schriftteil5 = BezierPath()
    
    # C in No. 5. 
    # nach der zweiten Hauptschneckenlinie mit einem Bogenmaase 
    # von der Theilinie E bis mit 4. Part zur Theilinie B.
    # 2. HSL, E>B, 8>4
    
    Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height-1)

    if version == "standard":   ### 0.5 modul
        bend = drawSchneckenzug(x, y , UPPER_E, 3, HSL_size=2, HSL_start=9.875, clockwise=False, inward=True)

    if version == "1":
        bend = drawSchneckenzug(x, y , UPPER_E, 3, HSL_size=5, HSL_start=21.55, clockwise=False, inward=True)
        # Wert mit Vergleich über Photoshop Tab 55 abgestimmt

    Schriftteil5 = bend
    drawPath(Schriftteil5)
    return Schriftteil5
    
    
#drawSchriftteil5(temp_x, temp_y, "standard")
# drawSchriftteil5(temp_x, temp_y, "1")




    
    
def drawSchriftteil6(x, y, version="standard"):
    
    Schriftteil6 = BezierPath()   
    
    Raute_a, Raute_b, Raute_c_St2, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, temp_x/modul_width+0.5, temp_y/modul_height)
    
    
    if version == "standard":   ### 0.5 modul
        HSL_size = 2
        HSL_start = 9.735   ### dieser Wert ist so, dass es mit Schriftzug 5 passt.
                            #### Komisch, dass es hier nicht passt. Warum nur?
        
    if version == "1":
        HSL_size = 5             # Wert mit Vergleich über Photoshop Tab 55 abgestimmt
        HSL_start = 21.55        # Wert mit Vergleich über Photoshop Tab 55 abgestimmt
    
    if version == "1.5":
        HSL_size = 10    # angelegt für KurrentKanzlei 
        HSL_start = 40      # Schriftzug 8c (TAB 49)

    Schriftteil6 = drawSchneckenzug(x, y , LOWER_E, 3, HSL_size, HSL_start, clockwise=False, inward=True)

    drawPath(Schriftteil6)   
    return Schriftteil6
    

# drawSchriftteil6(temp_x, temp_y, "standard")
# drawSchriftteil6(temp_x, temp_y, "1")
# drawSchriftteil6(temp_x, temp_y, "1.5")











