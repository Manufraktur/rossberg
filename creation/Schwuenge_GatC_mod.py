import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Halbboegen_GatC_mod

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Halbboegen_GatC_mod)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Halbboegen_GatC_mod import *
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m




temp_x = 3 #* modul_width
temp_y = 9 #* modul_height


# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 7
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)



# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)





# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)
   
strokeWidth(.1)





def drawSchwung1(x,y):
    
    Schwung1 = BezierPath()
    Schwung1 = drawHalbbogen1(x, y, instroke=False) + drawHalbbogen10(x, y, outstroke=False)
    
    return Schwung1
    



   
def drawSchwung2(x,y):
    
    Schwung2 = BezierPath()    
    Schwung2 = drawHalbbogen2(x, y, instroke=False) + drawHalbbogen9(x, y, outstroke=False)

    return Schwung2
    
    
    
    
    
def drawSchwung3(x,y):
    
    Schwung3 = BezierPath()
    Schwung3 = drawHalbbogen3(x, y, instroke=False) + drawHalbbogen8(x, y, outstroke=False)
    return Schwung3

   
   
   
def drawSchwung4(x,y):
    
    Schwung4 = BezierPath()
    Schwung4 = drawHalbbogen4(x, y, instroke=False) + drawHalbbogen7(x, y, outstroke=False)

    return Schwung4
    
    
    
    
def drawSchwung5(x,y):
    
    Schwung5 = BezierPath()
    Schwung5 = drawHalbbogen5(x, y, instroke=False) + drawHalbbogen6(x, y, outstroke=False)

    return Schwung5
    
    
    
    
def drawSchwung6(x, y, Endspitze=12):
    
    Schwung6 = BezierPath()

    # ___________ 1. Schwung unten anfangen _______________

    # Es ist bei diesem Schwunge die Spitze des untern Bogens mit einem Radio von
    # 3. Bogenmaas angefangen, und nach einer Schneckenlinie der vierten Hauptschnecke abfallend, 
    # bis mit 2. Bogenmaas zur Richtlinie B, gezogen,
    # und bei derselben 5. Modul gerade Bestandtheile eingeschaltet. 

    # unten: 4. HSL; E>B; 3BT>2BT = 24>16
    # 5. Modul gerade Bestandtheile gerade

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y) 
    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y-5)
    
    HSL_size = 4    
    HSL_start = 28 

    E1, E2 = line_E_vonF_u_kl_s(Raute_c, *angles, part, HSL_size, HSL_start)

    D1, D2 = line_D_vonE_u_kl(E1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schwung6.arc(*drawKreisSeg(D1, HSL_start-HSL_size, angle_13, angle_14))

    C1, C2 = line_C_vonD_u_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schwung6.arc(*drawKreisSeg(C1, HSL_start-HSL_size*2, angle_14, angle_15))

    B1, B2 = line_B_vonC_u_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schwung6.arc(*drawKreisSeg(B1, HSL_start-HSL_size*3, angle_15, angle_16))

  
    # ___________ 2. Schwung in der Mitte _______________    

    # Der obere Bogen ist mit eben dem Maase, nämlich mit 2. Bogenmaas, nach dem eingeschalteten 
    # gleichen Einsatze, wieder von der Richtlinie B angefangen, aber nach der dritten 
    # Hauptschneckenlinie bis mit 10. Part zu Richtlinie F construiret. 

    # oben: 3. HSL; B>F; 2BT>10 = 16 > 10

    HSL_size = 3  
    HSL_start = 16 
    # Linie B 
    # plus 5 gerade Bestandteile! 5BT = 40 part
    # meint er hier 1,5 BT ? Weil sonst passt es nicht ...
    B3 = B1[0]-part*3, B1[1] + 12*part
    B4 = B2[0], B2[1] + 12*part
    polygon(B3, B4)
    

    A1, A2 = line_A_vonB_o_kl(B3, *angles, part, HSL_size, HSL_start)
    Schwung6.arc(*drawKreisSeg(A1, 16, angle_0, angle_1))

    H1, H2 = line_H_vonA_o_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size)
    Schwung6.arc(*drawKreisSeg(H1, HSL_start-HSL_size, angle_1, angle_2))

    G1, G2 = line_G_vonH_o_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schwung6.arc(*drawKreisSeg(G1, HSL_start-HSL_size*2, angle_2, angle_3))

    F1, F2 = line_F_vonG_o_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schwung6.arc(*drawKreisSeg(F1, HSL_start-HSL_size*3, angle_3, angle_4))

    
    # Schwung oben - Wendung bei F, Krümmung bis E
    
    if Endspitze == 11:
        
        # Auf dieser Richtlinie F nimmt der zweite Bogen oben bei dc eine Wendung und krümmt sich rückwärts 
        # in Form eines Halbbogens, mit einem Radio von 11. Part nach der ersten Hauptschneckenlinie abfallend 
        # bis zur Endspitze. 
        # 1. HSL; F>E; 11>4

        HSL_size = 1   
        HSL_start = 12
        
    
    if Endspitze == 12:

        # Oder, wie er eigentlich nach der vollen Höhe aus 9. Grundbestandtheilen zu bilden wäre, 
        # mit einem Radio von 12. Part.
        # 1. HSL; F>E; 12>5             
        
        HSL_size = 1   
        HSL_start = 13
        

    F3, F4 = line_F_vonE_u_kl_s(F2, *angles, part, HSL_size, HSL_start)

    G1, G2 = line_G_vonF_u_kl(F3, *angles, part, HSL_size, HSL_start-HSL_size)
    Schwung6.arc(*drawKreisSeg(G1, HSL_start-HSL_size, angle_12, angle_11, True))

    H1, H2 = line_H_vonG_u_kl(G1, *angles, part, HSL_size, HSL_start-HSL_size*2)
    Schwung6.arc(*drawKreisSeg(H1, HSL_start-HSL_size*2, angle_11, angle_10, True))

    A1, A2 = line_A_vonH_u_kl(H1, *angles, part, HSL_size, HSL_start-HSL_size*3)
    Schwung6.arc(*drawKreisSeg(A1, HSL_start-HSL_size*3, angle_10, angle_9, True))
    
    B1, B2 = line_B_vonA_o_kl(A1, *angles, part, HSL_size, HSL_start-HSL_size*4)
    Schwung6.arc(*drawKreisSeg(B1, HSL_start-HSL_size*4, angle_9, angle_8, True))
    
    C1, C2 = line_C_vonB_o_kl(B1, *angles, part, HSL_size, HSL_start-HSL_size*5)
    Schwung6.arc(*drawKreisSeg(C1, HSL_start-HSL_size*5, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_kl(C1, *angles, part, HSL_size, HSL_start-HSL_size*6)
    Schwung6.arc(*drawKreisSeg(D1, HSL_start-HSL_size*6, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_kl(D1, *angles, part, HSL_size, HSL_start-HSL_size*7)
    Schwung6.arc(*drawKreisSeg(E1, HSL_start-HSL_size*7, angle_6, angle_5, True))
    
        

    drawPath(Schwung6)
    
    return Schwung6
    

#drawSchwung6(temp_x, temp_y, Endspitze=11)     # Endspitze kann optional auch 11 sein, 12 ist default




