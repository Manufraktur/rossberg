import math as m
from drawBot import *

from version_3.creation.arc_path import ArcPath as BezierPath   ### from JvR

from version_3.creation.create_stuff import drawKreisSeg, part, angles, angle_0, angle_1, angle_2, angle_3, angle_4, angle_5, angle_6, angle_7, angle_8, angle_9, angle_10, angle_11, angle_12, angle_13, angle_14, angle_15
from .arc_path import ArcPath as BezierPath
from version_3.creation.special_drawbot import BezierPath, drawPath

'''

BENENNUNG, Schema:

H_vonA_u_kl
H_vonA_u_gr

H_vonA_o_kl
H_vonA_o_gr

H_vonG_u_kl
H_vonG_u_gr

H_vonG_o_kl
H_vonG_o_gr



ERKLÄRUNG:

H : Name der Linie, zB. H = H1 bis H2

vonA : von wo komme ich her, also Name der vorhergehenden Linie

o/u: oben, unten; d.h. oben ist oberhalb von A–A, unten unterhalb von A–A

kl/gr : wird kleiner, wird größer

'''



# zeichnet alle Hilfslinien (Segmente der Spirale)

def drawPolygon(x1, x2):
    save()
    stroke(.7)
    strokeWidth(.0001)
    polygon(x1, x2)
    restore()


def drawPolygon_s(x1, x2):
    save()
    stroke(1, 0, 0, .5)
    strokeWidth(.0001)
    polygon(x1, x2)
    restore()
    


# ___________________________ am 5. Mai rausgelöscht __________________________________________

# HSL = part * HSL_size


    
# ___________________________ Linie A __________________________________________
    
    
def line_A_vonH_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
        
    A2 = sPoint
    A1 = A2[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),   A2[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)

    drawPolygon_s(A1, A2)
    return A1, A2    


def line_A_vonH_u_kl(H1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
        
    A1 = H1[0] - m.cos(m.radians(epsilon)) * (part*HSL_size),        H1[1] - m.sin(m.radians(epsilon)) * (part*HSL_size)
    A2 = A1[0] - m.cos(m.radians(angle_seg_4)) * (part*length),      A1[1] - m.sin(m.radians(angle_seg_4)) * (part*length)

    drawPolygon(A1, A2)
    return A1, A2  


    



def line_A_vonH_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start): 
    
    A2 = sPoint
    A1 = A2[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      A2[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)

    drawPolygon_s(A1, A2)
    return A1, A2


def line_A_vonH_u_gr(H1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length): 
    
    A1 = H1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    H1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    A2 = A1[0] - m.cos(m.radians(angle_seg_4)) * (part*length),      A1[1] - m.sin(m.radians(angle_seg_4)) * (part*length)

    drawPolygon(A1, A2)
    return A1, A2


    
    
    
def line_A_vonH_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
        
    A2 = sPoint
    A1 = A2[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      A2[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)

    drawPolygon_s(A1, A2)
    return A1, A2   
    
    
def line_A_vonH_o_kl(H1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
        
    A1 = H1[0] + m.cos(m.radians(epsilon)) * (part*HSL_size),        H1[1] + m.sin(m.radians(epsilon)) * (part*HSL_size)
    A2 = A1[0] + m.cos(m.radians(angle_seg_4)) * (part*length),      A1[1] + m.sin(m.radians(angle_seg_4)) * (part*length)

    drawPolygon(A1, A2)
    return A1, A2  



     

    
def line_A_vonH_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start): 
        
    A2 = sPoint
    A1 = A2[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    A2[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)

    drawPolygon_s(A1, A2)
    return A1, A2  
       
    
def line_A_vonH_o_gr(H1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length): 
        
    A1 = H1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    H1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    A2 = A1[0] + m.cos(m.radians(angle_seg_4)) * (part*length),      A1[1] + m.sin(m.radians(angle_seg_4)) * (part*length)

    drawPolygon(A1, A2)
    return A1, A2  
    
    
    

    
def line_A_vonB_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
        
    A2 = sPoint
    A1 = A2[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      A2[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)

    drawPolygon_s(A1, A2)
    return A1, A2 


def line_A_vonB_u_kl(B1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
        
    A1 = B1[0] - (part*HSL_size),                                    B1[1]
    A2 = A1[0] - m.cos(m.radians(angle_seg_4)) * (part*length),      A1[1] - m.sin(m.radians(angle_seg_4)) * (part*length)

    drawPolygon(A1, A2)
    return A1, A2 






def line_A_vonB_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start): 
        
    A2 = sPoint
    A1 = A2[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),      A2[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)

    drawPolygon_s(A1, A2)
    return A1, A2 


def line_A_vonB_u_gr(B1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
        
    A1 = B1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    B1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    A2 = A1[0] - m.cos(m.radians(angle_seg_4)) * (part*length),      A1[1] - m.sin(m.radians(angle_seg_4)) * (part*length)

    drawPolygon(A1, A2)
    return A1, A2 




    
    
    
def line_A_vonB_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
        
    A2 = sPoint
    A1 = A2[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    A2[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)

    drawPolygon_s(A1, A2)
    return A1, A2

    
def line_A_vonB_o_kl(B1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
        
    A1 = B1[0] + (part*HSL_size),                                    B1[1] 
    A2 = A1[0] + m.cos(m.radians(angle_seg_4)) * (part*length),      A1[1] + m.sin(m.radians(angle_seg_4)) * (part*length)

    drawPolygon(A1, A2)
    return A1, A2






def line_A_vonB_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start): 
        
    A2 = sPoint
    A1 = A2[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    A2[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)

    drawPolygon_s(A1, A2)
    return A1, A2


def line_A_vonB_o_gr(B1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):       ### Bevor Änderung der Vorzeichen erst auf Blatt Rückseite checken!!!
        
    A1 = B1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    B1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    A2 = A1[0] + m.cos(m.radians(angle_seg_4)) * (part*length),      A1[1] + m.sin(m.radians(angle_seg_4)) * (part*length)

    drawPolygon(A1, A2)
    return A1, A2


    
    








    
    
    
    
# ________________ Linie B __________________________________________


def line_B_vonA_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
      
    B2 = sPoint
    B1 = B2[0] - (part*HSL_start),       B2[1]
    
    drawPolygon_s(B1, B2)
    return B1, B2   

    
def line_B_vonA_u_kl(A1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
      
    B1 = A1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),       A1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    B2 = B1[0] + (part*length),                                         B1[1]
    
    drawPolygon(B1, B2)
    return B1, B2 





    
def line_B_vonA_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
      
    B2 = sPoint
    B1 = B2[0] - (part*HSL_start),       B2[1]
    
    drawPolygon_s(B1, B2)
    return B1, B2 

    
def line_B_vonA_u_gr(A1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
      
    B1 = A1[0] - (part*HSL_size),       A1[1]
    B2 = B1[0] + (part*length),         B1[1]
    
    drawPolygon(B1, B2)
    return B1, B2 

    




        
def line_B_vonA_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    B2 = sPoint
    B1 = B2[0] + (part*HSL_start),          B2[1]
    
    drawPolygon_s(B1, B2)
    return B1, B2


def line_B_vonA_o_kl(A1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
      
    B1 = A1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_size),     A1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    B2 = B1[0] - (part*length),                                       B1[1]
    
    drawPolygon(B1, B2)
    return B1, B2




    

def line_B_vonA_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
      
    B2 = sPoint
    B1 = B2[0] + (part*HSL_start),              B2[1]
    
    drawPolygon_s(B1, B2)
    return B1, B2

    
def line_B_vonA_o_gr(A1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
      
    B1 = A1[0] + (part*HSL_size),               A1[1]
    B2 = B1[0] - (part*length),                 B1[1]
    
    drawPolygon(B1, B2)
    return B1, B2 

    
    
    

    
def line_B_vonC_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
      
    B2 = sPoint
    B1 = B2[0] - (part*HSL_start),              B2[1]
    
    drawPolygon_s(B1, B2)
    return B1, B2 

    
def line_B_vonC_u_kl(C1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
      
    B1 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),       C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    B2 = B1[0] + (part*length),                                         B1[1]
    
    drawPolygon(B1, B2)
    return B1, B2 






def line_B_vonC_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
      
    B2 = sPoint
    B1 = B2[0] - (part*HSL_start),     B2[1]
    
    drawPolygon_s(B1, B2)
    return B1, B2 
     

def line_B_vonC_u_gr(C1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
      
    B1 = C1[0] - (part*HSL_size),      C1[1]
    B2 = B1[0] + (part*length),        B1[1]
    
    drawPolygon(B1, B2)
    return B1, B2 





def line_B_vonC_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    B2 = sPoint
    B1 = B2[0] + (part*HSL_start),     B2[1]
    
    drawPolygon_s(B1, B2)
    return B1, B2  
    
    
def line_B_vonC_o_kl(C1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    B1 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_size),       C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    B2 = B1[0] - (part*length),                                         B1[1]
    
    drawPolygon(B1, B2)
    return B1, B2  
    




    
def line_B_vonC_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
      
    B2 = sPoint
    B1 = B2[0] + (part*HSL_start),     B2[1]
    
    drawPolygon_s(B1, B2)
    return B1, B2  
    
    
def line_B_vonC_o_gr(C1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
      
    B1 = C1[0] + (part*HSL_size),        C1[1]
    B2 = B1[0] - (part*length),          B1[1]
    
    drawPolygon(B1, B2)
    return B1, B2     
    












# ___________________________ Linie C __________________________________________



def line_C_vonB_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    C2 = sPoint
    C1 = C2[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),   C2[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    
    drawPolygon_s(C1, C2)
    return C1, C2 


def line_C_vonB_u_kl(B1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    C1 = B1[0] + (part*HSL_size),                                    B1[1]
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*length),      C1[1] - m.sin(m.radians(angle_seg_4)) * (part*length)
    
    drawPolygon(C1, C2)
    return C1, C2 





def line_C_vonB_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    C2 = sPoint
    C1 = C2[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),   C2[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    
    drawPolygon_s(C1, C2)
    return C1, C2 


def line_C_vonB_u_gr(B1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    # hier fehlt noch was! ECHT?
    C1 = B1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    B1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*length),      C1[1] - m.sin(m.radians(angle_seg_4)) * (part*length)
    
    drawPolygon(C1, C2)
    return C1, C2 





 
def line_C_vonB_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    C2 = sPoint
    C1 = C2[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),   C2[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    
    drawPolygon_s(C1, C2)
    return C1, C2


def line_C_vonB_o_kl(B1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    C1 = B1[0] - (part*HSL_size),                                    B1[1]
    C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*length),      C1[1] + m.sin(m.radians(angle_seg_4)) * (part*length)
    
    drawPolygon(C1, C2)
    return C1, C2
    




    
def line_C_vonB_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    C2 = sPoint
    C1 = C2[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),   C2[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    
    drawPolygon_s(C1, C2)
    return C1, C2 

    
def line_C_vonB_o_gr(B1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    # hier fehlt noch was! Was???
    C1 = B1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    B1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*length),      C1[1] + m.sin(m.radians(angle_seg_4)) * (part*length)
    
    drawPolygon(C1, C2)
    return C1, C2 
    
    
  

    
      
def line_C_vonD_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):    

    C2 = sPoint
    C1 = C2[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),   C2[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    
    drawPolygon_s(C1, C2)
    return C1, C2  

    
def line_C_vonD_u_kl(D1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    # hier fehlt noch was!  ???
    C1 = D1[0] + m.cos(m.radians(epsilon)) * (part*HSL_size),        D1[1] - m.sin(m.radians(epsilon)) * (part*HSL_size)
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*length),      C1[1] - m.sin(m.radians(angle_seg_4)) * (part*length)
    
    drawPolygon(C1, C2)
    return C1, C2  







def line_C_vonD_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):

    C2 = sPoint
    C1 = C2[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_start),   C2[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    
    drawPolygon_s(C1, C2)
    return C1, C2


def line_C_vonD_u_gr(D1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    # hier fehlt noch was!
    C1 = D1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    D1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    C2 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*length),      C1[1] - m.sin(m.radians(angle_seg_4)) * (part*length)
    
    drawPolygon(C1, C2)
    return C1, C2




    
def line_C_vonD_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    C2 = sPoint
    C1 = C2[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),    C2[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    
    drawPolygon_s(C1, C2)
    return C1, C2    


def line_C_vonD_o_kl(D1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):

    # hier fehlt noch was! Ach ja? Was denn?
    C1 = D1[0] - m.cos(m.radians(epsilon)) * (part*HSL_size),        D1[1] + m.sin(m.radians(epsilon)) * (part*HSL_size)
    C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*length),      C1[1] + m.sin(m.radians(angle_seg_4)) * (part*length)
    
    drawPolygon(C1, C2)
    return C1, C2    
    
    



    
def line_C_vonD_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):

    C2 = sPoint
    C1 = C2[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_start),   C2[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_start)
    
    drawPolygon_s(C1, C2)
    return C1, C2  

    
def line_C_vonD_o_gr(D1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    # hier fehlt noch was!
    C1 = D1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    D1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    C2 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*length),      C1[1] + m.sin(m.radians(angle_seg_4)) * (part*length)
    
    drawPolygon(C1, C2)
    return C1, C2  
    
    
    
    
    









        
    

# ___________________________ Linie D __________________________________________

    
def line_D_vonC_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    D2 = sPoint
    D1 = D2[0] - m.cos(m.radians(epsilon)) * (part*HSL_start),    D2[1] + m.sin(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(D1, D2)
    return D1, D2 


def line_D_vonC_u_kl(C1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    D1 = C1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),   C1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    D2 = D1[0] + m.cos(m.radians(epsilon)) * (part*length),         D1[1] - m.sin(m.radians(epsilon)) * (part*length)
    
    drawPolygon(D1, D2)
    return D1, D2 






def line_D_vonC_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    D2 = sPoint
    D1 = D2[0] - m.sin(m.radians(epsilon)) * (part*HSL_start),    D2[1] + m.cos(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(D1, D2)
    return D1, D2 


def line_D_vonC_u_gr(C1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):

    HSL = part * HSL_size
    
    D1 = C1[0] - m.sin(m.radians(epsilon)) * (part*HSL_size),    C1[1] + m.cos(m.radians(epsilon)) * (part*HSL_size)
    D2 = D1[0] + m.sin(m.radians(epsilon)) * (part*length),      D1[1] - m.cos(m.radians(epsilon)) * (part*length)
    
    drawPolygon(D1, D2)
    return D1, D2 




def line_D_vonC_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    D2 = sPoint
    D1 = D2[0] + m.sin(m.radians(epsilon)) * (part*HSL_start),   D2[1] - m.cos(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(D1, D2)
    return D1, D2 


def line_D_vonC_o_kl(C1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    D1 = C1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_size),  C1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    D2 = D1[0] - m.sin(m.radians(epsilon)) * (part*length),        D1[1] + m.cos(m.radians(epsilon)) * (part*length)
    
    drawPolygon(D1, D2)
    return D1, D2 
    


    
def line_D_vonC_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    D2 = sPoint
    D1 = D2[0] + m.sin(m.radians(epsilon)) * (part*HSL_start),    D2[1] - m.cos(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(D1, D2)
    return D1, D2 

    
def line_D_vonC_o_gr(C1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    ### geändert am 7.Jan 2021 während ich an der Dokumentation gearbeitet habe
    ### anscheinend habe ich hier falsche Winkel benutzt. Warum nur? Und wie kann das passieren und nicht auffallen???
    D1 = C1[0] + m.sin(m.radians(90-epsilon)) * (part*HSL_size),    C1[1] - m.cos(m.radians(90-epsilon)) * (part*HSL_size)
    D2 = D1[0] - m.sin(m.radians(90-epsilon)) * (part*length),      D1[1] + m.cos(m.radians(90-epsilon)) * (part*length)
    ### vorher
    #D1 = C1[0] + m.cos(m.radians(epsilon)) * (part*HSL_size),    C1[1] - m.sin(m.radians(epsilon)) * (part*HSL_size)
    #D2 = D1[0] - m.sin(m.radians(epsilon)) * (part*length),      D1[1] + m.cos(m.radians(epsilon)) * (part*length)
    
    drawPolygon(D1, D2)
    return D1, D2 
    
    
    
    
    

def line_D_vonE_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    D2 = sPoint
    D1 = D2[0] - m.cos(m.radians(epsilon)) * (part*HSL_start),      D2[1] + m.sin(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(D1, D2)
    return D1, D2


def line_D_vonE_u_kl(E1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    D1 = E1[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_size),   E1[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    D2 = D1[0] + m.cos(m.radians(epsilon)) * (part*length),         D1[1] - m.sin(m.radians(epsilon)) * (part*length)
    
    drawPolygon(D1, D2)
    return D1, D2






def line_D_vonE_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    D2 = sPoint
    D1 = D2[0] - m.sin(m.radians(90-epsilon)) * (part*HSL_start),      D2[1] + m.cos(m.radians(90-epsilon)) * (part*HSL_start)
    
    drawPolygon_s(D1, D2)
    return D1, D2


def line_D_vonE_u_gr(E1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    D1 = E1[0] - m.sin(m.radians(90-epsilon)) * (part*HSL_size),      E1[1] + m.cos(m.radians(90-epsilon)) * (part*HSL_size)
    D2 = D1[0] + m.sin(m.radians(90-epsilon)) * (part*length),        D1[1] - m.cos(m.radians(90-epsilon)) * (part*length)
    
    drawPolygon(D1, D2)
    return D1, D2



    

    
def line_D_vonE_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    D2 = sPoint
    D1 = D2[0] + m.cos(m.radians(epsilon)) * (part*HSL_start),      D2[1] - m.sin(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(D1, D2)
    return D1, D2 
    

def line_D_vonE_o_kl(E1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    D1 = E1[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_size),    E1[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    D2 = D1[0] - m.cos(m.radians(epsilon)) * (part*length),          D1[1] + m.sin(m.radians(epsilon)) * (part*length)
    
    drawPolygon(D1, D2)
    return D1, D2 





    
    
def line_D_vonE_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    D2 = sPoint
    D1 = D2[0] + m.cos(m.radians(epsilon)) * (part*HSL_start),      D2[1] - m.sin(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(D1, D2)
    return D1, D2 
    
        
def line_D_vonE_o_gr(E1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    D1 = E1[0] + m.cos(m.radians(epsilon)) * (part*HSL_size),       E1[1] - m.sin(m.radians(epsilon)) * (part*HSL_size)
    D2 = D1[0] - m.cos(m.radians(epsilon)) * (part*length),         D1[1] + m.sin(m.radians(epsilon)) * (part*length)
    
    drawPolygon(D1, D2)
    return D1, D2 
    
    
    
    
    
  











# ___________________________ Linie E __________________________________________


def line_E_vonD_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    E2 = sPoint
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),   E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(E1, E2)
    return E1, E2

def line_E_vonD_u_kl(D1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    E1 = D1[0] + m.cos(m.radians(epsilon)) * (part*HSL_size),         D1[1] - m.sin(m.radians(epsilon)) * (part*HSL_size)
    E2 = E1[0] + m.sin(m.radians(angle_seg_1)) * (part*length),       E1[1] - m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(E1, E2)
    return E1, E2





def line_E_vonD_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    E2 = sPoint
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),     E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(E1, E2)
    return E1, E2


def line_E_vonD_u_gr(D1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    E1 = D1[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_size),       D1[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    E2 = E1[0] + m.sin(m.radians(angle_seg_1)) * (part*length),         E1[1] - m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(E1, E2)
    return E1, E2
    




def line_E_vonD_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    E2 = sPoint
    E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),   E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(E1, E2)
    return E1, E2

    
def line_E_vonD_o_kl(D1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    E1 = D1[0] - m.sin(m.radians(epsilon)) * (part*HSL_size),           D1[1] + m.cos(m.radians(epsilon)) * (part*HSL_size)
    E2 = E1[0] - m.sin(m.radians(angle_seg_1)) * (part*length),         E1[1] + m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(E1, E2)
    return E1, E2





def line_E_vonD_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    E2 = sPoint
    E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),      E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(E1, E2)
    return E1, E2
    
    
def line_E_vonD_o_gr(D1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    E1 = D1[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_size),       D1[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    E2 = E1[0] - m.sin(m.radians(angle_seg_1)) * (part*length),         E1[1] + m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(E1, E2)
    return E1, E2
    
    



       
def line_E_vonF_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    E2 = sPoint
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),      E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(E1, E2)
    return E1, E2
       

def line_E_vonF_u_kl(F1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    E1 = F1[0],            F1[1] - (part*HSL_size)
    E2 = E1[0] + m.sin(m.radians(angle_seg_1)) * (part*length),       E1[1] - m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(E1, E2)
    return E1, E2 






def line_E_vonF_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    E2 = sPoint
    E1 = E2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),     E2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(E1, E2)
    return E1, E2    


def line_E_vonF_u_gr(F1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):

    E1 = F1[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_size),       F1[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    E2 = E1[0] + m.sin(m.radians(angle_seg_1)) * (part*length),         E1[1] - m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(E1, E2)
    return E1, E2 





def line_E_vonF_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    E2 = sPoint
    E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),       E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(E1, E2)
    return E1, E2

     
def line_E_vonF_o_kl(F1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    E1 = F1[0],                                                         F1[1] + (part*HSL_size)
    E2 = E1[0] - m.sin(m.radians(angle_seg_1)) * (part*length),         E1[1] + m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(E1, E2)
    return E1, E2






def line_E_vonF_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    E2 = sPoint
    E1 = E2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),      E2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(E1, E2)
    return E1, E2

    
def line_E_vonF_o_gr(F1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    E1 = F1[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_size),        F1[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    E2 = E1[0] - m.sin(m.radians(angle_seg_1)) * (part*length),          E1[1] + m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(E1, E2)
    return E1, E2


    
    







  
  

# ___________________________ Linie F __________________________________________



def line_F_vonE_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start): 
    
    F2 = sPoint
    F1 = F2[0],             F2[1] + (part*HSL_start)
    
    drawPolygon_s(F1, F2)
    return F1, F2  

    
def line_F_vonE_u_kl(E1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length): 
    
    F1 = E1[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_size),     E1[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    F2 = F1[0],                                                       F1[1] - (part*length)
    
    drawPolygon(F1, F2)
    return F1, F2 




def line_F_vonE_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start): 
    
    F2 = sPoint
    F1 = F2[0],             F2[1] + (part*HSL_start)
    
    drawPolygon_s(F1, F2)
    return F1, F2


def line_F_vonE_u_gr(E1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length): 
    
    F1 = E1[0],             E1[1] + (part*HSL_size)
    F2 = F1[0],             F1[1] - (part*length)
    
    drawPolygon(F1, F2)
    return F1, F2




def line_F_vonE_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    F2 = sPoint
    F1 = F2[0],             F2[1] - (part*HSL_start)
    
    drawPolygon_s(F1, F2)
    return F1, F2 


def line_F_vonE_o_kl(E1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    F1 = E1[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_size),   E1[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    F2 = F1[0],                                                     F1[1] + part*length
    
    drawPolygon(F1, F2)
    return F1, F2 




    
def line_F_vonE_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    F2 = sPoint
    F1 = F2[0],         F2[1] - (part*HSL_start)
    
    drawPolygon_s(F1, F2)
    return F1, F2 


def line_F_vonE_o_gr(E1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
        
    F1 = E1[0],         E1[1] - (part*HSL_size)
    F2 = F1[0],         F1[1] + (part*length)
    
    drawPolygon(F1, F2)
    return F1, F2 




    
def line_F_vonG_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    F2 = sPoint
    F1 = F2[0],        F2[1] + (part*HSL_start)
    
    drawPolygon_s(F1, F2)
    return F1, F2  


def line_F_vonG_u_kl(G1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    F1 = G1[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_size),    G1[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    F2 = F1[0],                                                      F1[1] - (part*length)
    
    drawPolygon(F1, F2)
    return F1, F2  





def line_F_vonG_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    F2 = sPoint
    F1 = F2[0],        F2[1] + (part*HSL_start)
    
    drawPolygon_s(F1, F2)
    return F1, F2  


def line_F_vonG_u_gr(G1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    F1 = G1[0],        G1[1] + (part*HSL_size)
    F2 = F1[0],        F1[1] - (part*length)
    
    drawPolygon(F1, F2)
    return F1, F2  
    
    



def line_F_vonG_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    F2 = sPoint
    F1 = F2[0],         F2[1] - (part*HSL_start)
    
    drawPolygon_s(F1, F2)
    return F1, F2


def line_F_vonG_o_kl(G1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    F1 = G1[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_size),       G1[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    F2 = F1[0],                                                         F1[1] + (part*length)
    
    drawPolygon(F1, F2)
    return F1, F2
    


    
    
    
def line_F_vonG_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    F2 = sPoint
    F1 = F2[0],        F2[1] - (part*HSL_start)
    
    drawPolygon_s(F1, F2)
    return F1, F2


def line_F_vonG_o_gr(G1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    F1 = G1[0],        G1[1] - (part*HSL_size)
    F2 = F1[0],        F1[1] + (part*length)
    
    drawPolygon(F1, F2)
    return F1, F2





  
  
  
  
  
# ___________________________ Linie G __________________________________________
  

def line_G_vonF_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):  
    
    G2 = sPoint
    G1 = G2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start), G2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(G1, G2)
    return G1, G2


def line_G_vonF_u_kl(F1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):  
    
    G1 = F1[0],                                                    F1[1] - (part*HSL_size)
    G2 = G1[0] - m.sin(m.radians(angle_seg_1)) * (part*length),    G1[1] - m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(G1, G2)
    return G1, G2

       



def line_G_vonF_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):  
    
    G2 = sPoint
    G1 = G2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),        G2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(G1, G2)
    return G1, G2


def line_G_vonF_u_gr(F1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):  
    
    G1 = F1[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_size),      F1[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    G2 = G1[0] - m.sin(m.radians(angle_seg_1)) * (part*length),        G1[1] - m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(G1, G2)
    return G1, G2





def line_G_vonF_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):  
    
    G2 = sPoint
    G1 = G2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),    G2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(G1, G2)
    return G1, G2

    
def line_G_vonF_o_kl(F1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):  
    
    G1 = F1[0],                                                    F1[1] + (part*HSL_size)
    G2 = G1[0] + m.sin(m.radians(angle_seg_1)) * (part*length),    G1[1] + m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(G1, G2)
    return G1, G2
    

    


def line_G_vonF_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    G2 = sPoint
    G1 = G2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),    G2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(G1, G2)
    return G1, G2
  

def line_G_vonF_o_gr(F1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    G1 = F1[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_size),    F1[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    G2 = G1[0] + m.sin(m.radians(angle_seg_1)) * (part*length),      G1[1] + m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(G1, G2)
    return G1, G2
    
    



    

def line_G_vonH_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    G2 = sPoint
    G1 = G2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),    G2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(G1, G2)
    return G1, G2

    
def line_G_vonH_u_kl(H1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    G1 = H1[0] - m.sin(m.radians(epsilon)) * (part*HSL_size),           H1[1] - m.cos(m.radians(epsilon)) * (part*HSL_size)
    G2 = G1[0] - m.sin(m.radians(angle_seg_1)) * (part*length),         G1[1] - m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(G1, G2)
    return G1, G2



  



def line_G_vonH_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    G2 = sPoint
    G1 = G2[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_start),    G2[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(G1, G2)
    return G1, G2

    
def line_G_vonH_u_gr(H1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    # so war es vorher, aber für Schriftzug 6 abgeändert: angle_seg_1 wird zu epsilon
    #G1 = H1[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_size),              H1[1] + m.cos(m.radians(angle_seg_1)) * HSL

    G1 = H1[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_size),    H1[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    G2 = G1[0] - m.sin(m.radians(angle_seg_1)) * (part*length),      G1[1] - m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(G1, G2)
    return G1, G2




    
def line_G_vonH_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    G2 = sPoint
    G1 = G2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),    G2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(G1, G2)
    return G1, G2 


def line_G_vonH_o_kl(H1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    G1 = H1[0] + m.sin(m.radians(90-epsilon)) * (part*HSL_size),     H1[1] + m.cos(m.radians(90-epsilon)) * (part*HSL_size)
    G2 = G1[0] + m.sin(m.radians(angle_seg_1)) * (part*length),      G1[1] + m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(G1, G2)
    return G1, G2 





def line_G_vonH_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    G2 = sPoint
    G1 = G2[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_start),    G2[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_start)
    
    drawPolygon_s(G1, G2)
    return G1, G2 
 
    
def line_G_vonH_o_gr(H1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    G1 = H1[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_size),     H1[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    G2 = G1[0] + m.sin(m.radians(angle_seg_1)) * (part*length),       G1[1] + m.cos(m.radians(angle_seg_1)) * (part*length)
    
    drawPolygon(G1, G2)
    return G1, G2 
 













# ___________________________ Linie H __________________________________________
    
    
def line_H_vonA_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):

    H2 = sPoint
    H1 = H2[0] + m.sin(m.radians(epsilon)) * (part*HSL_start),     H2[1] + m.cos(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(H1, H2)
    return H1, H2


def line_H_vonA_u_kl(A1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):

    H1 = A1[0] - m.cos(m.radians(angle_seg_4)) * (part*HSL_size),   A1[1] - m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    H2 = H1[0] - m.sin(m.radians(epsilon)) * (part*length),         H1[1] - m.cos(m.radians(epsilon)) * (part*length)
    
    drawPolygon(H1, H2)
    return H1, H2





     
def line_H_vonA_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    H2 = sPoint
    H1 = H2[0] + m.sin(m.radians(90-epsilon)) * (part*HSL_start),    H2[1] + m.cos(m.radians(90-epsilon)) * (part*HSL_start)
    
    drawPolygon_s(H1, H2)
    return H1, H2

def line_H_vonA_u_gr(A1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    H1 = A1[0] + m.sin(m.radians(90-epsilon)) * (part*HSL_size),    A1[1] + m.cos(m.radians(90-epsilon)) * (part*HSL_size)
    H2 = H1[0] - m.sin(m.radians(90-epsilon)) * (part*length),      H1[1] - m.cos(m.radians(90-epsilon)) * (part*length)
    
    drawPolygon(H1, H2)
    return H1, H2



    

def line_H_vonA_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start): 
    
    H2 = sPoint
    H1 = H2[0] - m.sin(m.radians(90-epsilon)) * (part*HSL_start),    H2[1] - m.cos(m.radians(90-epsilon)) * (part*HSL_start)
    
    drawPolygon_s(H1, H2)
    return H1, H2


def line_H_vonA_o_kl(A1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length): 
    
    H1 = A1[0] + m.cos(m.radians(angle_seg_4)) * (part*HSL_size),    A1[1] + m.sin(m.radians(angle_seg_4)) * (part*HSL_size)
    H2 = H1[0] + m.sin(m.radians(90-epsilon)) * (part*length),       H1[1] + m.cos(m.radians(90-epsilon)) * (part*length)
    
    drawPolygon(H1, H2)
    return H1, H2



    
    
    
def line_H_vonA_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    H2 = sPoint
    H1 = H2[0] - m.sin(m.radians(90-epsilon)) * (part*HSL_start),    H2[1] - m.cos(m.radians(90-epsilon)) * (part*HSL_start)
    
    drawPolygon_s(H1, H2)
    return H1, H2

    
def line_H_vonA_o_gr(A1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    H1 = A1[0] - m.sin(m.radians(90-epsilon)) * (part*HSL_size),     A1[1] - m.cos(m.radians(90-epsilon)) * (part*HSL_size)
    H2 = H1[0] + m.sin(m.radians(90-epsilon)) * (part*length),       H1[1] + m.cos(m.radians(90-epsilon)) * (part*length)
    
    drawPolygon(H1, H2)
    return H1, H2





def line_H_vonG_u_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start): 
    
    H2 = sPoint
    H1 = H2[0] + m.cos(m.radians(epsilon)) * (part*HSL_start),      H2[1] + m.sin(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(H1, H2)
    return H1, H2


def line_H_vonG_u_kl(G1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length): 
    
    H1 = G1[0] - m.sin(m.radians(angle_seg_1)) * (part*HSL_size),    G1[1] - m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    H2 = H1[0] - m.cos(m.radians(epsilon)) * (part*length),          H1[1] - m.sin(m.radians(epsilon)) * (part*length)
    
    drawPolygon(H1, H2)
    return H1, H2




    

def line_H_vonG_u_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start): 
    
    H2 = sPoint
    H1 = H2[0] + m.cos(m.radians(epsilon)) * (part*HSL_start),    H2[1] + m.sin(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(H1, H2)
    return H1, H2
    

def line_H_vonG_u_gr(G1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length): 
    
    H1 = G1[0] + m.cos(m.radians(epsilon)) * (part*HSL_size),     G1[1] + m.sin(m.radians(epsilon)) * (part*HSL_size)
    H2 = H1[0] - m.cos(m.radians(epsilon)) * (part*length),       H1[1] - m.sin(m.radians(epsilon)) * (part*length)
    
    drawPolygon(H1, H2)
    return H1, H2



    
    
def line_H_vonG_o_kl_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    H2 = sPoint
    H1 = H2[0] - m.cos(m.radians(epsilon)) * (part*HSL_start),    H2[1] - m.sin(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(H1, H2)
    return H1, H2


def line_H_vonG_o_kl(G1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    H1 = G1[0] + m.sin(m.radians(angle_seg_1)) * (part*HSL_size),  G1[1] + m.cos(m.radians(angle_seg_1)) * (part*HSL_size)
    H2 = H1[0] + m.cos(m.radians(epsilon)) * (part*length),        H1[1] + m.sin(m.radians(epsilon)) * (part*length)
    
    drawPolygon(H1, H2)
    return H1, H2
    
    




def line_H_vonG_o_gr_s(sPoint, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, HSL_start):
    
    H2 = sPoint
    H1 = H2[0] - m.cos(m.radians(epsilon)) * (part*HSL_start),    H2[1] - m.sin(m.radians(epsilon)) * (part*HSL_start)
    
    drawPolygon_s(H1, H2)
    return H1, H2
    

def line_H_vonG_o_gr(G1, angle_seg_1, angle_seg_4, epsilon, part, HSL_size, length):
    
    H1 = G1[0] - m.cos(m.radians(epsilon)) * (part*HSL_size),      G1[1] - m.sin(m.radians(epsilon)) * (part*HSL_size)
    H2 = H1[0] + m.cos(m.radians(epsilon)) * (part*length),        H1[1] + m.sin(m.radians(epsilon)) * (part*length)
    
    drawPolygon(H1, H2)
    return H1, H2







#### –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

#### mit Marcus gemeinsam 11. September 2020
    
    
        


UPPER_A=0
UPPER_H=1
UPPER_G=2
UPPER_F=3
UPPER_E=4
UPPER_D=5
UPPER_C=6
UPPER_B=7
LOWER_A=8
LOWER_H=9
LOWER_G=10
LOWER_F=11
LOWER_E=12
LOWER_D=13
LOWER_C=14
LOWER_B=15

DIRECTION_NAME = [ 'A', 'H', 'G', 'F', 'E', 'D', 'C', 'B' ]



def findRadiusFunction(direction, clockwise, inward, suffix):
    direction_name = DIRECTION_NAME[direction%8]
    if clockwise:
        coming_from = DIRECTION_NAME[(direction+1)%8]
    else:
        coming_from = DIRECTION_NAME[(direction-1)%8]
    obenunten = 'o' if direction < 8 else 'u'
    groesserkleiner = 'kl' if inward else 'gr'
    name = f"line_{direction_name}_von{coming_from}_{obenunten}_{groesserkleiner}{suffix}"
    return globals()[name]



def drawSchneckenzug(x, y, direction_index, count, HSL_size, HSL_start, off_x=0, off_y=0, clockwise=True, inward=True, HSL_size_multipliers=None):
    #HSL_size_multipliers = []
    #for i in range(0,17):
    #    HSL_size_multipliers.append(1-.056*i)
    
    schneckenzug = BezierPath()
    
    if HSL_size_multipliers is None:
        #HSL_size_multipliers = [ 1 for i in range(0,count+1)]
        HSL_size_multipliers = (count+1) * [ 1 ]

    assert count+1==len(HSL_size_multipliers)

    #Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientKurTop(offset, offsetDir, x+off_x, y+off_y)
 
    suffix = "_s"
    HSL_offset = HSL_size if inward else -HSL_size
    #on first iteration the names are really wrong:
    inner = x+off_x, y+off_y
    outer = None
    
    
    if inward:
        multiplier = HSL_size_multipliers[0]
        HSL_size_multipliers = HSL_size_multipliers[1:]
        
        HSL_offset -= HSL_size*multiplier
        #print(HSL_offset)
        
        radiusFunction = findRadiusFunction(direction_index, clockwise, inward, suffix)
        inner, outer = radiusFunction(inner, *angles, part, HSL_size*multiplier, HSL_start + HSL_offset)  
        suffix = ""
        if clockwise:
            direction_index = (direction_index - 1) %16
        else:
            direction_index = (direction_index + 1) %16
               
    for multiplier in (HSL_size_multipliers if inward else HSL_size_multipliers[:-1]):
        if inward:
            HSL_offset -= HSL_size*multiplier
        else:
            HSL_offset += HSL_size*multiplier
        #print(f"multiplier={multiplier}, HSL_offset={HSL_offset}, HSL_size={HSL_size}, eff_HSL_start={HSL_start + HSL_offset}, eff_HSL_size={HSL_size*multiplier}")
        
        angle_index1 = direction_index
        angle_index2 = direction_index
        if clockwise:
            if inward:
                angle_index1 += 2
                angle_index2 += 1
            else:
                angle_index1 += 1
        else:
            if inward:
                angle_index1 += 0
                angle_index2 += 1
            else:
                angle_index1 += 1
                angle_index2 += 2
        angle1 = globals()['angle_'+str(angle_index1%16)]
        angle2 = globals()['angle_'+str(angle_index2%16)]

        radiusFunction = findRadiusFunction(direction_index, clockwise, inward, suffix)
        inner, outer = radiusFunction(inner, *angles, part, HSL_size*multiplier, HSL_start + HSL_offset)
        schneckenzug.arc(*drawKreisSeg(inner, HSL_start + HSL_offset, angle1, angle2, clockwise))

        suffix = ""
        if clockwise:
            direction_index = (direction_index - 1) %16
        else:
            direction_index = (direction_index + 1) %16

    if not inward:
        HSL_offset += HSL_size*multiplier
        radiusFunction = findRadiusFunction(direction_index, clockwise, inward, suffix)
        inner, outer = radiusFunction(inner, *angles, part, HSL_size*HSL_size_multipliers[-1], HSL_start + HSL_offset)
        
    return schneckenzug


        
        

    

