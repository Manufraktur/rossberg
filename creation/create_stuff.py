import math as m
from drawBot import *
from version_3.creation.special_drawbot import BezierPath, drawPath
import math





# HV 6:6
modul_width = 6
modul_height = 6

# _____________ Pen for drawing shape in glyph window _______________

penWidth = 10
penThickness = 4

#    +--------- width --------+
#    |                        |  thickness
#    +------------------------+








def pageSetup(x, y):
    page_width_cal = x * modul_width
    page_height_cal = y * modul_height
    return page_width_cal, page_height_cal
    
    
    
def modulStart(x, y):
    modul_start_x = x * modul_width - modul_width
    modul_start_y = modul_height * y
    return modul_start_x, modul_start_y




def backgroundGrid(page_width_cal, page_height_cal, x_height=6):
    
    save()
    stroke(0.8)
    strokeWidth(.001)
    
    # Horizontale Linien
    
    if x_height == 6:   ### Fraktur, Kanzlei = 6

        baseline = modul_height * 4
        
        line_pos_hor = 0
        while line_pos_hor < page_height_cal:
            line((0, line_pos_hor), (page_width_cal, line_pos_hor))
            line_pos_hor += modul_height
    
        whitespace = modul_height
        valueToMoveGlyph = 3.75


    elif x_height == 0:   ### Fraktur, Kanzlei = 6

        baseline = modul_height * 5
        
        line_pos_hor = 0
        while line_pos_hor < page_height_cal:
            line((0, line_pos_hor), (page_width_cal, line_pos_hor))
            line_pos_hor += modul_height
    
        whitespace = modul_height
        valueToMoveGlyph = 3.75
    
    
    elif x_height == 5:  ### Kurrentkanzlei = 5
        
        baseline = modul_height * 4.5

        line_pos_hor = [0.5, 1, 5, 5.5, 6, 8, 8.5, 9, 13, 13.5]
        for lph in line_pos_hor:
            line((0, modul_height*lph), (page_width_cal, modul_height*lph))
        
        whitespace = modul_height/2
        valueToMoveGlyph = 4.25

        
    
    elif x_height == 1.5:  ### Kurrent = 1.5
        
        baseline = modul_height * 6
        
        line_pos_hor = [0.5, 6.5, 7, 7.5, 8, 13.5]
        for lph in line_pos_hor:
            line((0, modul_height*lph), (page_width_cal, modul_height*lph))
        
        whitespace = 0
        valueToMoveGlyph = 5.75


    else:
        print("Please enter correct x-height: \nFraktur, Kanzlei = 6\nKurrentkanzlei = 5\nKurrent = 2")
        valueToMoveGlyph = 0
    
    # Linien dicker -- hervorgehoben zur Orientierung
    stroke(0.5)
    strokeWidth(.075)
    
    # Grundline
    line((0, baseline), (page_width_cal, baseline))

    # x-Höhe
    line((0, baseline+x_height*modul_height), (page_width_cal, baseline+x_height*modul_height))
    
    
    # Vertikale Linien
    stroke(0.8)
    strokeWidth(.001)
    
    line_pos_ver = 0
    while line_pos_ver < page_width_cal:
        line((line_pos_ver, whitespace), (line_pos_ver, page_height_cal-whitespace))
        line_pos_ver += modul_width
        
    restore()

    baseline = baseline/modul_height 
    return baseline, valueToMoveGlyph
    


def backgroundGridContem(page_width_cal, page_height_cal, x_height=5):
    
    save()
    stroke(0.8)
    strokeWidth(.001)
    
    # Horizontale Linien
    
    
    if x_height == 5:  ### Kurrentkanzlei = 5
        
        baseline = modul_height * 4.5

        line_pos_hor = [0.5, 1, 4.25, 5, 5.5, 6, 8, 8.5, 9, 9.25, 13, 13.5]
        for lph in line_pos_hor:
            line((0, modul_height*lph), (page_width_cal, modul_height*lph))
        
        whitespace = modul_height/2
        valueToMoveGlyph = 4.25

    if x_height == 6:  ### Kanzlei = 6
        

        baseline = modul_height * 4
        
        line_pos_hor = 0
        while line_pos_hor < page_height_cal:
            line((0, line_pos_hor), (page_width_cal, line_pos_hor))
            line_pos_hor += modul_height
    
        whitespace = modul_height
        valueToMoveGlyph = 3.75
        

    else:
        print("Please enter correct x-height: \nFraktur, Kanzlei = 6\nKurrentkanzlei = 5\nKurrent = 2")
        valueToMoveGlyph = 0
    
    # Linien dicker -- hervorgehoben zur Orientierung
    stroke(0.5)
    strokeWidth(.075)
    
    # Grundline
    line((0, baseline), (page_width_cal, baseline))

    # x-Höhe
    line((0, baseline+x_height*modul_height), (page_width_cal, baseline+x_height*modul_height))
    
    
    # Vertikale Linien
    stroke(0.8)
    strokeWidth(.001)
    
    line_pos_ver = 0
    while line_pos_ver < page_width_cal:
        line((line_pos_ver, whitespace), (line_pos_ver, page_height_cal-whitespace))
        line_pos_ver += modul_width
        
    restore()

    baseline = baseline/modul_height 
    return baseline, valueToMoveGlyph











### siehe Hilfszeichnung calcModul
def calcModul():
    
    A1 = 0,                         0 + modul_height/4
    A2 = 0 + modul_width,           0 + modul_height/4*3
    B1 = 0,                         0 + modul_height/4*2
    B2 = 0 + modul_width,           0 + modul_height/2
    C1 = 0,                         0 + modul_height/4*3
    C2 = 0 + modul_width,           0 + modul_height/4
    D1 = 0,                         0 + modul_height
    D2 = 0 + modul_width,           0
    E1 = 0 + modul_width/4,         0 + modul_height
    E2 = 0 + modul_width/4*3,       0
    F1 = 0 + modul_width/2,         0 + modul_height
    F2 = 0 + modul_width/2,         0
    G1 = 0 + modul_width/4*3,       0 + modul_height
    G2 = 0 + modul_width/4,         0
    H1 = 0,                         0
    H2 = 0 + modul_width,           0 + modul_height
    
    return A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2  







### siehe Hilfszeichnung calcOffsetStroke

def calcOffsetStroke(A1, A2):
    offset = A2[0] - A1[0],     A2[1] - A1[1]
    return offset

def calcOffsetDirection(Raute_b_ini, Raute_c_ini):
    offsetDir = (Raute_c_ini[0] - Raute_b_ini[0]), (Raute_b_ini[1] - Raute_c_ini[1])
    return offsetDir


    



### Raute wird erstmal nur berechnet – wo die Punkte liegen, ohne etwas zu zeichnen.
### siehe Hilfszeichnung calcRauteIni

def calcRauteIni(modul_width, modul_height, Gattung="C"):
    
    Pos1 = 0,                         0 + modul_height/4
    Pos2 = 0 + modul_width,           0 + modul_height/4*3
    
    # Aufbau Raute
    a_x = Pos1[0] - modul_width/2
    a_y = Pos1[1] + modul_height/4
    b_x = Pos1[0] + modul_width/2
    b_y = Pos1[1] + modul_height/4*3
    
    angle_prelim = calcAngle(Gattung)
    
    def Hilfspunkt(Pos1, angle_prelim, modul_height):
        # Hilfslinie startet aus A1 im eingegebenen Winkel bis HP=Hilfspunkt
        HP_x = Pos1[0] - m.sin(m.radians(angle_prelim)) * modul_height
        HP_y = Pos1[1] + m.cos(m.radians(angle_prelim)) * modul_height
        # line((A1_x, A1_y), (HP_x, HP_y))
        # text("Hilfspunkt", (HP))
        return HP_x, HP_y
    
    # Hilfspunkt für Berechnung
    HP_x, HP_y = Hilfspunkt(Pos1, angle_prelim, modul_height)
    
    
    # berechnet Schnittpunkt mittels Hilfslinie

    def line(p1, p2): 
        A = (p1[1] - p2[1]) 
        B = (p2[0] - p1[0]) 
        C = (p1[0]*p2[1] - p2[0]*p1[1]) 
        return A, B, -C 

    def intersection(Inter1, Inter2): 
        D  = Inter1[0] * Inter2[1] - Inter1[1] * Inter2[0] 
        Dx = Inter1[2] * Inter2[1] - Inter1[1] * Inter2[2] 
        Dy = Inter1[0] * Inter2[2] - Inter1[2] * Inter2[0] 
        if D != 0: 
            x = Dx/D 
            y = Dy/D 
            return x,y 
        else: 
            return False 
        
    
    Inter1 = line([a_x, a_y], [b_x, b_y]) 
    Inter2 = line(Pos1, [HP_x, HP_y]) 
   
   
    # vier Eckpunkte der Raute
    Raute_a_ini = intersection(Inter1, Inter2)
    Raute_b_ini = Pos2[0] - (Pos1[0] - Raute_a_ini[0]), Pos2[1] + (Raute_a_ini[1] - Pos1[1])
    Raute_c_ini = Pos2[0] + (Pos1[0] - Raute_a_ini[0]), Pos2[1] - (Raute_a_ini[1] - Pos1[1])
    Raute_d_ini = Pos1[0] + (Pos1[0] - Raute_a_ini[0]), Pos1[1] - (Raute_a_ini[1] - Pos1[1])
    
    return Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini





### Raute wird gezeichnet

def drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y):
    
    start_pos = (x-1) * modul_width,     y * modul_height
    
    Raute_a = Raute_a_ini[0] + start_pos[0],     Raute_a_ini[1] + start_pos[1]
    Raute_b = Raute_b_ini[0] + start_pos[0],     Raute_b_ini[1] + start_pos[1]
    Raute_c = Raute_c_ini[0] + start_pos[0],     Raute_c_ini[1] + start_pos[1]
    Raute_d = Raute_d_ini[0] + start_pos[0],     Raute_d_ini[1] + start_pos[1]
    
    
    # zeichnet Raute als Linie
    save()
    stroke(0, 1, 0)      # grün
    strokeWidth(.001)      

    rect(start_pos[0], start_pos[1], modul_width, modul_height)

    path = BezierPath()
    path.moveTo(Raute_a)
    path.lineTo(Raute_b)
    path.lineTo(Raute_c)
    path.lineTo(Raute_d)
    path.closePath()
    drawPath(path)
    restore()
    
    return Raute_a, Raute_b, Raute_c, Raute_d
    
    




# ____________ Kanzlei _____________

def drawRauteOrientKanzTop(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y):
    
    start_pos = (x-1) * modul_width,     y * modul_height
    
    Raute_a = Raute_a_ini[0] + start_pos[0] + offset[0]/5,     Raute_a_ini[1] + start_pos[1] + offset[1]/5
    Raute_b = Raute_b_ini[0] + start_pos[0] + offset[0]/5,     Raute_b_ini[1] + start_pos[1] + offset[1]/5
    Raute_c = Raute_c_ini[0] + start_pos[0] + offset[0]/5,     Raute_c_ini[1] + start_pos[1] + offset[1]/5
    Raute_d = Raute_d_ini[0] + start_pos[0] + offset[0]/5,     Raute_d_ini[1] + start_pos[1] + offset[1]/5
    
    
    # zeichnet Raute als Linie
    save()
    stroke(0, 1, 0)      # grün
    strokeWidth(.001)      

    rect(start_pos[0], start_pos[1], modul_width, modul_height)
        
    stroke(3, 0, 5)      # rot

    path = BezierPath()
    path.moveTo(Raute_a)
    path.lineTo(Raute_b)
    path.lineTo(Raute_c)
    path.lineTo(Raute_d)
    path.closePath()
    drawPath(path)
    restore()
    
    return Raute_a, Raute_b, Raute_c, Raute_d

    
    
def drawRauteOrientKanzBtm(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, offset, x, y):
    
    start_pos = (x-1) * modul_width,     y * modul_height
    
    Raute_a = Raute_a_ini[0] + start_pos[0] - offset[0]/5,     Raute_a_ini[1] + start_pos[1] - offset[1]/5
    Raute_b = Raute_b_ini[0] + start_pos[0] - offset[0]/5,     Raute_b_ini[1] + start_pos[1] - offset[1]/5
    Raute_c = Raute_c_ini[0] + start_pos[0] - offset[0]/5,     Raute_c_ini[1] + start_pos[1] - offset[1]/5
    Raute_d = Raute_d_ini[0] + start_pos[0] - offset[0]/5,     Raute_d_ini[1] + start_pos[1] - offset[1]/5
    
    
    # zeichnet Raute als Linie
    save()
    stroke(0, 1, 0)      # grün
    rect(start_pos[0], start_pos[1], modul_width, modul_height)

    stroke(0, 4, 8)      # blau
    strokeWidth(.001)      

    path = BezierPath()
    path.moveTo(Raute_a)
    path.lineTo(Raute_b)
    path.lineTo(Raute_c)
    path.lineTo(Raute_d)
    path.closePath()
    drawPath(path)
    restore()
    
    return Raute_a, Raute_b, Raute_c, Raute_d
        





# ____________ Kurrent _____________

def drawRauteOrientKurBtm(offset, offsetDir, x, y):
    
    RauteSingle = BezierPath()
    
    start_pos = (x-1) * modul_width,     (y-0.5) * modul_height

    x = (x-1) *modul_width
    y = (y+0.5) *modul_width
    
    # berechnet Grundelement F
    Grund_a = x,                   y
    Grund_b = x + modul_width,     y + modul_height*0.5
    Grund_c = x + modul_width,     y - modul_height*0.5
    Grund_d = x,                   y - modul_height

    # zeichnet Modul (grünes Rechteck) als Linie
    save()
    stroke(0, 1, 0)      # grün
    strokeWidth(.001)      

    rect(start_pos[0], start_pos[1], modul_width, modul_height)
    
    # zeichnet Grundelement F als Linie
    stroke(0.74, 0.51, 1.04, 1)      # lila von https://www.drawbot.com/content/color.html
    strokeWidth(.001)      

    path = BezierPath()
    path.moveTo(Grund_a)
    path.lineTo(Grund_b)
    path.lineTo(Grund_c)
    path.lineTo(Grund_d)
    path.closePath()
    drawPath(path)
    restore()
    
    # berechnet Grundelement E in Ecke 
    Raute_d = Grund_d
    Raute_a = Raute_d[0]- offsetDir[0], Raute_d[1]+offsetDir[1]
    Raute_b = Raute_a[0] + offset[0],  Raute_a[1] + offset[1]
    Raute_c = Raute_d[0] + offset[0],  Raute_d[1] + offset[1]

          
    # zeichnet Grundelement E
    save()
    stroke(3, 0, 5)      # rot
    strokeWidth(.001)      

    RauteSingle.moveTo(Raute_a)
    RauteSingle.lineTo(Raute_b)
    RauteSingle.lineTo(Raute_c)
    RauteSingle.lineTo(Raute_d)
    RauteSingle.closePath()
    drawPath(RauteSingle)
    restore()
    
    return Raute_a, Raute_b, Raute_c, Raute_d


    
    

def drawRauteOrientKurTop(offset, offsetDir, x, y):
    
    RauteSingle = BezierPath()
    
    start_pos = (x-1) * modul_width,     y * modul_height

    x = (x-1) *modul_width
    y = (y+0.5) *modul_width
    
    # berechnet Grundelement F
    Grund_a = x,                   y
    Grund_b = x + modul_width,     y + modul_height*0.5
    Grund_c = x + modul_width,     y - modul_height*0.5
    Grund_d = x,                   y - modul_height

    # zeichnet Modul (grünes Rechteck) als Linie
    save()
    stroke(0, 1, 0)      # grün
    strokeWidth(.001)      

    rect(start_pos[0], start_pos[1], modul_width, modul_height)
    
    # zeichnet Grundelement F als Linie
    stroke(0.74, 0.51, 1.04, 1)      # lila von https://www.drawbot.com/content/color.html
    strokeWidth(.001)      

    path = BezierPath()
    path.moveTo(Grund_a)
    path.lineTo(Grund_b)
    path.lineTo(Grund_c)
    path.lineTo(Grund_d)
    path.closePath()
    drawPath(path)
    restore()
    
    # berechnet Grundelement E in Ecke 
    Raute_a = x,                          y
    Raute_b = x + offset[0],              y + offset[1]
    Raute_d = x + offsetDir[0],           y - offsetDir[1]
    Raute_c = Raute_d[0] + offset[0],     Raute_d[1] + offset[1]
          
    # zeichnet Grundelement E
    save()
    stroke(3, 0, 5)      # rot
    strokeWidth(0.001)      

    RauteSingle.moveTo(Raute_a)
    RauteSingle.lineTo(Raute_b)
    RauteSingle.lineTo(Raute_c)
    RauteSingle.lineTo(Raute_d)
    RauteSingle.closePath()
    drawPath(RauteSingle)
    restore()
    
    return Raute_a, Raute_b, Raute_c, Raute_d
    





# ____________ Grundelement _____________
        
        
def drawGrundelOrient(A1, A2, offset, x, y):
    
    start_pos = (x-1) * modul_width,     y * modul_height
    #hola = modul_height/2
    
    Grund_a = A1[0] + start_pos[0],     A1[1] + start_pos[1] + modul_height*0.25
    Grund_b = A2[0] + start_pos[0],     A2[1] + start_pos[1] + modul_height*0.25
    Grund_c = A2[0] + start_pos[0],     A2[1] + start_pos[1] - modul_height*0.75
    Grund_d = A1[0] + start_pos[0],     A1[1] + start_pos[1] - modul_height*0.75

    # zeichnet Modul als Linie
    save()
    stroke(0, 1, 0)      # grün
    strokeWidth(.001)      

    rect(start_pos[0], start_pos[1], modul_width, modul_height)
    
    # zeichnet Grundelement F als Linie
    stroke(0.74, 0.51, 1.04, 1)      # lila von https://www.drawbot.com/content/color.html
    strokeWidth(.001)      

    path = BezierPath()
    path.moveTo(Grund_a)
    path.lineTo(Grund_b)
    path.lineTo(Grund_c)
    path.lineTo(Grund_d)
    path.closePath()
    drawPath(path)
    restore()
    
    return Grund_a, Grund_b, Grund_c, Grund_d   




def drawGrundelOrientMittig(A1, A2, offset, x, y):
    
    start_pos = (x-1) * modul_width,     y * modul_height
        
    Grund_a = A1[0] + start_pos[0],     A1[1] + start_pos[1] + modul_height*0.5
    Grund_b = A2[0] + start_pos[0],     A2[1] + start_pos[1] + modul_height*0.5
    Grund_c = A2[0] + start_pos[0],     A2[1] + start_pos[1] - modul_height*0.5 
    Grund_d = A1[0] + start_pos[0],     A1[1] + start_pos[1] - modul_height*0.5 

    # zeichnet Modul als Linie
    save()
    stroke(0, 1, 0)      # grün
    strokeWidth(.001)      

    rect(start_pos[0], start_pos[1], modul_width, modul_height)
    
    # zeichnet Grundelement F als Linie
    stroke(0.74, 0.51, 1.04, 1)      # lila von https://www.drawbot.com/content/color.html
    strokeWidth(.001)      

    path = BezierPath()
    path.moveTo(Grund_a)
    path.lineTo(Grund_b)
    path.lineTo(Grund_c)
    path.lineTo(Grund_d)
    path.closePath()
    drawPath(path)
    restore()
    
    return Grund_a, Grund_b, Grund_c, Grund_d   
    
    
    
    
    
    






# ___________ Winkel berechnen _______________

### siehe Hilfszeichnung Winkelberechnung



# tan(𝛼) = Geka/Anka
alpha = 90 - m.degrees(m.atan((modul_height/2) / modul_width))
gamma = 90 - m.degrees(m.atan((modul_width/2) / modul_height))
epsilon = 90 - m.degrees(m.atan((modul_width) / modul_height))    


# Abfolge path_backe (???) (CW, starting at 12):
angle_seg_1 =  90 - gamma
angle_seg_2 =  gamma - epsilon
angle_seg_3 =  alpha - (90-epsilon)
angle_seg_4 =  90 - alpha

# Zusammenfassen, damit einfacher in Formel anwenden
angles = angle_seg_1, angle_seg_4, epsilon


angle_1 = angle_seg_4 
angle_2 = angle_seg_4 + angle_seg_3
angle_3 = angle_seg_4 + angle_seg_3 + angle_seg_2
angle_4 = angle_seg_4 + angle_seg_3 + angle_seg_2 + angle_seg_1
angle_5 = angle_seg_4 + angle_seg_3 + angle_seg_2 + angle_seg_1*2
angle_6 = angle_seg_4 + angle_seg_3 + angle_seg_2*2 + angle_seg_1*2
angle_7 = angle_seg_4 + angle_seg_3*2 + angle_seg_2*2 + angle_seg_1*2
angle_8 = angle_seg_4*2 + angle_seg_3*2 + angle_seg_2*2 + angle_seg_1*2
angle_9 = angle_seg_4*3 + angle_seg_3*2 + angle_seg_2*2 + angle_seg_1*2
angle_10 = angle_seg_4*3 + angle_seg_3*3 + angle_seg_2*2 + angle_seg_1*2
angle_11 = angle_seg_4*3 + angle_seg_3*3 + angle_seg_2*3 + angle_seg_1*2
angle_12 = angle_seg_4*3 + angle_seg_3*3 + angle_seg_2*3 + angle_seg_1*3
angle_13 = angle_seg_4*3 + angle_seg_3*3 + angle_seg_2*3 + angle_seg_1*4
angle_14 = angle_seg_4*3 + angle_seg_3*3 + angle_seg_2*4 + angle_seg_1*4
angle_15 = angle_seg_4*3 + angle_seg_3*4 + angle_seg_2*4 + angle_seg_1*4
angle_16 = angle_seg_4*4 + angle_seg_3*4 + angle_seg_2*4 + angle_seg_1*4
angle_0 = 0





### siehe Hilfszeichnung part
# Berechnung Länge Hypothenuse
A_A = sqrt((modul_height/2)*(modul_height/2) + modul_width*modul_width)
#print("\nLength of A_A: "+ str(A_A)  + " (auch modul genannt)")

# Berechnung Einheit "part" (Begriff von Roßberg)
part = A_A / 8
# part = 0.838525
# print("1 part = 1/8 modul (1/8 von A_A) = " + str(part))






# ___________ Fläche (Raute) _______________

# Winkel der Hilfslinie, min und max, startet bei 12 Uhr, gegen den Uhrzeigersinn


def calcAngle(Gat="C"):
    
    if Gat == "C":
        Gattung = 1
    
    if Gat == "D":
        Gattung = 50
    
    if Gat == "E":
        Gattung = 100
        
    range = (90-(90-alpha)) - (90 - gamma)

    angle_prelim = ((90 - gamma) + range) - (Gattung/100 * range) 
    return angle_prelim


#print(calcAngle(Gat="C"))



# ___________ Länge einer Linie mit zwei Punkten _______________

def calcDistance(x1,y1,x2,y2):
    dist = m.sqrt((x2 - x1)**2 + (y2 - y1)**2)
    return dist




    
# ___________ Funktion Kreissegment zeichnen _______________

# NEU: nimmt als Zentrum nicht zwei Zahlen sondern Koordinaten

# False=CCW, True=CW
def drawKreisSeg(arc_center, arc_radius, arc_start_angle, arc_end_angle, CCW=False):
    save()
    stroke(0)
    strokeWidth(.1)
    return  arc_center, part*arc_radius, arc_start_angle, arc_end_angle, CCW
    restore()     
      
      

         
     
      
          

# ___________ für RF _______________  
    
def writePathInGlyph(path, glyph):
    pen = glyph.getPen()
    path.drawToPen(pen)



        
        

# ___________ von Lisa _______________

# Calculate the eucledian distance of two points in two dimensions
def distance(point1, point2):
    return m.sqrt(m.pow(point1[0] - point2[0], 2) + m.pow(point1[1] - point2[1], 2))


# In a rectangular triangle in Rossbergs constructions, given a point at a corner of the cathetus, the angle at that point
# and the length of the cathetus, calcualte the position of the other corner depending on the orientation of the triangle.
def rightup(point, angle, distance):
    return point[0] + m.cos(m.radians(angle)) * distance, point[1] + m.sin(m.radians(angle)) * distance
def rightdown(point, angle, distance):
    return point[0] + m.cos(m.radians(angle)) * distance, point[1] - m.sin(m.radians(angle)) * distance
def leftup(point, angle, distance):
    return point[0] - m.cos(m.radians(angle)) * distance, point[1] + m.sin(m.radians(angle)) * distance
def leftdown(point, angle, distance):
    return point[0] - m.cos(m.radians(angle)) * distance, point[1] + m.sin(m.radians(angle)) * distance

    
#Calculate the radius of a circle given the arclength and the corresponding angle
def radiusFromArcLengthAndAngle(arcLength, angle):
    return arcLength/(2*m.sin(m.radians(angle/2)))

        



# ___________ to scale and translate for Glyph window _______________

#### explanation for valueToMoveGlyph
#### the following values are defined in the very beginning, 
#### in the function of the background grid: def backgroundGrid()

#### Wert Fraktur, Kanzlei = 3.75
#### Wert KurrentKanzlei = 4.25
#### Wert Kurrent = 5.75

valueToMoveGlyph = 0 ### this is just a backup

scaleValueToGlyphWindow = 10

def trans_scale(x, valueToMoveGlyph):
    x.translate(0,-(modul_height*valueToMoveGlyph))
    x.scale(scaleValueToGlyphWindow) 
    return x

def trans_scale_invert(x, valueToMoveGlyph):
    x.scale(1/scaleValueToGlyphWindow) 
    x.translate(0,+(modul_height*valueToMoveGlyph))
    return x
    
def trans_thinStroke_down_left(x): 
    x.translate(-(modul_width*0.5)+0.18, -(modul_height*0.25-0.1))
    return x
    
def trans_thinStroke_up_right(x): 
    x.translate((modul_width*0.5)+0.17, (modul_height*0.25+0.085))
    return x
  




# ___________ these formulas are from robstevenson _______________
# ___________ to remove extra points from curved outlines ________
###  added in January 2021


def calc_vector(point1, point2):
    x1, y1 = point1
    x2, y2 = point2
    dx = x2 - x1
    dy = y2 - y1
    return dx, dy

def calc_angle(point1, point2):
    dx, dy = calc_vector(point1, point2)
    return math.degrees(math.atan2(dy, dx))

def bp_angles(bp):
    ax, ay = bp.anchor
    ix, iy = bp.bcpIn
    ox, oy = bp.bcpOut
    in_angle = calc_angle((ix+ax, iy+ay), bp.anchor)
    out_angle = calc_angle(bp.anchor, (ox+ax, oy+ay))
    diff = abs(in_angle - out_angle)
    return in_angle, out_angle, diff

def select_extra_points(glyph, remove=False):
    if False: # flip to True if you want to select a point and see it’s angles printed, useful for debugging
        for bp in glyph.selectedBPoints:
            in_angle, out_angle, diff = bp_angles(bp)
            print("----------", in_angle, out_angle, diff)
    else:
        glyph.deselect()
        for contour in glyph.contours:
            for bp in contour.bPoints:
                in_angle, out_angle, diff = bp_angles(bp)
                if diff < 10:
                    if abs(abs(in_angle / 90) - round(abs(in_angle / 90))) > 0.1:
                        bp.selected = True

        if remove:
            glyph.removeSelection(
                removePoints=True,
                removeComponents=False,
                removeAnchors=False,
                removeImages=False)
        
   # _____________________________________________________________________________















