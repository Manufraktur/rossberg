import importlib
import version_3.creation.create_stuff
import version_3.creation.create_spirals
import version_3.creation.Grundelemente

importlib.reload(version_3.creation.create_stuff)
importlib.reload(version_3.creation.create_spirals)
importlib.reload(version_3.creation.Grundelemente)

from version_3.creation.create_stuff import *
from version_3.creation.create_spirals import *
from version_3.creation.Grundelemente import *
from version_3.creation.special_drawbot import BezierPath, drawPath

import math as m




temp_x = modul_width * 3
temp_y = modul_height * 9



# _____________ Seite + Allgemeines _______________

# page setup (Einheit in Modulen)
page_width = 5
page_height = 14

page_width_cal, page_height_cal = pageSetup(page_width, page_height)
newPage(page_width_cal, page_height_cal)


# some general settings
stroke(.1)    # grau
strokeWidth(.01)
fill(None)

fontSize(2)

    

# ___________ x-Höhe, Hintergrund _______________

# x-Höhe bestimmen (wird in Modulen gerechnet):
# Fraktur, Kanzlei = 6
# Kurrentkanzlei = 5
# Kurrent = 1.5
x_height = 6

# Hintergrund
backgroundGrid(page_width_cal, page_height_cal, x_height)


drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-1)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-2)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-3)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-3.5)
drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, (temp_x/modul_width)+0.5, (temp_y/modul_height)-4)


# ______________ Modul, Raute, Offset __________________  


# initialize Modul + Raute
# zeichnet nichts, wird nur generiert für später
A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2, H1, H2 = calcModul()
Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini = calcRauteIni(modul_width, modul_height)

offset = calcOffsetStroke(A1, A2)
offsetDir = calcOffsetDirection(Raute_b_ini, Raute_c_ini)
    
strokeWidth(.1)







    
def drawHalbbogen4(x, y, instrokeLen=0.5, outstrokeLen=0):
    
    Halbbogen4 = BezierPath()

    
    instroke = drawInstroke(x, y, instrokeLen)
            
    # Fig. 4.
    # ist ein Halbbogen aus 2 1/2 Bestandtheil, nach einer mit 1 1/4 Part Vieleck-Maases construierten Schneckenlinie, 
    # von 8. bis mit 14 1/2 Part construiret.
    # >>> ich komme hier nur auf 14.25!
    # 1.25 HSL; E>G; 8>14.25

    Raute_a, Raute_b, Raute_c, Raute_d = drawRauteOrientStandard(Raute_a_ini, Raute_b_ini, Raute_c_ini, Raute_d_ini, x, y)
    
    ### Rossberg gibt 8 an, mit 7.55 würde es genau passen :/
    bogen = drawSchneckenzug(x, y , UPPER_G, 6, HSL_size=1.25, HSL_start=7.55, clockwise=True, inward=False)
    #bogen = drawSchneckenzug(x, y , UPPER_G, 6, HSL_size=1.25, HSL_start=8, clockwise=True, inward=False)
   
    outstroke = drawOutstroke(*bogen.points[-1], outstrokeLen, "down")

    Halbbogen4 += instroke + bogen + outstroke
    drawPath(Halbbogen4)
    return Halbbogen4


drawHalbbogen4(temp_x, temp_y)
    
    
              
    
def drawHalbbogen7(x, y, instrokeLen=0, outstrokeLen=0.5):
    
    Halbbogen7 = BezierPath()
        
    # siehe Fig. 4.

    HSL_size = 1.25 
    HSL_start = 8
        
    G1, G2 = line_G_vonF_u_gr_s((x, y), *angles, part, HSL_size, HSL_start)
    
    H1, H2 = line_H_vonG_u_gr(G1, *angles, part, HSL_size, HSL_start+HSL_size*1)
    Halbbogen7.arc(*drawKreisSeg(G1, HSL_start, angle_11, angle_10, True))
    
    A1, A2 = line_A_vonH_u_gr(H1, *angles, part, HSL_size, HSL_start+HSL_size*2)
    Halbbogen7.arc(*drawKreisSeg(H1, HSL_start+HSL_size, angle_10, angle_9, True))
    
    B1, B2 = line_B_vonA_o_gr(A1, *angles, part, HSL_size, HSL_start+HSL_size*3)
    Halbbogen7.arc(*drawKreisSeg(A1, HSL_start+HSL_size*2, angle_9, angle_8, True))
    
    C1, C2 = line_C_vonB_o_gr(B1, *angles, part, HSL_size, HSL_start+HSL_size*4)
    Halbbogen7.arc(*drawKreisSeg(B1, HSL_start+HSL_size*3, angle_8, angle_7, True))
    
    D1, D2 = line_D_vonC_o_gr(C1, *angles, part, HSL_size, HSL_start+HSL_size*5)
    Halbbogen7.arc(*drawKreisSeg(C1, HSL_start+HSL_size*4, angle_7, angle_6, True))
    
    E1, E2 = line_E_vonD_o_gr(D1, *angles, part, HSL_size, HSL_start+HSL_size*6)
    Halbbogen7.arc(*drawKreisSeg(D1, HSL_start+HSL_size*5, angle_6, angle_5, True))


    instroke = drawInstroke(*E2, instrokeLen, "down")
    outstroke = drawOutstroke(x, y, outstrokeLen)
    
    Halbbogen7 += instroke + outstroke
    drawPath(Halbbogen7)  
    return Halbbogen7
        
#drawHalbbogen7(temp_x, temp_y)  

